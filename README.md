# Cours de mathématiques approfondies

Ce dépôt contient le cours, avec les exercices, de mathématiques approfondies enseigné par Antoine Crouzet au lycée Carnot (Paris). 

* Répertoire scripts et Makefile : outils permettant de créer les fichiers avec mon fichier style
* Répertoire tex : les sources, par chapitre
* Répertoire inc : les includes (images, scripts)
* Répertoire pdf : les fichiers compilés (version elève, sans démonstration et correction, version prof et la feuille de TD)

# Remerciement

* Cédric Pierquet, dont je me suis fortement inspiré pour l'environnement/cadre Python ([Lien forge](https://forge.apps.education.fr/pierquetcedric/package-latex-proflycee))

# Remarque

Pour compiler ces fichiers, il est nécessaire d'utiliser une classe, qui se situe dans le répertoire classe et qu'il faudra installer d'abord.

ATTENTION : ces cours ne seront plus maintenus, n'étant plus en ECG. Si des coquilles me sont remontées, elles seront bien évidemment corrigées.

# Progression ECG - 2023-2024

## Premier semestre

### Phase 1

* Logique et raisonnements 
* Propriétés des réels
* Introduction à l'informatique avec Python 
* Sommes et produits
* Fonctions usuelles
* Suites
* Limites de suites

### Phase 2

* Ensembles et applications 
* Éléments de combinatoire
* Probabilités finies 
* Variables aléatoires finies  

### Phase 3

* Limites de fonctions 
* Continuité
* Dérivabilité
* Intégration sur un segment 

### Phase 4

* Polynômes  
* Systèmes linéaires 
* Matrices 
* Intro aux espaces vectoriels 

## Second semestre

### Phase 5

* Applications linéaires 

### Phase 6

* Analyse asymptotique
* Formule de Taylor 
* Développements limités 
* Séries

### Phase 7

* Sommes d'EV
* Dimension finie
* Codage matriciel

### Phase 8

* Probabilités sur un univers quelconque
* Variables aléatoires discrètes
* Couples de VA

### Phase 9

* Intégrales généralisées
* Convexité 
* Convergence et approximation
