%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{30}{2}[\'Etude d'une matrice à paramètre]

Pour tout $t\in \R$, on définit
\[ A(t)=\matrice{1-t&-t&0\\-t&1-t&0\\-t&t&1-2t}.\]
On note $\EE$ l'ensemble des matrices de cette forme, c'est-à-dire 
\[ \EE = \left \{ A(t),\quad t\in \R\right\}.\]
\begin{enumerate}
	\item Donner $A(1)$, et montrer que $Q=\matrice{\frac12&-\frac12&0\\-\frac12&\frac12&0\\-\frac12&\frac12&0}\in \EE$.
	\item Soient $s$ et $t$ deux réels. Déterminer le réel $u$ tel que $A(s)A(t) = A(u)$. En déduire que $A(s)A(t)\in \EE$ et que $A(s)$ et $A(t)$ commutent.
	\item \begin{enumerate}
	\item Montrer que $Q$ n'est pas inversible.
	\item Montrer que si $t\neq \frac12$, $A(t)\in \mathrm{GL}_3(\R)$.
	\end{enumerate}
	\item Déterminer l'ensemble des matrices $S$ de $\EE$ telles que $S^2=A\left(-\frac32\right)$.
	\item On pose $J=A(-1)$.
	\begin{enumerate}
		\item Montrer qu'il existe une suite $(t_n)$ telle que \[ \forall n\in \N,\quad J^n = A(t_n).\]
		\item Déterminer une relation de récurrence entre $t_{n+1}$ et $t_n$.
		\item En déduire l'expression de $J^n$.
	\end{enumerate}
\end{enumerate}

\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item Rapidement 
\[ A(1) = \matrice{0&-1&0\\-1&0&0\\-1&1&-1}\qeq Q = A\left(\frac12\right) \in \EE.\]
	\item On calcule tout simplement :
\begin{align*}
A(s)A(t) &= \matrice{1-s&-s&0\\-s&1-s&0\\-s&s&1-2s}\matrice{1-t&-t&0\\-t&1-t&0\\-t&t&1-2t}\\
&= \matrice{(1-s)(1-t)+st & (1-s)(-t)-s(1-t) & 0\\
-s(1-t)-t(1-s) & st + (1-s)(1-t)& 0\\
-s(1-t)-st-t(1-2s) & st+s(1-t)+t(1-2s) & (1-2s)(1-2t)}\\
&= \matrice{1-(s+t-2st) & -((s+t-2st) & 0 \\
-(s+t-2st) & 1-(s+t-2st) & 0 \\
-(s+t-2st) & s+t-2st & 1- 2(s+t-2st)} = A(s+t-2st).
\end{align*}
Ainsi, $A(s)A(t)\in \EE$. De plus, $A(t)A(s) = A(t+s-2ts) = A(s+t-2st) = A(s)A(t)$ : les matrices commutent.
\item \begin{enumerate}
\item $Q$ possède une colonne nulle. Ainsi $Q\matrice{0\\0\\1} = \matrice{0\\0\\0}$. Par le critère du noyau, $Q$ n'est pas inversible. \textit{Bien sûr, une méthode du pivot fonctionne.}
\item Deux méthodes.\\
\textbf{Méthode naïve} : on applique la méthode du pivot :
\begin{align*}
\matrice{1-t&-t&0 \\ -t & 1-t & 0\\-t & t & 1-2t} &\sim
\matrice{-t&t&1-2t\\-t&1-t&0\\1-t&-t&0}\\
&\sim \matrice{-t & t & 1-2t \\ 0 & 1-2t & 2t-1\\1 & -2t & 2t-1} \\
&\sim \matrice{1& -2t & 2t-1\\ 0 & 1-2t & 2t-1\\-t & t & 1-2t}\\
&\sim \matrice{1&-2t&2t-1\\0&1-2t&2t-1\\0&-2t^2+t&2t^2+1-3t}\\
&\sim \matrice{	1&-2t&2t-1\\0&1-2t&2t-1\\ 0 & 0 & 1-2t}
\end{align*}
Ainsi, la matrice est inversible si et seulement si $1-2t\neq 0$, c'est-à-dire si et seulement si $t\neq \frac12$.\\
\textbf{Méthode rapide} : remarquons que $A(0)=I_3$. Soit $t\neq \frac12$. On a alors 
	\[ s+t-2st = 0 \iff s(1-2t)=-t\iff s=\frac{-t}{1-2t}.\]
	Remarquons que si $t\neq \frac12$, $s$ existe. La question $2$ garantit alors que 
\[ A(t) A \left(-\frac{t}{1-2t}\right) = A(0)=I_3.\]
Par théorème, $A(t)$ est alors inversible. Le cas $t=\frac12$ a été traité à la question précédente.
\end{enumerate}
\item Soit $S\in \EE$ tel que $S^2=A\left(-\frac32\right)$. Il existe $t\in \R$ tel que $S=A(t)$. Alors, d'après la question $2$ 
\[ A(t)^2 = A\left(-\frac32\right) \iff A(2t-2t^2)=A\left(-\frac32\right).\]
Soit, par unicité des coefficients d'une matrice, $2t-2t^2=-\frac32$. Les solutions sont $\frac32$ et $-\frac12$.

Ainsi, il existe deux matrices de $\EE$ vérifiant $S^2=A\left(-\frac32\right)$ : $A\left(\frac32\right)$ et $A\left(-\frac12\right)$.
\item \begin{enumerate}
\item On utilise la question $2$. Montrons par récurrence sur $n$ la proposition $P_n$: il existe un réel $t_n$ vérifiant $J^n=A(t_n)$.
\begin{itemize}
	\item Pour $n=0$, $J^0=I_3 = A(0)$. Ainsi, en posant $t_0=0$, on a $J^0=A(t_0)$.
	\item Supposons que la proposition $P_n$ est vraie pour un certain entier $n$ fixé. Ainsi, il existe un réel $t_n$ tel que $J^n = A(t_n)$. Mais alors
	\[ J^{n+1}=J^nJ=A(t_n) A(-1) = A(t_n-1+2t_n)=A(3t_n-1).\]
	Ainsi, en posant $t_{n+1}=3t_n-1$, $J^{n+1}=A(t_{n+1})$ et la proposition $P_{n+1}$ est vérifiée.
\end{itemize}
D'après le principe de récurrence, il existe une suite $(t_n)$ telle que pour tout $n$ $J^n=A(t_n)$.
\item Le travail de la récurrence nous garantit que $t_0=0$ et que pour tout entier $n$, $t_{n+1}=3t_n-1$.
\item $(t_n)$ est une suite arithmético-géométrique. En notant $\ell$ le réel vérifiant $\ell = 3\ell -1$, c'est-à-dire $\ell=\frac12$, la suite $(v_n)$ définie pour tout $n$ par $v_n=t_n-\frac12$ est une suite géométrique, de premier terme $v_0=t_0-\frac12=-\frac12$ et de raison $3$. Alors
\[ \forall n\in \N,\quad v_n =-\frac12 3^n \qeq t_n = \frac{1-3^n}{2}. \]
Finalement, 
\[ \boxed{\forall n\in \N,\quad J^n = A\left(\frac{1-3^n}{2}\right) = \matrice{
\frac{3^n+1}{2} & \frac{3^n-1}{2} & 0 \\
\frac{3^n-1}{2} & \frac{3^n+1}{2} & 0 \\
\frac{3^n-1}{2} & \frac{1-3^n}{2} & 3^n
}.}\]
\end{enumerate}
\end{enumerate}











\end{correction}
%%% Fin exercice %%%
