%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{60}{2}[Matrices orthogonales]
Soit $n\in \N\setminus \{0,1\}$. On dit qu'une matrice $M\in \MM_n(\R)$ est \textbf{orthogonale} si $M^\mathsf{T} M = M M^\mathsf{T} = I_n$. Autrement dit, $M$ est orthogonale si et seulement si $M$ et inversible et $M^{-1}=M^\mathsf{T}$.

On note $\mathcal{O}_n(\R)$ l'ensemble des matrices orthogonales de $\MM_n(\R)$.

\subsection*{Partie A : le cas particulier $\mathcal{O}_2(\R)$}

Dans cette partie, on se donne $M=\matrice{a&b\\c&d}\in \mathcal{O}_2(\R)$.
\begin{enumerate}
	\item Montrer que $a^2+b^2=a^2+c^2=d^2+c^2=d^2+b^2=1$ et que $ac+bd=0$.
	\item Supposons que $a=0$. Montrer que $M$ est l'une des matrices suivantes :
	\[ \matrice{0&1\\1&0},\quad \matrice{0&-1\\-1&0},\quad \matrice{0&-1\\1&0},\quad \matrice{0&1\\-1&0}.\]
	\item Supposons que $a\neq 0$. 
		\begin{enumerate}
			\item Justifier qu'il existe $\theta \in \interfo{0 2\pi}\setminus \left\{-\tfrac{\pi}{2}, \tfrac{\pi}{2}\right\}$ tel que $a=\cos(\theta)$ et $c=\sin(\theta)$.
			\item En déduire qu'il existe $\eps \in \{-1,1\}$ tel que $M=\matrice{\cos(\theta)&-\eps\sin(\theta)\\\sin(\theta)&\eps\cos(\theta)}$.
		\end{enumerate}
	\item Réciproquement, soit $\theta\in \R$, et $\eps\in \{-1, 1\}$. Vérifier que $\matrice{\cos(\theta)&-\eps\sin(\theta)\\\sin(\theta)&\eps\cos(\theta)}\in \mathcal{O}_2(\R)$.
	\item \'Ecrire une fonction \textsc{Python}, prenant en argument $\theta\in \R$ et $\eps\in \{-1,1\}$ et qui renvoie la matrice ci-dessus.
\end{enumerate}

\subsection*{Partie B : généralités sur les matrices orthogonales}

\begin{enumerate}
	\item \begin{enumerate}
	\item Vérifier que, si $A\in \mathcal{O}_n(\R)$, alors $A^{-1}\in \mathcal{O}_n(\R)$ et $A^\mathsf{T}\in \mathcal{O}_n(\R)$.
	\item Montrer que si $A$ et $B$ sont deux matrices de $\mathcal{O}_n(\R)$, alors $AB\in \mathcal{O}_n(\R)$.
	\end{enumerate}
	\item Pour tout $X=\matrice{x_1\\x_2\\\vdots\\x_n}\in \MM_{n,1}(\R)$, on appelle norme de $X$ le réel positif \[\norme{X}=\sqrt{x_1^2+x_2^2+\hdots +x_n^2}.\]
	\begin{enumerate}
		\item Soit $X\in \MM_{n,1}(\R)$. Montrer que $\norme{X}=0$ si et seulement si $X=0$.
		\item Justifier que\footnote{En réalité, $X^\mathsf{T}X$ est une matrice à un seul coefficient, ce qu'on assimile à un réel.}, si $X\in \MM_{n,1}(\R)$, alors $\norme{X}^2=X^\mathsf{T}X$.
		\item En déduire que si $A\in \mathcal{O}_n(\R)$ et $X\in \MM_{n,1}(\R)$, alors $\norme{AX}=\norme{X}$.
	\end{enumerate}
	\item Soit $A=(a_{i,j})_{1\leq i,j\leq n} \in \mathcal{O}_n(\R)$.
	\begin{enumerate}
		\item Montrer que, pour tout $i\in \interent{1 n}$,  $\ds \sum_{k=1}^n a_{i,k}^2=1$.
		\item En déduire que, pour tout $(i,j)\in \interent{1 n}^2$, $|a_{i,j}|\leq 1$.
	\end{enumerate}
	\item Notons $\mathcal{P}_n(\R)$ l'ensemble des matrices $M\in \MM_n(\R)$ telles que, sur chaque ligne et chaque colonne de $M$ se trouve un et un seul coefficient non nul, qui vaut $1$ ou $-1$.
	\begin{enumerate}
		\item Donner un exemple d'une matrice de $\mathcal{P}_4(\R)$ qui n'est pas $I_4$.
		\item Soient $M\in \mathcal{P}_n(\R)$ et $(i,j)\in \interent{1 n}^2$. Calculer $\left(M M^\mathsf{T}\right)_{i,j}$.
		\item En déduire que les matrices de $\mathcal{P}_n(\R)$ sont les seules matrices de $\mathcal{O}_n(\R)$ dont les coefficients sont entiers. \\\textit{On s'aidera de la question $3$.}
	\end{enumerate}
\end{enumerate}
\end{exoApp}

\begin{correction}
\subsection*{Partie A : le cas particulier $\mathcal{O}_2(\R)$}

\begin{enumerate}
	\item Par définition, $MM^\mathsf{T} = I_2$, donc 
	\[ \matrice{a&b\\c&d}\matrice{a&c\\b&d} = I_2\iff \matrice{a^2+b^2&ac+bd\\ac+bd&c^2+d^2} = I_2.\]
	Par identification des coefficients, $a^2+b^2=c^2+d^2=1$ et $ac+bd=0$.\\
	De même, $M^\mathsf{T} M= I_2$ soit
	\[ \matrice{a&c\\b&d} \matrice{a&b\\c&d} = \matrice{a^2+c^2 & ab+cd\\ab+cd & b^2+d^2} = I_2 \]
	soit \[ a^2+c^2=b^2=d^2=1 \qeq ab+cd =0 . \]
	\item Si $a=0$, alors $b^2=c^2=1$ donne $b \in \{-1,1\}$ et $c\in \{-1,1\}$. De même, $d^2+b^2=1$ donne $d^2=0$ soit $d=0$. Cela donne donc quatre possibilités : 
	\[ \matrice{0&1\\1&0},\, \matrice{0&1\\-1&0},\, \matrice{0&-1\\1&0},\,\matrice{0&-1\\-1&0}.\]
	On constate que ces quatre matrices sont orthogonales.
	\item \begin{enumerate}
	\item Si $a\neq 0$, l'équation $a^2+c^2=1$ nous impose $a^2\leq 1$ et $c^2\leq 1$ soit $a\in \interff{-1 1}$ et $c\in \interff{-1 1}$. Par surjectivité de cosinus, il existe $\theta \in \interff{0 \pi}$ tel que $a=\cos(\theta)$. Puisque $a\neq 0$, $\theta \neq \frac{\pi}{2}$, et on a $\sin(\theta)\geq 0$.  
	
	L'équation $a^2+b^2=1$ donne $\cos^2(\theta)+b^2=1=\cos^2(\theta)+\sin^2(\theta)$, c'est-à-dire $b^2 = \sin^2(\theta)$.\\
		Si $b\geq 0$, alors $b=\sin(\theta)$.\\
		Si $b<0$, alors $b=-\sin(\theta)=\sin(-\theta)$ et on constate que $a=\cos(\theta)=\cos(-\theta)$.
		
	Dans tous les cas, $a=\cos(\theta)$ et $b=\sin(\theta)$, avec (en utilisant la $2\pi$-périodicité), $\theta \in \interfo{0 2\pi} \setminus \left \{ -\frac{\pi}{2},\, \frac{\pi}{2}\right \}$.
		\item Les relations $a^2+b^2=1$ et $c^2+d^2=1$ garantissent que 
			\[ d^2 = \cos^2(\theta) \qeq b^2=\sin^2(\theta)\]
	donc il existe $(\alpha, \beta ) \in \{-1,1\}^2$ tels que 
	\[ b=\alpha \sin(\theta) \qeq d = \beta \cos(\theta).\]
	Enfin, $ac+bd=0$ nous donne 
	\[ \cos(\theta)\sin(\theta) + \alpha \beta \cos(\theta)\sin(\theta) = (1+\alpha\beta)\cos(\theta)\sin(\theta) \]
	ce qui impose $\alpha=-\beta$. Finalement, 
	\[ \exists~ \epsilon \in \{-1,1\},\quad M = \matrice{\cos(\theta) & -\epsilon \sin(\theta) \\\sin(\theta) & \epsilon\cos(\theta)}.\]
	\end{enumerate}
	\item On calcule :
\small \begin{align*}
 \matrice{\cos(\theta) & -\epsilon \sin(\theta) \\\sin(\theta) & \epsilon\cos(\theta)} \matrice{\cos(\theta) & -\epsilon \sin(\theta) \\\sin(\theta) & \epsilon\cos(\theta)}^\mathsf{T} &= 
  \matrice{\cos(\theta) & -\epsilon \sin(\theta) \\\sin(\theta) & \epsilon\cos(\theta)}  \matrice{\cos(\theta) & \sin(\theta) \\ -\epsilon\sin(\theta) & \epsilon\cos(\theta)}	\\
  &= \matrice{\cos^2(\theta) + \epsilon^2\sin^2(\theta) & \cos(\theta)\sin(\theta)-\epsilon^2 \cos(\theta)\sin(\theta)\\
  \cos(\theta)\sin(\theta)-\epsilon^2(\theta)\cos(\theta)\sin(\theta)&\sin^2(\theta)+\epsilon^2\cos^2(\theta)}\\
  &= \matrice{1&0\\0&1}\text{ car } \epsilon^2=1 \et \cos^2(\theta)+\sin^2(\theta)=1.
\end{align*}
\normalsize
\item On applique naïvement ce qui précède :
\begin{CodePython}[Lignes=false]
import numpy as np

def ortho(theta, eps):
  	M = np.array([ [np.cos(theta), -eps*np.sin(theta)],
  				   [np.sin(theta), eps*np.cos(theta)]])
  	return M
\end{CodePython}
ce qui donne par exemple
\begin{python}
import numpy as np

def ortho(theta, eps):
  	M = np.around(np.array([ [np.cos(theta), eps*np.sin(theta)],
  				   [np.sin(theta), eps*np.cos(theta)]]))
  	return M	
\end{python}

\begin{ConsolePython}
ortho(0, -1)
ortho(np.pi/2, -1)	
ortho(np.pi, 1)	
\end{ConsolePython}

\end{enumerate}

\subsection*{Partie B : généralités sur les matrices orthogonales}

\begin{enumerate}
	\item \begin{enumerate}
	\item Soit $A\in \OO_n(\R)$. Par définition, $A \times \transp{A} = I_n$. Par définition, $A$ est inversible et $A^{-1}=\transp{A}$. Enfin, en utilisant les propriétés de la transposée :
	\[ A^{-1}\transp{\left(A^1\right)} = A^{-1}\left(\transp{A}\right)^{-1} = \left(\transp{A}A\right)^{-1}=I_n^{-1}=I_n\]
	le résultat étant valable dans l'autre sens, on a bien que $A^{-1}\in \OO_n(\R)$. \\Enfin,
	\[ \transp{A} \transp{\left(\transp{A}\right)} = \transp{A}A = I_n\]
	et de même dans l'autre sens : $\transp{A}\in \OO_n(\R)$.
	\item Il s'agit d'un calcul simple. Soient $A$ et $B$ deux matrices de $\OO_n(\R)$. Alors 
	\[ (AB)\transp{(AB)} = A\underbrace{B \transp{B}}_{=I_n} \transp{A}=A\transp{A}=I_n \]
	et 
		\[ \transp{(AB)}AB = \transp{B} \underbrace{\transp{A}A}_{=I_n}B=\transp{B}B=I_n. \]
		Ainsi $AB\in \OO_n(\R)$.
	\end{enumerate}
	\item \begin{enumerate}
	\item Si $X=0$ alors $\norme{X}=\sqrt{0}=0$. \\Réciproquement, si $\norme{X}=0$, alors $\sqrt{x_1^2+\hdots+x_n^2}=0$, soit $x_1^2+\hdots +x_n^2=0$. Une somme de termes positifs est nulle si et seulement si tous les termes sont nuls; ainsi, $x_1^2=\hdots = x_n^2=0$ et donc $x_1=\hdots=x_n=0$ : $X=0$.
	\item Il s'agit d'un calcul, déjà vu dans l'exercice \lienexo{02} :
	\begin{align*}
		\transp{X}X &= \matrice{x_1&\hdots&x_n} \matrice{x_1\\\vdots\\x_n}\\ 
		&= x_1^2+\hdots+ x_n^2 = \norme{X}^2.
	\end{align*}
	\item On utilise la remarque précédente :
	\begin{align*}
	\norme{AX}^2 &= \transp{(AX)}AX = \transp{X} \underbrace{\transp{A}A}_{=I_n}X=\transp{X} X = \norme{X}^2. 	
	\end{align*}
	Les deux termes étant positifs, on en déduit bien que $\norme{AX}=\norme{X}$.
 	\end{enumerate}
 	\item \begin{enumerate}
 	\item Soit $i\in \interent{1 n}$. Par définition du produit matriciel :
 	\begin{align*}
 	 (A\transp{A})_{i,i} &= \sum_{k=1}^n (A)_{i,k} (\transp{A})_{k,i} \\
 	 &= \sum_{k=1}^n a_{i,k} (A)_{i,k} = \sum_{k=1}^n a_{i,k}^2.
 	\end{align*}
	Or $A\transp{A}=I_n$ donc $(A\transp{A})_{i,i} = 1$. On peut conclure que 
	\[ \forall i\in \interent{1 n},\quad \sum_{k=1}^n a_{i,k}^2=1.\]
	\item Soient $(i,j)\in \interent{1 n}^2$. D'après ce qui précède 
	\[ \sum_{k=1}^n a_{i,k}^2=1 \implies a_{i,j}^2 = 1 - \underbrace{\sum_{\substack{1\leq k \leq n\\k\neq j}} a_{i,k}^2}_{\text{termes positifs}} \leq 1. \]
	Par application de la fonction racine, croissante sur $\R+$ 
	\[ \forall (i,j)\in \interent{1 n}^2,\quad |a_{i,j}| \leq 1.\]
 	\end{enumerate}
 	\item \begin{enumerate}
 	\item Un exemple (pas unique) : $\matrice{1&0&0&0\\0&0&-1&0\\0&1&0&0\\0&0&0&-1}$.
 	\item On fixe $M\in \mathcal{P}_n(\R)$ et $(i,j)\in \interent{1 n}^2$.
 	\begin{align*}
 	(M\transp{M})_{i,j} &= \sum_{k=1}^n (M)_{i,k} (\transp{M})_{	k,j} \\
 	&= \sum_{k=1}^n m_{i,k} m_{j,k}.
 	\end{align*}
	Or, dans la colonne $k$, il y a un seul terme non nul. Si on note $\ell$ sa position (i.e. $m_{\ell,k}\in \{-1,1\}$ et $m_{i,k}=0$ si $k\neq \ell$), alors le résultat précédent est nul si $i\neq j$, et si $i=j$, un seul terme est non nul et 
	\[ (M\transp{M})_{i,i}=\sum_{k=1}^n m_{i,k} m_{i,k}=m_{\ell,k}^2 = 1.\]
	Finalement,
	\[ (M\transp{M})_{i,j}=\begin{cases} 1 \text{ si } i=j\\0 \text{ sinon}\end{cases}\]
	et donc
	\[ M\transp{M} = I_n.\]
	Le raisonnement dans l'autre sens est similaire et finalement $M\in \OO_n(\R)$.
	\item La question $3$ assure que si $M$ est une matrice de $\OO_n(\R)$ à coefficients entiers, puisque pour tous $i$ et $j$, $|m_{i,j}|\leq 1$ alors ses seuls coefficients possibles sont $0$, $1$ ou $-1$.\\
	Par ailleurs, puisque $\ds\sum_{k=1}^n a_{i,k}^2=1$, si un terme vaut $1$ ou $-1$, les autres termes sont nécessairement nuls : ainsi, sur chaque colonne, un seul terme est non nul et vaut $1$ ou $-1$. Le raisonnement sur les lignes est le même (en utilisant l'autre produit) et finalement une matrice orthogonale à coefficients entiers a nécessairement tous ses termes nuls, sauf 1 sur chaque ligne et sur chaque colonne, qui vaut $1$ ou $1$. Ainsi
	\[ \boxed{\OO_n(\Z) = \mathcal{P}_n.}\]
 	\end{enumerate}
\end{enumerate}

\end{correction}
%%% Fin exercice %%%
