\begin{exoApp}{30}{1}[Sujet de concours - IV]\label{\formatexo{13}}
Soit $f$ la fonction définie sur $\R^+$ par \[f(x)=2\sqrt{x}\eu{-x}\]
\begin{enumerate}
	\item Montrer que $f$ est continue sur $\R^+$, et vérifier que $f\left(\frac{1}{2}\right) = \sqrt{\frac{2}{\E}}$.
	\item Etudier la dérivabilité de $f$ sur $\R^+$.
	\item Etudier la branche infinie de $f$ au voisinage de $+\infty$.
	\item Dresser le tableau de variations complet de $f$.
	\item On définit la fonction $g:\left[0;\frac{1}{2}\right] \rightarrow \R$ par $\forall~x\in \left[0;\frac{1}{2}\right],~~g(x)=2\sqrt{x}\eu{-x}$. Montrer que $g$ est bijective de $ \left[0;\frac{1}{2}\right]$ dans un intervalle à considérer.
	\item Dresser le tableau de variations complet de $g^{-1}$
	\item Démontrer que $g$ est dérivable en tout point de l'intervalle $\left]0; \sqrt{\frac{2}{\E}}\right[$. Est-elle dérivable aux bornes ?
	\item Tracer l'allure des fonctions $g$ et $g^{-1}$.
\end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item Les fonctions $x\mapsto \sqrt{x}$ et $x\mapsto \eu{-x}$ sont continues sur $\R^+$ (fonctions usuelles). Par produit, $f$ est continue sur $\R^+$. De plus, \[ f\left(\frac{1}{2}\right)=2\sqrt{\frac{1}{2}}\eu{-\frac{1}{2}} = \frac{2}{\sqrt{2}\eu{\frac{1}{2}}} = \frac{2\sqrt{2}}{\sqrt{2}\sqrt{2}\sqrt{\E}} = \sqrt{\frac{2}{\E}}\]
	\item $x\mapsto \sqrt{x}$ est dérivable sur $\R^*_+$, et $x\mapsto \eu{-x}$ est dérivable sur $\R$. Par produit, $f$ est dérivable sur $\R^*_+$, et sa dérivée est donnée, pour tout $x>0$, par 
		\[f'(x)=2\frac{1}{2\sqrt{x}}\eu{-x} + 2\sqrt{x}(-\eu{-x})\]
		c'est-à-dire,
		\[ \boxed{\forall~x>0, ~f'(x)=\frac{1-2x}{\sqrt{x}}\eu{-x}} \]
		Pour déterminer si $f$ est dérivable en $0$, on détermine le taux d'accroissement de $f$ en $0$, noté $T_0$ :
		\[ \forall~x>0,~T_0(x)=\frac{f(x)-f(0)}{x-0}=\frac{2\sqrt{x}\eu{-x}}{x}=2\frac{\eu{-x}}{\sqrt{x}} \]
		Remarquons que $\displaystyle{\lim_{x\rightarrow 0^+} \sqrt{x}=0^+}$ et $\displaystyle{\lim_{x\rightarrow 0^+} \eu{-x}=1}$. Par quotient, \[ \lim_{x\rightarrow +0^+} T_0(x)=+\infty\]
		Ainsi, $f$ n'est pas dérivable en $0$, et la courbe de $f$ admet une demi-tangente verticale au point d'abscisse $0$.\\\textbf{Bilan} : $f$ est dérivable sur $\R^*_+$ mais pas en $0$.
	\item Pour tout $x>0$, on a \[ f(x)=2\sqrt{x}\eu{-x} = 2 \frac{x\eu{-x}}{\sqrt{x}}\] Puisque $\displaystyle{\lim_{x\rightarrow +\infty} x\eu{-x}=0}$ (par croissance comparée) et $\displaystyle{\lim_{x\rightarrow +\infty} \sqrt{x}=+\infty}$, par quotient, \[\boxed{\lim_{x\rightarrow +\infty} f(x)=0}\]
		Ainsi, l'axe des abscisses est asymptote horizontale à la courbe de $f$ au voisinage de $+\infty$.
	\item On a vu que, pour tout $x>0$, $\displaystyle{f'(x)=\frac{1-2x}{\sqrt{x}}\eu{-x}}$. Puisque $\sqrt{x}>0$, et $\eu{-x}>0$ pour $x>0$, $f'(x)$ est du signe de $1-2x$. On obtient le tableau de variations suivant :
\begin{center}
\begin{tikzpicture}[yscale=0.7]
   \tkzTabSetup[doubledistance = 1pt]
         \tkzTabInit{$x$ / 1 ,  $f'(x)$ / 1, $f(x)$ / 2}{$0$, $\frac{1}{2}$, $+\infty$}
        \tkzTabLine{d,+,z,-}
        \tkzTabVar{-/$0$, + / $f\left(\frac{1}{2}\right)$, - / $0$}
\end{tikzpicture}
\end{center}
	\item On constate que $g$ est la restriction de $f$ à l'intervalle $I=\left[0;\frac{1}{2}\right]$. D'après l'étude précédente, $g$ est continue sur $I$, strictement croissante sur $I$. D'après le théorème de la bijection, $g$ établit une bijection de $I$ sur $g(I)=\left[0; f\left(\frac{1}{2}\right)\right]=\left[0; \sqrt{\frac{2}{\E}} \right]$.
	\item Par théorème, $g^{-1}$ est continue et de même variation de $g$. D'après le tableau de $f$, on obtient le tableau de variations suivant 
\begin{center}
\begin{tikzpicture}[yscale=0.7]
   \tkzTabSetup[doubledistance = 1pt]
         \tkzTabInit{$x$ / 1 ,  $g^{-1}(x)$ / 2}{$0$, $\sqrt{\frac{2}{\E}}$}
        \tkzTabVar{-/$0$, + / $\frac{1}{2}$}
\end{tikzpicture}
\end{center}
	\item Pour tout réel $x\in \left]0;\frac{1}{2} \right[$, $f'(x)\neq 0$. Par théorème, $g^{-1}$ est dérivable sur $f\left(\left]0;\frac{1}{2}\right[\right) = \left]0;\sqrt{\frac{2}{\E}}\right[$.
		\begin{itemize}[label=\textbullet]
			\item En $x=\sqrt{\frac{2}{\E}}$, $g'\left(g^{-1}(x)\right)=g'\left(\frac{1}{2}\right)=0$. D'après un résultat du cours, $g^{-1}$ n'est pas dérivable en $\sqrt{\frac{2}{\E}}$ et la courbe de $g^{-1}$ admet une tangente verticale au point d'abscisse $x=\sqrt{\frac{2}{\E}}$.
			\item En $x=0$, on a vu (question 2) que $g$ n'est pas dérivable en $0$ et que la courbe de $g$ admet une demi-tangente verticale au point d'abscisse $0$. Par symétrie par rapport à l'axe $y=x$, la courbe de $g$ va admettre une tangente horizontale au point d'abscisse $0$. 
			\begin{remarque}On peut le montrer, en calculant la limite du taux d'accroissement, et \textbf{cette méthode est à connaitre}.
				\[ \lim_{x\rightarrow 0} \frac{g^{-1}(x)-g^{-1}(0)}{x-0} = \lim_{x\rightarrow 0} \frac{g^{-1}(x)}{x}\]
				Posons $X=g^{-1}(x)$, et donc $x=g(X)$. Par continuité de $g^{-1}$ (comme fonction réciproque d'une fonction continue strictement monotone) on a
				\[ \lim_{x\rightarrow 0} X=\lim_{x\rightarrow 0} g^{-1}(x)=g^{-1}(0)=0 \]
				et 
				\[ \lim_{X\rightarrow 0} \frac{X}{g(X)} = \lim_{X\rightarrow 0} \frac{1}{\frac{g(X)}{X}} = 0 \]
				car $\displaystyle{\lim_{X\rightarrow 0} \frac{g(X)}{X}=+\infty}$ (question 2). Par composée,
				\[ \lim_{x\rightarrow 0} \frac{g^{-1}(x)}{x} = 0 \]
				Donc $g^{-1}$ est dérivable en $0$, et $(g^{-1})'(0)=0$.
			\end{remarque}
		\item En regroupant tous les résultats précédents, on obtient les graphiques suivants : \begin{center}
			\includegraphics{exo13.mps}
			\includegraphics[width=10cm]{exo12.pdf}
		\end{center}
		\end{itemize}
\end{enumerate}

\end{correction}
