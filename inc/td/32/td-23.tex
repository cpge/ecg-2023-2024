%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{30}{3}[Inégalités de Hölder et de Minkowski]
Soit $(p, q) \in \interoo{1 +\infty}^2$ vérifiant $\frac{1}{p}+\frac{1}{q}=1$.
\begin{enumerate}
\item Montrer que, pour tout $(u, v) \in\left(\R+\right)^{2}$, $u v \leq \frac{u^{p}}{p}+\frac{v^{q}}{q}$.
\item Soient $u_{1}, \hdots, u_{n}$ et $v_{1}, \hdots, v_{n}$ des réels vérifiant $\sum\limits_{i=1}^{n}\left|u_{i}\right|^{p}=\sum\limits_{i=1}^{n}\left|v_{i}\right|^{q}=1$.\\Montrer que $\sum\limits_{i=1}^{n}\left|u_{i} v_{i}\right| \leq 1$.
\item Soient $x_{1}, \hdots, x_{n}$ et $y_{1}, \hdots, y_{n}$ des réels. Montrer l'inégalité, appelée \textbf{inégalité de Hölder} :
\[
\sum_{i=1}^{n}\left|x_{i} y_{i}\right| \leq\left(\sum_{i=1}^{n}\left|x_{i}\right|^{p}\right)^{1 / p}\left(\sum_{i=1}^{n}\left|y_{i}\right|^{q}\right)^{1 / q} .
\]
On pourra poser, pour tout $k \in \interent{1 n}$, \[u_{k}=x_{k}\left(\sum_{j=1}^{n}\left|x_{j}\right|^{p}\right)^{-1 / p} \quad \qeq \quad v_{k}=y_{k}\left(\sum_{j=1}^{n}\left|y_{j}\right|^{q}\right)^{-1 / q}.\]
\item En déduire l'inégalité suivante, appelée \textbf{inégalité de Minkowski} :
\[ \left(\sum_{i=1}^n |x_i+y_i|^p\right)^{1/p} \leq \left(\sum_{i=1}^n |x_i|^p\right)^{1/p} + \left(\sum_{i=1}^n |y_i|^p\right)^{1/p}. \]
On pourra remarquer que $|x_k+y_k|^p\leq |x_k|\cdot |x_k+y_k|^{p-1} + |y_k|\cdot |x_k+y_k|^{p-1}$.
\end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item Si $u$ ou $v$ sont nuls, le résultat est vrai. Supposons $u$ et $v$ strictement positifs. Par concavité du logarithme, puisque $\frac1p+\frac1q=1$ :
	\begin{align*}
	 \ln\left(\frac{1}{p}u^p + \frac{1}{q}v^q\right)\geq \frac{1}{p}\ln(u^p) + \frac{1}{q}\ln(u^q) 	
	\end{align*}
	soit, 
	\begin{align*}
		\ln\left(\frac{1}{p}u^p + \frac{1}{q}v^q\right)\geq \ln(uv).
	\end{align*}
	En appliquant la fonction exponentielle, croissante sur $\R$, on en déduit 
	\begin{align*}
	 \boxed{\frac1pu^p+\frac1qu^q\geq uv.}	
	\end{align*}
	\item Soit $i\in \interent{1 n}$. Appliquons le résultat précédent à $|u_i|\in \R+$ et $|v_i|\in \R+$.
	\begin{align*}
	 |u_i||v_i| &\leq \frac{|u_i|^p}{p}+\frac{|v_i|^q}{q} 	
	\end{align*}
	En sommant ces inégalités :
	\begin{align*}
	\sum_{i=1}^n |u_iv_i| &\leq \sum_{i=1}^n \frac{|u_i|^p}{p}+\frac{|v_i|^q}{q}\\
	&\leq \frac{1}{p}\underbrace{\sum_{i=1}^n |u_i|^p}_{=1} + \frac1q \underbrace{\sum_{i=1}^n |v_i|^q}_{=1}= \frac{1}{p} + \frac{1}{q} = 1.	
	\end{align*}
	Ainsi, $\ds\boxed{\sum_{i=1}^n |u_iv_i| \leq 1.}$
	\item Suivons l'indication. Notons $\ds \lambda = \left(\sum_{j=1}^n |x_j|^p\right)^{-1/p}$ qui est un réel indépendant de $k$. Alors :
	\begin{align*}
	\sum_{k=1}^n |u_k|^q &= \sum_{k=1}^n \left(|x_k|\lambda\right)^p = \lambda^p \sum_{k=1}^n |x_k|^q=\left(\sum_{j=1}^n |x_j|^p\right)^{-1} \sum_{k=1}^n |x_k|^q=1.
	\end{align*}
	De même, $\ds\sum_{k=1}^n |v_k|^q = 1$. On peut appliquer le résultat de la question précédente aux réels $(u_k)$ et $(v_k)$. 
	
	En notant $\ds \lambda = \left(\sum_{j=1}^n |x_j|^p\right)^{-1/p}$ et $\ds \mu = \left(\sum_{j=1}^n |y_j|^q\right)^{-1/q}$:
	\begin{align}
	\sum_{k=1}^n |u_kv_k| &= \sum_{k=1}^n |x_k\lambda y_k\mu| = \lambda\mu \sum_{k=1}^n |x_ky_k| \leq 1	
	\end{align}
 	et puisque $\lambda\geq 0$ et $\mu\geq 0$ 
 	\[\boxed{\sum_{k=1}^n |x_ky_k|\leq \frac{1}{\lambda\mu} = \left(\sum_{j=1}^n |x_j|^p\right)^{1/p}\left(\sum_{j=1}^n |y_j|^q\right)^{1/q}.}\]
	\item La remarque est vraie :
	\begin{align*}
	(x_k+y_k)^p &= (x_k+y_k)(x_k+y_k)^{p-1} = x_k (x_k+y_k)^{p-1}+y_k(x_k+y_k)^{p-1}	
	\end{align*}
	et on conclut par inégalité triangulaire. 
	
	Passons à l'inégalité à démontrer. On prend $p\in \interoo{1 +\infty}$ et on pose $q$ tel que $\frac{1}{q}=1-\frac{1}{p}$ (c'est-à-dire $q=\frac{p}{p-1}$). D'après l'inégalité de Hölder, appliquée aux $(x_k)$ et au $|x_k+y_k|^{p-1}$ :
	\begin{align*}
	\sum_{k=1}^n |x_k|\cdot |x_k+y_k|^{p-1} &\leq \left(\sum_{k=1}^n |x_k|^p\right)^{1/p}\left(\sum_{k=1}^n \left(|x_k+y_k|^{p-1}\right)^q\right)^{1/q}
	\end{align*}
	soit, puisque $(p-1)q=p$
	\begin{align*}
	\sum_{k=1}^n |x_k|\cdot |x_k+y_k|^{p-1} &\leq \left(\sum_{k=1}^n |x_k|^p\right)^{1/p}\left(\sum_{k=1}^n |x_k+y_k|^q\right)^{1/q}	
	\end{align*}
	Par le même raisonnement 
\begin{align*}
	\sum_{k=1}^n |y_k|\cdot |x_k+y_k|^{p-1} &\leq \left(\sum_{k=1}^n |y_k|^p\right)^{1/p}\left(\sum_{k=1}^n |x_k+y_k|^q\right)^{1/q}	
	\end{align*}
	et en utilisant la remarque
	\begin{align*}
	\sum_{k=1}^n |x_i+y_i|^p &\leq \sum_{k=1}^n |x_k|\cdot |x_k+y_k|^{p-1} +\sum_{k=1}^n |y_k|\cdot |x_k+y_k|^{p-1}\\
	&\leq \left(\sum_{k=1}^n |x_k|^p\right)^{1/p}\left(\sum_{k=1}^n |x_k+y_k|^q\right)^{1/q}+\left(\sum_{k=1}^n |y_k|^p\right)^{1/p}\left(\sum_{k=1}^n |x_k+y_k|^q\right)^{1/q}	\\
	&\leq \left(\sum_{k=1}^n |x_k+y_k|^q\right)^{1/q}	\left( \left(\sum_{k=1}^n |x_k|^p\right)^{1/p}+\left(\sum_{k=1}^n |y_k|^p\right)^{1/p}\right)
	\end{align*}	
	En divisant par $\ds\left(\sum_{k=1}^n |x_k+y_k|^q\right)^{1/q}\neq 0$
	\begin{align*}
	\left(\sum_{k=1}^n |x_i+y_i|^p\right)^{1-1/q} &\leq 	\left(\sum_{k=1}^n |x_k|^p\right)^{1/p}+\left(\sum_{k=1}^n |y_k|^p\right)^{1/p}
	\end{align*}
	c'est-à-dire, puisque $1-\frac1q=\frac1p$
	\begin{align*}
		\boxed{\left(\sum_{k=1}^n |x_i+y_i|^p\right)^{1/p} \leq 	\left(\sum_{k=1}^n |x_k|^p\right)^{1/p}+\left(\sum_{k=1}^n |y_k|^p\right)^{1/p}}
	\end{align*}

\end{enumerate}
\end{correction}
%%% Fin exercice %%%
