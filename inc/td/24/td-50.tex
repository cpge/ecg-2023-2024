%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoConc}{45}{1}[EML maths appro -- 2023]
Dans cet exercice, $x$ désigne un élément de $\interoo{0 1}$. Pour $n\in \N*$, on pose $\ds S_n=\sum_{k=1}^n \frac1k$.

\begin{enumerate}
	\item \begin{enumerate}
	\item Soit $k\in \N*$. Démontrer que l'on a : $\ds \frac{1}{k+1} \leq \int_k^{k+1} \frac1t \dd t\leq \frac1k$.
	\item Soit $n$ un entier naturel supérieur ou égal à $2$. Démontrer que \[\ds S_n-1 \leq \int_1^n \frac{\dd t}{t}Â \leq S_n-\frac{1}{n}.\]
	\item En déduire, pour tout entier $n$ supérieur ou égal à $2$, un encadrement de $S_n$.
	\item Démontrer que $S_n\eq{+\infty} \ln n$;
	\end{enumerate}
	\item \textbf{\textit{Informatique}}.
	\begin{enumerate}
		\item On considère la fonction suivante écrite en langage Python.
\begin{CodePython}[BarreTitre=false,Lignes=false,Largeur=0.5\textwidth]
def rang(a):
    k=1
    s=1
    while s<a:
        k=k+1
        s=s+1/k
    return k	
\end{CodePython}
	Expliquer ce que produit l'appel \piton{rang(50)}.
	\item Le code suivant renvoie :
\begin{ConsolePython}
from numpy import exp
print(exp(49))
\end{ConsolePython}

	Expliquer rapidement ce que cela laisse penser si l'on fait l'appel \piton{rang(50)}.
	\end{enumerate}
	\item \begin{enumerate}
	\item Soient $n\in \N*$ et $t\in \interff{0 x}$. Simplifier la somme $\ds\sum_{k=1}^n t^{k-1}$.
	\item En déduire que pour tout $n\in \N*$, on a :
	\[ \sum_{k=1}^n \frac{x^k}{k} = -\ln(1-x) - \int_0^x \frac{t^n}{1-t}\dd t.\]
	\item Démontrer que : $\ds \lim_{n\to +\infty} \int_0^x \frac{t^n}{1-t}\dd t=0$.
	\item En déduire que la série $\ds\sum_{k\geq 1} \frac{x^k}{k}$ converge, de somme
	\[Â \sum_{k=1}^{+\infty} \frac{x^k}{k} = - \ln(1-x).\]
	\end{enumerate}
\end{enumerate}
\end{exoConc}

\begin{correction}
\begin{enumerate}
	\item \begin{enumerate}
	\item Soit $k\in \N*$ et $t\in \interent{k k+1}$. Alors, par décroissance de la fonction inverse sur $\R>$ :
\begin{align*}
k\leq t \leq k+1 &\implies \frac{1}{k+1}	\leq \frac1t\leq \frac1k. 
\end{align*}
Par croissance de l'intégrale (puisque $k\leq k+1$), on a alors 
\begin{align*}
\int_k^{k+1} \frac{1}{k+1} \D t \leq \int_k^{k+1} \frac{\D t}{t} \leq \int_k^{k+1} \frac1k\D t
\end{align*}
soit
\[ \boxed{\frac{1}{k+1} \leq \int_k^{k+1} \frac{\D t}{t}\leq \frac1k.}\]
\item Soit $n\geq 2$. Sommons ces inégalités pour $k$ entre $1$ et $n-1$ :
\begin{align*}
\sum_{k=1}^{n-1} \frac{1}{k+1} \leq \sum_{k=1}^{n-1} \int_k^{k+1} \frac{\D t}{g}\leq \sum_{k=1}^{n-1} \frac1k.
\end{align*}
En faisant le changement d'indice $i=k+1$ dans la première somme :
\begin{align*}
 \sum_{k=1}^{n-1} \frac{1}{k+1} &= \sum_{i=2}^n \frac{1}{i} = \sum_{i=1}^n \frac{1}{i}- 1 = S_n-1.
\end{align*} 
La somme de droite s'écrit :
\begin{align*}
\sum_{k=1}^{n-1} \frac{1}{k} &= \sum_{k=1}^n \frac{1}{k} - \frac{1}{n}.
\end{align*}
Enfin, par la relation de Chasles :
\begin{align*}
\sum_{k=1}^{n-1} \int_k^{k+1} \frac{\D t}{t} &= \int_1^n \frac{\D t}{t}.	
\end{align*}
Finalement, l'inégalité s'écrit 
\[ \boxed{S_n-1 \leq \int_1^n \frac{\D t}{t} \leq S_n-\frac1n.}\]
\item Remarquons tout d'abord que
\begin{align*}
\int_1^n \frac{\D t}{t} &= \left[ \ln(|t|)\right]_1^n = \ln(n).	
\end{align*}
Réécrivons alors l'inégalité précédente :
\[ S_n-1\leq \ln(n)Â \implies S_n \leq \ln(n)+1\]
et
\[ \ln(n)\leq S_n-\frac1n \implies \ln(n)+\frac1n \leq S_n.\]
Finalement
\[ \boxed{\forall n\geq 2,\quad \ln(n)+\frac1n \leq S_n \leq \ln(n)+1.}\]
\item Divisons l'inégalité précédente par $\ln(n)>0$ : pour $n\geq 2$, on a
\[  1+Â \frac{1}{n\ln(n)} \leq \frac{S_n}{\ln(n)}\leq 1+\frac{1}{\ln(n)}.\]
Par quotient 
\[ \frac{1}{n\ln(n)}\tendversen{n\to +\infty} 0\qeq \frac{1}{\ln(n)}\tendversen{n\to +\infty} 0.\]
Finalement 
\[ \lim_{n\to +\infty} 1+\frac{1}{n\ln(n)}=\lim_{n\to +\infty} 1+\frac{1}{\ln(n)} = 1.\]
Par encadrement,
\[ \lim_{n\to +\infty} \frac{S_n}{\ln(n)}=1 \]
soit 
\[Â \boxed{S_n \eq{+\infty} \ln(n).}\]
\end{enumerate}
\item\begin{enumerate}
\item Comme son nom l'indique, c'est un algorithme de seuil : l'instruction \piton{rang(a)} renvoie le premier rang $n$ à partir duquel $S_n\geq a$. Ainsi, \piton{rang(50)} renvoie le premier rang à partir duquel $S_n\geq 50$.
\item Puisque $S_n\eq{+\infty} \ln(n)$, on peut imaginer que $(S_n)$ dépassera $50$ à peu près lorsque $(\ln(n))$ le fera. Or $\ln(n)\geq 50 \iff n\geq \eu{50}$.\newline
L'instruction laisse donc à penser qu'il faudra attendre la $10^{21}$ rang avant que $(S_n)$ ne dépasse $50$ (c'est donc une convergence vers $+\infty$ extrêmement lente).
\end{enumerate}
\item \begin{enumerate}
\item Puisque $t\in \interff{0 x}$ et $x<1$, on est sûr que $t\neq 1$. On reconnait alors la somme des termes usuelles et 
\[ \boxed{ \sum_{k=1}^n t^{k-1} = \frac{1-t^n}{1-t}.}\]
\item Le terme $\frac{x^k}{k}$ laisse à penser que nous avons intégrer le résultat précédent.

Soit $n\in \N*$. Puisque, pour tout $t\in \interff{0 x}$, on a 
\[ \sum_{k=1}^n t^{k-1}  = \frac{1-t^n}{1-t} \]
alors
\begin{align*}
\int_0^x \sum_{k=1}^n t^{k-1}\D t &= \int_0^x \frac{1-t^n}{1-t} \D t	.
\end{align*}
Par linéarité de l'intégrale :
\begin{align*}
\int_0^x \sum_{k=1}^n t^{k-1}\D t &= \sum_{k=1}^n \int_0^x t^{k-1}\D t\\
&= \sum_{k=1}^n \left[ \frac{t^k}{k}\right]_0^x = \sum_{k=1}^n \frac{x^k}{k}.
\end{align*}
Toujours par linéarité :
\begin{align*}
\int_0^x \frac{1-t^n}{1-t}\D t &= \int_0^x\frac{\D t}{1-t} - \int_0^x \frac{t^n}{1-t}\D t\\
&= \left[ -\ln(|1-t|)\right]_0^x - \int_0^x \frac{t^n}{1-t}\D t\\
&= -\ln(1-x) - \int_0^x \frac{t^n}{1-t}\D t.	
\end{align*}
Finalement, on obtient bien
\[ \boxed{ \sum_{k=1}^n \frac{x^k}{k} = -\ln(1-x)-\int_0^x \frac{t^n}{1-t}\D t.}\]
\item Procédons par encadrement. Pour tout $t\in \interff{0 x}$ :
\[ t \leq x \implies 1-x\leq 1-t.\]
et finalement, par décroissance de la fonction inverse sur $\R>$
\[ \frac{1}{1-t} \leq \frac{1}{1-x}.\]
Enfin, puisque $t\leq x \leq 1$, $1-t \geq 0$. Finalement, pour tout $t\in \interff{0 x}$ :
\[ 0\leq \frac{1}{1-t}\leq \frac{1}{1-x}.\]
Mais alors, par produit (puisque $t^n\geq 0$) :
\[ 0 \leq \frac{t^n}{1-t}\leq \frac{t^n}{1-x} \]
et par croissance de l'intégrale :
\begin{align*}
0 \leq \int_0^x \frac{t^n}{1-t} \D t \leq \int_0^x \frac{t^n}{1-x}\D t.	
\end{align*}
Or 
\begin{align*}
\int_0^x \frac{t^n}{1-x} \D t &= \frac{1}{1-x} \left[ \frac{t^{n+1}}{n+1}\right]_0^x\\
&= \frac{1}{1-x} \frac{x^{n+1}}{n+1}.
\end{align*}
L'inégalité s'écrit alors
\begin{align*}
0\leq \int_0^x \frac{t^n}{1-t}\D t \leq \frac{1}{1-x}Â \frac{x^{n+1}}{n+1}.	
\end{align*}
Or, $0<x<1$, donc par croissances comparées
\[Â \lim_{n\to +\infty} \frac{x^{n+1}}{n+1} =0 .\]
Par encadrement, on en déduit donc que 
\[ \boxed{\lim_{n\to +\infty} \int_0^x \frac{t^n}{1-t}\D t = 0.}\]
\item On combine les deux derniers résultats. La question 3.c) garantit alors que, pour tout $x\in \interoo{0 1}$ :
\[ -\ln(1-x)-\int_0^x \frac{t^n}{1-t}\D t \tendversen{n\to +\infty} -\ln(1-x).\]
Or, la question 3.a) nous indique que
\[ \sum_{k=1}^n \frac{x^k}{k} = -\ln(1-x) - \int_0^x \frac{t^n}{1-t}\dd t.\]
Ainsi, la suite des sommes partielles de la série $\ds \sum_{k\geq 1} \frac{x^k}{k}$ converge. On peut en déduire que la série converge, et que
\[ \boxed{\forall x\in \interoo{0 1},\quad \sum_{k=1}^{+\infty} \frac{x^k}{k} = -\ln(1-x).}\]

\end{enumerate}
\end{enumerate}
\end{correction}
%%% Fin exercice %%%
