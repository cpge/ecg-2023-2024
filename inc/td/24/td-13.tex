%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{20}{2}[Séries à paramètres]
Étudier la nature des séries suivantes en fonction du paramètre $\alpha \in \R$ :
\everymath{\ds}
\begin{tasks}(3)
\task $\sum_{n \in \N} n \sin \left(\frac{1}{\sqrt{1+n^{\alpha}}}\right)$,\vspace*{.1cm}
\task $\sum_{n \in \N}(\sqrt{n+1}-\sqrt{n})^{\alpha}$,\vspace*{.1cm}
\task $\sum_{n \geqslant 1} \exp \left(-(\ln (n))^{\alpha}\right)$,\vspace*{.1cm}
\task $\sum_{n \in \N} \frac{\alpha^{n}}{1+\alpha^{2 n}}$,\vspace*{.1cm}
\task $\sum_{n \geqslant 1} \frac{1}{\alpha^{\ln (n)}}$,\vspace*{.1cm}
\task $\sum_{n \geqslant 1} \sqrt{1-\left(\frac{n^{\alpha}}{1+n^{\alpha}}\right)^{n}}$.
\end{tasks}
\everymath{}
\end{exoApp}

\begin{correction}
Pour la première, on remarque que si $\alpha<0$, $\frac{1}{\sqrt{1+n^\alpha}}\tendversen{n\to +\infty} 1$ et si $\alpha=0$, $\frac{1}{\sqrt{1+n^\alpha}}\tendversen{n\to +\infty} \frac{1}{\sqrt{2}}$. Dans les deux cas
\[ n\sin\left(\frac{1}{\sqrt{1+n^\alpha}}\right)\tendversen{n\to +\infty} +\infty. \]
Ainsi, la série diverge grossièrement si $\alpha \leq 0$. Si $\alpha >0$, alors $\frac{1}{\sqrt{1+n^\alpha}}\tendversen{n\to +\infty} 0$ et par équivalence classique :
\begin{align*}
  n\sin\left(\frac{1}{\sqrt{1+n^\alpha}}\right) &\eq{+\infty} n\frac{1}{\sqrt{1+n^\alpha}} \\
  &\eq{+\infty} \frac{n}{n^{\alpha/2}} \quad\text{car } 1+n^{\alpha} \eq{+\infty} n^\alpha\\
  &\eq{+\infty} \frac{1}{n^{\alpha/2-1}}.
\end{align*}
Les séries sont à termes positifs. D'après le résultat sur les séries de Riemann, $\sum \frac{1}{n^{\alpha/2-1}}$ converge si et seulement si $\frac{\alpha}{2}-1>1$, c'est-à-dire $\alpha > 4$. Par comparaison de séries à termes positifs, la série  $\sum_{n \in \N} n \sin \left(\frac{1}{\sqrt{1+n^{\alpha}}}\right)$ converge si et seulement si $\alpha>4$.

\textbf{Bilan} : la première série converge si et seulement $\alpha>4$.
\trait[0.2]
Pour la deuxième, remarquons que :
\begin{align*}
  \sqrt{n+1}-\sqrt{n} &= \frac{1}{\sqrt{n+1}+\sqrt{n}} \\
  &= \frac{1}{\sqrt{n}\left(\sqrt{\frac{n+1}{n}}+1\right)} \eq{+\infty} \frac{1}{2\sqrt{n}}
\end{align*}
Par exponentiation
\[ \left(\sqrt{n+1}-\sqrt{n}\right)^\alpha \eq{+\infty} \frac{1}{2^{\alpha}\sqrt{n}^{\alpha}} = \frac{1}{2^\alpha n^{\alpha/2}}. \]
Les séries sont à termes positifs, donc $\sum \left(\sqrt{n+1}-\sqrt{n}\right)^\alpha $ converge si et seulement si $\sum \frac{1}{2^\alpha n^{\alpha/2}}$ converge, c'est-à-dire, d'après les séries de Riemann, si et seulement si $\frac{\alpha}{2}>1$, soit $\alpha>2$.

\textbf{Bilan} : la deuxième série converge si et seulement si $\alpha>2$.
\trait[0.2]
Pour la troisième, si $\alpha<0$, $\exp\left(-\ln(n)^\alpha\right)\tendversen{n\to +\infty} 1$ et si $\alpha=0$, $\exp\left(-\ln(n)^\alpha\right)\tendversen{n\to+\infty} \frac{1}{\E}$. Dans les deux cas, la limite est différente de $0$ et la série est donc grosisèrement divergente.

Si $\alpha>0$, on remarque que
\begin{align*}
  n^2 \exp(-\ln(n)^\alpha) &= \eu{2\ln(n)-\ln(n)^{\alpha}}\\
  &= \eu{\ln(n)\left(2-\ln(n)^{\alpha-1}\right)} \tendversen{n\to +\infty} 0 \text{ si } \alpha>1 \\
  n\exp(-\ln(n)^\alpha) &= \eu{\ln(n)\left(2-\ln(n)^{\alpha-1}\right)}  \tendversen{n\to +\infty} +\infty \text{ si } \alpha< 1\\
  \exp(-\ln(n)) &= \frac{1}{n}
\end{align*}
On a donc démontré que :
\begin{itemize}
  \item Si $\alpha>1$, $\exp(-\ln(n)^\alpha)=\petito[+\infty]{\frac{1}{n^2}}$, et $\sum \frac{1}{n^2}$ est une série convergente,
  \item si $\alpha=1$, $\exp(-\ln(n)^\alpha)=\frac{1}{n}$, série divergente,
  \item si $\alpha<1$, $\frac{1}{n}=\petito[+\infty]{\exp(-\ln(n)^\alpha)}$,  et $\sum \frac1n$ est une série divergente.
\end{itemize}
\textbf{Bilan} : par comparaison de séries à terme positif, on peut en déduire que la série est convergente si et seulement si $\alpha>1$.
\trait[0.2]
Pour la quatrième, si $-1\leq\alpha\leq 1$, $\alpha^{2n}\tendversen{n\to +\infty} 0$ et donc \[ \frac{\alpha^n}{1+\alpha^{2n}} \eq{+\infty} \alpha^n. \]
Par équivalence de séries à terme positif, la série converge si et seulement si $\alpha \in \interoo{-1 1}$.

Si $\alpha>1$, alors $\alpha^n \tendversen{n\to+\infty} +\infty$. Par équivalence,
\[ \frac{\alpha^n}{1+\alpha^{2n}} \eq{+\infty} \frac{\alpha^n}{\alpha^{2n}} = \frac{1}{\alpha^n}=\left(\frac{1}{\alpha}\right)^n. \]
Puisque $\alpha>1$, la série $\sum \left(\frac1\alpha\right)^n$ converge et par théorème de comparaison de séries à terme positif, notre série converge.

Enfin, si $\alpha<-1$, on peut majorer :
\[ \left| \frac{\alpha^n}{1+\alpha^{2n}}\right| \leq \frac{|\alpha|^n}{1+2\alpha^{2n}} \]
et on applique le résultat précédent.

\textbf{Bilan} : la quatrième série converge pour toute valeur de $\alpha$.
\trait[0.2]
On écrit tout d'abord
\[ \frac{1}{\alpha^{\ln(n)}} = \frac{1}{\eu{\ln(n)\ln(\alpha)}} = \frac{1}{n^{\ln(\alpha)}}.\]
On peut alors appliquer le critère des séries de Riemann : la série $\sum \frac{1}{n^{\ln(\alpha)}}$ converge si et seulement si $\ln(\alpha)>1$, c'est-à-dire $\alpha > \E$.

\text{Bilan} : la série $\sum \frac{1}{\alpha^{\ln(n)}}$ converge si et seulement si $\alpha>\E$.
\trait[0.2]
On écrit tout d'abord, pour $n\geq 1$ :
\[ \frac{n^\alpha}{1+n^\alpha} = \frac{1}{1+\frac{1}{n^\alpha}} \]
et donc
\begin{align*}
  1-\left(\frac{n^\alpha}{1+n^\alpha}\right)^n &= 1-\eu{n\ln\left(\frac{1}{1+\frac{1}{n^\alpha}}\right)} \\
  &= 1-\eu{- n \ln\left(1+\frac{1}{n^\alpha}\right)}
\end{align*}
Si $\alpha\leq 0$, par composée, $1-\left(\frac{n^\alpha}{1+n^\alpha}\right)^n \tendversen{n\to +\infty} 1$. La série diverge donc grossièrement.

Si $\alpha >0$, $\frac{1}{n^\alpha}\tendversen{n\to +\infty} 0$ et par développement asymptotique :
\begin{align*}
  1-\left(\frac{n^\alpha}{1+n^\alpha}\right)^n  &= 1-\eu{-n\left(\frac{1}{n^\alpha}+\petito{\frac{1}{n^\alpha}}\right)}\\
  &= 1-\eu{-\frac{1}{n^{\alpha-1}}+\petito{\frac{1}{n^{\alpha-1}}}}\\
  &= 1-\left(1-\frac{1}{n^{\alpha-1}} + \petito{\frac{1}{n^{\alpha-1}}}\right) \\
  &\eq{+\infty} \frac{1}{n^{\alpha-1}}
\end{align*}
et par exponentiation :
\begin{align*}
\sqrt{1-\left(\frac{n^\alpha}{1+n^\alpha}\right)^n} &\eq{+\infty} \frac{1}{n^{\frac{\alpha-1}{2}}}.
\end{align*}
Les séries sont à termes positifs, et par critère de Riemann, $\sum \frac{1}{n^{\frac{\alpha-1}{2}}}$ converge si et seulement si $\frac{\alpha-1}{2}>1$, c'est-à-dire $\alpha>3$.

\textbf{Bilan} : la série $\sum \sqrt{1-\left(\frac{n^\alpha}{1+n^\alpha}\right)^n}$ converge si et seulement si $\alpha>3$.
\end{correction}
%%% Fin exercice %%%
