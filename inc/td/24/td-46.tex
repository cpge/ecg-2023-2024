%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{20}{3}[Règle de Raabe-Duhamel]
  Soit $\left(u_{n}\right)$ une suite réelle à termes strictement positifs. On suppose qu'il existe deux réels $\alpha$ et  $c$ tels que
  \[
  \frac{u_{n+1}}{u_{n}} \underset{+\infty}{=} 1-\frac{\alpha}{n}+\frac{c}{n^{2}}+o\left(\frac{1}{n^{2}}\right) .
  \]
  \begin{enumerate}
  \item Pour tout $n \in \N*$, posons $v_{n}=\ln \left(n^{\alpha} u_{n}\right)$. Montrer que $n^{3 / 2}\left(v_{n+1}-v_{n}\right) \underset{n \rightarrow+\infty}{\longrightarrow} 0$.
  \item En déduire la nature de la série $\sum\limits_{n \in \N}\left(v_{n+1}-v_{n}\right)$.
  \item  En déduire qu'il existe $\lambda>0$ tel que $u_{n} \eq{+\infty} \frac{\lambda}{n^{\alpha}}$.
  \item Application : soient $x$ et $y$ deux réels strictement positifs. On considère la suite $\left(u_{n}\right)_{n \in \N}$ définie par $u_{0}>0$ et, pour tout $n \in \N,(n+y) u_{n+1}=(n+x) u_{n}$. Déterminer la nature de $\sum u_{n}$.
  \item Application : soit $x \in \mathbb{R}_{+}^{*}$. Déterminer la nature de $\sum_{n \geqslant 1} x^{H_{n}}$ où $\left(H_{n}\right)_{n \in \N^{*}}$ désigne la série harmonique.
\end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item Soit $n\in \N*$. Calculons :
	\begin{align*}
	 n^{3/2}\left(v_{n+1}-v_n\right) &= n^{\frac32}\left(\ln((n+1)^\alpha u_{n+1})-\ln(n^\alpha u_n\right)\\
	 &= n^{3/2}\ln\left( \frac{(n+1)^{\alpha}u_{n+1}}{n^\alpha u_n} \right)\\
	 &= n^{3/2}\ln\left( \left(1+\frac{1}{n}\right)^\alpha \frac{u_{n+1}}{u_n} \right) 
	\end{align*}
	Utilisons l'hypothèse et effectuons un développement asymptotique :
	\begin{align*}
		n^{3/2}\left(v_{n+1}-v_n\right) &= n^{3/2}\ln\left( \left(1+\frac{\alpha}{n}+\frac{\alpha(\alpha-1)}{2n^2}+\petito{\frac{1}{n^2}} \right) \left( 1-\frac{\alpha}{n}+\frac{c}{n^{2}}+\petito{\frac{1}{n^{2}}}\right) \right)\\
		&= n^{3/2}\ln\left(1  + \left(c-\frac{\alpha(\alpha+1)}{2}\right)\frac{1}{n^2}+ \petito{\frac{1}{n^2}}\right)\\
		&= \left(c-\frac{\alpha(\alpha-1)}{2}\right)\frac{1}{n^{1/2}} + \petito{\frac{1}{n^{1/2}}} \tendversen{n\to +\infty} 0.
	\end{align*}
	Ainsi \[ \boxed{\lim_{n\to +\infty} n^{3/2}\left(v_{n+1}-v_n\right) = 0. } \]
	\item On en déduit, par continuité de la valeur absolue, d'après ce qui précède, que 
	\[ |v_{n+1}-v_n| = \petito{\frac{1}{n^{3/2}}}.\]
	La série $\ds \sum_{n\geq 1} \frac{1}{n^{3/2}}$ est convergente (série de Riemann avec $\frac32>1$). Par comparaison de séries à termes positifs, on en déduit que $\sum |v_{n+1}-v_n|$ est convergente. Ainsi \[ \boxed{\sum (v_{n+1}-v_n) \text{ est absolument convergente.}}\]
	\item Notons $(S_n)$ la suite des sommes partielles de la série précédente : pour tout $n$, $\ds S_n = \sum_{k=0}^n v_{k+1}-v_k$. Par télescopage :
		\[ S_n = v_{n+1} - v_0. \]
		Puisque la série converge, $(S_n)$ converge, on en déduit que la suite $(v_{n+1})$ et donc $(v_n)$ est également convergente. Il existe $\ell \in \R$ tel que $v_n\tendversen{n\to +\infty} \ell$. \\Par continuité de la fonction exponentielle, on en déduit que 
	\[ n^\alpha u_n = \exp{v_n} \tendversen{n\to +\infty} \eu{\ell} > 0 \]
	soit finalement 
	\[ \boxed{ u_n \eq{+\infty} \frac{\eu{\ell}}{n^\alpha}=\frac{\lambda}{n^\alpha}.}\]
	\item Essayons d'appliquer ce qui précède. Par une récurrence rapide, on constate, puisque $x$ et $y$ sont strictement positifs, que la suite $(u_n)$ est à termes strictement positifs. Mais alors :
	\begin{align*}
	  \frac{u_{n+1}}{u_n} &= \frac{n+x}{n+y} 
	  = \frac{1+\frac{x}{n}}{1+\frac{y}{n}} \\
	  &= \left(1+\frac{x}{n}\right) \left( 1 - \frac{y}{n}+\frac{y^2}{n^2}+\petito{\frac{1}{n^2}}\right)	\\
	  &= 1 - \frac{y-x}{n} + \frac{y^2-xy}{n^2}+\petito{\frac{1}{n^2}}
	\end{align*}
	D'après la règle de Raabe-Duhamel, il existe $\lambda >0$ tel que 
	\[ \boxed{u_n \eq{+\infty} \frac{\lambda}{n^{y-x}}.}\]
	Par comparaison de séries à termes positifs :
	\begin{itemize}
		\item Si $y-x>1$, c'est-à-dire si $y>x+1$, la série $\sum u_n$ converge.
		\item Si $y-x\leq 1$, c'est-à-dire si $y\leq x+1$, la série $\sum u_n$ diverge.
	\end{itemize}
	\item Notons, pour tout $n\in \N*$, \[ u_n = x^{H_n} = x^{\sum\limits_{k=1}^n \frac{1}{k}}. \]
	Puisque $x\in \R>$, $u$ est à terme strictement positifs. Calculons alors :
	\begin{align*}
	 \frac{u_{n+1}}{u_n} &= \frac{x^{H_{n+1}}}{x^{H_n}} = x^{H_{n+1}-H_n}\\
	 &= x^{\frac{1}{n+1}}= \eu{\frac{1}{n+1}\ln(x)}\\
	 &= 1 + \frac{\ln(x)}{n+1} + \frac{\ln^2(x)}{2(n+1)^2} +\petito{\frac{1}{n^2}}	
	\end{align*}
	D'après la règle de Raabe-Duhamel (qu'on pourrait, de manière rigoureuse, appliquer à $(u_{n-1})$ et non $(u_n)$), il existe $\lambda>0$ tel que 
	\[ \boxed{u_n \eq{+\infty} \frac{\lambda}{n^{-\ln(x)}} = \lambda x^{\ln(n)}.} \] 
	Si $x<1$, alors puisque pour $n\geq 1$, $\ln(n)\leq n$, $|x|^{\ln(n)}\leq |x|^n$. Par comparaison à une série géométrique convergente, la série $\ds \sum u_n$ converge.
	
	Si $x\geq 1$, la série diverge grossièrement.
	
	Ainsi, 
	\[ \boxed{\sum u_n \text{ converge si et seulement si } x\in \interoo{-1 1}.}\]
\end{enumerate}
\end{correction}
%%% Fin exercice %%%
