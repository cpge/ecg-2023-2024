%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{25}{3}[Sur la série harmonique]
  \everymath{\ds}
  On pose, pour tout $n\geq 1$, $\ds{H_{n}=\sum_{k=1}^n \frac{1}{k}}$.
\begin{enumerate}
  \item En encadrant $t\mapsto \frac{1}{t}$ sur $\interff{k k+1}$, prouver que $H_{n} \eq{+\infty} \ln n$.
  \item On pose $u_{n}=H_{n}-\ln n$, et $v_{n}=u_{n+1}-u_{n}$. Étudier la nature de la série $\sum_{n} v_{n}$. En déduire que la suite $\left(u_{n}\right)$ est convergente. On notera $\gamma$ sa limite\footnote{Cette constante est appelée constante d'Euler.}.
  \item Soit $R_{n}=\sum_{k=n}^{+\infty} \frac{1}{k^{2}}$. Donner un équivalent de $R_{n}$.
  \item Soit $w_{n}$ tel que $H_{n}=\ln n+\gamma+w_{n}$, et soit $t_{n}=w_{n+1}-w_{n}$. Donner un équivalent du reste
  $\sum_{k \geq n} t_{k}$. En déduire que $H_{n}=\ln n+\gamma+\frac{1}{2 n}+o\left(\frac{1}{n}\right)$.
\end{enumerate}
\everymath{}
\end{exoApp}

\begin{correction}
\begin{enumerate}
  \item Puisque $t\donne \frac1t$ est décroissante sur $\R>$, donc pour  $t\in \interff{k k+1}$, on a \[ \frac{1}{k+1} \leq \frac{1}{t}\leq \frac{1}{k}. \]
  On intègre entre $k$ et $k+1$. Par croissance de l'intégrale (ou inégalité de la moyenne), on a
  \[ \int_k^{k+1} \frac{1}{k+1}\D t \leq \int_k^{k+1} \frac{1}{t}\d t \leq \int_k^{k+1} \frac{1}{k}\D t \]
  soit
  \[ \frac{1}{k+1} \leq \ln(k+1)-\ln(k) \leq \frac{1}{k}. \]
  En sommant,
  \begin{align*}
    & \sum_{k=1}^{n} \frac{1}{k+1} \leq \sum_{k=1}^n (\ln(k+1)-\ln(k)) \leq \sum_{k=1}^n \frac{1}{k}\\
    \text{soit par telescopage}\quad & H_{n+1}-1 \leq  \ln(n+1) \leq H_n \\
    \text{ou encore}\quad & H_n + \frac{1}{n+1} -1\leq \ln(n+1) \leq H_n \\
    \text{et finalement}\quad &      \ln(n+1) \leq H_n \leq \ln(n+1)+1-\frac{1}{n+1}
  \end{align*}
  Soit, en divisant par $\ln(n)$,
  \[ \frac{\ln(n+1)}{\ln(n)} \leq \frac{H_n}{\ln(n)} \leq \frac{\ln(n+1)}{\ln(n)} + \frac{1}{\ln(n)}-\frac{1}{(n+1)\ln(n)}. \]
  Par quotient, les deux termes extrèmes tendent vers $1$. Par encadrement, $\frac{H_n}{\ln(n)}\tendversen{n\to +\infty} 1$, soit \[ \boxed{H_n \eq{+\infty} \ln(n).}\]
  \item Déterminons un équivalent de $(v_n)$ :
  \begin{align*}
    v_n &= H_{n+1}-\ln(n+1) -\left(H_n-\ln(n)\right) \\
    &= H_{n+1}-H_n - \left(\ln(n+1)-\ln(n)\right) \\
    &= \frac{1}{n+1}-\ln\left(\frac{n+1}{n}\right) = \frac{1}{n+1}-\ln\left(1+\frac{1}{n}\right).
  \end{align*}
  Puisque $\frac{1}{n}\tendversen{n\to +\infty} 0$, on a :
  \begin{align*}
    v_n &= \frac{1}{n+1}-\left( \frac{1}{n}-\frac{1}{2n^2}+\petito[+\infty]{\frac{1}{n^2}} \right) \\
    &= \frac{2n^2-2n(n+1)+(n+1)}{2n^2(n+1)} + \petito[+\infty]{\frac{1}{n^2}}\\
    &=  \underbrace{\frac{-n+1}{2n^2(n+1)}}_{\eq{+\infty} -\frac{1}{2n^2}} + \petito[+\infty]{\frac{1}{n^2}}
  \end{align*}
  donc $v_n\eq{+\infty} -\frac{1}{2n^2}$.

  Puisque la série $\sum\limits_{n\geq 1} -\frac{1}{2n^2}$ converge (Riemann, avec $2>1$) et que les suites sont à termes négatifs, par comparaisons de suites, on peut en déduire que la série $\sum\limits_{n\geq 1} v_n$ converge.

  Or, remarquons que la suite partielle de la série $(v_n)$ se télescope :
  \[ \sum_{k=1}^n v_k = \sum_{k=1}^n u_{k+1}-u_k = u_{n+1}-u_1. \]
  Puisque la série $ \sum\limits_{n\geq 1} v_n$ converge, la suite $(u_{n+1}-u_1)$ converge, et donc $(u_n)$ converge également.
  \item On remarque que le reste a un sens, puisque la série $\sum\limits_{n\geq 1} \frac{1}{n^2}$ converge.

  On s'inspire de la question $1$. On va encadrer $t\donne \frac{1}{t^2}$ sur $\interff{k k+1}$, intégrer puis sommer. Par décroissance de la fonction $t\donne \frac{1}{t^2}$ sur $\interff{k k+1}$ (pour $k\geq 1$), \[ \frac{1}{(k+1)^2}\leq \frac{1}{t^2} \leq \frac{1}{k^2}. \]
  soit par inégalité de la moyenne sur $\interff{k k+1}$ :
  \[ \frac{1}{(k+1)^2} \leq \int_k^{k+1} \frac{1}{t^2}\D t\leq \frac{1}{k^2} \]
  ou encore
  \[ \frac{1}{(k+1)^2} \leq \frac{1}{k}-\frac{1}{k+1} \leq \frac{1}{k^2}. \]
  En sommant entre $n$ et $p$ :
  \[ \sum_{k=n}^p \frac{1}{(k+1)^2} \leq \sum_{k=n}^p \frac{1}{k}-\frac{1}{k+1} \leq \sum_{k=n}^p \frac{1}{k^2}. \]
  et par télescopage
  \[ \sum_{k=n}^p \frac{1}{(k+1)^2} \leq \frac{1}{n}-\frac{1}{p+1} \leq \sum_{k=n}^p \frac{1}{k^2}.\]
  On peut passer à la limite (le reste converge) :
  \[ \sum_{k=n}^{+\infty} \frac{1}{(k+1)^2} \leq \frac{1}{n} \leq \sum_{k=n}^{+\infty} \frac{1}{k^2} \]
  et finalement
  \[ R_{n+1} \leq \frac{1}{n} \leq R_n\text{ ou encore }  \frac{1}{n}\leq R_n \leq \frac{1}{n} +\frac{1}{n^2}. \]
  En multipliant par $n$ :
  \[ 1\leq nR_n\leq 1+\frac{1}{n}\tendversen{n\to +\infty} 1. \]
  Ainsi, par encadrement, $nR_n\tendversen{n\to +\infty} 1$ et donc
  \[ \boxed{R_n\eq{+\infty} \frac{1}{n}.}\]
   \item On réfléchit comme en $2$ :
   \begin{align*}
     t_n &=\left (H_{n+1}-\ln(n+1)-\gamma\right) - \left( H_n-\ln(n)-\gamma\right) \\
        &= H_{n+1}-H_n - \ln\left(1+\frac{1}{n}\right)\\
        &\eq{+\infty} - \frac{1}{2n^2} \eq{+\infty} -\frac{1}{2n^2}
   \end{align*}
Mais alors, comme nous l'avons vu en $2$, $t_n\eq{+\infty} -\frac{1}{2n^2}$.

Il reste alors à utiliser le résultat suivant : si $t_n\eq{+\infty} -\frac{1}{2n^2}$, alors $\sum\limits_{k=n}^{\infty} t_k \eq{+\infty} \sum\limits_{k=n}^{+\infty} -\frac{1}{2k^2}$. Et en utilisant le résultat de la question $3$,
\[ \sum_{k=n}^{+\infty} t_k \eq{+\infty} -\frac{1}{2n}. \]
Par télescopage, \[ \sum_{k=n}^{+\infty} t_k = \left(\lim_{n\to +\infty} w_n\right) - w_n.\]
Or, par définition de $\gamma$, $w_n\tendversen{n\to +\infty} 0$. Finalement
\[ -w_n \eq{+\infty} -\frac{1}{2n} \text{ soit } w_n\eq{+\infty} \frac{1}{2n} \]
c'est-à-dire $w_n=\frac{1}{2n}+\petito[+\infty]{\frac{1}{2n}} = \frac{1}{2n}+\petito[+\infty]{\frac{1}{n}}$.

En utilisant la définition de $w_n$, on peut alors écrire :
\[ \boxed{H_n = \ln(n)+\gamma + \frac{1}{2n}+\petito[+\infty]{\frac{1}{n}}.}\]
\end{enumerate}

\begin{remarque}
  Le résultat de la question $4$ serait à démontrer : si $a_n\eq{+\infty} b_n$, à termes positifs (ou négatifs) et si les séries $\sum a_n$ et $\sum b_n$ convergent, alors les restes $R_n=\sum\limits_{k=n}^{+\infty} a_k$ et $S_n=\sum\limits_{k=n}^{+\infty} b_k$ sont équivalents.
\end{remarque}

\preuve{Pour cela, on écrit $a_n=b_n+\petito[+\infty]{b_n}$. Ainsi, pour tout $\eps > 0$, il existe un rang $N$ tel que, pour tout $n\geq N$, on ait
  \[ \left|\frac{a_n-b_n}{b_n}\right| \leq \eps \]
  (c'est la définition de $\petito[+\infty]{b_n}$). On peut écrire alors
  \[ | a_n-b_n | \leq \eps |b_n|. \]
  Mais alors, par inégalité triangulaire, pour $n\geq N$ :
  \begin{align*}
    \left| R_n-S_n\right| &= \left| \sum_{k=n}^{+\infty} a_k-b_k\right|
    \leq \sum_{k=n}^{+\infty} |a_k-b_k| \\
    &\leq \sum_{k=n}^{+\infty} \eps |b_k|
    \leq \eps \sum_{k=n}^{+\infty} |b_k| = \eps S_n.
  \end{align*}
On a ainsi montré que $R_n-S_n = \petito[+\infty]{S_n}$, c'est-à-dire $R_n\eq{+\infty}S_n$. }
\begin{attention}
  Bien sûr, ce résultat n'est pas au programme.
\end{attention}

\end{correction}
%%% Fin exercice %%%
