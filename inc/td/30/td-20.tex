%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{30}{2}[Des pièces au hasard]
Soit $p\in \interoo{0 1}$ et $N\in \N\setminus \{0,1\}$. On dispose de $N$ pièces de monnaie non équilibrées, chacune des pièces amenant un Pile avec probabilité $p$. On lance les $N$ pièces de monnaie. Un joueur, yeux bandés, qui n'a pas assisté au lancer, choisis au hasard $n$ pièces parmi les $N$ (avec $n\in \interent{1 N-1}$). Il gagne celles choisies qui sont tombées sur Pile. 

On note $X$ le nombre de Piles obtenues parmi les $N$ pièces, et $Y$ le nombre de pièces gagnées par le joueur.
\begin{enumerate}
	\item Déterminer la loi de $X$.
	\item Justifier que $Y(\Omega)=\interent{0 n}$.
	\item Déterminer, pour tout $k\in \interent{0 N}$, et $j\in \interent{0 n}$, la valeur de $\PP_{[X=k]}(Y=j)$. 
	\item En déduire la loi du couple $(X,Y)$. On pourra montrer que 
	\[ \binom{k}{j}\binom{N-k}{n-j}\binom{N}{k} = \binom{n}{j} \binom{N-n}{k-j} \binom{N}{n}. \]
	\item Déterminer alors la loi de $Y$ et justifier que $Y$ suit une loi usuelle.
\end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item On dispose d'une épreuve de Bernoulli, de succès \guill{obtenir Pile}, de probabilité $p$. On répète $N$ fois cette épreuve de manière successive et indépendante. $X$, qui compte le nombre de succès, suit donc une loi binomiale de paramètre $N$ et $p$: $\boxed{X\hookrightarrow \mathcal{B}(N,p).}$.\vspace*{.1cm}
	\item $Y$ compte, parmi les $n$ pièces choisies, le nombre de pièce tombées sur Pile. Ainsi, il s'agit d'un entier compris entre $0$ (aucune pièce n'est tombée sur Pile) et $n$ (toutes les pièces sont tombées sur Pile). Ainsi $\boxed{Y(\Omega)=\interent{0 n}.}$.
	\item Tout d'abord, si $j>k$, il est impossible d'avoir choisi $j$ pièces ayant fait Pile sachant qu'il y a $k$ pièces tombées sur Pile. Donc
		\[ \boxed{\forall j> k,\quad \PP_{[X=k]}(Y=j) = 0.} \]
	 Si $j\leq k$ : on suppose qu'on a pris $n$ pièces et $k$ pièces sur les $N$ sont sur Pile. Pour obtenir $j$ pièces avec Pile, on doit choisir $j$ pièces parmi les $k$ pièces tombées sur Pile, et les $n-j$ pièces restantes parmi les $N-k$ pièces tombées sur Face. Pour cela, il faut que $n-j\leq N-k$ c'est-à-dire $j\geq k+n-N$, sinon la probabilité est nulle. Tous les tirages étant équiprobables :
	 \begin{align*}
	 \Aboxed{\PP_{[X=k]}(Y=j) &= \frac{\binom{k}{j} \binom{N-k}{n-j}}{ \binom{N}{n}}	.}
	 \end{align*}
	 \item Soient $k\in \interent{0 N}$ et $j\in \interent{0 n}$. Par théorème :
	 \begin{align*}
	 \PP(X=k, Y=j) &= \PP(X=k) \times \PP_{[X=k]}(Y=j).	
	 \end{align*}
 	Si $j>k$, ou $j<k+n-N$, $\PP(X=k, Y=j)=0$. Si $j \leq k$, en utilisant l'indication qu'on démontre en dessous :
 	\begin{align*}
 		\PP(X=k,Y=j) &= \binom{N}{k}p^k(1-p)^{N-k} \frac{\binom{k}{j}\binom{N-k}{n-j}}{\binom{N}{n}}\\
 		&= p^k(1-p)^{N-k} \frac{\binom{n}{j} \binom{N-n}{k-j} \binom{N}{n}}{\binom{N}{n}}\\
 		&= \binom{n}{j}\binom{N-n}{k-j} p^k(1-p)^{N-k}.
 	\end{align*}
	Ainsi,
	\[ \boxed{\forall (k,j)\in \interent{0 N}\times \interent{0 n},\quad \PP(X=k,Y=j) = \left \{ \begin{array}{lll} 0&\text{si}& j>k\text{ ou } j< k+n-N\\\binom{n}{j}\binom{N-n}{k-j}p^k(1-p)^{N-k}&\text{sinon}&\end{array}\right.}\]
	Démontrons le résultat proposé :
	\begin{align*}
	\binom{k}{j}\binom{N-k}{n-j}\binom{N}{k} &= \frac{k!}{j!(k-j)!} \frac{(N-k)!}{(n-j)! (N-k-(n-j))!} \frac{N!}{k!(N-k)!}	\\
	&= \frac{N!}{j! (k-j)! (n-j)! (N-k-n+j)!}
	\end{align*}
	de même :
	\begin{align*}
	\binom{n}{j}\binom{N-n}{k-j}\binom{N}{n}&=\frac{n!}{j!(n-j)!}\frac{(N-n)!}{(k-j)!(N-n-(k-j))!} \frac{N!}{n!(N-n)!}\\
	&= \frac{N!}{j!(n-j)!(k-j)!(N-n-k+j)}	
	\end{align*}
	On obtient bien le même résultat.
	\item Utilisons les lois marginales. Soit $j \in \interent{0 n}$. 
	\begin{align*}
	\PP(Y=j) &= \sum_{k=0}^N \PP(X=k, Y=j) \\
	&= \sum_{k=j}^{N-n+j} \binom{n}{j}\binom{N-n}{k-j}p^k(1-p)^{N-k}\\
	&= \binom{n}{j} \sum_{k=j}^{N-n+j} \binom{N-n}{k-j} p^k (1-p)^{N-k}. 
	\end{align*}
	Posons le changement d'indice $m = k-j$.
	\begin{align*}
	\PP(Y=j) &= \binom{n}{j}	\sum_{m=0}^{N-n} \binom{N-n}{m} p^{m+j}(1-p)^{N-(m+j)}\\
	&= \binom{n}{j} p^j\sum_{m=0}^{N-n} \binom{N-n}{m}p^m(1-p)^{N-j-m}\\
	&= \binom{n}{j} p^j (1-p)^{n-j}\underbrace{\sum_{m=0}^{N-N} \binom{N-n}{m} p^m (1-p)^{N-n-m}}_{= (p+(1-p))^{N-n}}\\
	&= \binom{n}{j}p^j(1-p)^{n-j}.
	\end{align*}
	Ainsi,
	\[ \boxed{ Y \suit \mathcal{B}(n, p).}\]

 \end{enumerate}
\end{correction}
%%% Fin exercice %%%
