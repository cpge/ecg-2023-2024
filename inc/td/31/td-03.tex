%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{15}{2}[Intégrales de Bertrand]
  \everymath{\ds}
\begin{enumerate}
  \item Montrer que l'intégrale $\int_2^{+\infty} \frac{\D t}{x^\alpha \left(\ln x\right)^\beta}$ converge si et seulement si $\alpha >1$ ou bien ($\alpha =1$ et $\beta >1$).
  \item Montrer que l'intégrale $\int_0^{1/2} \frac{\D t}{x^\alpha \left|\ln x\right|^\beta}$ converge si et seulement si $\alpha <1$ ou bien ($\alpha =1$ et $\beta >1$).
\end{enumerate}
\everymath{}
\end{exoApp}

\begin{correction}
Constatons tout d'abord que les fonctions $x\donne \frac{1}{x^\alpha\left(\ln x\right)^\beta}$ et $x\donne \frac{1}{x^\alpha\left|\ln x\right|^\beta}$ sont continues sur $\R>$. 
\begin{enumerate}
	\item On va raisonner par disjonction de cas, en se ramenant à une intégrale de Riemann, avec la règle du \guill{$\frac{\alpha+1}{2}$}.
\begin{itemize}
	\item Si $\alpha>1$. Remarquons que :
\begin{align*}
x^{\frac{\alpha+1}{2}} \frac{1}{x^\alpha \left(\ln x\right)^\beta} &= \frac{1}{x^{\frac{\alpha-1}{2}} \left(\ln x\right)^\beta} 
\end{align*}
Si $\beta \geq 0$, par produit et quotient 
\[ \frac{1}{x^{\frac{\alpha-1}{2}} \left(\ln x\right)^\beta}\tendversen{x\to +\infty} 0.\]
Si $\beta <0$, par croissance comparée, puisque $\frac{\alpha-1}{2}>0$ :
\[ \frac{1}{x^{\frac{\alpha-1}{2}} \left(\ln x\right)^\beta} = \frac{\left(\ln x\right)^{-\beta}}{x^{\frac{\alpha-1}{2}} } \tendversen{x\to +\infty} 0.\]
Dans tous les cas 
\[ \frac{1}{x^{\frac{\alpha-1}{2}}\left(\ln x\right)^\beta} \tendversen{x\to +\infty} 0 \]
et donc
\[ \frac{1}{x^\alpha\left(\ln x\right)^\beta} = \petito[+\infty]{\frac{1}{x^\frac{\alpha+1}{2}}.} \]
Puisque $\frac{\alpha+1}{2}>1$, l'intégrale $\int_2^{+\infty} \frac{\dd x}{x^{\frac{\alpha+1}{2}}}$ converge (Riemann). Par comparaison d'intégrales de fonctions positives (ce qui est bien le cas sur $\interfo{2 +\infty}$), on en déduit que \[ \boxed{\int_2^{+\infty} \frac{\dd x}{x^\alpha \left(\ln x\right)^\beta} \text{ converge}}.\]
\item Si $\alpha=1$, on peut calculer l'intégrale partielle. Si $\beta \neq 1$, soit $t > 2$. Alors 
\begin{align*}
\int_2^t \frac{\dd x}{x \left(\ln x\right)^\beta} &= \int_2^t \frac{1}{x}\left(\ln x\right)^{-\beta}\dd x\\
&= \left[ \frac{\left(\ln x\right)^{-\beta+1}}{-\beta +1}\right]_2^t \\
&= \frac{1}{1-\beta} \left(\left(\ln t\right)^{1-\beta}-\left(\ln 2\right)^{1-\beta}\right).
\end{align*}
Ceci admet une limite finie en $+\infty$ si et seulement si $1-\beta <0$, c'est-à-dire si et seulement si $\beta>1$.

Enfin, si $\beta=1$, avec $t>2$ :
\begin{align*}
\int_2^t \frac{\dd x}{x\ln x} &= \left[ \ln(\ln(x))\right]_2^t \tendversen{t\to +\infty} +\infty. 	
\end{align*}
Ainsi, \fbox{si $\alpha=1$, l'intégrale converge si et seulement si $\beta>1$.}
\item Enfin, si $\alpha<1$, remarquons que 
\begin{align*}
x^{\frac{\alpha+1}{2}} \frac{1}{x^\alpha \left(\ln x\right)^\beta} &= \frac{1}{x^{\frac{\alpha-1}{2}} \left(\ln x\right)^\beta} = \frac{x^{\frac{1-\alpha}{2}}}{(\ln x)^\beta} 
\end{align*}
Si $\beta>0$, cela tend vers $+\infty$ par croissances comparées; si $\beta\leq 0$, cela tend vers $+\infty$ par quotient.

Ainsi,
\[ \frac{1}{x^\frac{\alpha+1}{2}} = \petito[+\infty]{\frac{1}{x^\alpha (\ln x)^\beta}}. \]
Les fonctions étant positives, et l'intégrale $\ds\int_2^{+\infty} \frac{\dd x}{x^\frac{\alpha+1}{2}}$ étant divergente (Riemann avec $\frac{\alpha+1}{2}<1$), on en déduit par comparaison d'intégrales de fonctions positives, on en déduit que 
\[ \boxed{\int_2^{+\infty} \frac{\dd x}{x^\alpha \left(\ln x\right)^\beta} \text{ diverge}}.\]

\end{itemize}
On peut conclure : 
\[ \boxed{\int_2^{+\infty} \frac{\dd x}{x^\alpha \left(\ln x\right)^\beta} \text{ converge si et seulement si } \alpha >1 \text{ ou } \alpha=1 \text{ et } \beta >1}.\]
\item Le raisonnement est strictement identique. La fonction est positive sur $\interof{0 \frac12}$. 

 Si $\alpha\neq 1$, on a 
\begin{align*}
n^\frac{\alpha+1}{2} \frac{1}{x^\alpha|\ln x|^\beta} = \frac{x^{\frac{1-\alpha}{2}}}{|\ln x|^\beta}.
\end{align*}
\begin{itemize}
\item Si $\alpha<1$, ce terme tend vers $0$ par quotient si $\beta \geq 0$, par croissance comparée si $\beta<0$. Dans tous les cas 
\[ \frac{1}{x^\alpha|\ln x|^\beta} = \petito[0]{\frac{1}{x^\frac{\alpha+1}{2}}}. \]
Puisque $\frac{\alpha+1}{2}<1$, l'intégrale $\ds\int_0^{\frac12} \frac{\dd x}{x^{\frac{\alpha+1}{2}}}$ converge et par comparaison d'intégrales de fonctions positives, on en déduit que notre intégrale converge.
\item Si $\alpha>1$, cette fois-ci $\ds \frac{x^{\frac{1-\alpha}{2}}}{|\ln x|^\beta}\tendversen{x\to 0^+} +\infty$, par quotient si $\beta \leq 0$, par croissance comparée et quotient si $\beta >0$. Dans tous les cas, 
\[  \frac{1}{x^\frac{\alpha+1}{2}} = \petito[0]{\frac{1}{x^\alpha|\ln x|^\beta}}. \]
Puisque $\frac{\alpha+1}{2}>1$, l'intégrale $\ds\int_0^{\frac12} \frac{\dd x}{x^{\frac{\alpha+1}{2}}}$ diverge et par comparaison d'intégrales de fonctions positives, on en déduit que notre intégrale diverge.
\item Enfin, si $\alpha=1$, on peut calculer les intégrales partielles :
\begin{itemize}
	\item Si $\beta \neq 1$, $\ds\int_t^{\frac{1}{2}} \frac{\dd x}{x|\ln x|^\beta} = \left[ \frac{1}{1-\beta}(-\ln x)^{-\beta+1}\right]_t^\frac{1}{2}$, et ce terme admet une limite finie en $0$ si et seulement si $1-\beta <0$, c'est-à-dire $\beta>1$.
	\item Si $\beta=1$, $\ds\int_t^\frac{1}{2} \frac{\dd x}{x|\ln x|} = \left[ \ln(-\ln(x))\right]_t^\frac{1}{2} \tendversen{t\to 0^+} +\infty$.
\end{itemize}
On peut conclure : 
\[ \boxed{\int_0^{\frac12} \frac{\dd x}{x^\alpha \left(\ln x\right)^\beta} \text{ converge si et seulement si } \alpha <1 \text{ ou } \alpha=1 \text{ et } \beta >1}.\]
\end{itemize}

\end{enumerate}

\end{correction}
%%% Fin exercice %%%
