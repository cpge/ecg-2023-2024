%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{20}{2}[Une intégrale semi-convergente]
{  \onehalfspacing
\label{chap18exosin} Soit $f:\R^*\rightarrow \R$ définie par $f:t\mapsto \frac{\sin(t)}{t}$.
  \begin{enumerate}
  	\item Montrer que $f$ peut être prolongée par continuité en $0$.
  	\item Soient $0<a<b$ deux réels. A l'aide d'une intégration par parties, montrer que
  	\[ \int_a^b f(t)\dd t = 2\left [ \frac{\sin^2(t/2)}{t}\right]_a^b + \int_{a/2}^{b/2} \frac{\sin^2(t)}{t^2}\dd t \]
  	\textit{Indication : on utilisera la formule $\cos(t)=\cos(2\times t/2)=2\sin^2(t/2)-1$ et on posera un changement de variable $u=2t$ pour terminer.}
  	\item Montrer que $\ds{\int_0^{+\infty} \frac{\sin^2(t)}{t^2}\dd t}$ est absolument convergente.
  	\item En déduire que $\ds{\int_0^{+\infty} \frac{\sin(t)}{t}\dd t}$ est convergente.
  	\item Montrer que \[ \int_{n\pi}^{(n+1)\pi} \left|\frac{\sin(t)}{t}\right|\dd t \geq \frac{2}{(n+1)\pi} \]
  		En déduire que $\ds{\int_0^{+\infty} \frac{\sin(t)}{t}\dd t}$ n'est pas absolument convergente.
  \end{enumerate}
  }
\end{exoApp}

\begin{correction}
  \begin{enumerate}
  	\item $f$ est continue sur $\R[*]$ comme quotient de fonctions continues dont le dénominateur ne s'annule pas.\\ On a, naturellement (limite usuelle ou développement limité) que \[ \lim_{t\rightarrow 0} \frac{\sin(t)}{t} = 1 \]
  	En posant $f(0)=1$, la fonction $f$ est alors continue sur $\R$.
  	\item Soient $0<a<b$. Pour $t\in [a;b]$, on pose $u'(t)=\sin(t)$ et $v(t)=\dfrac{1}{t}$, c'est-à-dire $u:t\mapsto -\cos(t)$ et $v:t\mapsto\dfrac{1}{t}$. \\
  	 $u$ et $v$ sont de classe $\CC^1$ sur $[a;b]$. Ainsi, par changement de variables :
  		\begin{align*}
  			\int_a^b f(t)\dd t &= \left [ -\frac{\cos(t)}{t}\right]_a^b -\int_a^b \frac{\cos(t)}{t^2}\dd t \\
  			&= \left [ \frac{2\sin^2(t/2)-1}{t}\right]_a^b -\int_a^b \frac{\cos(t)}{t^2}\dd t \text{ car } \cos(t)=\cos(2t/2)=1-2\sin^2(t/2)\\
  			&= 2\left[ \frac{\sin^2(t)}{t}\right]_a^b - \left(\frac{1}{b}-\frac{1}{a}\right)-\int_a^b \frac{\cos(t)}{t^2}\dd t\\
  			&= 2\left[ \frac{\sin^2(t)}{t}\right]_a^b + \int_a^b \frac{1}{t^2}\dd t - \int_a^b \frac{\cos(t)}{t^2}\dd t\\
  			&= 2\left[ \frac{\sin^2(t)}{t}\right]_a^b + \int_a^b \frac{1-\cos(t)}{t^2} \dd t\\
  			&= 2\left[ \frac{\sin^2(t)}{t}\right]_a^b + \int_a^b \frac{2\sin^2(t/2)}{t^2}\dd t \text{ par la même formule qu'avant}\\
  			&= 2\left[ \frac{\sin^2(t)}{t}\right]_a^b + \int_{a/2}^{b/2} \frac{\sin^2(u)}{u^2}\dd u \text{ en posant $t=2u, \CC^1$ strictement croissant}
  		\end{align*}
  	\item La fonction $g:t\mapsto \frac{\sin^2(t)}{t^2}$ est continue sur $]0;+\infty[$, prolongeable par continuité en $0$ (car $g=f^2$ donc en posant $g(0)=1$). L'intégrale est donc impropre au voisinage de $+\infty$. Or, pour tout $t\geq 1$ :
  	\begin{align*}
  		\left| \frac{\sin^2(t)}{t^2}\right| \leq \frac{1}{t^2}
  	\end{align*}
  	La fonction $t\mapsto \dfrac{1}{t^2}$ est positive et d'intégrale convergente sur $[1;+\infty[$ (Riemann avec $2>1$). Par comparaison de fonctions positives, on en déduit que l'intégrale $\ds{\int_1^{+\infty} \left| \frac{\sin^2(t)}{t^2}\right|\dd t }$ converge, et donc que l'intégrale $\ds{\int_1^{+\infty} \frac{\sin^2(t)}{t^2}\dd t }$ converge également. Par continuité, on en déduit que \[ \int_0^{+\infty} \frac{\sin^2(t)}{t^2} \dd t \text{ converge}\]
  	\item En utilisant la formule obtenue en 2, on constate que l'intégrale du membre de droite converge quand $a$ tend vers $0$ et $b$ tend vers $+\infty$. Si le terme crochet converge, on pourra alors conclure que l'intégrale de $f$ converge sur $[0;+\infty[$.\\
  	 Or \[ \frac{\sin^2(a)}{a} = \sin(a) \frac{\sin(a)}{a} \stackrel{a\rightarrow 0}{\longrightarrow} 0 \]
  	 et \[ \left | \frac{\sin^2(b)}{b}\right| \leq \frac{1}{b} \stackrel{b\rightarrow +\infty}{\longrightarrow} 0 \]
  	 Donc le terme crochet tend vers $0$ et par conséquent \[\int_0^{+\infty} \frac{\sin(t)}{t}\dd t \text{ converge} \]
  	\item Remarquons que, pour $t\in [n\pi; (n+1)\pi]$, par décroissance de la fonction inverse :
  		\[ \frac{1}{(n+1)\pi} \leq \frac{1}{t} \leq \frac{1}{n\pi}\]
  		et donc
  		\begin{align*}
  			\int_{n\pi}^{(n+1)\pi} \left|\frac{\sin(t)}{t}\right|\dd t &\geq \frac{1}{(n+1)\pi} \int_{n\pi}^{(n+1)\pi} |\sin(t)|\dd t \\
  				&\geq \frac{1}{(n+1)\pi} \int_0^\pi \sin(t)\dd t \text{ par périodicité} \\
  				&\geq \frac{2}{(n+1)\pi}
  		\end{align*}
   Mais alors, pour tout $N\geq 1$ :
   \begin{align*}
   	\int_0^{N\pi} \left|\frac{\sin(t)}{t}	\right| \dd t &= \sum_{n=0}^{N-1}\int_{n\pi}^{(n+1)\pi} \left| \frac{\sin(t)}{t}\right| \dd t \\
   	&\geq \sum_{n=0}^{N-1} \frac{2}{(n+1)\pi} \\
   	&\geq \frac{2}{\pi}\sum_{k=1}^N \frac{1}{k} \stackrel{N\rightarrow +\infty}{\longrightarrow} +\infty
   \end{align*}
   Ainsi, l'intégrale $\ds{\int_0^{+\infty} \frac{\sin(t)}{t}\dd t}$ est convergente, mais pas absolument convergente.

  \end{enumerate}
\end{correction}
%%% Fin exercice %%%
