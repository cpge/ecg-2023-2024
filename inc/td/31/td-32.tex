%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{25}{3}[Une équivalence d'intégrales]
  {\onehalfspacing
Pour tout $n\in \N$, on pose \[ I_n=\int_0^{+\infty} \frac{\D x}{(1+x^3)^{n+1}}. \]
\begin{enumerate}
  \item Justifier que l'intégrale $I_n$ converge.
  \item Montrer qu'il existe $(a, b, c)\in \R^3$ tel que
  \[ \forall x\in \R+,\quad \frac{1}{1+x^3}=\frac{a}{1+x}+b\frac{2x-1}{x^2-x+1} + \frac{c}{1+\left(\frac{2x-1}{\sqrt{3}}\right)^2}. \]
  En déduire une primitive de $x\donne \dfrac{1}{1+x^3}$ sur $\R+$.
  \item Calculer $I_0$ et exprimer $I_{n+1}$ en fonction de $I_n$.
  \item Pour tout $n\in \N*$, on pose $u_n=\ln\left(\sqrt[3]{n}I_n\right)$. \'Etudier la nature de la série $\ds{\sum_{n\in \N*} (u_{n+1}-u_n)}$.
  \item En déduire qu'il existe un réel $\ell>0$ tel que $\ds{I_n \eq{} \frac{\ell}{\sqrt[3]{n}}}$.
\end{enumerate}}
\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item Pour tout $n\in \N$, la fonction $f_n:x\donne \frac{1}{(1+x^3)^{n+1}}$ est continue, et positive, sur $\R+$. L'intégrale est impropre en $+\infty$. Or, on a rapidement que 
	\[ f_n(x) \eq{x\to +\infty} \frac{1}{x^{3n+3}}. \]
	Puisque $n\geq 0$, $3n+3>1$ et l'intégrale $\ds\int_1^{+\infty} \frac{\dd x}{x^{3n+3}}$ est donc convergente (Riemann). Par comparaison d'intégrales de fonctions positives, on en déduit que  \underline{$I_n$ converge}.
	\item On met le membre de droite au même dénominateur : pour tout $x\in \R+$,
	\begin{align*}
		\frac{a}{1+x}+b\frac{2x-1}{x^2-x+1}+\frac{c}{1+\left(\frac{2x-1}{\sqrt{3}}\right)^2} &= \frac{a(x^2-x+1)+b(x+1)(2x-1)}{(1+x)(x^2-x+1)} + \frac{3c}{4x^2-4x+4}\\
	&= \frac{a(x^2-x+1)+b(x+1)(2x-1)}{(1+x)(x^2-x+1)} + \frac{\frac{3}{4}c}{x^2-x+1}\\
		&= \frac{(a+2b)x^2+\left(b-a+\frac{3c}{4}\right)x+a-b+\frac{3c}{4}}{x^3+1}
	\end{align*}
Par identification, on en déduit le système 
\begin{align*}
 \systeme{a+2b=0, b-a+\frac{3}{4}c=0, a-b+\frac{3}{4}c=1} &\iff \systeme{a+2b=0,3b+\frac34c=0,-3b+\frac34c=1}\\
 &\iff \systeme*{a=\frac13,b=-\frac16,c=\frac23}
 \end{align*}
 Mais alors, en prenant une primitive de chacune des trois fonctions obtenues, une primitive de $f_0$ est :
 \begin{align*}
 x\donne & a\ln |1+x| + b \ln |x^2-x+1| + c\frac{\sqrt{3}}{2} \arctan\left(\frac{2x-1}{\sqrt{3}}\right)
\end{align*}
Ainsi, une primitive de $x\donne \frac{1}{x^3}$ sur $\R+$ est 
\[ \boxed{x\donne \frac{1}{3}\ln(1+x)-\frac{1}{6}\ln(x^2-x+1)+\frac{1}{\sqrt{3}}\arctan\left(\frac{2x-1}{\sqrt{3}}\right)=\frac16\ln\left(\frac{(1+x)^2}{x^2-x+1}\right)+\frac{1}{\sqrt{3}}\arctan\left(\frac{2x-1}{\sqrt{3}}\right).}\]
\item En utilisant ce qui précède, pour tout $t>0$,
\begin{align*}
\int_0^t \frac{\dd x}{1+x^3} &= \int_0^1 \frac16\ln\left(\frac{(1+x)^2}{x^2-x+1}\right)+\frac{1}{\sqrt{3}}\arctan\left(\frac{2x-1}{\sqrt{3}}\right) \dd x\\
&= \frac16\ln\left(\frac{(1+t)^2}{t^2-t+1}\right)+\frac{1}{\sqrt{3}}\arctan\left(\frac{2t-1}{\sqrt{3}}\right) - \frac{1}{\sqrt{3}}\arctan\left(-\frac{1}{\sqrt{3}}\right)\\
&\tendversen{t\to +\infty} 0 + \frac{1}{\sqrt{3}}\frac{\pi}{2} + \frac{1}{\sqrt{3}}\frac{\pi}{6}\boxed{=\frac{2\pi}{3\sqrt{3}}}
\end{align*}
Ainsi, $\boxed{I_0=\frac{2\pi}{3\sqrt{3}}.}$.

Soit $t>0$. Procédons à une intégration par partie dans l'intégrale partielle. $u:x\donne \frac{1}{(1+x^3)^{n+1}}$ et $v:x\donne x$ sont de classe $\CC^1$ sur $\interff{0 t}$. Par intégration par partie :
\begin{align*}
\int_0^t \frac{\dd x}{(1+x^3)^{n+1}} &= \left[ \frac{x}{(1+x^3)^{n+1}} \right]_0^t - \int_0^t \frac{-(n+1)\times 3x^2\times x}{(1+x^3)^{n+2}}\dd x\\
&= \frac{t}{(1+t^3)^{n+1}} +3(n+1)\int_0^t \frac{x^3}{(1+x^3)^{n+2}}\dd x\\
&= \frac{t}{(1+t^3)^{n+1}}  + 3(n+1)\int_0^t \frac{x^3+1-1}{(1+x^3)^{n+2}}\dd x \\
&= \frac{t}{(1+t^3)^{n+1}}  + 3(n+1)\int_0^t \frac{1}{(1+x^3)^{n+1}}\dd x - 3(n+1)\int_0^t \frac{1}{(1+x^3)^{n+2}}\dd x
\end{align*}
Ainsi, en réorganisation cette inégalité :
\begin{align*}
3(n+1)\int_0^t \frac{1}{(1+x^3)^{n+2}}\D x &= \frac{t}{(1+t^3)^{n+1}} + 3(n+1)\int_0^t \frac{1}{(1+x^3)^{n+1}} \D x - \int_0^t \frac{\D x}{(1+x^3)^{n+1}}\\
&= \frac{t}{(1+t^3)^{n+1}} + (3n+2)\int_0^t \frac{\D x}{(1+x^3)^{n+1}}.
\end{align*}
et en faisant tendre $t$ vers $+\infty$, puisque les intégrales convergent, on obtient finalement
\[ \boxed{3(n+1)I_{n+1} = (3n+2)I_n}\]
\item En utilisant les propriétés de la fonction logarithme :
\begin{align*}
u_{n+1}-u_n &= \ln\left( \frac{\sqrt[3]{n+1}I_{n+1}}{\sqrt[3]{n}I_n}\right) \\
&= \ln \left( \frac{(n+1)^{1/3}}{n^{1/3}} \frac{3n+2}{3n+3}\right) \text{ d'après la question précédente}\\
&= \frac{1}{3}\ln\left(1+\frac1n\right) + \ln\left(1-\frac{1}{3n+3}\right)\\
&= \frac{1}{3}\left(\frac{1}{n}-\frac{1}{2n^2}+\petito[+\infty]{\frac{1}{n^2}} \right)+ \frac{-1}{3n+3} - \frac{1}{2(3n+3)^2} + \petito[+\infty]{\frac{1}{n^2}}\\
&= \frac{1}{3n(n+1)}-\frac{1}{6n^2} -\frac{1}{2(3n+3)^2}  + \petito[+\infty]{\frac{1}{n^2}}
\end{align*}
Remarquons alors que 
\begin{align*}
n^2(u_{n+1}-u_n) &= \frac{n^2}{3n(n+1)} - \frac{1}{6} -\frac{n^2}{18(n+1)^2} +\petito[+\infty]{1} \tendversen{n\to +\infty} \frac{1}{9}.
\end{align*}
Ainsi, \[ u_{n+1}-u_n \eq{+\infty} \frac{1}{9n^2}.\]
La série $\ds \sum_{n\geq 1} \frac{1}{9n^2}$ étant convergente, on en déduit par théorème de comparaison des séries à termes positifs que la série $\sum u_{n+1}-u_n$ est également convergente.
\item Notons alors $\ds S_n=\sum_{k=1}^{n} u_{k+1}-u_k$. Par télescopage,
 \[ S_n = u_{n+1}-u_1 \iff u_{n+1}= S_n+u_1. \]
 Par convergence de la série $(S_n)$, la suite $(u_{n+1})$ admet une limite finie, et par continuité d'exponentielle, $\exp(u_n)$ également. Notons alors $\ell=\lim \exp(u_n) >0$. Alors
 \[ \sqrt[3]{n}I_n\tendversen{n\to +\infty} \ell \iff\boxed{  I_n \eq{+\infty} \frac{\ell}{\sqrt[3]{n}}.}\]
\end{enumerate}
\end{correction}
%%% Fin exercice %%%
