%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{20}{2}[Fonctions injectives continues]
Soit $f$ une fonction définie sur un intervalle $I$ non vide et non réduit à un point.
\begin{enumerate}
  \item Montrer que, si $f$ est strictement monotone, alors $f$ est injective sur $I$.
  \item \'Etudions une réciproque partielle. On suppose que $f$ est injective et continue sur $I$. Fixons $a$ et $b$ dans $I$ tels que $a<b$. $f$ étant injective, $f(a)\neq f(b)$ et quitte à considérer $-f$, on suppose que $f(a)<f(b)$. On raisonne par l'absurde et on suppose que $f$ n'est pas strictement croissante sur $I$.
  \begin{enumerate}
    \item Justifier l'existence de $x$ et $y$ dans $I$ tels que $x<y$ et $f(y)-f(x)\leq 0$.
    \item Montrer brièvement que, pour tous $(u, v)\in I^2$  et $t\in \interff{0 1}$, $u+t(v-u)$ est compris entre $u$ et $v$.
    \item Considérons alors la fonction $\phi:\interff{0 1}\to \R$ définie par $\phi(t)=f(b+t(y-b))-f(a+t(x-a))$. Montrer qu'il existe $c\in \interff{0 1}$ tel que $\phi(c)=0$.
    \item En déduire que $(1-c)(b-a) = -c(y-x)$ et aboutir à une absurdité.
  \end{enumerate}
  \item Donner un exemple de fonction injective sur $\interff{0 1}$ qui n'est pas strictement croissante.
\end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
  \item Question déjà vue en cours. Supposons $f$ est strictement monotone et soient $x, y$ deux éléments de $I$ tels que $x\neq y$. Alors $x<y$ ou bien $x>y$ et par stricte monotonie de $f$, on en déduit que $f(x)<f(y)$ ou $f(x)>f(y)$. Dans tous les cas $f(x)\neq f(y)$, ce qui garantit que $f$ est injective.
  \item \begin{enumerate}
  \item Puisque $f$ n'est pas strictement croissante par hypothèse sur $I$, il existe deux éléments $x$ et $y$ de $I$ tels que $x<y$ et pourtant $f(x)\geq f(y)$ (ce qui contredit ainsi la stricte croissance). Dit autrement, $x<y$ et $f(y)-f(x)\leq 0$.
  \item Plusieurs démonstrations possibles. La plus simple : si $u<v$ alors  \[ 0\leq t \leq 1 \implies 0 \leq t(v-u) \leq v-u \implies u \leq u+t(v-u)\leq u+v-u=v \]
  et si $u\geq v$ :
  \[ 0\leq t \leq 1 \implies 0 \geq t(v-u) \geq v-u \implies u\geq u+t(v-u) \geq u+v-u=v \]
  et dans tous les cas, $u+t(v-u)$ est compris entre $u$ et $v$.
  \item Tout d'abord, remarquons que $\phi$ est bien définie. En effet, d'après la question 2b), $t\mapsto b+t(y-b)$ est compris entre $y$ et $b$, et $t\mapsto a+t(x-a)$ est compris entre $x$ et $a$. Puisque $I$ est un intervalle, que $x, y, a$ et $b$ sont des éléments de $I$, on est sûr que l'intervalle de borne $x$ et $a$, ainsi que celui de borne $y$ et $b$ est inclus dans $I$, où $f$ est bien définie.

  De plus, $f$ étant continue sur $I$, $\phi$ est continue sur $\interff{0 1}$ par composée.

  Enfin, puisque $f(a)<f(b)$ et $f(y)-f(x)\leq 0$ :
  \[ \phi(0) = f(b)-f(a)>0 \qeq \phi(1) = f(y)-f(x)\leq 0. \]
  Le théorème des valeurs intermédiaires nous garantit que toute valeur comprise entre $\phi(0)$ et $\phi(1)$ est atteinte. Or, $0\in \interff{ \phi(1) \phi(0)}$. Il existe donc un réel $c \in \interff{0 1}$ tel que $\phi(c)=0$.
  \item Puisque $\phi(c)=0$, on peut écrire que \[ f(b+c(y-b))-f(a+c(x-a))=0 \implies f(b+c(y-b)) = f(a+c(x-a)). \]
  Par injectivité de $f$, on peut en déduire que
  \begin{align*}
   b+c(y-b) = a+c(x-a)  &\implies b-a +c(a-b) = c(x-y) \\
   &\implies (1-c)(b-a) = -c(y-x)
 \end{align*}
 Or, $b>a$ donc $b-a>0$, $y>x$ donc $y-x>0$, et $-c\leq 0$ et $1-c\geq 0$. Cela implique que \[ (1-c)(b-a)\geq 0 \qeq -c(y-x) \leq 0. \]
 Enfin, $1-c=0$ quand $c=1$ et $-c=0$ quand $c=0$. Les deux termes ne s'annulent pas en même temps, et donc il est impossible d'avoir un $c\in \interff{0 1}$ vérifiant $\phi(c)=0$ : c'est donc absurde et notre hypothèse de départ est fausse : $f$ est bien strictement croissante sur $\interff{a b}$. Ceci étant vrai pour tous $a, b$ de $I$ tels que $a<b$, on en déduit que $f$ est strictement croissante sur $I$.
\end{enumerate}
\item La fonction $x\mapsto -x$ est injective sur $\interff{0 1}$ mais n'est pas strictement croissante. Remarquons que, d'après ce qui précède, si la fonction est continue, elle est strictement monotone. On peut donc proposer une fonction non continue sur $\interff{0 1}$ et injective, qui n'est pas strictement monotone. Par exemple :\begin{center}
\begin{minipage}{0.4\textwidth}
\[ f:x\mapsto \left\{ \begin{array}{lll} x & \text{si}& x\in \interff{0 \frac12}\\
\frac{3}{2}-x&\text{si}& x\in \interof{\frac12 1}\end{array}\right. \]
\end{minipage}\begin{minipage}{0.4\textwidth}
\begin{center}\includegraphics{exoinjnonmon.mps}\end{center}
\end{minipage}\end{center}
\end{enumerate}
\end{correction}
%%% Fin exercice %%%
