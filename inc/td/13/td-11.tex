\begin{exoApp}{40}{2}[EML E 2015]
Dans cet exercice on pourra utiliser l'encadrement suivant : $2 < \E  < 3$.
\subsubsection*{Partie I : Etude d'une fonction}
On considère l'application $\phi : \R \longrightarrow \R, x\longmapsto \phi(x)=x^2\E^x-1$.
\begin{enumerate}
\item Dresser le tableau de variations de $\phi$, en précisant la limite de $\phi$ en $-\infty$, sa valeur en 0 et sa limite en $+\infty$.
\item Etablir que l'équation $\displaystyle \E^x=\frac{1}{x^2}$, d'inconnue $x\in \interoo{0 +\infty}$, admet une solution et une seule, notée $\alpha$, et que
$\alpha$ appartient à l'intervalle $\left ] \dfrac{1}{2};1\right[$.\\
\end{enumerate}
On considère l'application $f:\R\longrightarrow \R, x\longmapsto f(x)=x^3\E^x$,\\
et la suite réelle $(u_n)_{n\in\N}$ définie par : $u_0=1$ et $\forall n\in\N$, $u_{n+1}=f(u_n)$.
\subsubsection*{Partie II : Etude d'une suite}
\begin{enumerate}[resume]
\item Montrer : $\forall n\in\N$, $u_n\geq 1$.
\item Etablir que la suite $(u_n)_{n\in\N}$ est croissante.
\item Quelle est la limite de $u_n$ lorsque l'entier $n$ tend vers l'infini?
\end{enumerate}
\end{exoApp}

\begin{correction}
\subsubsection*{Partie I}

\begin{enumerate}
	\item $\phi$ est dérivable sur $\R$ comme produit de fonctions usuelles (exponentielle, polynôme). Pour tout réel $x\in \R$,
	\begin{align*}
	\phi'(x) &= 2x\eu{x}+x^2\eu{x} = x\eu{x}(2+x).	
	\end{align*}
	$\phi'(x)$ est donc du signe de $x(2+x)$. De plus, par croissance comparée en $-\infty$ et par produit :
	\[ \lim_{x\to -\infty} \phi(x)=-1,\quad \phi(0)=-1 \qeq \lim_{x\to +\infty} \phi(x)=+\infty.\]
	On obtient alors le tableau de variations suivant :
\begin{center}
	\begin{tikzpicture}
   \tkzTabInit{$x$ / 1 , $\phi'(x)$ / 1, variation de $\phi$ / 1.5}{$-\infty$, $-2$, $0$, $+\infty$}
   \tkzTabLine{, +, z, -, z, + }
   \tkzTabVar{-/ $-1$, +/ $\phi(2)$, - / $-1$, + / $+\infty$}
\end{tikzpicture}
\end{center}
	\item Remarquons que, pour $x>0$, $\eu{x}=\frac{1}{x^2}$ si et seulement si $x^2\eu{x}-1=0$, c'est-à-dire si et seulement si $\phi(x)=0$. \\
		D'après la question précédente, $\phi$ est continue et strictement croissante sur $\R>$. $\phi$ établit donc une bijection de $\R>$ dans $f(\R>)=\interoo{-1 +\infty}$. Or $0\in \interoo{-1 +\infty}$. Ainsi, l'équation $\phi(x)=0$ admet une unique solution sur $\R>$.\\
		\textbf{Bilan} : l'équation $\eu{x}=\frac{1}{x^2}$ admet une unique solution sur $\R>$. De plus :
		\begin{align*}
		 \phi\left(\frac12\right) &= \frac{1}{4}\eu{1/2}-1 \\
		 \phi\left(1\right) &= \E-1 > 0	
		\end{align*}
	Puisque $\E<4$, $\eu{1/2}<2$ et donc $\frac{\eu{1/2}}{4}<1$ : ainsi, $\phi\left(\frac12\right)<0$. Par stricte croissance de $\phi$ :
	\[ \phi\left(\frac12\right) < 0=\phi(\alpha) < \phi(1)\implies \frac12 < \alpha < 1.\]
\end{enumerate}

\subsubsection*{Partie II}

Constatons que $f$ est dérivable sur $\R$ et $f':x\donne x^2(3+x)\eu{x}$. $f'$ est strictement positive sur $\interoo{-3 +\infty}$ donc $f$ y est strictement croissante. De plus, $f(1)=\E$.
\begin{enumerate}[resume]
	\item Puisque $f(1)=\E \geq 1$, l'intervalle $\interfo{1 +\infty}$ est stable par $f$. Le résultat découle alors de ce résultat, par récurrence. Soit $P$ la proposition définie pour tout entier $n$ par $P_n$:\guill{$u_n\geq 1$}. 
		\begin{itemize}
			\item Pour $n=0$, $u_0=1\geq 1$ : $P_0$ est vérifiée.
			\item Supposons la proposition $P_n$ vraie pour un certain entier $n$ fixé. Mais alors, par hypothèse de récurrence : $u_n\geq 1$ et donc $u_n\in \interfo{1 +\infty}$. L'intervalle $\interfo{1 +\infty}$ étant stable par $f$, $f(u_n)\in \interfo{1 +\infty}$, c'est-à-dire $u_{n+1}\geq 1$ : $P_{n+1}$ est ainsi vérifiée.
		\end{itemize}
		Le principe de récurrence nous garantit alors que la proposition $P_n$ est vraie pour tout entier $n$ : \[ \boxed{\forall n \in \N,\quad u_n\geq 1.}\]
	\item Notons $P$ la proposition définie pour tout entier $n$ par $P_n$: \guill{$u_n\leq u_{n+1}$}. 
		\begin{itemize}
			\item Pour $n=0$, $u_1=f(u_0)=\E\geq 1=u_0$ : ainsi, $P_0$ est vérifiée.
			\item Supposons la proposition $P_n$ vérifiée pour un certain entier $n$ fixé. Mais alors, $u_n\leq u_{n+1}$. Puisque $f$ est croissante sur $\interfo{1 +\infty}$, intervalle où se trouve $u_n$ et $u_{n+1}$, on en déduit que 
			\[ f(u_n) \leq f(u_{n+1}) \implies u_{n+1}\leq u_{n+2}.\]
			Ainsi, $P_{n+1}$ est vérifiée.
		\end{itemize}
		Le principe de récurrence nous garantit alors que la proposition $P_n$ est vraie pour tout entier $n$ : \fbox{$(u_n)$ est une suite croissante.}\vspace*{.1cm}
		\item La suite $(u_n)$ étant croissante, d'après le théorème de la limite monotone, soit elle admet une limite finie dans $\interfo{1 +\infty}$, soit elle tend vers $+\infty$. Supposons qu'elle admette une limite finie $\ell\in \interfo{1 +\infty}$. Mais alors, puisque $u_{n+1}=f(u_n)$, et que $f$ est continue sur $\R$, $\ell$ est un point de $f$. Or, pour $x>0$, \[ f(x)=x \implies x^2\eu{x}=1\implies \phi(x)=0.\]
			Or, le seul point fixe de $\phi$ est dans $\interoo{\frac12 1}$. Ainsi, sur $\interfo{1 +\infty}$, $f$ n'admet pas de point fixe : la convergence de $(u_n)$ est donc absurde.
			\\\textbf{Bilan} : $\boxed{u_n\tendversen{n\to +\infty} +\infty.}$
\end{enumerate}

\end{correction}
