%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{20}{1}[Des bijections]
Dans chacun des cas suivants, montrer que $f$ est une bijection d'un intervalle $I$ sur un intervalle $J$ à déterminer, et expliciter $f^{-1}$ sur $J$.
\begin{multicols}{2}
\begin{enumerate}
  \item $f:x\donne \sqrt{1+3(\ln(x))^2}$,
  \item $f:x\donne \dfrac{1}{\sqrt{x^2+x+2}}$,
  \item $f:x\donne \dfrac{\eu{x}+\eu{-x}}{2}$,
  \item $f:x\donne \ln\left(x^{3/4}-1\right)$.
\end{enumerate}
\end{multicols}
\end{exoApp}

\begin{correction}
\begin{enumerate}
\item $f$ est définie sur $\R>$ (comme $\ln$) car le radicande est strictement positif. De plus, elle y est dérivable (par composée) et on a \[ \forall x\in \R>,\quad f'(x)=\frac{3\times 2\times \frac{1}{x}\times \ln(x)}{2\sqrt{1+3(\ln(x))^2}} = \frac{3\ln(x)}{x\sqrt{1+3(\ln(x))^2}}. \]
$f(x)$ est donc, sur $\R>$, du signe de $\ln$. Les limites en $+\infty$ et $0$ ne sont pas indéterminée et valent respectivement $+\infty$ et $+\infty$. On obtient le tableau de variations suivant :
\begin{center}
  \begin{tikzpicture}
   \tkzTabInit{$x$ / 1 , $f'(x)$ / 1, $f(x)$ / 1.5}{$0$, $1$, $+\infty$}
   \tkzTabLine{d, -, z, +, }
   \tkzTabVar{D+/ $+\infty$, -/ $1$, +/ $+\infty$}
\end{tikzpicture}
\end{center}
$f$ étant continue et strictement monotone sur $\interof{0 1}$ et $\interfo{1 +\infty}$, d'après le théorème de la bijection, $f$ établit donc une bijection de $\interof{0 1}$ dans $\interfo{1 +\infty}$ et de $\interfo{1 +\infty}$ dans $\interfo{1 +\infty}$.

Prenons le cas $\interfo{1 +\infty}$. Soit $x\in \interfo{1 +\infty}$ et $y\in \interfo{1 +\infty}$ tels que $f(x)=y$. Alors :
\begin{align*}
  f(x)=y &\Longleftrightarrow \sqrt{1+3(\ln(x))^2}=y\\
   &\Longleftrightarrow \ln(x)^2 = \frac{y^2-1}{3} \\
   &\Longleftrightarrow \ln(x) = \sqrt{ \frac{y^2-1}{3}} \text{ qui a un sens car }y\geq 1 \text{ et } x\geq 1 \text{ (donc ) }\ln(x)\geq 0)\\
   &\Longleftrightarrow x = \eu{\sqrt{\frac{y^2-1}{3}}}
\end{align*}
Ainsi \[ \boxed{f^{-1}: y\mapsto \eu{\sqrt{\frac{y^2-1}{3}}}.}\]
\item $f$ est définie sur $\R$ (en effet, le discriminant du trinôme $x\donne x^2+x+2$ est strictement négatif donc le trinôme est strictement positif), et est dérivable sur ce domaine, par composée (la fonction racine étant dérivable sur $\R>$. Pour tout réel $x$,
\begin{align*}
f'(x) &= -\frac{1}{2} \frac{2x+1}{(x^2+x+2)^{3/2}}.	
\end{align*}
Remarquons que $f'(x)$ est donc du signe de $-(2x+1)$. On obtient le tableau suivant :
\begin{center}
  \begin{tikzpicture}
   \tkzTabInit{$x$ / 1 , $f'(x)$ / 1, $f(x)$ / 1.5}{$-\infty$, $-\frac12$, $+\infty$}
   \tkzTabLine{, +, z, -, }
   \tkzTabVar{-/ $0$, +/ $\frac{2}{\sqrt{7}}$, -/ $0$}
\end{tikzpicture}
\end{center}
Les limites s'obtenant sans difficultés par composée. Le théorème de la bijection indique que $f$ établit une bijection de $\interof{-\infty{} -\frac12}$ dans $\interof{0 \frac{2}{\sqrt{7}}}$ et de $\interof{-\frac12 +\infty}$ dans $\interof{0 \frac{2}{\sqrt{7}}}$.

Si on prend $I=\interof{-\infty -\frac{1}{2}}$, par exemple, alors, pour $x\in I$ et $y\in \interof{0 \frac{2}{\sqrt{7}}}$ :
\begin{align*}
   f(x)=y &\iff \frac{1}{\sqrt{x^2+x+2}}=y\\
   &\iff  x^2+x+2=\frac{1}{y^2}\\
   &\iff x^2+x + \left(2-\frac{1}{y^2}\right).
\end{align*}
Le discriminant est 
\[ \Delta = 1^2-4\times\left(2-\frac{1}{y^2}\right)= \frac{4}{y^2}-7\]
Puisque $y\in \interof{1 \frac{2}{\sqrt{7}}}$, le discriminant est strictement positif. Il y a donc deux racines :
\[ x_1=\frac{-1-\sqrt{\frac{4}{y^2}-7}}{2} \qeq x_2=\frac{-1+\sqrt{\frac{4}{y^2}-7}}{2} \]
Sur $I$, on prend $x_1$. Ainsi :
\[ \boxed{f^{-1} : y\mapsto \frac{-1-\sqrt{\frac{4}{y^2}-7}}{2} .} \]
\item $f$ est dérivable sur $\R$ comme somme de deux fonctions exponentielles. Pour tout réel $x$,
\[ f'(x)=\frac{\eu{x}-\eu{-x}}{2}.\]
Remarquons que 
\begin{align*}
 f'(x)>0 &\iff \eu{x}>\eu{-x}\\
 &\iff x>-x \text{ par stricte croissance d'$\exp$}\\
 &\iff x>0.
\end{align*}
On obtient le tableau suivant, les limites s'obtenant par somme :
\begin{center}
  \begin{tikzpicture}
   \tkzTabInit{$x$ / 1 , $f'(x)$ / 1, $f(x)$ / 1.5}{$-\infty$, $0$, $+\infty$}
   \tkzTabLine{, -, z, +, }
   \tkzTabVar{+/ $+\infty$, -/ $1$, +/ $+\infty$}
\end{tikzpicture}
\end{center}
On aurait également pu montrer que la fonction $f$ est paire.

\`A nouveau, le théorème de la bijection nous garantit que $f$ établit une bijection de $\R-$ dans $\interfo{1 +\infty}$ et de $\R+$ dans $\interfo{1 +\infty}$.

Si on prend $I=\R+$ et $J=\interfo{1 +\infty}$, soit $x\in I$ et $y\in J$. Alors
\begin{align*}
f(x)=y &\iff \frac{\eu{x}+\eu{-x}}{2} = y \\
&\iff \eu{x}+\eu{-x}-2y=0 \\
&\iff \eu{2x}+1-2y\eu{x}=0 \text{ en multipliant par $\eu{x}$.}	
\end{align*}
On pose classiquement $X=\eu{x}$. L'équation s'écrit $X^2-2yX+1$, dont le discriminant est $\Delta=4y^2-4=4(y^2-1)$.

Puisque $y\geq 1$, $\Delta\geq 0$. Les solutions sont alors \[ X_1= \frac{2y-\sqrt{4(y^2-1)}}{2}=y-\sqrt{y^2-1} \qeq X_2= \frac{2y+\sqrt{4(y^2-1)}}{2}=y+\sqrt{y^2-1} . \]
Puisque $y\geq 1$, $(y-1)^2 \leq y^2-1$ et donc $X_1\leq 1$. $X_2\geq 1$. On revient à la variable de départ :
\[ x_1=\ln\left(y-\sqrt{y^2-1}\right) \qeq x_2=\ln\left(y+\sqrt{y^2-1}\right). \]
Puisque $X_1\leq 1$ et $X_2\geq 1$, on en déduit que $x_1\leq 0$ et $x_2\geq 0$. Or $x\in I=\R+$, donc $x=x_2$.

Ainsi, \[ \boxed{f^{-1}: y\donne \ln\left(y+\sqrt{y^2-1}\right). }\] 
\item $f$ est définie si et seulement si $x^{3/4}-1>0$, c'est-à-dire si et seulement si $x>1$. Sur $\interoo{1 +\infty}$, $f$ est dérivable comme composé de fonctions puissances et logarithme. Pour tout réel $x>1$ :
\begin{align*}
f'(x) &= \frac{\frac34x^{-1/4}}{x^{3/4}-1}>0	
\end{align*}
$f$ est donc strictement croissante sur $I=\interoo{1 +\infty}$ et le théorème de la bijection garantit que $f$ établit une bijection de $I$ dans $J=f(I)=\interoo{-\infty{} +\infty}=\R$ (les limites s'obtenant par composée). Enfin, pour tout $x\in I$ et $y\in J$ :
\begin{align*}
f(x)=y &\iff \ln\left(x^{3/4}-1\right)=y\\
&\iff x^{3/4}-1=\eu{y} \\
&\iff x^{3/4} = \eu{y}+1\\
&\iff x = \left(\eu{y}+1\right)^{4/3} \in I.	
\end{align*}
Ainsi 
\[ \boxed{f^{-1} : y\donne \left(\eu{y}+1\right)^{4/3}.}\]

 
\end{enumerate}
\end{correction}
%%% Fin exercice %%%
