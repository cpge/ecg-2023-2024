%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{15}{1}[Des espaces de dimension finie]
Montrer que les ensembles suivants sont des espaces vectoriels de dimension finie. On précisera leur dimension et une base.
\begin{enumerate}
	\item $F=\left\{f \in \mathcal{F}(\R, \R),\, \exists \,(a, b, c) \in \R^{3}, \quad \forall x \in \R, \quad f(x)=a \sin (x)+b \cos (x)+c\right\}$.
	\item L'ensemble des suites réelles arithmétiques.
	\item Les ensembles de matrices $\mathcal{T}_{n}^{+}(\R), \mathcal{T}_{n}^{-}(\R), \mathcal{S}_{n}(\R)$ et $\mathcal{A}_{n}(\R)$.
	\item $\left\{A \in \MM_{n}(\R),\,\trace(A)=0\right\}$, où $\trace(M)$ est la somme des termes diagonaux d'une matrice $M \in \MM_{n}(\R)$.
	\item  $\left\{x\donne P(x) \eu{\alpha x}+Q(x) \eu{-\alpha x},\, (P, Q) \in\left(\R_{n}[X]\right)^{2}\right\}$ pour $\alpha \in \R$ et $n \in \mathbb{N}^{*}$.
	\end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item Remarquons que par définition, \[ F = \{ x\donne a\sin(x)+b\cos(x)+c,\,(a,b,c)\in \R^3\} = \Vect( \sin, \cos, 1) \]
	où $1$ désigne la fonction constante égale à $1$. La famille $(\sin, \cos, 1)$ est donc génératrice de $F$, qui est de dimension finie. Constatons qu'elle est libre : s'il existe $(a,b,c)$ tels que $a\sin+b\cos+c = 0$, alors en évaluant en $0$, $\frac{\pi}{2}$ et $\pi$ par exemple, on en déduit \[ \systeme{b+c=0,a+c=0,-b+c=0}\implies \systeme*{a=0,b=0,c=0}. \]
	La famille $(\sin, \cos, 1)$ est donc une base de $F$, qui est de dimension $3$.
	\item On note $F$ l'ensemble des suites arithmétiques. Remarquons que :
	\[ F = \{ \forall n\in N,\, u_n=a + bn,\quad (a,b)\in \R^2\} = \Vect( (u_n), (v_n)) \]
	où $(u_n)$ est la suite constante égale à $1$, et $(v_n)$ la suite définie pour tout $n$ par $v_n=n$. Donc $F$ est de dimension finie. De plus, $(u,v)$ est libre; en effet, si $(a,b)\in \R^2$, tels que $au+bv=0$. Alors, pour tout $n$, $a+bn = 0$ implique $a=b=0$ : la famille est libre, et finalement $\dim(F)=2$.
	\item Pour les matrices triangulaires, il suffit de repartir de la définition. En notant $(E_{i,j})_{1\leq i,j\leq n}$ les matrices élémentaires, remarquons que
	\[ T^+_n(\R) = \Vect( E_{1,1}, E_{1,2},\hdots, E_{1,n}, E_{2,2},\hdots E_{2,n}, \hdots, E_{n,n}) = \Vect\left( \left(E_{i,j}\right)_{1\leq i\leq j\leq n}\right). \]
	Ainsi, $T_n^+(\R)$ est de dimension finie, et la famille précédente est libre car extraite de la base canonique, elle-même libre. Finalement, $ \left(E_{i,j}\right)_{1\leq i\leq j\leq n}$ forme une base de $T_{n}^+(\R)$ et sa dimension est $\dim(T_n^+(\R))= \sum\limits_{i=1}^n i = \frac{n(n+1)}{2}$.

	Par le même raisonnement,
	\[ T^-_n(\R) = \Vect( E_{1,1}, E_{2,1},E_{2,2}, \hdots, E_{n,1},\hdots, E_{n,n}) = \Vect\left( \left(E_{i,j}\right)_{1\leq j\leq i\leq n}\right). \]
	qui est également libre : $\dim(T_n^-(\R)) = \sum\limits_{j=1}^n j = \frac{n(n+1)}{2}$.

	Pour $\SS_n(\R)$ remarquons qu'une matrice $M\in \SS_n(\R)$ va s'écrire
	\[ S = \sum_{i=1}^n \sum_{j=1}^{i-1} s_{i,j}(E_{i,j}+E_{j,i}) + \sum_{i=1}^n s_{i,i}E_{i,i}. \]
	Ainsi, \[ \SS_n(\R) = \Vect\left( \left(E_{i,j}+E_{j,i}\right)_{1\leq j < i\leq n}, \left(E_{i,i}\right)_{1\leq i\leq n}\right). \]
	Cette famille est libre : s'il existe $(s_{i,j})$ des réels tels que \[ \sum_{i=1}^n \sum_{j=1}^{i-1} s_{i,j}(E_{i,j}+E_{j,i}) + \sum_{i=1}^n s_{i,i}E_{i,i} =0 \]
	alors, la famille $(E_{i,j})_{1\leq i,j\leq n}$ étant une base, on en déduit que les $(s_{i,j})$ sont tous nuls. Finalement, c'est une base de $\SS_n(\R)$ et $\dim(\SS_n(\R)) = \sum\limits_{i=1}^n i-1 + \sum\limits_{i=1}^n 1 = \frac{(n-1)n}{2}+n = \frac{n(n+1)}{2}$.
	
	Enfin, pour $\AA_n(\R)$, remarquons qu'une matrice $M \in \AA_n(\R)$ va s'écrire 
		\[ S = \sum_{i=1}^n \sum_{j=1}^{i-1} s_{i,j}(E_{i,j}-E_{j,i}) \]
	(les termes sur la diagonale étant nécessairement nuls). Alors
	\[ \AA_n(\R) = \Vect\left( \left(E_{i,j}-E_{j,i}\right)_{1\leq j<i\leq n}\right).\]
	On démontre, comme précédemment, que cette famille est libre et forme une base de $\AA_n(\R)$. Ainsi, \[ \dim(\AA_n(\R)) = \sum_{i=1}^n (i-1) = \frac{n(n-1)}{2}.\]
	\item Soit $A=(a_{i,j})_{\substack{1\leq i\leq n\\1\leq j\leq n}}$. Remarquons que $\trace(A)=0$ si et seulement si $\ds\sum_{i=1}^n a_{i,i}=0$, soit si et seulement si $\ds a_{n,n}=-\sum_{i=1}^{n-1}a_{i,i}$.\\Ainsi,
	\begin{align*}
	\{ A\in \MM_{n}(\R),\quad \trace(A)=0\} &= \left 
	\{ \matrice{a_{1,1}&a_{1,2}&\hdots&a_{1,n-1}&a_{n,n}\\a_{2,1}&a_{2,2}&\hdots&a_{2,n-1}&a_{2,n}\\
		\vdots & \vdots & &\vdots & \vdots \\
		a_{n-1,1}&a_{n-1,2}&\hdots&a_{n-1,n-1}&a_{n-1,n}\\
		a_{n,1}&a_{n,2}&\hdots&a_{n,n-1}&-\sum_{i=1}^{n-1} a_{i,i}}
		,\quad (a_{i,j}) \in \R^{n^2-1}\right \}	\\
		&= \Vect( (E_{1,1}-E_{n,n}), E_{1,2},\hdots, E_{1,n},E_{2,1},(E_{2,2}-E_{n,n}),\hdots, E_{2,n},\hdots E_{n,n-1})\\
		&= \Vect \left( \left(E_{i,j}\right)_{i\neq j}, \left(E_{i,i}-E_{n,n}\right)_{1\leq i\leq n-1} \right).
	\end{align*}
	L'ensemble est donc de dimension finie, car engendrée par une famille finie. Par ailleurs, la famille précédente est libre, puisque que si une combinaison linéaire est nulle, en reprenant l'expression de la matrice, cela donne tous les coefficients nuls. Ainsi, sa dimension est $n^2-1$.
	\item Remarquons que :
	\begin{align*}
	F &= \left\{x\donne P(x) \eu{\alpha x}+Q(x) \eu{-\alpha x},\, (P, Q) \in\left(\R_{n}[X]\right)^{2}\right\} \\&= \left \{ x\donne (a_nx^n+a_{n-1}x^{n-1}+\hdots +a_0)\eu{\alpha x}+(b_nx^n+\hdots +b_0)\eu{-\alpha x},\quad (a_0,\hdots,a_n,b_0,\hdots, b_n)\in \R^{2n+2}\right\}.\\
	&= \Vect\underbrace{\left(  x\donne x^n\eu{\alpha x},\hdots,x\donne \eu{\alpha x},x\donne x^n\eu{-\alpha x},\hdots x\donne \eu{-\alpha x}\right)}_{=\BB}.
	\end{align*}
	Ainsi, $F$ est de dimension finie. Or $\BB$ est libre. Soit une combinaison linéaire telle que 
	\[ x\donne a_nx^n\eu{\alpha x}+\hdots +a_0\eu{\alpha x}+b_nx^n\eu{-\alpha x}+\hdots + b_0 = 0. \]
	Notons $p$ le plus grand indice tel que $a_p\neq 0$, s'il existe. La relation précédente s'écrit alors 
		\[ x\donne a_px^p\eu{\alpha x}+\hdots +a_0\eu{\alpha x}+b_nx^n\eu{-\alpha x}+\hdots + b_0 = 0. \]
  	Or \[ a_px^p\eu{\alpha x}+\hdots +a_0\eu{\alpha x}+b_nx^n\eu{-\alpha x}+\hdots + b_0 \eq{+\infty} a_px_p \]
  	car $a_p\neq 0$ et donc 
  	\[ a_px^p\eu{\alpha x}+\hdots +a_0\eu{\alpha x}+b_nx^n\eu{-\alpha x}+\hdots + b_0 \tendversen{x\to +\infty} \pm\infty\]
  	ce qui est absurde puisque la somme est nulle. Donc tous les $a_i$ sont nuls. Notons alors $q$ le plus grand indice tel que $b_q\neq 0$. La relation devient 
  	\[ x\donne b_qx^q\eu{-\alpha x}+\hdots + b_0 =0 \]
  	soit, en divisant par $\eu{-\alpha x}$
  	\[ \forall x,\quad b_qx^q+\hdots + b_0 =0 \]
  	Or, $b_px^p+\hdots + b_0\eq{+\infty} b_px^p$ car $b_q\neq 0$, et donc $b_qx^q+\hdots + b_0\tendversen{x\to +\infty} \pm \infty$, ce qui est absurde puisque la somme est nulle. Finalement, tous les coefficients sont nuls, et la famille est libre.
  	
  	Ainsi, $\dim(F) = 2n+2$.
\end{enumerate}
\end{correction}
%%% Fin exercice %%%
