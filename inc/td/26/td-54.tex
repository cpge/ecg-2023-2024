%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{30}{3}[Des résultats étranges]
\begin{enumerate}
  \item Soit $f \in \LL(E)$. Montrer que
  \[
  \Image(f)=\Image\left(f^{2}\right) \quad \iff \quad \Ker(f)=\Ker\left(f^{2}\right) \quad \iff \quad  \Ker(f) \oplus \Image(f)=E .
  \]
  \item Montrer que la dimension de $E$ $n$ est paire si et seulement s'il existe $f \in \LL(E)$ tel que $\Image(f)=\Ker(f)$.

  On pourra, connaissant une base $\left(e_{1}, \hdots, e_{p}, e_{p+1}, \hdots, e_{2 p}\right)$ de $E$, construire $f$ vérifiant $\Image(f)=\Ker(f)=\Vect\left(e_{1}, \hdots, e_{p}\right)$.
  \item  Soit $f \in \LL(E)$. Montrer que $\dim(\Ker(f)) \leq \dim\left(\Ker\left(f^{2}\right)\right) \leq 2 \dim(\Ker(f))$.
\end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item Nous sommes en dimension finie, on peut donc utiliser le théorème du rang. Ainsi,
	\[ \rg(f)+\dim(\Ker(f)) = E = \rg(f^2)+\dim(\Ker(f^2)). \]
	\begin{itemize}
		\item 	Si $\Image(f)=\Image(f^2)$, cette égalité devient $\dim(\Ker(f))=\dim(\Ker(f^2))$. Or, $\Ker(f)\subset \Ker(f^2)$ puisque si $x\in \Ker(f)$ alors $f(x)=0$ et par composition $f(f(x))=f^2(x)=0$. Par inclusion et égalité des dimensions, $\Ker(f)=\Ker(f^2)$.
		\item Réciproquement, si $\Ker(f)=\Ker(f^2)$, l'égalité devient $\rg(f)=\rg(f^2)$. Or, $\Image(f^2)\subset \Image(f)$ puisque si $x\in \Image(f^2)$ alors il existe $y\in E$ tel que $x=f^2(y)=f(f(y)) \in \Image(f)$. Par inclusion et égalité des dimensions, $\Image(f)=\Image(f^2)$.
	\item Supposons $\Ker(f)=\Ker(f^2)$. Montrons que $\Ker(f)\oplus \Image(f)=E$. Tout d'abord, le théorème du rang nous donne déjà $\dim(\Ker(f))+\dim(\Image(f))=\dim(E)$. Il reste à montrer que $\Ker(f)\cap \Image(f)=\{0\}$ pour pouvoir conclure. Soit $x\in \Ker(f)\cap \Image(f)$. Il existe $y\in E$ tel que $x=f(y)$ et $f(x)=0$. Cela devient $f^2(y)=0$ : $y\in \Ker(f^2)$. Or $\Ker(f^2)=\Ker(f)$ donc $y\in \Ker(f)$ : $f(y)=0$, c'est-à-dire $x=0$. On peut donc conclure que $\Ker(f)\cap \Image(f)=\{0\}$ et finalement, par égalité de dimension, que $\Ker(f)\oplus \Image(f)=E$.
	\item Enfin, supposons $\Ker(f)\oplus \Image(f)=E$ et montrons que $\Image(f)=\Image(f^2)$, ce qui permettra de conclure puisqu'on a déjà montré que $\Image(f)=\Image(f^2)\iff \Ker(f)=\Ker(f^2)$. 
	 
	On a déjà l'inclusion $\Image(f^2)\subset \Image(f)$. Soit $x\in \Image(f)$. Il existe donc $y\in E$ tel que $x = f(y)$. Par supplémentarité, il existe $a\in \Ker(f)$ et $b\in \Image(f)$ tel que $y=a + b$, c'est-à-dire il existe $c\in E$ tel que $y=a+f(c)$. En appliquant $f$, on en déduit 
		\[ f(y) = \underbrace{f(a)}_{=0}+f^2(c) = f^2(c) .\]
		Or $f(y)=x$, donc $x=f^2(c) \in \Image(f^2)$.
	\end{itemize}
	\item Si $\Image(f)=\Ker(f)$, le théorème du rang nous donne $\rg(f)+\dim(\Ker(f))=2\rg(f)=n$ et donc $n$ est pair.\\Réciproquement, supposons $n$ pair, qu'on écrit $n=2p$ avec $p\in \N*$. Soit $(e_1,\hdots, e_p,e_{p+1},\hdots, e_{2p})$ une base de $E$ (qui existe car de dimension finie). Définissons $f$ par la caractérisation sur une base : soit $f$ l'unique application linéaire vérifiant :
		\[ \forall i\in \interent{1 p},\quad f(e_i)=0 \qeq \forall i \in \interent{p+1 2p},\quad f(e_i)=e_{i-p}.\]
		Par définition, $\Ker(f)=\Vect(e_1,\hdots, e_p)$ et toujours par définition
		\[ \Image(f)=\Vect(f(e_1),\hdots, f(e_{2p})) = \Vect(0,\hdots, 0, e_1,\hdots, e_p) = \Vect(e_1,\hdots, e_p). \]
		Ainsi, $\Image(f)=\Ker(f)$.
	\item Tout d'abord, nous avons vu plusieurs fois que $\Ker(f)\subset \Ker(f^2)$. Puisqu'on est en dimension finie, $\boxed{\dim(\Ker(f))\leq \dim(\Ker(f^2))}$ car ce sont des sous-espaces vectoriels de $E$.

	Soit $g$ l'application $\restric{f}{\Image(f)}$. $g$ est linéaire de $\Image(f)$ dans $E$ et le théorème du rang donne 
	\[ \rg(g)+\dim(\Ker(g)) = \dim(\Image(f))=\rg(f). \]
	Constatons que 
	\[ x\in \Ker(g)\iff x\in \Image(f)\text{ et } f(x)=0 \iff x\in \Image(f)\cap \Ker(f).\]
	Par ailleurs :
	\[ x\in \Image(g)\iff \exists y\in \Image(f),\quad x=f(y) \iff \exists z\in \Image(f),\quad x=f(f(z)) \iff x\in \Image(f^2). \]
	
	Finalement 
	\[ \rg(f) = \dim(\Image(f)\cap \Ker(f)) + \dim(\Image(f^2)) = \dim(\Image(f)\cap \Ker(f)) + \rg(f^2).\]
	Or, le théorème du rang appliqué à $f$ et $f^2$ nous donne également 
\[ \rg(f)+\dim(\Ker(f))=n=\rg(f^2)+\dim(\Ker(f^2))\]
donc les deux relations précédentes s'écrivent
\begin{align*}
\dim(\Ker(f^2)) = \dim(\Image(f)\cap \Ker(f))+\dim(\Ker(f)).	
\end{align*}
Pour terminer, puisque $\Image(f)\cap \Ker(f)\subset \Ker(f)$, $\dim(\Image(f)\cap \Ker(f))\leq \dim(\Ker(f))$ et donc
\[ \boxed{\dim(\Ker(f^2))\leq \dim(\Ker(f))+\dim(\Ker(f))=2\dim(\Ker(f)).}\]
\end{enumerate}
\end{correction}
%%% Fin exercice %%%
