%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoConc}{50}{2}[EML 2023 voie S -- problème (partiel)]

Dans tout le problème, $n$ est un entier supérieur ou égal à $2$ et $E$ un espace vectoriel de dimension finie $n$.

Lorsque $F$ est un espace vectoriel de dimension finie, on admettra que la dimension de l'espace vectoriel $\LL(E,F)$ est : \[ \dim(\LL(E,F)) = \dim E \times \dim F.\]

\subsection*{Préliminaire}

\begin{enumerate}
	\item Justifier que les espaces $E$ et $E^*$ ont la même dimension.
	\item Soit $\phi$ un élément de $E^*$.
	\begin{enumerate}
		\item Quelles sont les dimensions possibles pour l'image $\Image \phi$ de $\phi$ ?
		\item En déduire que $\phi$ est soit nulle, soit surjective.
		\item On suppose que $\phi$ n'est pas l'application nulle. Démontrer que $\ker \phi$ est un hyperplan de $E$.
	\end{enumerate}
	\item Soit $f:\R^p\to \R$ une forme linéaire. Montrer qu'il existe des réels $\lambda_1,\hdots,\lambda_n$ tels que
		\[ \forall (x_1,\hdots, x_p)\in \R^p,\quad f(x_1,\hdots,x_p)=\sum_{k=1}^p \lambda_kx_k.\]
\end{enumerate}

\subsection*{Partie I -- Des exemples}

\begin{enumerate}[resume]
	\item \textbf{Premier exemple}\\
	Dans cette question, $p$ est un entier naturel non nul et $E$ est l'espace vectoriel $\R_p[x]$ des fonctions polynômes à coefficients réels de degré inférieur ou égal à $p$.\\
	On considère l'application $g:E\to \R$ définie par : $\ds g(P)=\int_0^1 P(t)\D t$.
	\begin{enumerate}
		\item Démontrer que $g$ est un élément de $E^*$.
		\item Quelle est la dimension du noyau de $g$ ?
		\item Pour $k\in \{ 1,\hdots, p\}$, on considère la fonction polynôme $Q_k:x\donne x^k-\frac{1}{k+1}$.\\Démontrer que la famille $(Q_1,\hdots, Q_p)$ est une base du noyau de $g$.
	\end{enumerate}
	\item \textbf{Second exemple}\\
	Dans cette question, $p$ est un entier naturel non nul et $E$ est l'espace $\R_p[x]$ des fonctions polynômes à coefficients réels de degré inférieur ou égal à $p$.\\On considère l'application $f:E\to \R$ définie par : $f(P)=P(0)$.
	\begin{enumerate}
		\item Démontrer que $f$ est un élément de $E^*$.
		\item Déterminer le noyau de $f$.
	\end{enumerate}
	\item Dans cette question, on revient au cadre général.\\Soient $f$ et $g$ deux éléments de $E^*$, non nuls, tels que $\ker f\subset \ker g$.
	\begin{enumerate}
		\item Démontrer que $\ker f = \ker g$.
		\item Justifier de l'existence d'un élément $x_0$ de $E$ qui n'appartient pas au noyau de $f$.
		\item Démontrer que $E=\ker f \oplus \Vect(x_0)$, où $\Vect(x_0)$ désigne le sous-espace vectoriel de $E$ engendré par le vecteur $x_0$.
		\item On pose $h=g(x_0)f-f(x_0)g$. Démontrer que $h$ est nulle.
		\item Que peut-on en conclure pour les formes linéaires $f$ et $g$ ?
	\end{enumerate} 
\end{enumerate}

\subsection*{Partie II -- Hyperplans et formes linéaires}

\begin{enumerate}[resume]
	\item On a vu à la question 2c que le noyau d'une forme linéaire non nulle est un hyperplan. Le but de cette question est de démontrer que \textbf{tout hyperplan de $E$ est le noyau d'une forme linéaire non nulle}.\\Soit $H$ un hyperplan de $E$.
	\begin{enumerate}
		\item Soit $(e_1,\hdots, e_{n-1})$ une base de $H$. Justifier de l'existence d'un vecteur $e_n$ dans $E$ tel que $\beta = (e_1,\hdots, e_n)$ soit une base de l'espace vectoriel $E$.
		\item Soit $\phi$ l'élément de $\LL(E,\R)$ défini par : 
		\[ \phi(e_i)=
		\begin{cases} 
			0 & \text{si } i\in \{1,\hdots, n-1\}\\
			1 & \text{si } i=n
			\end{cases}.\]	
	Justifier que cette définition est correcte et démontrer que $\ker \phi = H$.
	\end{enumerate}
\end{enumerate}
\textit{Dans la suite de cette partie, on considère un entier $p\geq 2$ et une famille $(f_1,\hdots, f_p)$ de formes linéaires sur $E$, ainsi que l'application 
\[ \appli{f}{E}{\R[p]}{x}{(f_1(x),\hdots, f_p(x))}.\]
On tiendra pour acquis que l'application $f$ est linéaire.}
\begin{enumerate}[resume]
	\item Démontrer que : $\ds\ker f = \bigcap_{i=1}^p \ker f_i$.
	\item On suppose dans cette question que l'application $f$ est surjective.
	\begin{enumerate}
		\item On note $(\eps_1,\hdots, \eps_p)$ la base canonique de $\R[p]$. Justifier que $\eps_1$ admet un antécédent $x$ par $f$.
		\item Démontrer que la famille $(f_1,\hdots, f_à)$ est libre dans $E^*$.
	\end{enumerate}
	\item On suppose dans cette question que l'application $f$ n'est pas surjective.
	\begin{enumerate}
		\item Que peut-on dire de la dimension $m$ de $\Image f$ ?
		\item En complétant une base $(e_1,\hdots, e_m)$ de $\Image f$ en une base de $\R[p]$, démontrer que $\Image f$ est inclus dans un hyperplan $H$ de $\R[p]$.
		\item En déduire que la famille $(f_1,\hdots, f_p)$ est liée dans $E^*$ (on pourra utiliser la question 6).
	\end{enumerate}
	\item On suppose dans cette question que la famille $(f_1,\hdots, f_p)$ est libre dans $E^*$.
	\begin{enumerate}
		\item Justifier que $f$ est surjective.
		\item Démontrer que : $\ds \dim\left( \bigcap_{i=1}^p \ker f_i\right) = n-p$.
	\end{enumerate}
\end{enumerate}

\end{exoConc}

\begin{correction}
\subsection*{Préliminaire}

\begin{enumerate}
	\item Nous sommes en dimension finie. Par propriété,
		\[ \dim(E^*) = \dim(\LL(E,\R)) = \dim(E)\times \dim(R)=\dim(E)\times 1 = \dim(E).\]
		Ainsi, $E$ et $E^°$ ont même dimension.
	\item \begin{enumerate}
	\item Par définition, $\Image \phi \subset \R$, donc $\dim(\Image \phi) \leq \dim(R)=1$. Ainsi, la dimension de $\Image \phi$ vaut $0$ ou $1$.
	\item Si $\dim(\Image \phi)=0$, cela signifie que $\Image \phi=\{0\}$ : tout élément de $E$ est envoyé sur $0$; la fonction $\phi$ est donc la fonction nulle.\\
	Si $\dim(\Image \phi)=1=\dim(\R)$, puisque $\Image \phi \subset \R$, par égalité des dimensions, $\Image \phi=\R$ : $\phi$ est surjective.\\
	Finalement, $\phi$ est soit nulle, soit surjective.
	\item Le théorème du rang, appliqué à $\phi$, nous donne 
	\[ \dim(E) = \dim(\ker \phi) + \dim(\Image \phi).\]
	Si $\phi$ est non nulle, d'après ce qui précède, $\dim(\Image \phi)=1$ et le théorème du rang donne alors 
	\[ \boxed{\dim(\ker \phi) = \dim(E)-1=n-1.}\]
	Ainsi, $\ker \phi$ est un hyperplan.
	\end{enumerate}
	\item Prenons la base canonique de $\R^p$, que l'on note $(e_1,\hdots,e_p)$. Par linéarité de $f$, pour tout $(x_1,\hdots, x_p)\in \R[p]$, on peut écrire
\begin{align*}
f(x_1,\hdots,x_p) &= f\left( x_1e_1+\hdots + x_pe_p\right) \\
&= \sum_{k=1}^p x_k f(e_k).	
\end{align*}
Or, $f$ étant une forme linéaire, pour tout $k\in \interent{1 p}$, $f(e_k)\in \R$; notons alors $\lambda_k$ le réel $f(e_k)$. On en déduit que, pour tout $(x_1,\hdots, x_p)\in \R[p]$ 
\[ f(x_1,\hdots,x_p) = \sum_{k=1}^p x_k f(e_k) = \sum_{k=1}^p \lambda_kx_k.\]
\end{enumerate}

\subsection*{Partie I -- Des exemples}

\begin{enumerate}[resume]
	\item \begin{enumerate}
	\item Rapidement, $g$ est linéaire. En effet, si $(P,Q)\in E^2$ et $\lambda \in \R$, par linéarité de l'intégrale :
	\[ g(\lambda P+Q) = \int_0^1 (\lambda P+Q)(t)\D t = \lambda \int_0^1 P(t)\D t + \int_0^1 Q(t)\D t = \lambda g(P)+g(Q).\]
	Puisque $g:E\to \R$ est linéaire, $g$ est une forme linéaire : $g\in E^*$.
	\item $g$ n'est pas nulle, puisque par exemple $g(1) = 1$. D'après le préliminaire, le noyau de $g$ est un hyperplan de $\R_p[x]$ et donc 
		\[ \boxed{ \dim(\ker g) = (p+1)-1 = p.}\]
	\item Montrons tout d'abord que les polynômes $Q_k$ sont dans le noyau de $g$.\\Soit $k\in \interent{1 p}$. On calcule :
\begin{align*}
g(Q_k) &= \int_0^1 \left( x^k-\frac{1}{k+1}\right)\D t \\
&= \left[ \frac{x^{k+1}}{k+1}- \frac{x}{k+1}\right]_0^1 = 0.	
\end{align*}
	Ainsi, pour tout $k\in \interent{1 p}$, $Q_k\in \ker g$.
	
La famille $(Q_k)_{1\leq k\leq p}$ est échelonnée en degré, sans polynôme nulle : elle est donc libre. Puisqu'elle est de bon cardinal (c'est-à-dire ici $p$, la dimension du noyau), il s'agit bien d'une base du noyau de $g$.
	\end{enumerate}
	\item \begin{enumerate}
	\item $f$ est bien linéaire. En effet, pour tout $(P,Q)\in \E^2$ et $\lambda \in \R$ :
	\[ f(\lambda P+Q) = (\lambda P+Q)(0) = \lambda P(0)+Q(0)=\lambda f(P)+f(Q).\]
	Puisque $f:E\to \R$ est linéaire, il s'agit d'une forme linéaire : $f\in E^*$.
	\item Soit $\ds P=\sum_{k=0}^n a_kX^k \in \ker f$. Alors $f(0)=0$ est équivalent à $a_0=0$. Ainsi
	\[ \boxed{\ker f = \{ a^nX^n+\hdots +a_1X,(a_1,\hdots, a_n)\in \R^n\} = \Vect(X,X^2,\hdots,X^n).}\]
	\end{enumerate}
	\item \begin{enumerate}
	\item $f$ et $g$ étant non nuls, d'après les préliminaires, $\dim(\ker f)=\dim(\ker g)=n-1$. Puisqu'on a l'inclusion $\ker f\subset \ker g$ et l'égalité des dimensions, on en conclut que $\ker f= \ker g$.
	\item Puisque $\dim(\ker f) = n-1 < \dim(E)$, il existe un élément $x_0$ de $E$ qui n'appartient pas à $\ker f$. 
	\item Puisque $x_0\notin \ker f$, $x_0\neq 0$ et donc $\dim(\Vect(x_0))=1$. Ainsi, $\dim(\ker f)+\dim(\Vect(x_0))=n-1+1=n = \dim(E)$. Il suffit donc de montrer que la somme est directe. \\Pour cela, prenons $x\in \ker f\cap \Vect(x_0)$. Il existe $\lambda \in \R$ tel que $x=\lambda x_0$. Puisque $x\in \ker f$, $f(x)=0$, c'est-à-dire $\lambda f(x_0)=0$. Or, par hypothèse, $f(x_0)\neq 0$, donc nécessairement $\lambda =0$ et finalement $x=0$.\\
		Ainsi, $\ker f\cap \Vect(x_0)=\{0\}$ et par égalité des dimensions, on en déduit bien que \[ \boxed{ \ker f \oplus \Vect(x_0) = E.}\]
	\item On va utiliser la décomposition précédente. Soit $x\in E$. Il existe $y\in \ker f$ et $z\in \Vect(x_0)$ tels que $x=y+z$. Puisque $z\in \Vect(x_0)$, il existe $\lambda \in \R$ tel que $z=\lambda x_0$. Mais alors :
\begin{align*}
h(x) &= h(y+\lambda x_0) = g(x_0)f(y+\lambda x_0) - f(x_0)g(y+\lambda x_0)\\
&= g(x_0) f(y) + \lambda g(x_0)f(x_0)-f(x_0)g(y) - \lambda f(x_0)g(x_0)\text{ par linéarité}\\
&= g(x_0)f(y)-f(x_0)g(y).	
\end{align*}
Or $y\in \ker f$, donc $f(y)=0$. Mais, d'après la question 5a) $\ker f= \ker g$, donc $y\in \ker f= \ker g$ et finalement $g(y)=0$. On conclut donc que $h(x)=0$. \\Ceci étant valable pour tout $x\in E$, on peut conclure que \underline{$h$ est l'application nulle.}
	\item Puisque $f(x_0)\neq 0$, et qu'il s'agit d'un réel (car $f$ est une forme linéaire), on peut écrire 
	\[ g = \frac{g(x_0)}{f(x_0)} f.\]
	On a montré que les applications $f$ et $g$ sont colinéaires. Ainsi, si $f$ et $g$ sont des formes linéaires non nulles telles que $\ker f\subset \ker g$ alors $f$ et $g$ sont colinéaires.
	\end{enumerate}
\end{enumerate}

\subsection*{Partie II -- Hyperplans et formes linéaires}

\begin{enumerate}[resume]
	\item \begin{enumerate}
	\item $(e_1,\hdots, e_{n-1})$ étant une base de $H\subset E$, il s'agit d'une famille libre de $E$. D'après le théorème de la base incomplète, on peut la compléter en une base de $E$. Puisque $\dim H = \dim E-1$, il existe donc un vecteur $e_n$ tel que $(e_1,\hdots, e_n)$ forme une base de $E$.
	\item $(e_1,\hdots, e_n)$ étant une base de $E$, $\phi$ est définie par l'image sur une base de $E$. L'application $\phi$ existe et est unique. $\phi$ étant non nulle (car $\phi(e_n)\neq 0$), la dimension du noyau est $n-1$. Remarquons que, par définition de $\phi$ :
		\[ \forall i\in \interent{1 p-1},\quad \phi(e_i)=0\]
		et donc, pour tout $i\in \interent{1 p-1}$, $e_i\in \ker \phi$. Puisque $\ker \phi$ est un espace vectoriel,
		\[ \Vect(e_1,\hdots, e_{p-1})\subset \ker \phi\iff H\subset \ker \phi.\]
		Puisque $\dim (\ker \phi)=\dim H$, on a bien démontré que $\ker \phi=H$.
	\end{enumerate}
	\item Raisonnons par double inclusion.
	\begin{itemize}
		\item[\envaleur{$\subset$}] Soit $x\in \ker f$. Ainsi, $f(x)=(0,0,\hdots, 0)$, c'est-à-dire $f_i(x)=0$ pour tout $i\in \interent{1 p}$ : $x\in \ker f_i$ pour tout $i\in \interent{1 p}$, c'est-à-dire $\ds x\in \bigcap_{i=1}^p \ker f$.
		\item [\envaleur{$\supset$}] Soit $x\in \ds\bigcap_{i=1}^p \ker f_i$. Alors, pour tout $i\in \interent{1 p}$, $f_i(x)=0$, et donc $f(x)=(0,\hdots, 0)$ : $x\in \ker f$.
	\end{itemize}
	\item \begin{enumerate}
	\item Puisque $f$ est surjective, tout élément de $\R^p$ admet au moins un antécédent par $f$. Puisque $\eps_1\in \R^p$, il existe $x\in E$ tel que $f(x)=\eps_1$.
	\item Soient $(\lambda_1,\hdots, \lambda_p)\in \R^p$ tels que 
	\[ \sum_{k=1}^n \lambda_i f_i = 0. \]
	Puisque $f(x)=\eps_1$, on en déduit que $f(x)=(1,0,\hdots,0)$, c'est-à-dire que $f_1(x)=1, f_2(x)=0,\hdots f_p(x)=0$. Appliquons l'égalité précédente en $x$ :
	\[ \sum_{k=1}^n \lambda_i f_i(x)=0 \implies \lambda_1 =0.\]
	En procédant de même pour tout vecteur $\eps_i$ de la base canonique de $\R^p$, on en déduit que $\lambda_1=\hdots =\lambda_p=0$. Ainsi, la famille \underline{$(f_1,\hdots, f_p)$ est libre.}
	\end{enumerate}
	\item \begin{enumerate}
	\item Puisque $f$ n'est pas surjective, $\Image f \neq \R^p$, et donc \[ \boxed{\dim(\Image f) \leq p-1.}\] 
	\item Complétons la base de $\Image f$, $(e_1,\hdots, e_m)$ en une base $(e_1,\hdots, e_n)$ de $E$. Remarquons alors que, puisque $m\leq p-1$ :
	\[ \Vect(e_1,\hdots, e_m) \subset \Vect(e_1,\hdots, e_{p-1}) = H.\]
	Puisque $H$ est de dimension $p-1$ (car la famille $(e_1,\hdots, e_{p-1})$, extraite d'une base, est libre), $H$ est un hyperplan. Ainsi, $\Image f$ est inclus dans un hyperplan de $\R^p$.
	\item	D'après ce qui précède, $\Image f\subset H$, et d'après la question $6$, $H=\ker \phi$ pour une forme linéaire $\phi$ non nulle. Ainsi,
	\[ \forall x\in E,\quad \phi(f(x))=0 \]
	Puisque $\phi$ est une forme linéaire de $\R^p$, alors, d'après les préliminaires, il existe $(\lambda_1,\hdots, \lambda_p)\in \R^p$ tel que 
	\[ \phi(x_1,\hdots, x_n) = \lambda_1x_1+\hdots +\lambda_px_p.\]
	Les $\lambda_i$ ne sont pas tous nuls, puisque $\phi$ n'est pas nulle. Mais alors
	\[ \forall x\in E,\, \phi(f(x))=0 \implies \forall x\in E,\, \lambda_1 f_1(x)+\hdots +\lambda_pf(x)=0\]
	c'est-à-dire $\lambda_1 f_1+\hdots +\lambda_nf_n=0$ : $(f_1,\hdots,f_n)$ est une famille liée.
	\end{enumerate}
	\item \begin{enumerate}
	\item On a montré à la question 9 que si $f$ n'est pas surjective, alors $(f_1,\hdots, f_p)$ est liée. Par contraposée, si $(f_1,\hdots,f_p)$ est libre, alors $f$ est surjective.
	\item Puisque $f$ est surjective, $\dim(\Image f) = \dim(\R^p) = p$. Le théorème du rang appliqué à $f$, nous donne
		\[ n =\dim(E) = \dim(\ker f)+\dim(\Image f) = \dim(\ker f)+ p.\]
		Or, d'après la question 7, $\ker f = \ds \bigcap_{i=1}^p \ker f_i$. Finalement
		\[ \boxed{ \dim\left(\bigcap_{i=1}^p \ker f_i\right) = n-p.}\]
	\end{enumerate}
\end{enumerate}
\end{correction}
%%% Fin exercice %%%
