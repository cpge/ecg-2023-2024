%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{15}{2}[Après le ski, le saut en hauteur]
   Un sauteur tente de franchir des barres successives numérotées. II n'essaye de franchir la barre de hauteur $k \in \N*$ que s'il a réussi à passer celle de hauteur $k-1$.

   Si $n \in \N*$, la probabilité de sauter la $n$-ième barre, sachant qu'il a sauté les $n-1$ premières, est $\frac{1}{n}$. On suppose qu'il saute la première barre avec probabilité $1$. Notons $X$ la variable aléatoire égale au numéro de la dernière barre franchie correctement.
\begin{enumerate}
\item Calculer $\PP(X>n)$ pour tout $n \in \N$ et en déduire que $X$ est presque sûrement finie.
\item Détermine la loi de $X$.
\item Calculer l'espérance et la variance de $X$ (si elles existent).
\end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item Soit $n\in \N*$. Pour tout $k\in \N*$, on note $B_k$ l'événement \guill{il a franchi la $k$\ieme barre}. D'après l'énoncé :
	\begin{align*}
		\PP(X>n) &= \PP(B_1\cap \hdots \cap B_n\cap B_{n+1}) \\
				&= \PP(B_1)\PP_{B_1}(B_2)\hdots \PP_{B_1\cap\hdots\cap B_{n}}(B_{n+1}) \text{ d'après la formule des probabilités composées}\\
				&= 1\times \frac12 \times \hdots \times \frac1n\times \frac{1}{n+1} \text{ d'après l'énoncé}\\
				&= \frac{1}{(n+1)!}.	
	\end{align*}
	Ainsi, $\PP(X\leq n) = 1-\frac{1}{(n+1)!}$. Finalement, d'après la propriété de la limite monotone, puisque $(X\leq n) \subset (X\leq n+1)$ :
	\begin{align*}
	\PP(X<\infty) &= \PP\left( \bigcup_{n=1}^{+\infty} (X\leq n)\right) \\
				  &= \lim_{n\to +\infty} \PP(X\leq n) = 1.
	\end{align*}
	Ainsi, \fbox{la variable aléatoire $X$ est presque sûrement finie.}\vspace*{.1cm}
	\item D'après l'énoncé et ce qui précède, remarquons déjà que $X(\Omega) = \N* \cup \{\infty\}$ et que $\PP(X=\infty) = 0$. Soit $n\in \N*$. Si $n\geq 2$ : 
		\begin{align*}
		\PP(X=n) &= \PP(X>n-1) - \PP(X>n) \\
				 &= \frac{1}{n!} - \frac{1}{(n+1)!} = \frac{n}{(n+1)!}
		\end{align*}
	    et si $n=1$,
	    \begin{align*}
	     \PP(X=1) &= \PP(B_1\cap \overline{B_2}) = \frac{1}{2}.	
	    \end{align*}
	  Ainsi, pour tout $n\in \N*$,
	  \[ \boxed{ \PP(X=n)= \frac{n}{(n+1)!}.}\]
	Remarquons, pour confirmation, que :
	\begin{align*}
	\sum_{n=1}^{+\infty} \PP(X=n) &= \sum_{n=1}^{+\infty} \frac{n}{(n+1)!}= \sum_{n=1}^{+\infty} \frac{n+1-1}{(n+1)!}\\
	&= \sum_{n=1}^{+\infty} \frac{1}{n!} - \sum_{n=1}^{+\infty} \frac{1}{(n+1)!} \text{  car les séries convergent}\\
	&= \sum_{n=1}^{+\infty} \frac{1}{n!} - \sum_{n=2}^{+\infty} \frac{1}{n!} \text{  par changement d'indice}\\
	&= 1.
	\end{align*}
	\item Rappelons que $\PP(X=\infty)=0$. $X$ admet une espérance si et seulement si $\ds\sum_{n\geq 1} n\PP(X=n)$ est absolument convergente, et donc convergente puisqu'elle est à terme positif. 
	
	Or $n\PP(X=n)=\frac{n^2}{(n+1)!} = \petito[+\infty]{\frac{1}{n^2}}$ puisque \[ n^2 \frac{n^2}{(n+1)!} = \frac{n^4}{(n+1)!} \tendversen{n\to +\infty} 0 \text{ par croissances comparées.}\]
	
	Ainsi, $X$ admet une espérance.
	Remarquons alors que 
	\begin{align*}
	\sum_{n=1}^{+\infty} n\PP(X=n) &= \sum_{n=1}^{+\infty} \frac{n^2}{(n+1)!} = \sum_{n=1}^{+\infty} \frac{n(n+1) - n}{(n+1)!} \\
	&= \sum_{n=1}^{+\infty} \frac{n(n+1)}{(n+1)!} - \sum_{n=1}^{+\infty} \frac{n}{(n+1)!} \text{ car les séries convergent (exp.)}\\
	&= \sum_{n=1}^{+\infty} \frac{1}{(n-1)!} - 1 \text{ d'après le calcul précédent} \\
	&= \eu{1}-1.
	\end{align*}
	Ainsi, $X$ admet une espérance et \[ \boxed{\esperance(X) = \E-1 \approx 1,7.}\]
	Déterminons l'éventuel moment d'ordre $2$ de la variable aléatoire.
	\begin{align*}
	\esperance(X^2) &= \sum_{n=1}^{+\infty} n^2\PP(X=n) = \sum_{n=1}^{+\infty} \frac{n^3}{(n+1)!}	
	\end{align*}
	La série est clairement convergente (par exemple, parce que $\frac{n^3}{(n+1)!} = \petito[+\infty]{\frac{1}{n^2}}$). Faisons le même genre de manipulation que précédemment.
	\begin{align*}
	\esperance(X^2) &= \sum_{n=1}^{+\infty} \frac{(n+1)n(n-1) +n}{(n+1)!} \\
		&= \sum_{n=1}^{+\infty} \frac{(n+1)n(n-1)}{(n+1)!} + \sum_{n=1}^{+\infty} \frac{n}{(n+1)!} \text{  car les séries convergent}	\\
		&= \sum_{n=2}^{+\infty} \frac{(n+1)n(n-1)}{(n+1)!} +  1 \text{ car le terme est nul en $n=1$}\\
		&= \sum_{n=2}^{+\infty} \frac{1}{(n-2)!} + 1 = \eu{1} + 1 
	\end{align*}
	Finalement, d'après la formule de Koenig-Huygens :
	\begin{align*}
	\Var(X) &= \esperance(X^2)-\left(\esperance(X)\right)^2 = \E+1 - (\E-1)^2=\E-1-(\eu{2}-2\E+1)=3\E-\E^2.
	\end{align*}
	Ainsi, \[ \boxed{\Var(X) = 3\E-\E^2 \approx 0,77.}\]


\end{enumerate}
\end{correction}
%%% Fin exercice %%%
