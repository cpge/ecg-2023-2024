%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{15}{2}[Un temps d'attente d'une séquence]
On réalise une suite de lancers indépendants d'une pièce de monnaie dont la probabilité d'obtenir Pile est $p \in \interoo{0 1}$.

On note $X$ la variable aléatoire comptant le nombre de lancers nécessaires pour que la séquence $P F$ apparaisse pour la première fois.
\begin{enumerate}
\item Pour tout $n \in \N \setminus \{0,1\}$, montrer que $\PP(X=n)=\sum\limits_{j=1}^{n-1} p^{j}(1-p)^{n-j}$ puis simplifier cette somme.
\end{enumerate}

Pour tout $k \in \N*$, on introduit l'événement $P_{k}$ : \og{}obtenir Pile au $k$-ième lancer\fg{}.
\begin{enumerate}[resume]
\item Justifier que $X$ ne prend que des valeurs finies.
\item Calculer l'espérance de $X$ si elle existe.
\end{enumerate}
\end{exoApp}

\begin{correction}
\allowdisplaybreaks{
Introduisons, dans l'ensemble de l'exercice, $P_n$ (respectivement $F_n$) l'événement \guill{obtenir pile au $n$-ième lancer} (respectivement \guill{obtenir face}).
\begin{enumerate}
  \item Soit $n\geq 2$. Supposons que l'on ait obtenir $PF$ au $n$-ième lancer. Pour ne pas avoir eu la séquence $PF$ avant, on n'autorise que les séquences $PP$, $FF$ et $FP$, c'est-à-dire, finalement, les tirages \[\underbrace{\underbrace{F~\hdots~F}_{\text{entre $0$ et $n-1$}} ~~~\underbrace{P~\hdots~P}_{\text{entre $1$ et $n-1$}} F}_{\text{$n$ au total}} .\]
  Ainsi,
  \[ [X=n] = \bigcup_{j=1}^{n-1} F_1\cap \hdots \cap F_{n-j-1}  \cap P_{n-j} \cap \hdots \cap P_{n-1}\cap F_n. \]
  soit, par incompatibilité et indépendance des tirages :
  \begin{align*}
    \boxed{\PP(X=n)} &= \sum_{j=1}^{n-1} \PP(F_1)\times \hdots \times \PP(F_{n-j-1})\times \PP(P_{n-j})\times \hdots \times \PP(P_{n-1})\times \PP(F_n) \\
    &= \sum_{j=1}^{n-1}   \underbrace{(1-p)\times \hdots \times (1-p)}_{n-j-1\text{ fois}} \times \underbrace{p\times \hdots \times p}_{j \text{ fois}} \times (1-p)\\
    &= \boxed{\sum_{j=1}^{n-1} (1-p)^{n-j} p^j}.
  \end{align*}
  On va ré-écrire cette somme, en reconnaissant une somme des termes d'une suite géométrique.
  \begin{align*}
    \PP(X=n) &= \sum_{j=1}^{n-1} p^j(1-p)^n (1-p)^{-j} \text{   car }p\neq 1\\
    &= (1-p)^{n} \sum_{j=1}^{n-1} \left(\frac{p}{1-p}\right)^j
  \end{align*}
  Remarquons que $\frac{p}{1-p}=1 \iff p=\frac12$.
  \begin{itemize}
    \item Si $p=\frac12$, alors $P(X=n) = (n-1)(1-p)^n$
    \item Si $p\neq \frac12$, alors
    \begin{align*}
      \PP(X=n) &= (1-p)^n \frac{\frac{p}{1-p}- \left(\frac{p}{1-p}\right)^n}{1-\frac{p}{1-p}}\\
      &= (1-p)^n \frac{p-\frac{p^n}{(1-p)^{n-1}} }{1-2p}\\
      &= \frac{p(1-p)^n - (1-p)p^n}{1-2p}
    \end{align*}
  \end{itemize}
  \textbf{Bilan} :
  \[ \boxed{\forall n\in \N\setminus\{0,1\},\quad \PP(X=n) = \left \{ \begin{array}{rcl} (n-1)(1-p)^n&\text{si}& p=\frac12\vspace*{.2cm}\\ \dfrac{p(1-p)^n-(1-p)p^n}{1-2p}&\text{si}&p\neq \frac12\end{array}\right.} \]
  \item Par définition, $X(\Omega)=\interfo{2 +\infty}\cup \{+\infty\}$. Calculons la probabilité que $X$ soit fini, en remarquant que la série converge car c'est la probabilité d'un événement. Si $p\neq \frac12$.
  \begin{align*}
    \PP(X<+\infty) &= \sum_{n=2}^{+\infty} \PP(X=n) \\
      =& \sum_{n=2}^{+\infty} \frac{p(1-p)^n-(1-p)p^n}{1-2p} \\
      &= \frac{1}{1-2p} \left( p\sum_{n=2}^{+\infty} (1-p)^n - (1-p)\sum_{n=2}^{+\infty} p^n \right)\\
      &= \frac{1}{1-2p}\left( p \frac{(1-p)^2}{1-(1-p)} - (1-p) \frac{p^2}{1-p} \right) \text{ en reconnaissant des séries géométriques } (p\in \interoo{0 1})\\
      &= \frac{1}{1-2p} ( (1-p)^2-p^2) = 1.
  \end{align*}
  Si $p=\frac12$, on utilisant une série géométrique dérivée première à un terme près :
  \begin{align*}
    \PP(X<+\infty) &= \sum_{n=2}^{+\infty} (n-1)(1-p)^n \\
    &= \sum_{k=1}^{+\infty} k(1-p)^{k+1} \text{ en posant }k=n-1\\
    &= (1-p)^2\sum_{k=1}^{+\infty} k(1-p)^{k-1} \\
    &= (1-p)^2 \frac{1}{(1-(1-p))^2} = \frac{(1-p)^2}{p^2}=1 \text{ car } p =\frac12.
  \end{align*}
  Ainsi, dans tous les cas, $\PP(X<+\infty)=1$. Puisque $( [X=n] )_{n\in \interfo{2 +\infty}\cup \{+\infty\}}$ forme un système complet d'événements, on a
  \[ \sum_{n=2}^{+\infty} \PP(X=n) + \PP(X=+\infty) =1 \]
  soit, d'après ce qui précède, $\PP(X=+\infty) = 0$.

  On peut conclure que la variable aléatoire $X$ prend des valeurs finies presque sûrement.
  \item Il faut tout d'abord montrer que la série $\sum n\PP(X=n)$ est absolument convergente, soit ici simplement convergente car à terme positif. Pour $p\neq \frac12$, on reconnait des séries géométriques :
  \begin{align*}
    \sum_{n=2}^{+\infty} n\PP(X=n) &= \frac{1}{1-2p}\left( p\sum_{n=2}^{+\infty} n(1-p)^n - (1-p)\sum_{n=2}^{+\infty} np^n \right)\\
    &= \frac{1}{1-2p} \left( p \left(\sum_{n=1}^{+\infty} n (1-p)^n - (1-p)\right) -(1-p) \left(\sum_{n=1}^{+\infty} np^n - p \right)\right)\\
    &= \frac{1}{1-2p} \left( p\frac{(1-p)}{(1-(1-p))^2} - (1-p)\frac{p}{(1-p)^2}   \right)\\
    &= \frac{1}{1-2p}\frac{(1-p)^2 -p^2}{p(1-p)} = \frac{1}{p(1-p)}
  \end{align*}
  Ainsi, $X$ admet une espérance et $\boxed{\esperance(X)=\frac{1}{p(1-p)}}$.

  Si $p=\frac12$, en reconnaissant une série géométrique dérivée seconde :
  \begin{align*}
    \sum_{n=2}^{+\infty} n\PP(X=n) &= \sum_{n=2}^{+\infty} n(n-1)(1-p)^n \\
    &= \frac{2(1-p)^2}{(1-(1-p))^3} = 4.
  \end{align*}
  Remarquons que ce résultat est cohérent avec le cas général, car
  \[ \frac{1}{p(1-p)}=\frac{1}{\frac{1}{2} \times \frac{1}{2}} = 4. \]
\end{enumerate}}
\end{correction}
%%% Fin exercice %%%
