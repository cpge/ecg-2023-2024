%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{20}{3}[Loi de Pascal]
  Soit $n \in \N*$. On considère une épreuve de Bernoulli. Soit $p \in \interoo{0 1}$ la probabilité d'obtenir un succès. On répète de façon indépendante cette expérience de Bernoulli et on note $S_{n}$ la variable aléatoire égale au nombre d'expériences réalisées pour obtenir le $n$-ième succès.

  On définit, pour $k\in \N$ tel que $k\geq n$, les événements
  \begin{itemize}
      \item $A_k$ : \og{} la $k$-ième expérience est un succès \fg{},
      \item $B_{n, k}$ : \og{} au cours des $k-1$ premières expériences, on a obtenu $n-1$ succès.
    \end{itemize}
  \begin{enumerate}
\item Soit $k \in \N$ tel que $k \geqslant n$. Exprimer l'événement $\left[S_{n}=k\right]$ au moyen des événements $A_{k}$ et $B_{n, k}$.
\item Calculer $\PP\left(A_{k}\right)$ et $\PP\left(B_{n, k}\right)$ et en déduire la loi de $S_{n}$ :
  \[
  \forall k \geqslant n \quad \PP\left(S_{n}=k\right)=\binom{k-1}{n-1} p^{n}(1-p)^{k-n} .
  \]
  On dit que $S_{n}$ suit une loi de Pascal de paramètres $n$ et $p$. Pour $n=1$, il s'agit de la loi géométrique de paramètre $p$.
\item  En déduire que
  \[
  \forall x \in \interoo{0 1},\quad \forall n \in \N^{*}, \quad \sum_{k=n}^{+\infty} \binom{k-1}{n-1} x^{k-n}=\frac{1}{(1-x)^{n}}
  \]
\item Calculer l'espérance et la variance de $S_{n}$, à l'aide de la question $3$.
\end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item Soit $k\in \N$ tel que $k\geq n$. $[S_n=k]$ désigne l'événement \guill{il a fallu $k$ expériences pour obtenir $n$ succès.}. Dis autrement, on a obtenu le $n$-ieme succès lors du $k$-ième tirage. Ainsi,
		\[ \boxed{[S_n=k] = B_{n,k} \cap A_k.} \]
	\item Tout d'abord, par définition, $S_n(\Omega)=\ll n +\infty\ll$ puisqu'il faut au moins $n$ expériences pour obtenir $n$ succès.
	
	Soit $k\geq n$. Les expériences étant successives et indépendantes, $A_k$ et $B_{n,k}$ sont indépendants. Ainsi
		\[ \PP(S_n=k) = \PP(A_k)\PP(B_{n,k}).\]
		\begin{itemize}
			\item $A_k$ désigne l'événement \guill{obtenir un succès lors de la $k$-ième expérience}. Ainsi, d'après l'énoncé : $\PP(A_k)=p$.
			\item $B_{n,k}$ désigne l'événement \guill{lors des $k-1$ premières expériences, on a obtenu $n-1$ succès}. Puisque on répète une expérience de Bernoulli $k-1$ fois, le nombre de succès $X$ obtenus lors des $k-1$ premières expériences suit une loi binomiale de paramètre $k-1$ et $p$. Ainsi
				\begin{align*}
				\PP(B_{n,k}) &= 	\PP(X=n-1) = \binom{k-1}{n-1}p^{n-1}(1-p)^{k-1-(n-1)} = \binom{k-1}{n-1} p^{n-1}(1-p)^{k-n}.
				\end{align*}
		\end{itemize}
		Finalement
		\[ \boxed{\forall k\geq n,\quad \PP(S_n=k) = \binom{k-1}{n-1}p^n(1-p)^{k-n}.}\]
	\item Puisque $S_n$ est une variable aléatoire,
		\[ \sum_{k=n}^{+\infty} \binom{k-1}{n-1}p^n(1-p)^{k-n} = 1\]
		soit
		\[ p^n\sum_{k=n}^{+\infty} \binom{k-1}{n-1}(1-p)^{n-k} = 1. \]
		En divisant par $p^n$, et en notant $x=1-p\in \interoo{0 1}$ :
		\[ \boxed{\forall x\in \interoo{0 1},\quad \sum_{k=n}^{+\infty} \binom{k-1}{n-1} x^{k-n} = \frac{1}{(1-x)^n}.}\]
	\item $S_n$ admet une espérance si et seulement si la série $\ds\sum_{k\geq n} k\PP(S_n=k)$ est absolument convergente et donc convergente, car à termes positifs. Remarquons alors que 
	\begin{align*}
	\sum_{k=n}^{+\infty} k\PP(S_n=k) &= \sum_{k=n}^{+\infty} k\binom{k-1}{n-1}p^n(1-p)^{k-n} \\
	&= \sum_{k=n}^{+\infty} n\binom{k}{n}p^n(1-p)^{k-n} \text{ d'après la formule du chef}\\
	&= n \sum_{m=n+1}^{+\infty} \binom{m-1}{n}p^n(1-p)^{m-1-n} \text{ en posant $m=k+1$}\\
	&= np^n \sum_{m=n+1}^{+\infty} \binom{m-1}{n}(1-p)^{m-(n+1)}\\
	&= np^n \frac{1}{(1-(1-p))^{n+1}} \text{ en utilisant la formule précédente en $n+1$}
	\end{align*}
	Ainsi, $S_n$ admet une espérance et 
	\[ \boxed{\esperance(S_n) = \frac{n}{p}.}\]
	De même, déterminons l'éventuel moment d'ordre $2$ de $S_n$. Sous réserve de convergence :
	\begin{align*}
	\esperance(S_n^2) &= \sum_{k=n}^{+\infty} k^2 \PP(S_n=k)\\
	&= \sum_{k=n}^{+\infty} k^2 \binom{k-1}{n-1}p^n(1-p)^{k-n}\\
	&=  \sum_{k=n}^{+\infty} kn\binom{k}{n}p^n(1-p)^{k-n} \text{ par la formule du chef}\\
	&= n\sum_{m=n+1}^{+\infty} (m-1)\binom{m-1}{n}p^n(1-p)^{m-(n+1)} \text{ en posant $m=k+1$}\\
	&= \frac{n}{p} \sum_{m=n+1}^{+\infty} m\binom{m-1}{n}p^{n+1}(1-p)^{m-(n+1)} - np^n\sum_{m=n+1}^{+\infty} \binom{m-1}{n}(1-p)^{m-(n+1)} \text{ par linéarité}.
	\end{align*}
	Or la première somme désigne l'espérance $S_{n+1}$ et la deuxième a déjà été calculée lors de l'espérance. Ainsi
	\begin{align*}
	\esperance(S_n^2) &= \frac{n}{p}\frac{n+1}{p} - np^n\frac{1}{p^{n+1}}= \frac{n(n+1)-np}{p^2}	
	\end{align*}
	Ainsi, $S_n$ admet un moment d'ordre $2$ et donc une variance. 
	D'après la formule de Koenig Huygens :
	\begin{align*}
	\Var(S_n) &= \esperance(S_n^2)-(\esperance(S_n))^2	\\
	&= \frac{n(n+1)-np}{p^2} - \frac{n^2}{p^2} = \frac{n-np}{p^2} = \frac{n(1-p)}{p^2}
	\end{align*}
	Ainsi,
	\[ \boxed{\Var(S_n) = \frac{nq}{p^2} \text{ avec } q=1-p.}\]



\end{enumerate}
\end{correction}
%%% Fin exercice %%%
