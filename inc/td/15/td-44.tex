%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{30}{2}[Intégrales de Wallis]
Pour tout entier naturel $n$, on pose \[W_n=\int_0^{\frac{\pi}{2}} \left(\cos t\right)^n\dd  t.\]

\subsection*{1 - Premières propriétés}

\begin{enumerate}
	\item Calculer $W_0$ et $W_1$.
	\item Montrer que la suite $(W_n)$ est décroissante.
	\item Justifier que, pour tout entier $n$, $W_n>0$.
	\item \`A l'aide du changement de variable $u=\frac{\pi}{2}-t$, montrer que pour tout entier naturel $n$, \[ W_n=\int_0^{\frac{\pi}{2}} \left(\sin t\right)^n\dd  t. \] 
\end{enumerate}

\subsection*{2 - Valeurs de $W_n$}

\begin{enumerate}
	\item Montrer, à l'aide d'une intégration par parties, que pour tout entier $n\in \N$,
	\[ (n+2)W_{n+2} = (n+1)W_n. \]
	\item En déduire que, pour tout entier $n$, \[ W_{2n} = \frac{(2n)!}{(2^n\times n!)^2} \frac{\pi}{2}. \]
	\textit{On peut le faire par récurrence ou par produit télescopique. Il est conseillé de faire les deux.}
	\item Montrer que la suite $((n+1)W_{n+1}W_n)$ est constante et calculer la valeur de cette constante.
	\item En déduire la valeur de $W_{2n+1}$ pour tout entier $n$.
\end{enumerate}

\subsection*{3 - \'Equivalent de $(W_n)$}

\begin{enumerate}
	\item En utilisant les résultats de la deuxième partie, justifier que 
	\[ W_{n+2}\eq{+\infty} W_{n}. \]
	\item Montrer que, pour tout $n$
	\[ \frac{n+1}{n+2}\leq \frac{W_{n+1}}{W_n}\leq 1.\]
	En déduire que \[ W_n \eq{+\infty} W_{n+1}.\]
	\item Montrer alors que $\ds (W_n)^2 \eq{+\infty} \frac{\pi}{2n}$ puis que 
	\[ W_n \eq{+\infty} \sqrt{\frac{\pi}{2n}}.\]
	Quelle est la limite de $(W_n)$ ?
\end{enumerate}

\end{exoApp}

\begin{correction}
\subsection*{1 - Premières propriétés}

\begin{enumerate}
	\item Rapidement 
	\begin{align*}
	W_0 &= \int_0^{\frac{\pi}{2}} 1\dd t = \frac{\pi}{2}\\
	W_1 &= \int_0^{\frac{\pi}{2}} \cos t \dd t =\left[\sin t\right]_0^{\frac{\pi}{2}} = 1	
	\end{align*}
	\item Pour tout $n\in \N$,
	\begin{align*}
		W_{n+1}-W_n &= \int_0^{\frac{\pi}{2}} (\cos t)^{n+1}\dd t - \int_0^{\frac{\pi}{2}} (\cos t)^n \dd t \\
		&= \int_0^{\frac{\pi}{2}} \left( (\cos t)^{n+1} - (\cos t)^n \right)\dd t &&\text{par linéarité}\\
		&= \int_0^{\frac{\pi}{2}} (\cos t)^n\left(\cos t - 1\right)\dd t
	\end{align*}
	Or, sur $\interff{0 {\frac{\pi}{2}}}$, $\cos t \geq 0$ et $\cos t -1 \leq 0$. Ainsi, $t\donne (\cos t)^{n+1}(\cos t - 1)\leq 0$ sur $\interff{0 {\frac{\pi}{2}}}$. Puisque $0<\frac{\pi}{2}$, par positivité de l'intégrale 
	\[ W_{n+1}-W_n = \int_0^{\frac{\pi}{2}} (\cos t)^n \left( \cos t-1\right)\dd t \leq 0. \]
	Ainsi, la suite $(W_n)$ est décroissante.
	\item Pour tout $t\in \interff{0 \frac{\pi}{2}}$, la fonction $t\donne (\cos t))^n$ est continue, positive et non identiquement nulle. Par positivité de l'intégrale et continuité 
		\[ \forall n\in \N,\quad W_n>0. \]
	\item Posons $u=\frac{\pi}{2}-t$, c'est-à-dire $t=\frac{\pi}{2}-u$. Si $t=0$,  $u = \frac{\pi}{2}$ et si $t=\frac{\pi}{2}$, $u=0$.\\
		La fonction $u\donne \frac{\pi}{2}-u$ est de classe $\CC^1$ sur $\interff{0 \frac{\pi}{2}}$. De plus, $\dd t = -\dd u$. Par changement de variable :
		\begin{align*}
			\boxed{W_n} &= \int_{\frac{\pi}{2}}^0 \left(\cos\left(\frac{\pi}{2}-u\right)\right)^n \left(-\dd u\right)	\\
			&= \boxed{\int_0^{\frac{\pi}{2}} \left(\sin u\right)^n\dd u.}
		\end{align*}
\end{enumerate}

\subsection*{2 - Valeurs de $W_n$}

\begin{enumerate}
	\item Soit $n\in \N$. Par définition, $\ds W_{n+2} = \int_0^{\frac{\pi}{2}} (\cos t)^{n+2}\dd t$.\\
		Posons $u:t\donne (\cos t)^{n+1}$ et $v:t\donne \sin t$. $u$ et $v$ sont de classe $\CC^1$ sur $\interff{0 \frac{\pi}{2}}$. Remarquons que $(\cos t)^{n+2}=u(t)v'(t)$, et $u':t\donne -(n+1)\sin t(\cos t)^n$. Par intégration par parties 
		\begin{align*}
		W_{n+2} &= \left[(\cos t)^{n+1}\sin t\right]_0^{\frac{\pi}{2}} - \int_0^{\frac{\pi}{2}} -(n+1)\sin t(\cos t)^n\sin t\dd t\\
		&= 0 + (n+1)\int_0^{\frac{\pi}{2}} (\cos t)^n (\sin t)^2 \dd t \\
		&= 	(n+1)\int_0^{\frac{\pi}{2}} (\cos t)^n \left(1- (\cos t)^2 \right)\dd t \\
		&= (n+1)\int_0^{\frac{\pi}{2}} (\cos t)^{n}\dd t - (n+1)\int_0^{\frac{\pi}{2}} (\cos t)^{n+2} \dd t  = (n+1)W_{n}-(n+1)W_{n+2}\\
		\end{align*}
Ainsi, \[ \boxed{\forall n\in \N,\quad (n+2)W_{n+2}=(n+1)W_n.}\]
	\item \textit{Première méthode : produit télescopique}. On remarque que, par télescopage :
	\begin{align*}
	W_{2n} &= \frac{W_{2n}}{W_{2(n-1)}}\times \frac{W_{2(n-1)}}{W_{2(n-2)}}\times \hdots \times \frac{W_2}{W_0} \times W_0\\
	&= \frac{2n-1}{2n}\times \frac{2n-3}{2n-2}\times \hdots \times \frac{1}{2} \times \frac{\pi}{2}\\
	&= \frac{(2n-1)\times(2n-3)\times\hdots\times 1}{2^nn!} \frac{\pi}{2}\\
	&= \frac{(2n)!}{(2^nn!)^2}\frac{\pi}{2}.
	\end{align*}
	\textit{Deuxième méthode : récurrence}. \\Soit $P$ la proposition définie pour tout $n\in \N$ par $P_n$ : \guill{$\ds W_{2n}=\frac{(2n)!}{(2^nn!)^2}\frac{\pi}{2}$}.
	\begin{itemize}
		\item Pour $n=0$, $\frac{(2\times 0)!}{(2^0 0!)^2} \frac{\pi}{2} = \frac{\pi}{2}=W_0$. Ainsi, $P_0$ est vérifiée.
		\item Supposons la proposition $P_n$ vérifiée pour un certain entier $n$ fixé. En utilisant la relation de la première question :
		\begin{align*}
		W_{2(n+1)} =W_{2n+2}&= \frac{2n+1}{2n+2}W_{2n} \\
		&= \frac{2n+1}{2(n+1)} \frac{(2n)!}{(2^nn!)^2}\frac{\pi}{2} &&\text{par hypothèse de récurrence}\\
		&= \frac{(2n+2)(2n+1)}{2^2(n+1)^2} \frac{(2n)!}{(2^nn!)^2}\frac{\pi}{2}\\
		&= \frac{(2n+2)!}{2^2(n+1)^2 (2^nn!)^2}\frac{\pi}{2}= \frac{(2n+2)!}{(2^{n+1}(n+1)!)^2}\frac{\pi}{2}
		\end{align*}
	et $P_n$ est donc vérifiée.
	\end{itemize}
	D'après le principe de récurrence, $P_n$ est vraie pour tout $n$, et finalement 
	\[ \boxed{\forall n\in \N,\quad W_{2n} = \frac{(2n)!}{(2^nn!)^2}\frac{\pi}{2}.}\]
	\item Notons, pour tout entier $n$, $u_n=(n+1)W_{n+1}W_n$. On remarque, en utilisant la relation de la question $1$ :
	\begin{align*}
	 u_{n+1} &= \underbrace{(n+2)W_{n+2}}_{=(n+1)W_n}W_{n+1} = (n+1)W_{n}W_{n+1}=u_n.	
	\end{align*}
	La suite $(u_n)$ est donc constante, égale à $u_0=W_0W_1=\frac{\pi}{2}$. Ainsi,
	\[ \boxed{\forall n\in \N,\quad (n+1)W_{n+1}W_n = \frac{\pi}{2}.}\]
	\item Soit $n\in \N$. En utilisant la relation précédente, $\ds (2n+1)W_{2n+1}W_{2n} = \frac{\pi}{2} $ et donc
	\begin{align*}
	 \boxed{W_{2n+1}} = \frac{\pi}{2}\frac{1}{(2n+1)W_{2n}} = \frac{(2^nn!)^2}{(2n+1)(2n)!}=\boxed{\frac{(2^nn!)^2}{(2n+1)!}.}
	\end{align*}
\end{enumerate}

\subsection*{\'Equivalent de $(W_n)$}

\begin{enumerate}
	\item En utilisant la relation de la question $1$ de la partie précédente,
	\[ \frac{W_{n+2}}{W_n} = \frac{n+1}{n+2} \tendversen{n\to +\infty} 1.\]
	Ainsi, $W_{n+2} \eq{+\infty} W_n$.
	\item Soit $n\in \N$. La suite $(W_n)$ est décroissante. Ainsi
	\[ W_{n+2} \leq W_{n+1} \leq W_n \]
	soit, en divisant par $W_n>0$ :
	\[ \frac{W_{n+2}}{W_n} \leq \frac{W_{n+1}}{W_n} \leq 1 \]
	puis, en utilisant toujours la même relation 
	\[ \frac{n+1}{n+2} \leq \frac{W_{n+1}}{W_n}\leq 1.\]
	Puisque $\frac{n+1}{n+2}\tendversen{n\to \infty} 1$, par encadrement, on en déduit que $\frac{W_{n+1}}{W_n}\tendversen{n\to +\infty} 1$ et finalement 
	\[ \boxed{W_n \eq{+\infty} W_{n+1}.}\]
	\item Rappelons que, pour tout $n$, $(n+1)W_{n+1}W_n = \frac{\pi}{2}$. Puisque 
	\[ n+1\eq{+\infty} n \qeq W_{n+1}\eq{+\infty} W_n \]
	par produit des équivalents 
	\[ (n+1)W_{n+1}W_n \eq{+\infty} n\left(W_n\right)^2 \]
	et puisque $(n+1)W_{n+1}W_n =\frac{\pi}{2}$, on obtient finalement 
	\[ n\left(W_n\right)^2 \eq{+\infty} \frac{\pi}{2} \text{ soit } \left(W_n\right)^2 \eq{+\infty} \frac{\pi}{2n}\]
	Par exponentiation :
	\[ \boxed{W_n\eq{+\infty} \sqrt{\frac{\pi}{2n}}.} \]
	Ainsi, puisque $\sqrt{\dfrac{\pi}{2n}} \tendversen{n\to +\infty} 0$, on en déduit que 
	\[ \boxed{W_n\tendversen{n\to +\infty} 0.}\]
\end{enumerate}

\end{correction}
%%% Fin exercice %%%
