%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{25}{2}[Suite d'intégrales II]
	Pour tout entier $n$, on note
$$I_n=\int_0^1 \frac{t^n}{1+t^2}\dd t \textrm{  et  } J_n=\int_0^1 t^n \ln(1+t^2) \dd t$$
\begin{enumerate}
    \item Déterminer la monotonie des suites $I$ et $J$.
    \item Montrer que pour tout $n$, $0\leq I_n\leq \frac{1}{n+1}$. En déduire la limite de $(I_n)$.
    \item Par un intégration par parties, démontrer que pour tout $n$, $$J_n=\frac{\ln(2)}{n+1}-\frac{2}{n+1}I_{n+2}$$
    En déduire la limite de $(J_n)$ puis celle de $(n\times J_n)$.
\end{enumerate}
\end{exoApp}

\begin{correction}
Les fonctions $x\mapsto \frac{t^n}{1+t^2}$ et $x\mapsto t^n\ln(1+t^2)$ sont définies et continues sur $[0;1]$, donc toutes les intégrales considérées existent.
\begin{enumerate}
	\item Pour tout entier $n$, et par linéarité de l'intégrale :
		$$I_{n+1}-I_n=\int_0^1 \frac{t^{n+1}}{1+t^2}\dd t=\int_0^1 \frac{t^n}{1+t^2}\dd t = \int_0^1 \frac{t^{n+1}-t^n}{1+t^2}\dd t = \int_0^1 \frac{t^n(t-1)}{1+t^2}\dd t$$
		Or, sur $[0;1]$, $t^n\geq 0$, $t-1\leq 0$ et $1+t^2>0$. Par quotient, $\displaystyle{\frac{t^n(t-1)}{1+t^2}\leq 0}$ sur $[0;1]$. Par positivité de l'intégrale ($0<1$), on a donc $\displaystyle{\int_0^1 \frac{t^n(t-1)}{1+t^2}\dd t \leq 0}$.
		$$\boxed{I_{n+1}-I_n \leq 0 : \textrm{ la suite $(I_n)$ est décroissante.}}$$
		De même,
		$$J_{n+1}-J_n=\int_0^1 t^n(t-1)\ln(1+t^2)\dd t$$
		Puisque, sur $[0;1]$, $t^n\geq 0$, $1+t^2\geq 1$ donc $\ln(1+t^2)\geq 0$ et $t-1\leq 0$. Par produit, $\displaystyle{t^n(t-1)\ln(1+t^2)\leq 0}$ sur $[0;1]$. Par positivité de l'intégrale, on a donc $\displaystyle{\int_0^1 t^n(t-1)\ln(1+t^2)\dd t\leq 0}$.
		$$\boxed{J_{n+1}-J_n\leq 0 : \textrm{ la suite $(J_n)$ est décroissante}}$$
	\item Pour tout $t\in [0;1]$ $t^n\geq 0$ et $1+t^2\geq 0$. Par quotient, $\displaystyle{\frac{t^n}{1+t^2}\geq 0}$ sur $[0;1]$. Par positivité de l'intégrale, $$I_n=\int_0^1 \frac{t^n}{1+t^2}\dd t \geq 0$$
		De plus, pour tout $t\in [0;1]$ on a
		$$0\leq t \leq 1 \Rightarrow 0 \leq t^2 \leq 1 \textrm{ par croissance de la fonction carrée sur $\R^+$}$$
		ainsi $1\leq 1+t^2 \leq 2$ et donc, par décroissance de la fonction inverse sur $\R^*_+$,
		$1\geq \frac{1}{1+t^2}$ et donc $t^n \geq \frac{t^n}{1+t^2}$ sur $[0;1]$ car $t^n\geq 0$. Par croissance de l'intégrale, on en déduit donc
		$$I_n=\int_0^1 \frac{t^n}{1+t^2}\dd t \leq \int_0^1 t^n\dd t = \left[ \frac{t^{n+1}}{n+1} \right]_0^1 = \frac{1}{n+1}$$
		Ainsi, pour tout entier $n$,
		 $$0\leq I_n \leq \frac{1}{n+1}$$
		 Puisque $\displaystyle{\lim_{n\rightarrow +\infty} \frac{1}{n+1}=0}$, par théorème d'encadrement, $(I_n)$ converge et
		 $$\boxed{\lim_{n\rightarrow +\infty} I_n=0}$$
	\item Partons de $J_n$ et faisons une intégration par parties. On pose, pour $t \in [0;1]$, $u(t)=\ln(1+t^2)$ et $v'(t)=t^n$, soit $u'(t)=\frac{2t}{1+t^2}$ et $v(t)=\frac{t^{n+1}}{n+1}$. $u$ et $v$ sont de classe $\mathcal{C}^1$ sur $[0;1]$ et, par intégration par parties,
			$$J_n=\int_0^1 t^n\ln(1+t^2)\dd t = \left[ \frac{t^{n+1}}{n+1}\ln(1+t^2)\right]_0^1 - \int_0^1 \frac{t^{n+1}}{n+1}\frac{2t}{1+t^2}\dd t$$
			soit
			$$J_n=\frac{\ln(2)}{n+1} - \int_0^1 \frac{2}{n+1}\frac{t^{n+2}}{1+t^2}\dd t = \frac{\ln(2)}{n+1}-\frac{2}{n+1}I_{n+2}$$
		On a alors $\displaystyle{\lim_{n\rightarrow +\infty} \frac{\ln(2)}{n+1} =0}$, $\displaystyle{\lim_{n\rightarrow +\infty} \frac{2}{n+1}=0}$ et $\displaystyle{\lim_{n\rightarrow +\infty} I_{n+2}=0}$ d'après la question précédente. Par somme et produit
		$$\boxed{\lim_{n\rightarrow +\infty} J_n=0}$$
		De plus, $$nJ_n=\frac{n\ln(2)}{n+1}-\frac{2n}{n+1}I_{n+2}$$
		Puisque, par la règle du terme de plus haut degré, $$\lim_{n\rightarrow +\infty} \frac{n\ln(2)}{n+1}=\ln(2) \textrm{  et  } \lim_{n\rightarrow +\infty} \frac{2n}{n+1}=2$$
		par somme et produit
		$$\boxed{\lim_{n\rightarrow +\infty} nJ_n=\ln(2)}$$
		\begin{remarque}
			On peut ainsi conclure que $(J_n)$ est équivalente à $\frac{\ln(2)}{n}$, c'est-à-dire $J_n \sim \frac{\ln(2)}{n}$.
		\end{remarque}
\end{enumerate}
\end{correction}
%%% Fin exercice %%%
