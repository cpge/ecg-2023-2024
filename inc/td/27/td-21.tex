%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{20}{2}[Sur les fonctions]
  Soit $E$ le sous-espace vectoriel de $\CC^{\infty}(\R, \R)$ engendré par les fonctions
  $$
  f_{1}: x \donne \eu{-x}, \quad f_{2}: x \donne (x+1) \eu{-x}, \qeq \quad f_{3}: x \donne \left(x^{2}-1\right) \eu{-x} .
  $$

  \begin{enumerate}
    \item Montrer que $\BB=\left(f_{1}, f_{2}, f_{3}\right)$ est une base de $E$.

    \item Soit $d: f\donne f^\prime$. Montrer que $d$ est un endomorphisme de $E$ et donner sa matrice $A$ dans la base $\BB$.

    \item Calculer $A^{n}$ pour tout $n \in \mathbb{N}$.\\
    \textit{On pourra écrire $A=-\mathrm{I}_{3}+N$ avec $N$ une matrice dont les puissances sont faciles à calculer.}
    \item En déduire l'expression de $f^{(n)}$ lorsque $f: x \longmapsto\left(a x^{2}+b x+c\right) \eu{-x}$ avec $(a, b, c) \in \R^{3}$.
  \end{enumerate}

\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item Par définition, $\BB$ engendre $E$ puisque $E=\Vect(f_1,f_2,f_3)$. Montrons la liberté : soient $(a,b,c)$ trois réels tels que $af_1+bf_2+cf_3=0$. Ainsi 
	\[ \forall x\in \R,\quad a\eu{-x}+b(x+1)\eu{-x}+c(x^2-1)\eu{-x} = 0. \]
	En prenant $x=-1$, on obtient $a\eu{1}=0$, soit $a=0$. En prenant alors $x=1$, on obtient $2b\eu{-1}=0$, soit $b=0$ et finalement $c=0$ (par exemple en prenant $x=0$). Au final, la famille $(f_1,f_2,f_3)$ est libre et $\BB$ forme une base de $E$.
	\item $d$ est une application linéaire (par linéarité de la dérivée). Montrons que $d$ est un endomorphisme; pour cela, on calcule l'image par $d$ de la base $\BB$ :
	\begin{align*}
	d(f_1) &= x\donne -\eu{-x}=  -f_1 \in E\\
	d(f_2) &= x\donne \eu{-x}-(x+1)\eu{-x} =f_1-f_2 \in E\\
	f(f_3) &= x\donne 2x\eu{-x}-(x^2-1)\eu{-x} \\&= x\donne 2(x+1-1)\eu{-x}-f_3(x)= x\donne 2(x+1)\eu{-x}-2\eu{-x}-f_3(x)=2f_2-2f_1-f_3\in E.	
	\end{align*}
	Puisque $d(f_1), d(f_2)$ et $d(f_3)$ sont des éléments de $E$, par linéarité (et puisque $E$ est un sous-espace vectoriel), pour tout $f\in E$, $d(f)\in E$ : $d$ est bien un endomorphisme de $E$. Sa matrice dans la base $\BB$, d'après le calcul précédent, est 
	\[ A=\Mat_{\BB}(d) = \matrice{-1&1&-2\\0&-1&2\\0&0&-1}\]
	\item Remarquons que 
	\[ A = -I_3+\matrice{0&1&-2\\0&0&2\\0&0&0}.\]
	Notons $N=\matrice{0&1&-2\\0&0&2\\0&0&0}$. On constate que $N^2=\matrice{0&0&2\\0&0&0\\0&0&0}$ et que $N^3=0$. Ainsi, pour tout $k\geq 3$, $N^k=0_3$. $I_3$ et $N$ commutant, la formule du binôme de Newton donne, pour tout $n\geq 2$ :
	\begin{align*}
		 A^n = (-I_3+N)^n &= \sum_{k=0}^n \binom{n}{k}(-I_3)^{n-k}N^k	\\
		&= \binom{n}{0}(-I_3)^nN^0 + \binom{n}{1}(-I_3)^{n-1}N^1+\binom{n}{2}(-I_3)^{n-2}N^2+\underbrace{\sum_{k=3}^{n} \binom{n}{k}(-I_3)^{n-k}N^k}_{=0}\\
		&= (-1)^nI_3 + n(-1)^{n-1}N+\frac{n(n-1)}{2}(-1)^{n-2}N^2\\
		&= \matrice{(-1)^n& n(-1)^{n-1} & -2n(-1)^{n-1}+2\frac{n(n-1)}{2}(-1)^n\\0&(-1)^n&2n(-1)^{n-1}\\0&0&(-1)^n}
	\end{align*}
	Finalement, ce résultat étant également valable pour $n=0$ et $n=1$, on en déduit que 
	\[ \boxed{\forall n\in\N,\quad A^n=\matrice{(-1)^n&n(-1)^{n-1}&n(n+1)(-1)^n\\0&(-1)^n&2n(-1)^{n-1}\\0&0&(-1)^n}.}\]
	\item $A^n$ désigne l'image dans la base $\BB$ de $d^n$. Ainsi,
		\begin{align*}
		d^n(f_1) &= (-1)^n f_1\\
		d^n(f_2) &= (-1)^n f_2+n(-1)^{n-1}f_1\\
		d^n(f_3) &= (-1)^n f_3 +2n(-1)^{n-1}f_2+n(n+1)(-1)^n f_1. 
	\end{align*}
	Soit $f:x\donne (ax^2+bx+c)\eu{-x}$. Remarquons que 
	\[ ax^2+bx+c=a(x^2-1) + b(x+1) +a - b + c\]
	Finalement, d'après ce qui précède :
	\begin{align*}
	d^n(f) &= ad^n(f_3)+bd^n(f_2)+(a-b+c)d^n(f_1) \\&= a((-1)^nf_3+2n(-1)^{n-1}f_2+n(n+1)(-1)^nf_1)+b((-1)^nf_2+n(-1)^{n-1}f_1) + (a-b+c)(-1)^n f_1\\
	&= (an(n+1)(-1)^n+ bn(-1)^{n-1}+(a-b+c)(-1)^n)f_1 + (2an(-1)^{n-1}+b(-1)^n)f_2 + a(-1)^nf_3.
	\end{align*}
	Ainsi, puisque $d^n : f \donne f^{(n)}$, on en déduit finalement 
	\[ \boxed{\forall x\in \R,\forall n\in \N,\quad f^{(n)}(x) = \left((an(n+1) - bn+(a-b+c)) + (b-2an)(x+1) + a(x^2-1)\right)(-1)^n\eu{-x}.} \]
\end{enumerate}

\end{correction}
%%% Fin exercice %%%
