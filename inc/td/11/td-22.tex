%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{30}{3}[Urnes de Pòlya]
Soient $r_0, b_0$ et $d$ des entiers strictement positifs. Une urne contient $b_0$ boules bleues et $r_0$ boules rouges. Une boule est choisie au hasard uniformément dans l'urne. On note sa couleur et on la remet dans l'urne en ajoutant un nombre $d$ de boules supplémentaires de la même couleur. Puis on recommence la procédure aussi souvent que nécessaire.

Pour tout $n\in \N*$, on note $X_n$ la variable aléatoire égale à $1$ (respectivement $0$) si la $k$-ième boule tirée est rouge (resp. bleue).
\begin{enumerate}
	\item \begin{enumerate}
	\item Donner la loi de $X_1$.
	\item Déterminer la loi de $X_2$.
	\end{enumerate}
	\item Soit $n\in \N*$. Notons $S_n$ le nombre de boules rouges tirées lors des $n$ premiers tirages.
	\begin{enumerate}
		\item Pour tout $k\in \interent{0 n}$, calculer $\PP_{[S_n=k]}(X_{n+1}=1)$.\vspace*{0.1cm}
		\item En déduire que $\PP(X_{n+1}=1) = \dfrac{r_0+d\esperance(S_n)}{r_0+b_0+dn}$.\vspace*{0.1cm}
		\item Exprimer $\esperance(S_{n+1})$ en fonction de $\esperance(S_n)$ et de $\PP(X_{n+1}=1)$.
	\end{enumerate}
	\item En déduire la loi de $X_n$ pour tout $n\in \N*$. Commenter ce résultat.
\end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item \begin{enumerate}
		\item Par définition, $X_1(\Omega)=\{ 0, 1\}$ et \[ \PP(X_1=0) = \frac{b_0}{b_0+r_0} \qeq \PP(X_1=1) = \frac{r_0}{b_0+r_0}. \]
		$X_1$ suit une loi de Bernoulli de paramètre $\frac{r_0}{b_0+r_0}$.
		\item La probabilité de tirer une boule au $2$-ième tirage dépend du premier tirage. $( (X_1=0), (X_1=1))$ forme un système complet d'événements. D'après la formule des probabilités totales :
		\begin{align*}
			\PP(X_2=0) &= \PP( (X_1=0)\cap (X_2=0)) + \PP( (X_1=1)\cap(X_2=0)) \\
			 &= \PP(X_1=0) \PP_{(X_1=0)}(X_2=0) + \PP(X_1=1) \PP_{(X_1=1)}(X_2=0) \\
			 &= \frac{b_0}{b_0+r_0} \frac{b_0+d}{b_0+r_0+d} + \frac{r_0}{b_0+r_0} \frac{b_0}{b_0+r_0+d}\\
			 &= \frac{b_0(b_0+r_0+d)}{(b_0+r_0)(b_0+r_0+d)}=\frac{b_0}{b_0+r_0}\\
			 \PP(X_2=1)&=\PP( (X_1=0)\cap (X_2=1)) + \PP( (X_1=1)\cap(X_2=1)) \\
			 &= \PP(X_1=0) \PP_{(X_1=0)}(X_2=1) + \PP(X_1=1) \PP_{(X_1=1)}(X_2=1) \\
			 &= \frac{b_0}{b_0+r_0} \frac{r_0}{b_0+r_0+d} + \frac{r_0}{b_0+r_0} \frac{r_0+d}{b_0+r_0+d}\\
			 &= \frac{r_0(b_0 + d+r_0)}{(b_0+r_0)(b_0+r_0+d)}=\frac{r_0}{b_0+r_0}
		\end{align*}
		Ainsi,		$X_2$ suit une loi de Bernoulli de paramètre $\frac{r_0}{b_0+r_0}$.
		\item \begin{enumerate}
		\item Soit $k\in \interent{0 n}$. Par définition, si $S_n=k$, on a tiré $k$ boules rouges (et donc ajouté $k\times d$ boules rouges) et $n-k$ boules bleues (et on a donc ajouté $(n-k)\times d$ boules bleues). On a ajouté au total $n\times d$ boules. Ainsi :
		\begin{align*}
			\PP_{[S_n=k]}(X_{n+1}=1) &= \frac{r_0+kd}{r_0+b_0+nd}
		\end{align*}
		\item On applique alors la formule des probabilités totales au système complet d'événements $([S_n=k])_{k\in \interent{0 n}}$. On a :
		\begin{align*}
			\PP(X_{n+1}=1) &= \sum_{k=0}^n \PP((S_n=k) \cap (X_{n+1}=1)\\
			&= \sum_{k=0}^n \PP(S_n=k) \PP_{[S_n=k]}(X_{n+1}=1)\\
			&=  \frac{r_0 \sum\limits_{k=0}^n \PP(S_n=k) + d \sum\limits_{k=0}^n k\PP(S_n=k)}{r_0+b_0+nd} \\
			&=  \frac{r_0 + d\esperance(S_n)}{r_0+b_0+nd} \text{ car }\sum_{k=0}^n \PP(S_n=k)=1 \qeq \sum_{k=0}^n k\PP(S_n=k)=\esperance(S_n)
		\end{align*}
		\item $((X_{n+1}=0), (X_{n+1}=1))$ forme un système complet d'événements. Pour tout $k\in \interent{0 n+1}$ :
		\begin{align*}
			\PP(S_{n+1}=k) &= \PP((X_{n+1}=0)\cap(S_{n+1}=k))+ \PP((X_{n+1}=1)\cap(S_{n+1}=k))\\
			&= \PP(X_{n+1}=0) \PP_{(X_{n+1}=0)}(S_{n+1}=k) + \PP(X_{n+1}=1)\PP_{(X_{n+1}=1)}(S_{n+1}=k) \\
			&= (1-\PP(X_{n+1}=1))\PP(S_n=k) + \PP(X_{n+1}=1) \PP(S_n=k-1)
		\end{align*}
		en remarquant que, si $X_{n+1}=0$, on n'a pas tiré une bleue au $n+1$-ième tirage, et donc il faut avoir tirer les $k$ boules bleues sur les $n$-ième tirages; sinon, on a tiré une boule bleue, et il faut donc tirer $k-1$ boules bleues sur les $n$ premiers tirages. Alors
		\begin{align*}
			k\PP(S_{n+1}=k) &= (1-\PP(X_{n+1}=1)) k \PP(S_n=k) + \PP(X_{n+1}=1)k\PP(S_n=k-1)\\
			\text{soit } \sum_{k=0}^{n+1} k\PP(S_{n+1}=k) &=  (1-\PP(X_{n+1}=1))\sum_{k=0}^{n+1} k\PP(S_n=k) + \PP(X_{n+1}=1) \sum_{k=0}^{n+1} k\PP(S_n=k-1)
		\end{align*}
		En constatant que $\PP(S_n=-1)=0$ et $\PP(S_n=n+1)=0$, on peut ré-écrire :
		\begin{align*}
			\esperance(S_{n+1}) &= (1-\PP(X_{n+1}=1))\esperance(S_n) + \PP(X_{n+1}=1)\sum_{k=0}^n (k+1)\PP(S_n=k) \\
			&= (1-\PP(X_{n+1}=1))\esperance(S_n) + \PP(X_{n+1}=1)\left( \esperance(S_n)+1\right)\\
			&= \esperance(S_n) + \PP(X_{n+1}=1)
		\end{align*}
	\end{enumerate}
	\item On repart de la formule vue en $d$, et en utilisant la relation vue en $3$.
 On a $\esperance(S_n) = \esperance(S_{n+1})-\PP(X_{n+1}=1)$. Ainsi :
 \begin{align*}
	 \PP(X_{n+1}=1) &= \frac{r_0+d\esperance(S_{n+1})-d\PP(X_{n+1}=1)}{r_0+b_0+nd}
 \end{align*}
 et donc
\begin{align*}
	 \esperance(S_{n+1}) &= \frac{(r_0+b_0+(n+1)d)\PP(X_{n+1}=1)-r_0}{d}
 \end{align*}
 soit, au rang $n$ :
 \begin{align*}
 	 \esperance(S_{n}) &= \frac{(r_0+b_0+nd)\PP(X_{n}=1)-r_0}{d}
  \end{align*}
	et en injectant dans la relation 2)b):
	\begin{align*}
		\PP(X_{n+1}=1) &= \frac{(r_0+b_0+nd)\PP(X_n=1)}{r_0+b_0+nd} = \PP(X_n=1)
	\end{align*}
	La suite $(\PP(X_n=1))_{n\in \N*}$ est donc constante, égale à $\PP(X_1=1)=\frac{r_0}{r_0+b_0}$. Enfin, $1-\PP(X_n=1)=\PP(X_n=0)=\frac{b_0}{b_0+r_0}$.

	\textbf{Bilan} : pour tout $n\in \N*$, $X_n$ suit une loi de Bernoulli de paramètre $\dfrac{r_0}{b_0+r_0}$.

	\begin{experiencehistorique}
		La dénomination fait référence au mathématicien \textbf{George Pólya} qui a proposé ce modèle. En s'intéressant au nombre de boules rouges tirées au bout de $n$ tirages, on obtient une loi, appelée \textbf{loi de Markov-Pólya}.
	\end{experiencehistorique}
	\end{enumerate}
\end{enumerate}
\end{correction}
%%% Fin exercice %%%
