%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{30}{3}[Encore des urnes, toujours des urnes]
Soit $n$ un entier naturel supérieur ou égal à $2$. On considère une urne contenant $n$ boules numérotées de $1$ à $n$. On effectue des tirages successifs avec remise et on s'arrête dès que l'on obtient un numéro supérieur ou égal au numéro précédent. On note $Y_n$ la variable aléatoire égale au nombre de tirages effectués.
\begin{enumerate}
	\item Justifier que $Y_n$ est une variable aléatoire réelle finie telle que $Y_n(\Omega) = \interent{2 n+1}$.
	\item Soit $k\in \interent{1 n}$. Justifier qu'il y a $\ds{\binom{n}{k}n^{n+1-k}}$ tirages de $n+1$ boules successivement avec remise dans cette urne tels que les $k$ premières sont dans l'ordre strictement décroissant.
	\item Déterminer alors $\PP(Y_n>k)$ pour tout $k\in \interent{1 n}$.
	\item En déduire que, pour tout $k\in \interent{2 n+1}$, \[ \PP(Y_n=k)=\frac{k-1}{n^k} \binom{n+1}{k}.\]
	\item Calculer $\esperance(Y_n)$ puis $\ds{\lim_{n\to +\infty} \esperance(Y_n)}$.
\end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item Au minimum, il faut tirer deux boules pour obtenir un numéro supérieur au précédent. Au maximum, il faut tirer les $n$ boules dans l'ordre décroissant $n, n-1 \hdots, 1$ avant de tirer une boule qui, nécessairement, aura un numéro supérieur ou égal au précédent. Cela donnera $n+1$ tirages. Toute valeur entre $2$ et $n+1$ est atteignable, et ainsi \[ Y_n(\Omega)=\interent{2 n+1}\]
	\item Soit $k\in \interent{1 n}$. Pour obtenir un tirage avec les $k$ premiers décroissants, on commence par déterminer les $k$ nombres en questions, qui seront nécessairement dans l'ordre décroissante. Il y en a $\binom{n}{k}$. On complète alors le tirage par des nombres quelconques entre $1$ et $n$, soit $n$ choix possibles pour les $n+1-k$ boules restantes. Ainsi, il y a bien
	\[ \underbrace{\binom{n}{k}}_{\text{$k$ premières st décroissantes}} \times \underbrace{n^{n+1-k}}_{\text{boules restantes}} \]
	tirages de $n+1$ boules avec remises, tels que les $k$ premières sont dans un ordre strictement décroissantes.
	\item $[Y_n>k]$ est composé des tirages tels que le nombre de tirages effectués avant de s'arrêter est strictement supérieur à $k$. Dit autrement, lorsqu'on effectue un tirage de $n+1$ boules au maximum, les $k$ premières, au moins, sont en ordre strictement décroissante. Il y a $n^{n+1}$ cas possibles. Par équiprobabilité des tirages, et d'après ce qui précède :
	\[ \forall k\in \interent{1 n}, \quad \PP(Y_n>k) = \frac{\binom{n}{k} n^{n+1-k}}{n^{n+1}}=\frac{1}{n^k}\binom{n}{k}  \]
	\item Soit $k\in \interent{2 n+1}$. On écrit $[Y_n=k]=[Y_n>k-1] \setminus [Y_n>k]$ et donc
	\begin{align*}
		\PP(Y_n=k)& = \PP(Y_n> k-1) - \PP(Y_n>k) \\
		&= \frac{1}{n^{k-1}}\binom{n}{k-1}  - \frac{1}{n^{k}}\binom{n}{k}\\
		&= \frac{1}{n^k}\left( n\binom{n}{k-1} - \binom{n}{k}\right) \\
		&= \frac{1}{n^k} \left( (n+1)\binom{n}{k-1} - \binom{n}{k-1}-\binom{n}{k}\right)\\
	\end{align*}
D'après la formule du chef, \[ (n+1)\binom{n}{k-1} = k\binom{n+1}{k}. \]
De plus, par la formule de Pascal, \[ \binom{n}{k-1}+\binom{n}{k}=\binom{n+1}{k}. \]
Ainsi,
\begin{align*}
	\PP(Y_n=k) &= \frac{1}{n^k}\left( k\binom{n+1}{k}- \binom{n+1}{k}\right)\\
	&= \frac{k-1}{n^k}\binom{n+1}{k}.
\end{align*}
Ainsi, \[ \boxed{\forall k \in \interent{2 n+1},\quad \PP(Y_n=k) = \frac{k-1}{n^k}\binom{n+1}{k}.}\]
\item Soit $n \geq 2$ fixé. Par définition :
\begin{align*}
	\esperance(Y_n) &= \sum_{k=2}^{n+1} k\frac{k-1}{n^k} \binom{n+1}{k} \\
	&= \sum_{k=0}^{n+1} k(k-1) \binom{n+1}{k} \left(\frac{1}{n}\right)^k
\end{align*}
D'après la formule du binôme de Newton, pour tout $x\in \R$ :
\[ \sum_{k=0}^{n+1} \binom{n+1}{k} x^k = (1+x)^{n+1}. \]
Ces termes sont dérivables sur $\R$ et en dérivant, on obtient pour tout réel $x$ :
\[ \sum_{k=1}^{n+1} k \binom{n+1}{k}x^{k-1} =(n+1)(1+x)^n \]
En dérivant à nouveau, pour tout réel $x$ :
\[ \sum_{k=2}^{n+1} k(k-1) \binom{n+1}{k}x^{k-2} = n(n+1)(1+x)^{n-1} \]
ou encore, en multipiant par $x^2$ :
\[ \sum_{k=2}^{n+1} k(k-1)\binom{n+1}{k}x^k = \sum_{k=0}^{n+1} k(k-1)\binom{n+1}{k}x^k  = n(n+1)x^2(1+x)^{n-1}. \]
Ainsi :
\begin{align*}
	\esperance(Y_n) &= n(n+1)\frac{1}{n^2}\left(1+\frac{1}{n}\right)^{n-1}\\
	&= \left(1+\frac{1}{n}\right)^n
\end{align*}
On a finalement \[ \boxed{\forall n\geq 2, \quad \esperance(Y_n) = \left(1+\frac1n\right)^n.}\]
Remarquons alors que
\begin{align*}
	\esperance(Y_n) &= \eu{n\ln\left(1+\frac{1}{n}\right)} \\
	&= \eu{\frac{\ln\left(1+\frac{1}{n}\right)}{\frac{1}{n}}}
\end{align*}
Lorsque $n$ tend vers $+\infty$, $\frac{1}{n}\tendversen{n\to +\infty} 0$ et \[ \lim_{x\to 0} \frac{\ln(1+x)}{x} = 1. \]
Par composée, on en déduit que \[ \boxed{\lim_{n\to +\infty} \esperance(Y_n) = \E.}\]
\end{enumerate}
\end{correction}
%%% Fin exercice %%%
