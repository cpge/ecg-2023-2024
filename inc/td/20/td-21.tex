%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{25}{3}[Un endomorphisme étrange]
Pour tout $f \in \CC^0\left(\R+, \R\right)$, on définit l'application
\[ \appli{T(f)}{\R+}{\R}{x}{\left \{ \begin{array}{rcl} \ds{\frac{1}{x}\int_0^x f(t)\D t}&\text{si}& x>0\\f(0) & \text{si} & x=0 \end{array}\right.}\]
et l'application
\[ \appli{T}{\CC^0(\R+,\R)}{\CC^0(\R+,\R)}{f}{T(f)}. \]
\begin{enumerate}
\item Montrer que $T(f) \in \CC^{0}\left(\R+, \R\right)$ pour tout $f \in \CC^{0}\left(\R+, \R\right)$.
\item Montrer que $T$ est un endomorphisme de $\CC^{0}\left(\R+, \R\right)$.
\item Montrer que $T$ est injective.
\item Soit $g \in \Image(T)$. Montrer que $g$ est de classe $\CC^{1}$ sur $\R>$ et que $x g^{\prime}(x) \tendversen{x \to 0^{+}} 0$. Est-ce que $T$ est surjective ?
\item Déterminer $\Image(T)$.
\end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item D'après le théorème fondamental de l'intégration, pour toute fonction $f$ continue sur $\R+$, $T(f)$ est continue (et même de classe $\CC^1$) sur $\R>$. Notons $F$ une primitive de $f$ sur $\R+$ (qui existe car $f$ est continue). $F$ est de classe $\CC^1$ sur $\R+$ et 
	\[ T(f) = x \donne \frac{F(x)-F(0)}{x}. \]
	Remarquons que $F$ étant dérivable en $0$, $T(f)$ admet une limite en $0$ et 
	\[ T(f)(x) \tendversen{x\to 0^+} F'(0)=f(0) = T(f)(0). \]
	Ainsi, $T(f)$ est également continue en $0$.\\\textbf{Bilan} : pour toute fonction $f$ continue sur $\R+$, $T(f)$ est bien continue sur $\R+$.
	\item Le côté \textit{endo} est donc assuré d'après la question précédente. Montrons que $T$ est linéaire. Soient $f$ et $g$ deux fonctions continues sur $\R+$ et $\lambda \in \R$. Par linéarité de l'intégrale :
	\begin{align*}
	\forall x>0,\quad T(\lambda f+g)(x) &= \frac{1}{x} \int_0^x (\lambda f+g)(t)\dd t \\
	&= \frac{1}{x} \left(\lambda \int_0^x f(t)\dd t + \int_0^x g(t)\dd t\right)\\
	&= \lambda \frac{1}{x}\int_0^x f(t)\dd t + \frac1x\int_0^x g(t)\dd t = \lambda T(f)(x)+T(g)(x) 	
	\end{align*}
	et pour $x=0$, \[T(\lambda f+g)(0) = (\lambda f+g)(0) = \lambda f(0)+g(0) =\lambda T(f)(0)+T(g)(0).\]
	Ainsi, pour tout réel $x\in \R$, $T(\lambda f+g)(x)=(\lambda T(f)+T(g))(x)$ : $T$ est linéaire.
	\item Soit $f\in \ker(T)$. Tout d'abord, $T(f)(0)=0$ et donc $f(0)=0$. Pour tout $x>0$ 
	\[ \frac{1}{x} \int_0^x f(t)\dd t =0 \implies \int_0^x f(t)\dd t= 0 . \]
	En dérivant (possible d'après le théorème fondamental de l'intégration), on en déduit que pour tout $x\in \R>$, $f(x)=0$.\\
	Ainsi, $f=0$ et $\boxed{\ker(T)=\{0_{\CC^0(\R+,\R)}\}}$ : $T$ est bien injective.
	\item Soit $g\in \Image(T)$. Il existe donc $f\in \CC^0(\R+,\R)$ telle que $T(f)=g$. Ainsi, pour tout $x>0$ :
	\[ \frac{1}{x}\int_0^x f(t)\D t = g(x) \implies \int_0^x f(t)\D t. \]
	D'après le théorème fondamental de l'intégration, $\ds x\donne \int_0^x f(t)\D t$ est de classe $\CC^1$ sur $\R$. Par produit, $g$ est de classe $\CC^1$ sur $\R>$.
	
	De plus, en multipliant par $x$, pour tout $x>0$
	\[ \int_0^x f(t)\D t = xg(x). \]
	Dérivons :
	\[ \forall x>0, f(x) = g(x)+xg'(x) \]
	soit 
	\[ xg'(x) = f(x)-g(x). \]
	Or, $f(x)\tendversen{x\to 0} f(0)$ par continuité de $f$ et puisque $g=T(f)$, $g(x)\tendversen{x\to 0} T(f)(0) = f(0)$ par continuité de $T(f)$. Par somme 
	\[ \boxed{\lim_{x\to 0^+} xg'(x) = 0.}\]
	Remarquons que $T$ n'est pas surjective : une fonction continue mais pas de classe $\CC^1$ n'est pas dans l'image de $T$ d'après ce qui précède (exemple : $x\donne |x-1|$).
	\item On vient de voir que si $g\in \Image(T)$, $g$ est de classe $\CC^1$ sur $\R>$ et $xg'(x)\tendversen{x\to 0^+} 0$. Réciproquement, soit $g$ une fonction continue sur $\R+$, $\CC^1$ sur $\R>$ et vérifiant $xg'(x)\tendversen{x\to 0^+} 0$. Montrons que $g\in \Image(T)$. Notons $f:x\donne g(x)+xg'(x)$ si $x>0$, et $f(0)=g(0)$ (en utilisant l'étude de la question précédente).
	
	 $f$ est par somme et produit continue sur $\R>$ mais par hypothèse $f(x)\tendversen{x\to 0^+} g(0)$. Ainsi, $f$ est continue sur $\R+$. Mais alors 
	 \begin{align*}
	 \forall x>0,\quad T(f)(x)&=\frac{1}{x}\int_0^x (g(t)+tg'(t))\D t \\
	 &= \frac{1}{x} \left[ tg(t)\right]_0^x = g(x)	
	 \end{align*}
	 et si $x=0$, $T(f)(0)=f(0)=g(0)$.
	 
	 Ainsi, \[ \forall x\in \R+,\quad T(f)(x) = g \]
	 et finalement $g\in \Image(T)$.
	 
	 \textbf{Bilan} : on a donc 
	 \[ \boxed{\Image(T)=\{ g\in \CC^0(\R+,\R),\quad g\in \CC^1(\R>,\R),\quad xg(x)\tendversen{x\to 0^+}0\}.}\]

\end{enumerate}
\end{correction}
%%% Fin exercice %%%
