%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{25}{3}[Un exercice théorique]
Soit $f$ une fonction de classe $\CC^2$ sur $\R$, telle que $f$ et $f''$ sont bornées sur $\R$.

On note \[ M_0=\sup_{x\in \R} |f(x)| \qeq M_2=\sup_{x\in \R} |f''(x)|.\]

L'objectif de l'exercice est de démontrer que $f'$ est également bornée sur $\R$ et  de trouver une inégalité entre les  $\sup$ de $f$, $f'$ et $f''$.
\begin{enumerate}
\item Montrer que, pour tout réel $x\in \R$, et tout $h>0$
\[ \left| f(x+h)-f(x)-hf'(x)\right| \leq \frac{M_2h^2}{2}\qeq
 \left| f(x-h)-f(x)+hf'(x)\right| \leq \frac{M_2h^2}{2}. \]
 \item Montrer que, pour tout réel $x\in \R$ et tout $h>0$ \[ |f'(x)|\leq \frac{M_0}{h}+\frac{M_2h}{2}. \]
 \item En déduire que $f'$ est bornée, et en notant $M_1=\sup\limits_{x\in \R} |f'(x)|$, que \[ M_1\leq \sqrt{2M_0M_2}. \]
\end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item $f$ étant de classe $\CC^2$ sur $\R$, on peut appliquer l'inégalité de Taylor-Lagrange à l'ordre $1$. Soit $x\in \R$ et $h>0$. L'inégalité de Taylor-Lagrange appliquée entre $x$ et $x+h$ donne, en utilisant la définition de $M_2$ :
	\begin{align*}
	 |f(x+h) - (f(x)+hf'(x)| \leq \frac{|x+h-x|^2}{2!}\max_{[x,x+h]} |f''(x)| 	\leq \frac{h^2}{2}M_2.
	\end{align*}
	Ainsi, \[ \boxed{\left|f(x+h)-f(x)-hf'(x)\right|\leq \frac{M_2h^2}{2}.}\]
	De même, l'inégalité de Taylor-Lagrange appliquée entre $x$ et $x-h$ donne, en utilisant la définition de $M_2$ :
	\begin{align*}
	 |f(x-h) - (f(x)-hf'(x)| \leq \frac{|x-h-x|^2}{2!}\max_{[x-h,x]} |f''(x)| 	\leq \frac{h^2}{2}M_2
	 \end{align*}
	 Ainsi,
	 \[ \boxed{\left|f(x-h)-f(x)+hf'(x)\right| \leq \frac{M_2h^2}{2}.}\]
	 \item Soient $x\in \R$ et $h>0$. Remarquons que 
	 \[ f(x-h)-f(x)+hf'(x)-(f(x+h)-f(x)-hf'(x)) = f(x-h)-f(x+h)+2hf'(x).\]
	 Ainsi, par inégalité triangulaire, les inégalités précédentes et définition de $M_0$ :
	 \begin{align*}
	 \left| 2hf'(x)\right| &= \left|  f(x-h)-f(x)+hf'(x)-(f(x+h)-f(x)-hf'(x)) - f(x-h)+f(x+h)\right|\\
	 &\leq \left|f(x-h)-f(x)+hf'(x)\right| + \left|f(x+h)-f(x)-hf'(x)\right| +\left|f(x+h)-f(x-h)\right|\\
	 &\leq \frac{M_2h^2}{2}+\frac{M_2h^2}{2}+|f(x+h)| +|f(x-h)| \leq M_2h^2 + 2 M_0
	 \end{align*}
	En divisant par $2h>0$ :
	\[ \boxed{|f'(x)| \leq \frac{M_2h}{2}+\frac{M_0}{h}.}\]
	\item Fixons par exemple $h=1$. Le résultat précédent indique que 
	\[ \forall x\in \R,\quad |f'(x)| \leq \frac{M_2}{2}+M_0. \]
	Le réel $\frac{M_2}{2}+M_0$ étant indépendant de $x$, on en déduit que \underline{$f'$ est bornée}.
	
	Pour trouver la \guill{meilleure borne}, étudions la fonction $g:h\donne \frac{M_2h}{2}+\frac{M_0}{h}$ sur $\R>$. $g$ est de classe $\CC^1$ sur $\R>$ et sa dérivée est 
	\[ g': h \donne \frac{M_2}{2}-\frac{M_0}{h^2} = \frac{h^2M_2-2M_0}{h^2}. \]
	Le trinôme du numérateur s'annule en $-\sqrt{\frac{2M_0}{M_2}}$ et en $\sqrt{\frac{2M_0}{M_2}}$. 
	De plus,
	\[ g\left(\sqrt{\frac{2M_0}{M_2}}\right) = \frac{M_2\sqrt{\frac{2M_0}{M_2}}}{2}+\frac{M_0}{\sqrt{\frac{2M_0}{M_2}}} = \sqrt{\frac{M_0M_2}{2}}+\sqrt{\frac{M_2M_0}{2}}=\sqrt{2M_0M_2} \]
	
	Le tableau, sur $\R>$, de $g'$ donne alors 
	\begin{center}
	\begin{tikzpicture}
   \tkzTabInit{$h$ / 1 , $g'(h)$ / 1, variation de $g$ / 1.5}{$0$, $\sqrt{\frac{2M_0}{M_2}}$, $+\infty$}
   \tkzTabLine{d, -, z, +, }
   \tkzTabVar{D+/ $+\infty$, -/ $\sqrt{2M_0M_2}$, +/ $+\infty$}
\end{tikzpicture}
	\end{center}
	L'inégalité 
\[ \forall x\in \R,\quad |f'(x)|\leq g(h) \]
étant vraie pour tout réel $h>0$, elle l'est, d'après le tableau précédent, pour $h=\sqrt{\frac{2M_0}{M_2}}$, ce qui donne 
\[ \forall x\in \R, \quad |f'(x)| \leq \sqrt{2M_0M_2}.\]
Ceci étant valable pour tout réel $x$, par passage à la borne supérieure :
\[ \boxed{M_1 \leq \sqrt{2M_0M_2}.}\]
\end{enumerate}
\end{correction}
%%% Fin exercice %%%
