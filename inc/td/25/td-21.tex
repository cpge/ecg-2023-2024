%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{20}{3}[Lien entre $f$ et $f^2$]
Soit $E$ un espace vectoriel, et $f\in \mathcal{L}(E)$. 
\begin{enumerate}
	\item Montrer que $\Ker(f)\subset \Ker(f^2)$ et $\Image(f^2)\subset \Image(f)$.
	\item Montrer que $\Ker(f)=\Ker(f^2)$ si et seulement si $\Ker(f)\cap \Image(f)=\{0\}$.
	\item Montrer que $\Image(f)=\Image(f^2)$ si et seulement si $\Ker(f) + \Image(f) = E$.
	\item Montrer alors le résultat 
\[ \Image(f)\oplus \Ker(f)=E \iff \left \{ \begin{array}{l} \Ker(f)=\Ker(f^2)\\\Image(f)=\Image(f^2)\end{array}\right.. \]
\end{enumerate}
\end{exoApp}

\begin{correction}
\begin{remarque}
Ces résultats théoriques sont classiques et à savoir refaire.
\end{remarque}
\begin{enumerate}
	\item Soit $x\in \Ker(f)$. Ainsi, $f(x)=0_E$ et par application de $f$, $f(f(x))=f(0_E)=0_E$ : $x\in \Ker(f^2)$.\\
	Soit $y\in \Image(f^2)$. Il existe $x\in E$ tel que $y=f^2(x)=f(f(x))$. En notant $z=f(x)$, on a donc $y=f(z)$ avec $z\in E$ : $y \in \Image(f)$.
	\\Ainsi,
	\[ \boxed{\Ker(f) \subset \Ker(f^2)\qeq \Image(f^2)\subset \Image(f).}\]
	\item L'inclusion $\Ker(f)\subset \Ker(f^2)$ vient d'être démontrée. Raisonnons par double implication :
		\begin{description}
			\item \colorbox{lightgray}{$\Rightarrow$} Supposons $\Ker(f)=\Ker(f^2)$. Soit $x\in \Ker(f)\cap \Image(f)$. Alors $f(x)=0_E$ et il existe $y\in E$ tel que $x=f(y)$. En regroupant ces deux informations :
			\[ 0_E= f(x) = f( f(y)) = f^2 (y). \]
			Ainsi, $y\in \Ker(f^2)$. Mais comme $\Ker(f^2)=\Ker(f)$, on en déduit que $y\in \Ker(f)$, c'est-à-dire $f(y)=0_E$. Or, $x=f(y)$ et donc $x=0_E$.\\
			On a donc bien $\Ker(f)\cap \Image(f) = \{0\}$, l'autre inclusion étant immédiate.
			\item \colorbox{lightgray}{$\Leftarrow$}  Réciproquement, supposons que $\Ker(f)\cap \Image(f)=\{0\}$ et montrons que $\Ker(f^2)\subset \Ker(f)$, ce qui permettra de conclure puisque l'autre inclusion a été démontrée lors de la première question.\\
				Soit $x\in \Ker(f^2)$. Alors, $f^2(x)=0_E$, c'est-à-dire $f(f(x))=0_E$. Notons $y=f(x)$. D'une part, $y\in \Image(f)$, et d'autre part, puisque $f(f(x))=0_E$, $f(y)=0_E$ et donc $y\in \Ker(f)$. Puisque $\Ker(f)\cap \Image(f)=\{0\}$, on peut en déduire que $y=0_E$, et donc $f(x)=0_E$ : $x\in \Ker(f)$. \\Cela montre donc l'inclusion $\Ker(f^2)\subset \Ker(f)$.
		\end{description} 
		On a donc bien démontré :
	\[ \boxed{\Ker(f)=\Ker(f^2) \iff \Image(f)\cap \Ker(f)=\{0_E\}.}\]
	\item Montrons également par double implication.
	\begin{description}
		\item \colorbox{lightgray}{$\Rightarrow$} Supposons $\Image(f)=\Image(f^2)$, et montrons $\Ker(f)+\Image(f)=E$ par analyse et synthèse. \\
			Soit $x\in E$ et supposons qu'il existe $y\in \Ker(f)$ et $z\in \Image(f)$ tels que $x=y+z$. Ainsi, $f(y)=0$ et il existe $u\in E$ tel que $z=f(u)$. Alors, par linéarité de $f$ :
		\[ x=y+z \implies f(x)=f(y)+f(z)=0_E+f(f(u))=f^2(u) \]
		Puisque $\Image(f)=\Image(f^2)$, $f(x)\in \Image(f)$ donc $f(x)\in \Image(f^2)$. Ainsi, il existe bien un $u\in E$ vérifiant $f(x)=f^2(u)$. On obtient un unique $z=f(u)$, puis un unique $y=x-z$.\\
		Synthèse : soit $x\in E$. Soit $u\in E$ tel que $f(z)=f^2(u)$ (qui existe par hypothèse). On pose alors \[ z = f(u) \qeq y = x-f(u). \]
		On remarque que $y+z=x$, $z\in \Image(f)$ par définition. Enfin 
		\[ f(y) = f(x)-f^2(u) = f(x)-f(x) = 0 \text{ puisque }f(x)=f^2(u).\]
		Cela conclut notre raisonnement : pour tout $x\in E$, il existe $y\in \Ker(f)$ et $z\in \Image(f)$ tels que $x=y+z$ : $E=\Ker(f)+\Image(f)$.
		\item \colorbox{lightgray}{$\Leftarrow$} Supposons $\Ker(f)+\Image(f)=E$. Montrons que $\Image(f)\subset \Image(f^2)$, ce qui permettra de conclure puisque l'autre inclusion a été démontrée lors de la première question.\\
		Soit $y\in \Image(f)$. Il existe donc $x\in E$ tel que $y=f(x)$. Puisque $E=\Ker(f)+\Image(f)$, il existe $a\in \Ker(f)$ et $b\in \Image(f)$ tels que $x=a+b$. Mais alors 
		\[ y=f(x) = f(a)+f(b)=f(b). \]
		Or $b\in \Image(f)$ : il existe $c\in E$ tel que $b=f(c)$ et finalement \[ y = f(b) = f^2(c) \in \Image(f^2).\]
		Ainsi, $\Image(f)\subset \Image(f^2)$.
	\end{description}
			On a donc bien démontré :
	\[ \boxed{\Image(f)=\Image(f^2) \iff \Image(f)+ \Ker(f)=E.}\]
	\item Il suffit de regrouper les deux résultats précédent :
	\begin{align*}
	\Image(f)\oplus \Ker(f) = E &\iff \Image(f)\cap \Ker(f)=\{0_E\} \qeq \Image(f)+\Ker(f) = E\\
	&\iff \Ker(f)=\Ker(f^2) \qeq \Image(f)=\Image(f^2).	
	\end{align*}

\end{enumerate}
\end{correction}
%%% Fin exercice %%%
