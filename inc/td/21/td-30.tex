%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{35}{2}[Formule de Stirling]
\textit{On pourra utiliser, dans cet exercice, un résultat prouvé dans l'exercice sur les intégrales de Wallis du chapitre 15 :
\[ \frac{(2n)!}{4^n(n!)^2}\frac{\pi}{2}\eq{+\infty} \sqrt{\frac{\pi}{4n}}.\]}

Pour tout $n\in \N*$, on pose 
\[ x_n=\frac{n^n\sqrt{n}}{\eu{n}n!},\quad u_n=\ln(x_n)\qeq v_n=\ln(x_n)+\frac{1}{12(n-1)}.\]
\begin{enumerate}
	\item \begin{enumerate}
	\item Vérifier par le calcul que, pour tout $n\in \N*$,
\[ \int_n^{n+1} (x-n)(n+1-x)\dd x= \frac16 \]
et que 
\[ \int_n^{n+1} (x-n)(n+1-x)\frac{1}{x^2}\dd x = -2+(2n+1)\left(\ln(n+1)-\ln(n)\right).\]
\item En déduire que
\[ \forall n\in \N*,\quad 0\leq \left(n+\frac12\right)\ln\left(1+\frac1n\right)-1\leq \frac{1}{12n^2}.\]
	\end{enumerate}
\item Montrer que 
\[ \forall n\in \N*,\quad \frac{x_{n+1}}{x_n} = \frac{1}{\E}\left(1+\frac{1}{n}\right)^{n+1/2}.\]
\doublespacing
\item Montrer que $(u_n)_{n\in \N*}$ et $(v_n)_{n\in \N*}$ convergent vers la même limite.
\item En déduire qu'il existe un réel $C>0$ tel que $\ds n! \eq{+\infty} C\sqrt{n}\left(\frac{n}{\E}\right)^n$.
\item Montrer enfin, en utilisant le résultat rappelé, que $C=\sqrt{2\pi}$.
\end{enumerate}
\singlespacing

\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item \begin{enumerate}
	\item Soit $n\in \N*$. On calcule en développant :
\begin{align*}
\int_n^{n+1} (x-n)(n+1-x)\D x &= \int_n^{n+1} ((n+1)x-x^2-n(n+1)+nx)\D x	\\
&= \int_n^{n+1} (2n+1)x -x^2 -n(n+1) \D x\\
&=\left[ (2n+1)\frac{x^2}{2}-\frac{x^3}{3}-n(n+1)x\right]_n^{n+1}\\
&=(2n+1)\frac{(n+1)^2-n^2}{2} - \frac{(n+1)^3-n^3}{3} - n(n+1)(n+1-n)\\
&= \frac{\cancel{12n^2}+\bcancel{12n}+3 - (\cancel{6n^2}+\bcancel{6n}+2)-\cancel{6n^2}-\bcancel{6n}}{6} = \frac16.
\end{align*}
De même :
\begin{align*}
\int_n^{n+1} (x-n)(n+1-x)\frac{1}{x^2}\D x &= \int_n^{n+1} \frac{2n+1}{x} - 1 -\frac{n(n+1)}{x^2}\D x\\
&= (2n+1)\int_n^{n+1} \frac{\D x}{x} - \int_n^{n+1} \D x - n(n+1)\int_n^{n+1} \frac{\D x}{x^2} \text{ par linéarité}\\
&= (2n+1)\left[ \ln(|x|)\right]_n^{n+1} - \left[1\right]_n^{n+1} -n(n+1) \left[-\frac{1}{x}\right]_n^{n+1}\\
&= (2n+1)(\ln(n+1)-\ln(n)) - (n+1-n) - n(n+1)\left(-\frac{1}{n+1}+\frac{1}{n}\right)\\
&= (2n+1)(\ln(n+1)-\ln(n) - 1 - n(n+1)\frac{1}{n(n+1)}\\&=-2+(2n+1)(\ln(n+1)-\ln(n)).
\end{align*}
\item Remarquons qu'en ré-écrivant la deuxième intégrale :
\begin{align*}
-2+(2n+1)(\ln(n+1)-\ln(n)) &= -2 + (2n+1)\ln\left(\frac{n+1}{n}\right)\\
&= -2 + (2n+1) \ln\left(1+\frac1n\right)\\
&= 2\left(-1+\left(n+\frac12\right)	\ln\left(1+\frac1n\right)\right).
\end{align*}
Encadrons donc l'intégrale en question. Tout d'abord, pour tout $x\in \interff{n n+1}$, on a
\[ x-n\geq 0 \qeq (n+1)-x\geq 0 \implies (x-n)(n+1-x)\geq 0.\]
Par positivité de l'intégrale, on peut en déduire que 
\[ \int_n^{n+1} (x-n)(n+1-x)\frac{1}{x^2}\D x \geq 0. \]
De même, pour $x\in \interff{n n+1}$, on a, par croissance de la fonction carrée sur $\R+$ et décroissance de la fonction inverse sur $\R>$ :
\[ n\leq x \leq n+1 \implies n^2 \leq x^2 \implies \frac{1}{x^2}\leq \frac{1}{n^2}.\]
Ainsi, pour tout $x\in \interff{n n+1}$ :
\[ (x-n)(n+1-x)\frac{1}{x^2} \leq (x-n)(n+1-x)\frac{1}{n^2}.\]
Par croissance de l'intégrale :
\begin{align*}
\int_n^{n+1} (x-n)(n+1)-x)\frac{1}{x^2}\D x &\leq \int_n^{n+1} (x-n)(n+1-x)\frac{1}{n^2}\D x \\
&= \frac{1}{n^2} \int_n^{n+1} (x-n)(n+1-x)\D x = \frac{1}{6n^2}.	
\end{align*}
Ainsi, 
\[ 0\leq \int_n^{n+1} (x-n)(n+1-x)\frac{1}{x^2}\D x \leq \frac{1}{6n^2} \]
soit, d'après le calcul vu précédemment :
\[ 0 \leq 2\left(-1+\left(n+\frac12\right)	\ln\left(1+\frac1n\right)\right)\leq \frac{1}{6n^2}.\]
En divisant par $2$ :
\[ \boxed{0 \leq -1+\left(n+\frac12\right)	\ln\left(1+\frac1n\right) \leq \frac{1}{12n^2}.}\]
\end{enumerate}
\item Soit $n\in \N*$. On calcule \guill{simplement} :
\begin{align*}
\frac{x_{n+1}}{x_n} &= \frac{ \frac{(n+1)^{n+1}\sqrt{n+1}}{\eu{n+1}(n+1)!}}{ \frac{n^n\sqrt{n}}{\eu{n}n!}}\\
&= \frac{(n+1)^{n+1} \sqrt{n+1}}{\eu{n+1}(n+1)!} \frac{\eu{n}n!}{n^n\sqrt{n}}\\
&= \frac{(n+1)^n \cancel{(n+1)} \cancel{n!} \sqrt{n+1}}{n^n \cancel{(n+1)!} \sqrt{n}}\eu{n-(n+1)}\\
&=\left(\frac{n+1}{n}\right)^n \sqrt{ \frac{n+1}{n}} \eu{-1} \\
&= \left(1+\frac1n\right)^n\left(1+\frac1n\right)^{1/2}\E{-1} = \frac{1}{\E}\left(1+\frac1n\right)^{n+1/2}.
\end{align*}
\item Montrons que les suites $u$ et $v$ sont adjacentes. Tout d'abord, elles sont bien définies car $x_n>0$ pour tout entier $n\geq 1$. \\Soit $n\in \N*$.
\begin{align*}
u_{n+1}-u_n	&= \ln(x_{n+1})-\ln(x_n) = \ln\left(\frac{x_{n+1}}{x_n}\right)\\
&= \ln\left( \frac{1}{\E}\left(1+\frac1n\right)^{n+1/2}\right)\\
&= -1 + \left(n+\frac12\right)\ln\left(1+\frac1n\right) \\
&= -1+\left(n+\frac12\right)(\ln(n+1)-\ln(n)).
\end{align*}
Cette quantité, d'après la question 1.b., est positive. Ainsi, la suite $(u_n)$ est croissante.
\begin{align*}
v_{n+1}-v_n &= u_{n+1} + \frac{1}{12n} - u_n-\frac{1}{12(n-1)}\\
&= u_{n+1}-u_n - \frac{1}{12n(n-1)}\\
&=  	-1+\left(n+\frac12\right)(\ln(n+1)-\ln(n)) - \frac{1}{12n(n-1)}.
\end{align*}
Toujours d'après la question 1.b. :
\[ -1+\left(n+\frac12\right)(\ln(n+1)-\ln(n)) - \frac{1}{12n(n-1)} \leq \frac{1}{12n^2}-\frac{1}{12n(n-1)} < 0. \]
Ainsi, la suite $(v_n)$ est décroissante. \\Enfin,
\[ v_n-u_n = \frac{1}{12(n-1)}\tendversen{n\to +\infty} 0.\]
Les suites $(u_n)$ et $(v_n)$ sont donc adjacentes. Par théorème, elles convergent et vers la même limite.
\item Notons $A$ la limite commune de $u$ et $v$. Mais alors, par continuité de $\exp$ sur $\R$ :
\[ x_n = \eu{u_n}\tendversen{n\to +\infty} \eu{A}> 0 .\]
En notant $C=\eu{A}$, on en déduit que 
\[ x_n \eq{+\infty} C \implies n! \eq{+\infty} C\frac{n^n\sqrt{n}}{\eu{n}}=C\left(\frac{n}{\E}\right)^n\sqrt{n}.\]
\item On utilise l'équivalent démontré à l'instant et celui rappelé dans l'énoncé. On a, par substitution :
\[ (2n)! \eq{+\infty} C\sqrt{2n}\left(\frac{2n}{\E}\right)^{2n}.\] 
et par exponentiation :
\[ (n!)^2 \eq{+\infty} C^2n\left(\frac{n}{\E}\right)^{2n}.\]
et donc, par produit :
\begin{align*}
\frac{(2n)!}{4^n(n!)^2} &\eq{+\infty} \frac{C\sqrt{2n} \left(\frac{2n}{\E}\right)^{2n}}{4^n C^2 n \left(\frac{n}{\E}\right)^{2n}}\\
&\eq{+\infty} \frac{C\sqrt{2n} \cancel{2^{2n}} \bcancel{n^{2n}} \eu{2n}}{\cancel{4^n} C^2 n \bcancel{n^{2n}} \eu{2n}} \eq{+\infty} \frac{\sqrt{2}}{C\sqrt{n}}
\end{align*}
Or, \[ \frac{(2n)!}{4^n(n!)^2}\frac{\pi}{2} \eq{+\infty} \sqrt{\frac{\pi}{4n}}\]
donc, par transitivité 
\[\frac{\sqrt{2}}{C\sqrt{n}}\frac{\pi}{2} \eq{+\infty} \frac{\sqrt{\pi}}{2\sqrt{n}} \implies C \eq{+\infty} \sqrt{2\pi} \]
et donc, par limite : $\ds C = \sqrt{2\pi}$. On a donc bien démontré que 
\[ \boxed{n! \eq{+\infty} \sqrt{2\pi}\left(\frac{n}{\E}\right)^n\sqrt{n} = \left(\frac{n}{\E}\right)^n \sqrt{2\pi n}}.\]
\end{enumerate}
\end{correction}
%%% Fin exercice %%%
