%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{20}{3}[Une première suite implicite]
\everymath{\ds}
\begin{enumerate}
  \item Montrer que, pour tout $n \in \N$, l'équation $\tan (x)=x$ admet une unique solution dans $\left] n \pi-\frac{\pi}{2},\, n \pi+\frac{\pi}{2}\right[$ que l'on notera $x_{n}$.
  \item Déterminer un équivalent simple de la suite $\left(x_{n}\right)_{n \in \N}$.\vspace*{.1cm}
  \item Pour tout $n \in \N$, posons $y_{n}=\frac{\pi}{2}+n \pi-x_{n}$.
  \begin{enumerate}
    \item Montrer que, pour tout $n \in \N^{*}, y_{n}=\arctan\left(\frac{1}{x_{n}}\right)$.\vspace*{.1cm}
    \item En déduire un équivalent simple de la suite $\left(y_{n}\right)_{n \in \N^{*}}$.\vspace*{.1cm}
    \item Montrer que $x_{n} \underset{n \to +\infty}{=} a_{n}+b_{n}+o\left(b_{n}\right)$ avec $\left(a_{n}\right)_{n \in \N^{*}}$ et $\left(a_{n}\right)_{n \in \N^{*}}$ des suites que l'on précisera.
  \end{enumerate}
\end{enumerate}
\everymath{}
\end{exoApp}

\begin{correction}
\begin{enumerate}
  \item Sur $\left] n \pi-\frac{\pi}{2},\, n \pi+\frac{\pi}{2}\right[$, la fonction $f:x\donne \tan(x)-x$ est de classe $\CC^1$ par somme de deux fonctions $\CC^1$. De plus, pour tout $x\in \R$, $f'(x) = 1+\tan^2(x)-1=\tan^2(x)$ et $\tan^2$ est strictement positive sur $\left] n \pi-\frac{\pi}{2},\, n \pi+\frac{\pi}{2}\right[$ sauf en $n\pi$ où elle s'annule. La fonction $f$ est donc strictement croissante sur $\left] n \pi-\frac{\pi}{2},\, n \pi+\frac{\pi}{2}\right[$. Puisqu'elle est de classe $\CC^1$ sur $\left] n \pi-\frac{\pi}{2},\, n \pi+\frac{\pi}{2}\right[$, elle y est continue. Le théorème de la bijection nous garantit que $f$ établit une bijection de $\left] n \pi-\frac{\pi}{2},\, n \pi+\frac{\pi}{2}\right[$ dans \[ \interoo{{\lim_{x\to np-\frac{\pi}{2}} f(x)} {\lim_{x\to np+\frac{\pi}{2}} f(x)}}= \interoo{-\infty{} +\infty}. \]
  Puisque $0\in \R$, l'équation $f(x)=0$ admet une unique solution sur $\left] n \pi-\frac{\pi}{2},\, n \pi+\frac{\pi}{2}\right[$, que l'on note $x_n$.

  Remarquons, puisque cela va nous servira plus tard, que $f(n\pi)=\tan(n\pi)-n\pi = -n\pi < 0$ pour $n\neq 0$. Ainsi, par stricte croissance de $f$, on peut même en déduire que $x_n\in \left] n \pi,\, n \pi+\frac{\pi}{2}\right[$ pour $n\geq 1$.
  \item Par définition de la suite $(x_n)$, on a
  \[ \forall n\in \N,\quad n\pi-\frac{\pi}{2}\leq x_n \leq n\pi+\frac{\pi}{2} \]
  soit
  \[ \forall n\in \N,\quad \pi-\frac{\pi}{2n}\leq \frac{x_n}{n}\leq \pi+\frac{\pi}{2n}. \]
  Puisque $\lim\limits_{n\to +\infty} \frac{\pi}{2n}=0$, par encadrement,
  \[ \lim_{n\to +\infty} \frac{x_n}{n}=\pi \]
  c'est-à-dire $\boxed{x_n \eq{+\infty} n\pi}$.
  \item \begin{enumerate}
  \item Tout d'abord, remarquons que, pour tout $n\geq 1$, nous avons vu que $x_n \in \left] n \pi,\, n \pi+\frac{\pi}{2}\right[$. Ainsi :
  \[  n\pi\leq x_n \leq n\pi+\frac{\pi}{2} \implies 0 \leq y_n < \frac{\pi}{2}. \]
  Calculons alors $\tan(y_n)$ qui a donc un sens :
  { \allowdisplaybreaks
  \begin{align*}
    \tan(y_n) &= \tan\left(\frac{\pi}{2}+n\pi-x_n\right) \\
    &= \tan\left(\frac{\pi}{2}-x_n\right) \text{car $\tan$ est $\pi$-périodique}\\
    &= \frac{\sin\left(\frac{\pi}{2}-x_n\right)}{\cos\left(\frac{\pi}{2}-x_n\right)}\\
    &= \frac{{\cos\left(x_n\right)}}{\sin\left(x_n\right)}\\
    &= \frac{1}{\tan(x_n)} = \frac{1}{x_n} \text{ puisque } \tan(x_n)=x_n.
  \end{align*}}
  Ainsi, $\tan(y_n)=\frac{1}{x_n}$. En appliquant la fonction $\arctan$, définie sur $\R$, et puisque $y_n\in \interoo{-\frac{\pi}{2} \frac{\pi}{2}}$, on peut en déduire que $y_n=\arctan\left(\frac{1}{x_n}\right)$
  \item Puisque $x_n\eq{+\infty} n\pi$, et que $n\pi \tendversen{n\to +\infty} +\infty$, alors $x_n\tendversen{n\to +\infty} +\infty$. Par quotient, $\frac{1}{x_n}\tendversen{n\to +\infty} 0$. Par équivalent classique,
  \[ y_n = \arctan\left(\frac{1}{x_n}\right) \eq{+\infty} \frac{1}{x_n}\]
   D'après ce qui précède, $x_n\eq{+\infty} n\pi$ et donc \[ y_n\eq{+\infty} \frac{1}{n\pi}. \]
   ce qu'on peut également écrire \[ y_n = \frac{1}{n\pi} + \petito[+\infty]{\frac{1}{~\pi}}.\]
   \begin{rappel}
     En effet, $x_n\eq{+\infty} y_n$ si et seulement si $x_n=y_n+\petito[+\infty]{y_n}$.
   \end{rappel}
  \item D'après ce qui précède, $x_n=\frac{\pi}{2}+n\pi - y_n$.
  On peut finalement écrire le développement asymptotique :
  \[ \boxed{ x_n = \frac{\pi}{2}+n\pi - \frac{1}{n\pi} + \petito[+\infty]{\frac{1}{n\pi}}.}\]
\end{enumerate}
\end{enumerate}
\end{correction}
%%% Fin exercice %%%
