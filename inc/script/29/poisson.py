import matplotlib.pyplot as plt # Pour représenter l'histogramme   
import pylab                    # Pour améliorer la présentation
import numpy as np              # Pour l'aléatoire

# On simule une loi geometrique avec np.random.geometric
# np.random.poisson(lambda, nb) simule nb tirages suivant la loi de Poisson de paramètre lambda.
data=np.random.poisson(5,10000)
# On compte les apparitions de chaque valeur
counts = np.bincount(data)/10000
# On dresse l'histogramme
plt.bar(range(max(data)+1), counts, width=0.5, align='center')
# On rend l'histogramme plus propre
plt.xlim(-1,max(data))
pylab.xticks(range(0,max(data)-1))
# On enregistre dans un fichier
plt.savefig('loi_poisson.eps') 
# On affiche
plt.show()
