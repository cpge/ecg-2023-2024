import matplotlib.pyplot as plt # Pour représenter l'histogramme   
import pylab                    # Pour améliorer la présentation
import numpy as np              # Pour l'aléatoire

# On simule une loi uniforme avec np.random.randint
# np.random.randint(n,p, nb) simule nb tirages 
# suivant la loi uniforme sur $\interent{n p-1}$.
data=np.random.randint(1,9,10000)
# On compte les apparitions de chaque valeur
counts = np.bincount(data)/10000
# On dresse l'histogramme
plt.bar(range(9), counts, width=0.5, align='center')
# On rend l'histogramme plus propre
plt.xlim(0,9)
pylab.xticks(range(1,9))
# On enregistre dans un fichier
plt.savefig('loi_uniforme.png') 
# On affiche
plt.show()
