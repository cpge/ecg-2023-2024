import numpy as np
import matplotlib.pyplot as plt

# Définition de la fonction phi
def phi(x): return 1/np.sqrt(2*np.pi)*np.exp((-x**2)/2)

def Phi(x,eps):
   ''' Fonction Phi prend 2 arguments 
        - x, la valeur en laquelle on la calcule
        - eps, la précision demandée
      Elle renvoie une valeur approchée de Phi(x) '''
   inf=1/2 # On commence à 1/2
   n = int( np.floor(x**2/(8*eps))+1) # Pour avoir la bonne précision
   for i in range(n):
      inf = inf+x/n*phi(i*x/n)
   return inf

t = np.arange(-5., 5.0, 0.1)
y = [Phi(u, 10**(-4)) for u in t]
#y = np.array( [Phi(t, 10**(-4)) for t in range(-5,5,.01)])
plt.plot(t, y, 'b')
plt.title("Fonction Φ")
plt.savefig('fonctionphi.eps', format='eps')  
#plt.show()
