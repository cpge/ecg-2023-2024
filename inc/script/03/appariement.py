def appariement(L):
  if len(L)<=1: return L # S'il y a un seul élement, on ne peut rien faire
  M=L[:]                 # On créé une copie
  tirage=[]              # Notre tirage, pour l'instant vide
  while len(M)>=2:       # Tant qu'on peut faire des couples
    petit=min(M)         # On prend le plus grand
    grand=max(M)         # On prend le plus petit
    tirage.append([L.index(petit), L.index(grand)]) # On cherche l'index (i.e.
    M.remove(petit)      # la position du plus petit et grand, puis on les 
    M.remove(grand)      # enlève de la liste.
  return tirage	

tirage = [10, 32, 29, 45, 72, 2]

print(appariement(tirage))
