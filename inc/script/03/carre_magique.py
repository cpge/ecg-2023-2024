def ligne_magique(L):
  n=len(L)
  somme=0
  for i in range(n):
    somme=somme+L[0][i]
  for i in range(1,n):
      ligne=0
      for j in range(n):
        ligne = ligne + L[i][j]
      if (ligne!=somme):
        return False
  return True
