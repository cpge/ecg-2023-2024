import math

def f(x): return math.sqrt(1+x**2)

def trapeze(a,b,n):
   ''' Fonction trapeze prend 3 arguments 
        - [a b] désigne le segment sur lequel on calcule l'intégrale
        - n représente le nombre de subdivision 
      Elle renvoie une valeur approchée de l'intégrale '''
   inf=0
   sup=0
   for i in range(n):
      inf = inf+(b-a)/n*f(a+i*(b-a)/n)
      sup = sup+(b-a)/n*f(a+(i+1)*(b-a)/n)
   return 1/2*(inf+sup)
