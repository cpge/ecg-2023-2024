import math

def f(x): return math.sqrt(1+x**2)

def rectangle(a,b,n):
   ''' Fonction rectangle prend 3 arguments 
        - [a b] désigne le segment sur lequel on calcule l'intégrale
        - n représente le nombre de subdivision 
      Elle renvoie une valeur approchée de l'intégrale '''
   inf=0
   for i in range(n):
      inf = inf+(b-a)/n*f(a+i*(b-a)/n)
   return inf
