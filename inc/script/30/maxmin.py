import matplotlib.pyplot as plt # Pour représenter l'histogramme   
import pylab                    # Pour améliorer la présentation
import numpy as np              # Pour l'aléatoire

X=[i for i in range(1,7)]
Y=[i for i in range(1,7)]
P1=[(1+2*(6-i))/36 for i in range(1,7)]
P2=[(1+2*(i-1))/36 for i in range(1,7)]

plt.bar(X, P1, width=0.5, align='center')
plt.xlim(0,max(X)+1) # On rend l'histogramme plus propre
pylab.xticks(range(1,max(X)+1))
plt.savefig('minmax1.eps') # On enregistre dans un fichier
plt.show() # On affiche

plt.bar(Y, P2, width=0.5, align='center')
plt.xlim(0,max(Y)+1) # On rend l'histogramme plus propre
pylab.xticks(range(1,max(Y)+1))
plt.savefig('minmax2.eps')  # On enregistre dans un fichier
plt.show()
