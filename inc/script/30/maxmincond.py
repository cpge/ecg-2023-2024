import numpy as np
import matplotlib.pyplot as plt
import pylab                    # Pour améliorer la présentation


def probcond(i,j):
    if j==i: return 1/(1+2*(6-i))
    if j<i: return 0
    return 2/(1+2*(6-i))

data = [[probcond(i,j) for j in range(1,7)] for i in range(1,7)]

X = np.arange(6)
fig = plt.figure()

for i in range(6):
    plt.bar(X+1 + i*0.15, data[i], width = 0.10)
plt.xlim(0,max(X)+2)
pylab.xticks(np.arange(1, 7.4,step=1.075), [1,2,3,4,5,6])
plt.savefig('minmaxcond.eps')

plt.show()
