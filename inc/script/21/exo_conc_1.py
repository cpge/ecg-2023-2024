from numpy import sqrt
import matplotlib.pyplot as plt
import pylab

def h(e):
    k = 0
    a = sqrt(3) / 2
    b = 1 / 2
    while ((2**k)*(2*a+a/b) - 9*(2**k)*a/(2+b))>e:
        a = sqrt((1-b)/2)
        b = sqrt((1+b)/2)
        k = k + 1
    x = (2**k) *(2*a+a/b)
    return (x, k)
    
def liste(p):
    res = []
    for i in range(1, p+1):
        val, prec = h(10**(-i))
        res.append(prec)
    return res
        

plt.plot([i for i in range(1, 31)], liste(30), "x")
# On rend l'histogramme plus propre
pylab.xticks(range(0,31,2))
# On enregistre dans un fichier
plt.savefig('exo_conc_1.png')
# On affiche
plt.show()