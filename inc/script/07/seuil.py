def seuil(val):
   n=0
   u=1
   while u<val:
      n=n+1     # on passe au rang suivant
      u=1+u**2  # on calcule le terme suivant de la suite
   return n     # on renvoie le rang du premier dépassement
