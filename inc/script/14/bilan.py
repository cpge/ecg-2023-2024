from numpy import exp, log # Pour utiliser les fonctions

def suiteu(n):
    u = exp(1) # u0
    for i in range(n):
    	u = u/log(u+1)
    return u
