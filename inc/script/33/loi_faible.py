import matplotlib.pyplot as plt     # Pour représenter l'histogramme
import numpy as np                  # Pour l'aléatoire
plt.rcParams['text.usetex'] = True  # Pour afficher du latex

n=15000 # nombre de lancers dans une expérience
m=2000  # nombre de répétitions de l'expérience
p=0.5   # proba de succès

# Simulation de m tirages d'une binomiale de paramètre n et p
simu = np.random.binomial(n,p,m)

# On calcule (X_1+...+X_n)/n
X_mean =np.cumsum(simu)/np.arange(1,m+1)

# On représente
plt.plot(range(1, m+1), X_mean, "r", label="$\overline{X}_n$") # (X_n)
plt.plot((1,m),(n*p,n*p),"b--",label="Espérance", linewidth=1) # espérance
plt.xlabel("$n$")                                        # Pour améliorer
plt.legend(loc='best')                                   # la représentation
plt.title("Loi faible des grands nombres")               # graphique
plt.savefig('loi_faible_grands_nombres.pdf',dpi=300)     # On enregistre
plt.show()                                               # On affiche
