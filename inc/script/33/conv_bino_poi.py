import matplotlib.pyplot as plt     # Pour représenter l'histogramme
import numpy as np                  # Pour l'aléatoire
import math                         # Pour les combinaisons et factoriels
plt.rcParams['text.usetex'] = True  # Pour afficher du latex

lamb = 10

list_n = [20, 100, 500, 1000]

prob_bino = [[ math.comb(n, k)*((lamb/n)**k) * ((1-lamb/n)**(n-k)) for k in range(n+1)] for n in list_n]
prob_poi = [ math.exp(-lamb)* (lamb**k)/math.factorial(k) for k in range(101)]

for i in range(len(list_n)):
    plt.clf()
    plt.bar(range(list_n[i]+1), prob_bino[i], color="#008080", 
	width=.5, label="$\mathcal{B}(n, \lambda/n)$")
    plt.plot(range(100+1), prob_poi, "r", label="$\mathcal{P}(\lambda)$")
    plt.xlabel("$k$")                                      # Pour améliorer
    plt.legend(loc='best')                                 # la représentation
    plt.title("Convergence avec $n = $"+str(list_n[i]))    # graphique
    plt.xlim(-1, 30)
    plt.savefig('conv_bino_poi_'+str(list_n[i])+".pdf",dpi=300) 



