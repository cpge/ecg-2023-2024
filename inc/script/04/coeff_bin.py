def coeff_bin(n,p):
    if n<0 or p<0: return 0                        # Cas négatif, convention: 0
    if p==0 or p==n: return 1                      # Cas $\binom{n}{0}=\binom{n}{n}=1$
    return coeff_bin(n-1,p-1)+coeff_bin(n-1,p)     # Formule de Pascal
