input TEX;

verbatimtex
%&latex
 \documentclass{article}
 \usepackage[latin1]{inputenc}
 \usepackage[frenchb]{babel}
 \usepackage{amsmath}
 \begin{document}
etex

% up to 34 digits of precision
pi := 3.141592653589793238462643383279503;

% conversions
vardef degrees(expr theta) = 180 / pi * theta enddef;
vardef radians(expr theta) = pi / 180 * theta enddef;
% trig functions that expect radians
vardef sin(expr theta) = sind(degrees(theta)) enddef;
vardef cos(expr theta) = cosd(degrees(theta)) enddef;
% inverse trig functions
vardef acosd(expr a) = angle (a,1+-+a) enddef;
vardef asind(expr a) = angle (1+-+a,a) enddef;
vardef acos(expr a) = radians(acosd(a)) enddef;
vardef asin(expr a) = radians(asind(a)) enddef;
% tangents
vardef tand(expr theta) = save x,y; (x,y)=dir theta; y/x enddef;
vardef atand(expr a) = angle (1,a) enddef;


outputtemplate := "arctan.mps";
beginfig(1)
  u:=1.2cm;

  path ctan; numeric N;
  path carctan;
  
  carctan=(-5, radians(atand(-5))) for i=-4.9 step .1 until 5: ..(i, radians(atand (i))) endfor;
  ctan=(-1.35, sin(-1.35)/cos(-1.35)) for i=-1.34 step .01 until 1.35: ..(i, sin(i)/cos(i)) endfor;
  
%  ctan=(tan (-1.57), -1.57) for i=-1.57 step .1 until 1.57: ..(tan (i) ,i) endfor;

  draw carctan scaled u withcolor red withpen pencircle scaled 2bp;
  draw ctan scaled u withcolor blue withpen pencircle scaled 1.5bp;

  pair O,A,B,C;
  A:=(-4u,-4u);
  B:=(4u, 4u);
  draw A--B withpen pencircle scaled 1.2bp;
  
  % asymptote
  drawoptions(dashed evenly withcolor .5red);
  draw (-5u,-pi/2*u)--(5u,-pi/2*u);
  label.bot(btex $y=-\frac{\pi}{2}$etex, (3u, -pi/2*u));
  draw (-5u,pi/2*u)--(5u,pi/2*u);
  label.top(btex $y=\frac{\pi}{2}$etex, (-3u, pi/2*u));
  drawoptions();
   
  for i=-4 upto -1: draw (i*u,-0.1u)--(i*u,0.1u); label.bot (TEX("$" & decimal i & "$") scaled 1, (i*u, -0.1*u)); endfor;
  for i=1 upto 4: draw (i*u,-0.1u)--(i*u,0.1u); label.bot (TEX("$" & decimal i & "$") scaled 1, (i*u, -0.1*u)); endfor;

  for j=-4 upto -1: draw (-0.1u,j*u)--(0.1u,j*u); label.rt (TEX("$" & decimal j & "$") scaled 1, (0.1*u, j*u)); endfor;
  for j=1 upto 4: draw (-0.1u,j*u)--(0.1u,j*u); label.rt (TEX("$" & decimal j & "$") scaled 1, (0.1*u, j*u)); endfor;
  drawarrow (-5u,0)--(5u,0);
  drawarrow (0,-5u)--(0,5u);

  label.urt (btex $y=x$ etex, (4u, 4u));
  label.bot (btex $\arctan$ etex, (4u, (radians(atand(4))-0.2)*u)) withcolor red; 
  label.rt (btex $\tan$ etex, (1.3u, sin(1.3)/cos(1.3)*u)) withcolor blue;

endfig;

end
