%!PS
%%BoundingBox: -77 -77 77 77 
%%HiResBoundingBox: -76.7862 -76.7862 76.7862 76.7862 
%%Creator: MetaPost 2.02
%%CreationDate: 2023.03.30:0943
%%Pages: 1
%*Font: cmr10 6.97382 9.96265 30:f8
%*Font: cmsy10 6.97382 9.96265 00:8
%%BeginProlog
%%EndProlog
%%Page: 1 1
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath -68.03217 -68.03217 moveto
-51.02412 -68.03217 lineto stroke
newpath -51.02412 -51.02412 moveto
-34.01608 -51.02412 lineto stroke
newpath -34.01608 -34.01608 moveto
-17.00804 -34.01608 lineto stroke
newpath -17.00804 -17.00804 moveto
0 -17.00804 lineto stroke
newpath 0 0 moveto
17.00804 0 lineto stroke
newpath 17.00804 17.00804 moveto
34.01608 17.00804 lineto stroke
newpath 34.01608 34.01608 moveto
51.02412 34.01608 lineto stroke
newpath 51.02412 51.02412 moveto
68.03217 51.02412 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 17.00804 -1.36067 moveto
17.00804 1.36067 lineto stroke
15.26459 -9.19518 moveto
(1) cmr10 6.97382 fshow
newpath 34.01608 -1.36067 moveto
34.01608 1.36067 lineto stroke
32.27263 -9.19518 moveto
(2) cmr10 6.97382 fshow
newpath 51.02412 -1.36067 moveto
51.02412 1.36067 lineto stroke
49.28067 -9.19518 moveto
(3) cmr10 6.97382 fshow
newpath 68.03217 -1.36067 moveto
68.03217 1.36067 lineto stroke
66.28871 -9.19518 moveto
(4) cmr10 6.97382 fshow
newpath -68.03217 -1.36067 moveto
-68.03217 1.36067 lineto stroke
-72.48769 -9.19518 moveto
(\000) cmsy10 6.97382 fshow
-67.06361 -9.19518 moveto
(4) cmr10 6.97382 fshow
newpath -51.02412 -1.36067 moveto
-51.02412 1.36067 lineto stroke
-55.47964 -9.19518 moveto
(\000) cmsy10 6.97382 fshow
-50.05557 -9.19518 moveto
(3) cmr10 6.97382 fshow
newpath -34.01608 -1.36067 moveto
-34.01608 1.36067 lineto stroke
-38.4716 -9.19518 moveto
(\000) cmsy10 6.97382 fshow
-33.04753 -9.19518 moveto
(2) cmr10 6.97382 fshow
newpath -17.00804 -1.36067 moveto
-17.00804 1.36067 lineto stroke
-21.46356 -9.19518 moveto
(\000) cmsy10 6.97382 fshow
-16.03949 -9.19518 moveto
(1) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -0.85045 -68.03217 moveto
0.85045 -68.03217 lineto stroke
-12.76147 -69.98874 moveto
(\000) cmsy10 6.97382 fshow
-7.3374 -69.98874 moveto
(4) cmr10 6.97382 fshow
newpath -0.85045 -51.02412 moveto
0.85045 -51.02412 lineto stroke
-12.76147 -52.9807 moveto
(\000) cmsy10 6.97382 fshow
-7.3374 -52.9807 moveto
(3) cmr10 6.97382 fshow
newpath -0.85045 -34.01608 moveto
0.85045 -34.01608 lineto stroke
-12.76147 -35.97266 moveto
(\000) cmsy10 6.97382 fshow
-7.3374 -35.97266 moveto
(2) cmr10 6.97382 fshow
newpath -0.85045 -17.00804 moveto
0.85045 -17.00804 lineto stroke
-12.76147 -18.96461 moveto
(\000) cmsy10 6.97382 fshow
-7.3374 -18.96461 moveto
(1) cmr10 6.97382 fshow
newpath -0.85045 0 moveto
0.85045 0 lineto stroke
-7.33734 -2.24713 moveto
(0) cmr10 6.97382 fshow
newpath -0.85045 17.00804 moveto
0.85045 17.00804 lineto stroke
-7.33734 14.76091 moveto
(1) cmr10 6.97382 fshow
newpath -0.85045 34.01608 moveto
0.85045 34.01608 lineto stroke
-7.33734 31.76895 moveto
(2) cmr10 6.97382 fshow
newpath -0.85045 51.02412 moveto
0.85045 51.02412 lineto stroke
-7.33734 48.777 moveto
(3) cmr10 6.97382 fshow
newpath -0.85045 68.03217 moveto
0.85045 68.03217 lineto stroke
-7.33734 65.78503 moveto
(4) cmr10 6.97382 fshow
-5.58688 -8.80539 moveto
(0) cmr10 6.97382 fshow
newpath -76.5362 0 moveto
76.5362 0 lineto stroke
newpath 72.83974 -1.53116 moveto
76.5362 0 lineto
72.83974 1.53116 lineto
 closepath
gsave fill grestore stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 -76.5362 moveto
0 76.5362 lineto stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath 1.53116 72.83974 moveto
0 76.5362 lineto
-1.53116 72.83974 lineto
 closepath
gsave fill grestore stroke
showpage
%%EOF
