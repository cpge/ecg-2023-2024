input TEX;

verbatimtex
%&latex
 \documentclass{article}
 \usepackage[latin1]{inputenc}
 \usepackage[frenchb]{babel}
 \usepackage{amsmath}
 \begin{document}
etex

% up to 34 digits of precision
pi := 3.141592653589793238462643383279503;

% conversions
vardef degrees(expr theta) = 180 / pi * theta enddef;
vardef radians(expr theta) = pi / 180 * theta enddef;
% trig functions that expect radians
vardef sin(expr theta) = sind(degrees(theta)) enddef;
vardef cos(expr theta) = cosd(degrees(theta)) enddef;
% inverse trig functions
vardef acosd(expr a) = angle (a,1+-+a) enddef;
vardef asind(expr a) = angle (1+-+a,a) enddef;
vardef acos(expr a) = radians(acosd(a)) enddef;
vardef asin(expr a) = radians(asind(a)) enddef;


outputtemplate := "arccos.mps";
beginfig(1)
  u:=2.2cm;
  v:=2.2cm;

  path courbe; 
  
  courbe=(-1*u, acos(-1)*v) for i=-0.99 step .01 until 1: ..(i*u,  acos(i)*v) endfor ..(1*u,0);
  

  draw courbe withcolor red withpen pencircle scaled 1.5bp;

  % asymptote
  drawoptions(dashed withdots scaled .5 withcolor .5blue);
  draw (-1*u, pi*v)--(0,pi*v);
  drawoptions(withcolor .5blue);
  label.rt(btex $\pi$ etex scaled .7, (0, pi*v));
  dotlabel.urt(btex $\pi\over 2$ etex scaled .7, (0, pi/2*v));
  drawoptions();
   
  for i=-1 upto 1: draw (i*u,-0.05v)--(i*u,0.05v); if i<>0: label.bot (TEX("$" & decimal i & "$") scaled .7, (i*u, -0.05*v)); fi; endfor;
  for j=1 upto 3: draw (-0.05u,j*v)--(0.05u,j*v);label.rt (TEX("$" & decimal j & "$") scaled .7, (0.05*u, j*v)); endfor;

  drawarrow (-1.2u,0)--(1.2u,0);
  drawarrow (0,-.5v)--(0,3.5v);
  
  % label
  label.rt(btex $\arccos$ etex, (.5*u, 2.5*v));
  label.llft(btex $0$ etex scaled .7, (0,-0.05v));


endfig;

end
