function max_des()
    x=1:6;
    y=zeros(6,6);
    for i=x
        y(i,i)= 1/(1+2*(6-i));
        for j=(i+1):6
            y(i,j)=2/(1+2*(6-i));
        end
    end
    clf()
    scf(0)
    bar(x,y');
    xs2pdf(0,"batons_max_des");
endfunction
