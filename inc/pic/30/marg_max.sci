function marg_max()
    x=1:6;
    y=zeros(1,6)
    for j=x
        y(j)=(1+2*(j-1))/36;
    end
    clf()
    scf(0)
    bar(x,y');
    xs2pdf(0,"batons_marg_max");
endfunction
