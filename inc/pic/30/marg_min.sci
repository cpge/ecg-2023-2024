function marg_min()
    x=1:6;
    y=zeros(1,6)
    for i=x
        y(i)=(1+2*(6-i))/36;
    end
    clf()
    scf(0)
    bar(x,y');
    xs2pdf(0,"batons_marg_min");
endfunction
