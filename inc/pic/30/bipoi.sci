function y=binom(n)
    // fournit le vecteur ligne des coef du binomes 
    y=1;
    i=0;
    while (i<n)
        y = [y,0]+[0,y]; // triangle de pascal
        i=i+1;
    end
endfunction

function bipoi()
    n=302;
    p=0.01;
    theta=n*p;
    borne=11;
    x=0:borne;
    y = binom(n);
    bi = y(1+x) .* p^x .*(1-p)^(n-x);
    poi = exp(-theta) * theta^x./[1,cumprod(1:borne)];
    clf()
    scf(0)
    bar(x,[bi;poi]');
    xs2pdf(0,"Bipoi");
endfunction

