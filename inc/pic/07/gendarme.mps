%!PS
%%BoundingBox: -15 -27 298 121 
%%HiResBoundingBox: -14.42323 -26.89114 297.88774 120.43452 
%%Creator: MetaPost 2.02
%%CreationDate: 2023.03.26:2226
%%Pages: 1
%*Font: cmmi10 6.97382 9.96265 75:e
%*Font: cmr7 4.88167 6.97385 30:ffc
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0 0 setrgbcolor 0 0.5 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath -14.17323 0 moveto
297.63774 0 lineto stroke
newpath 293.941 -1.53128 moveto
297.63774 0 lineto
293.941 1.53128 lineto
 closepath
gsave fill grestore stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 -14.17323 moveto
0 119.05501 lineto stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath 1.53105 115.35881 moveto
0 119.05501 lineto
-1.53105 115.35881 lineto
 closepath
gsave fill grestore stroke
 0.2 0.8 0 setrgbcolor 0 2 dtransform truncate idtransform setlinewidth pop
newpath 28.34645 -19.84242 moveto 0 0 rlineto stroke
24.78606 -25.84506 moveto
(u) cmmi10 6.97382 fshow
28.77827 -26.89114 moveto
(1) cmr7 4.88167 fshow
newpath 56.6929 18.42502 moveto 0 0 rlineto stroke
53.1325 12.42238 moveto
(u) cmmi10 6.97382 fshow
57.12473 11.3763 moveto
(2) cmr7 4.88167 fshow
newpath 85.03935 31.18127 moveto 0 0 rlineto stroke
81.47896 25.17863 moveto
(u) cmmi10 6.97382 fshow
85.47118 24.13255 moveto
(3) cmr7 4.88167 fshow
newpath 113.3858 37.55896 moveto 0 0 rlineto stroke
109.82541 31.55632 moveto
(u) cmmi10 6.97382 fshow
113.81763 30.51024 moveto
(4) cmr7 4.88167 fshow
newpath 141.73225 41.386 moveto 0 0 rlineto stroke
138.17186 35.38336 moveto
(u) cmmi10 6.97382 fshow
142.16408 34.33728 moveto
(5) cmr7 4.88167 fshow
newpath 170.0787 43.93709 moveto 0 0 rlineto stroke
166.51831 37.93445 moveto
(u) cmmi10 6.97382 fshow
170.51053 36.88837 moveto
(6) cmr7 4.88167 fshow
newpath 198.42516 45.75934 moveto 0 0 rlineto stroke
194.86476 39.7567 moveto
(u) cmmi10 6.97382 fshow
198.85698 38.71062 moveto
(7) cmr7 4.88167 fshow
newpath 226.7716 47.12614 moveto 0 0 rlineto stroke
223.21121 41.1235 moveto
(u) cmmi10 6.97382 fshow
227.20343 40.07742 moveto
(8) cmr7 4.88167 fshow
newpath 255.11806 48.18887 moveto 0 0 rlineto stroke
251.55766 42.18623 moveto
(u) cmmi10 6.97382 fshow
255.54988 41.14015 moveto
(9) cmr7 4.88167 fshow
newpath 283.46451 49.03925 moveto 0 0 rlineto stroke
278.51419 43.0366 moveto
(u) cmmi10 6.97382 fshow
282.50641 41.99052 moveto
(10) cmr7 4.88167 fshow
 0 0 1 setrgbcolor
newpath 28.34645 113.3858 moveto 0 0 rlineto stroke
24.28583 117.43188 moveto
(w) cmmi10 6.97382 fshow
29.27849 116.3858 moveto
(1) cmr7 4.88167 fshow
newpath 56.6929 85.03935 moveto 0 0 rlineto stroke
52.63228 89.08543 moveto
(w) cmmi10 6.97382 fshow
57.62494 88.03935 moveto
(2) cmr7 4.88167 fshow
newpath 85.03935 75.59068 moveto 0 0 rlineto stroke
80.97873 79.63676 moveto
(w) cmmi10 6.97382 fshow
85.97139 78.59068 moveto
(3) cmr7 4.88167 fshow
newpath 113.3858 70.86613 moveto 0 0 rlineto stroke
109.32518 74.91222 moveto
(w) cmmi10 6.97382 fshow
114.31784 73.86613 moveto
(4) cmr7 4.88167 fshow
newpath 141.73225 68.03131 moveto 0 0 rlineto stroke
137.67163 72.0774 moveto
(w) cmmi10 6.97382 fshow
142.66429 71.03131 moveto
(5) cmr7 4.88167 fshow
newpath 170.0787 66.14157 moveto 0 0 rlineto stroke
166.01808 70.18765 moveto
(w) cmmi10 6.97382 fshow
171.01074 69.14157 moveto
(6) cmr7 4.88167 fshow
newpath 198.42516 64.79207 moveto 0 0 rlineto stroke
194.36453 68.83815 moveto
(w) cmmi10 6.97382 fshow
199.3572 67.79207 moveto
(7) cmr7 4.88167 fshow
newpath 226.7716 63.77951 moveto 0 0 rlineto stroke
222.71098 67.82559 moveto
(w) cmmi10 6.97382 fshow
227.70364 66.77951 moveto
(8) cmr7 4.88167 fshow
newpath 255.11806 62.99231 moveto 0 0 rlineto stroke
251.05743 67.03839 moveto
(w) cmmi10 6.97382 fshow
256.0501 65.99231 moveto
(9) cmr7 4.88167 fshow
newpath 283.46451 62.3621 moveto 0 0 rlineto stroke
278.01398 66.40819 moveto
(w) cmmi10 6.97382 fshow
283.00664 65.3621 moveto
(10) cmr7 4.88167 fshow
 1 0 0 setrgbcolor
newpath 28.34645 45.35449 moveto 0 0 rlineto stroke
31.34645 44.3762 moveto
(v) cmmi10 6.97382 fshow
34.7268 43.33012 moveto
(1) cmr7 4.88167 fshow
newpath 56.6929 62.3621 moveto 0 0 rlineto stroke
59.6929 61.38382 moveto
(v) cmmi10 6.97382 fshow
63.07326 60.33774 moveto
(2) cmr7 4.88167 fshow
newpath 85.03935 52.91344 moveto 0 0 rlineto stroke
88.03935 51.93515 moveto
(v) cmmi10 6.97382 fshow
91.41971 50.88907 moveto
(3) cmr7 4.88167 fshow
newpath 113.3858 59.52773 moveto 0 0 rlineto stroke
116.3858 58.54944 moveto
(v) cmmi10 6.97382 fshow
119.76616 57.50336 moveto
(4) cmr7 4.88167 fshow
newpath 141.73225 54.42514 moveto 0 0 rlineto stroke
144.73225 53.44685 moveto
(v) cmmi10 6.97382 fshow
148.11261 52.40077 moveto
(5) cmr7 4.88167 fshow
newpath 170.0787 58.58264 moveto 0 0 rlineto stroke
173.0787 57.60435 moveto
(v) cmmi10 6.97382 fshow
176.45906 56.55827 moveto
(6) cmr7 4.88167 fshow
newpath 198.42516 55.07307 moveto 0 0 rlineto stroke
201.42516 54.09479 moveto
(v) cmmi10 6.97382 fshow
204.80551 53.0487 moveto
(7) cmr7 4.88167 fshow
newpath 226.7716 58.1103 moveto 0 0 rlineto stroke
229.7716 57.13202 moveto
(v) cmmi10 6.97382 fshow
233.15196 56.08594 moveto
(8) cmr7 4.88167 fshow
newpath 255.11806 55.43294 moveto 0 0 rlineto stroke
258.11806 54.45465 moveto
(v) cmmi10 6.97382 fshow
261.49841 53.40857 moveto
(9) cmr7 4.88167 fshow
newpath 283.46451 57.82657 moveto 0 0 rlineto stroke
286.46451 56.84828 moveto
(v) cmmi10 6.97382 fshow
289.84486 55.8022 moveto
(10) cmr7 4.88167 fshow
showpage
%%EOF
