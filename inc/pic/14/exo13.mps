%!PS
%%BoundingBox: -21 -21 120 179 
%%HiResBoundingBox: -20.09372 -20.09372 119.3063 178.83144 
%%Creator: MetaPost 2.00
%%CreationDate: 2019.12.13:2238
%%Pages: 1
%*Font: cmr10 6.97382 9.96265 2e:bfe
%*Font: cmr10 9.96265 9.96265 43:8000000168092
%%BeginProlog
%%EndProlog
%%Page: 1 1
 1 0 0 setrgbcolor 0 2 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 0 0 moveto
-2.254 28.20769 4.70953 56.38486 19.84372 80.29533 curveto
22.79985 84.96574 26.05743 89.44481 29.7656 93.54468 curveto
32.79753 96.8969 36.116 99.97859 39.68745 102.74896 curveto
42.8199 105.1788 46.1368 107.36113 49.60931 109.27371 curveto
52.80507 111.03387 56.1235 112.56052 59.53117 113.86375 curveto
62.77267 115.10344 66.08832 116.13847 69.45305 116.98837 curveto
72.72484 117.81479 76.0382 118.46497 79.37491 118.9685 curveto
82.66531 119.46506 85.97571 119.81862 89.29677 120.0282 curveto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 19.84372 -1.98315 moveto
19.84372 1.98315 lineto stroke
15.3882 -9.47739 moveto
(0.1) cmr10 6.97382 fshow
newpath 39.68443 -1.98315 moveto
39.68443 1.98315 lineto stroke
35.22891 -9.47739 moveto
(0.2) cmr10 6.97382 fshow
newpath 59.52815 -1.98315 moveto
59.52815 1.98315 lineto stroke
55.07263 -9.47739 moveto
(0.3) cmr10 6.97382 fshow
newpath 79.36885 -1.98315 moveto
79.36885 1.98315 lineto stroke
74.91333 -9.47739 moveto
(0.4) cmr10 6.97382 fshow
newpath 99.21259 -1.98315 moveto
99.21259 1.98315 lineto stroke
94.75706 -9.47739 moveto
(0.5) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.98315 19.84372 moveto
1.98315 19.84372 lineto stroke
-11.91103 17.5966 moveto
(0.1) cmr10 6.97382 fshow
newpath -1.98315 39.68443 moveto
1.98315 39.68443 lineto stroke
-11.91103 37.43732 moveto
(0.2) cmr10 6.97382 fshow
newpath -1.98315 59.52815 moveto
1.98315 59.52815 lineto stroke
-11.91103 57.28104 moveto
(0.3) cmr10 6.97382 fshow
newpath -1.98315 79.36885 moveto
1.98315 79.36885 lineto stroke
-11.91103 77.12173 moveto
(0.4) cmr10 6.97382 fshow
newpath -1.98315 99.21259 moveto
1.98315 99.21259 lineto stroke
-11.91103 96.96547 moveto
(0.5) cmr10 6.97382 fshow
newpath -1.98315 119.0563 moveto
1.98315 119.0563 lineto stroke
-11.91103 116.80919 moveto
(0.6) cmr10 6.97382 fshow
newpath -1.98315 138.897 moveto
1.98315 138.897 lineto stroke
-11.91103 136.64989 moveto
(0.7) cmr10 6.97382 fshow
newpath -1.98315 158.74072 moveto
1.98315 158.74072 lineto stroke
-11.91103 156.4936 moveto
(0.8) cmr10 6.97382 fshow
2.09999 -8.57738 moveto
(0) cmr10 6.97382 fshow
newpath -19.84372 0 moveto
119.0563 0 lineto stroke
newpath 115.36137 -1.53053 moveto
119.0563 0 lineto
115.36137 1.53053 lineto
 closepath
gsave fill grestore stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 -19.84372 moveto
0 178.58144 lineto stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath 1.53061 174.88629 moveto
0 178.58144 lineto
-1.53061 174.88629 lineto
 closepath
gsave fill grestore stroke
12.96513 163.67792 moveto
(Courb) cmr10 9.96265 fshow
40.39014 163.67792 moveto
(e) cmr10 9.96265 fshow
48.13884 163.67792 moveto
(de) cmr10 9.96265 fshow
61.42233 163.67792 moveto
(g) cmr10 9.96265 fshow
showpage
%%EOF
