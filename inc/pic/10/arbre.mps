%!PS
%%BoundingBox: -1 -75 220 96 
%%HiResBoundingBox: -0.25 -74.07634 219.87845 95.33617 
%%Creator: MetaPost 2.02
%%CreationDate: 2023.11.09:1557
%%Pages: 1
%*Font: cmr10 9.96265 9.96265 31:d000000044
%*Font: cmr7 5.92781 6.97385 30:fa
%*Font: cmmi10 8.4683 9.96265 3d:8
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0 0 setrgbcolor 0 0.5 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 0 0 moveto
113.3858 49.6063 lineto stroke
59.1615 36.9764 moveto
(6) cmr7 5.92781 fshow
61.59618 35.24043 moveto
(=) cmmi10 8.4683 fshow
65.35992 35.24043 moveto
(10) cmr7 5.92781 fshow
116.3858 46.2024 moveto
(R) cmr10 9.96265 fshow
newpath 0 0 moveto
113.3858 -49.6063 lineto stroke
63.95334 -22.55174 moveto
(4) cmr7 5.92781 fshow
66.38802 -24.2877 moveto
(=) cmmi10 8.4683 fshow
70.15176 -24.2877 moveto
(10) cmr7 5.92781 fshow
116.3858 -53.0102 moveto
(V) cmr10 9.96265 fshow
newpath 126.7194 49.6063 moveto
211.75876 92.12598 lineto stroke
170.28325 82.2008 moveto
(1) cmr7 5.92781 fshow
172.71793 80.46484 moveto
(=) cmmi10 8.4683 fshow
176.48167 80.46484 moveto
(2) cmr7 5.92781 fshow
214.75876 88.91577 moveto
(1) cmr10 9.96265 fshow
newpath 126.7194 49.6063 moveto
211.75876 49.6063 lineto stroke
172.95654 57.31982 moveto
(1) cmr7 5.92781 fshow
175.39122 55.58386 moveto
(=) cmmi10 8.4683 fshow
179.15497 55.58386 moveto
(3) cmr7 5.92781 fshow
214.75876 46.39609 moveto
(2) cmr10 9.96265 fshow
newpath 126.7194 49.6063 moveto
211.75876 7.08661 lineto stroke
175.62984 31.17667 moveto
(1) cmr7 5.92781 fshow
178.06451 29.4407 moveto
(=) cmmi10 8.4683 fshow
181.82826 29.4407 moveto
(6) cmr7 5.92781 fshow
214.75876 3.8764 moveto
(4) cmr10 9.96265 fshow
newpath 126.8578 -49.6063 moveto
211.89716 -28.34645 lineto stroke
171.64388 -29.31554 moveto
(1) cmr7 5.92781 fshow
174.07855 -31.0515 moveto
(=) cmmi10 8.4683 fshow
177.8423 -31.0515 moveto
(2) cmr7 5.92781 fshow
214.89716 -31.55666 moveto
(2) cmr10 9.96265 fshow
newpath 126.8578 -49.6063 moveto
211.89716 -70.86613 lineto stroke
174.54344 -54.82698 moveto
(1) cmr7 5.92781 fshow
176.97812 -56.56294 moveto
(=) cmmi10 8.4683 fshow
180.74187 -56.56294 moveto
(2) cmr7 5.92781 fshow
214.89716 -74.07634 moveto
(4) cmr10 9.96265 fshow
showpage
%%EOF
