%!PS
%%BoundingBox: -1 -65 158 68 
%%HiResBoundingBox: -0.40001 -64.17953 157.7227 67.03392 
%%Creator: MetaPost 2.000
%%CreationDate: 2018.01.26:1345
%%Pages: 1
%*Font: cmr10 9.96265 9.96265 2b:8
%*Font: cmsy10 9.96265 9.96265 00:80000000000000001
%*Font: cmmi7 6.97385 6.97385 66:8
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0 0 setrgbcolor 0 1 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 42.51968 0 moveto
52.39406 11.76762 40.44386 33.95653 55.27573 42.51968 curveto
79.39406 56.44437 72.38638 12.65298 85.03937 0 curveto
99.82808 -14.78871 88.28534 -47.17143 110.55144 -55.27573 curveto
141.35532 -66.48755 132.47829 -16.3406 148.8189 0 curveto stroke
 0.7 0.7 0.7 setrgbcolor
newpath 42.51968 0 moveto
52.39406 11.76762 40.44386 33.95653 55.27573 42.51968 curveto
79.39406 56.44437 72.38638 12.65298 85.03937 0 curveto
 closepath fill
 0.6 0.6 0.6 setrgbcolor
newpath 85.03937 0 moveto
99.82808 -14.78871 88.28534 -47.17143 110.55144 -55.27573 curveto
141.35532 -66.48755 132.47829 -16.3406 148.8189 0 curveto
 closepath fill
 0 0 0 setrgbcolor 0 0.8 dtransform truncate idtransform setlinewidth pop
newpath 0 0 moveto
157.3227 0 lineto stroke
newpath 153.6278 -1.53052 moveto
157.3227 0 lineto
153.6278 1.53052 lineto
 closepath
gsave fill grestore stroke
 0.8 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 21.25984 -63.77953 moveto
21.25984 51.02348 lineto stroke
 0 0.8 dtransform truncate idtransform setlinewidth pop
newpath 22.79031 47.32867 moveto
21.25984 51.02348 lineto
19.72937 47.32867 lineto
 closepath
gsave fill grestore stroke
57.77905 12.44835 moveto
(+) cmr10 9.96265 fshow
113.05478 -30.07133 moveto
(\000) cmsy10 9.96265 fshow
23.35983 60.22612 moveto
(C) cmsy10 9.96265 fshow
28.60544 58.73172 moveto
(f) cmmi7 6.97385 fshow
showpage
%%EOF
