%!PS
%%BoundingBox: -41 -11 157 100 
%%HiResBoundingBox: -40.3091 -10.7487 156.3055 99.6126 
%%Creator: MetaPost 2.000
%%CreationDate: 2018.01.25:1452
%%Pages: 1
%*Font: cmsy10 9.96265 9.96265 41:a
%*Font: cmr10 9.96265 9.96265 28:d08
%*Font: cmmi10 9.96265 9.96265 61:850001
%*Font: cmsy5 4.98132 4.98132 00:80000000000000004
%*Font: cmr5 4.98132 4.98132 28:d
%*Font: cmmi5 4.98132 4.98132 68:80008
%*Font: cmmi7 6.97385 6.97385 66:8
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0.7 0.7 0.7 setrgbcolor
newpath 28.34645 0 moveto
28.34645 22.67725 lineto
46.02068 23.60355 60.0421 36.51657 76.53532 42.51968 curveto
76.53532 0 lineto
 closepath fill
 0.9 0.9 0.9 setrgbcolor
newpath 76.53532 0 moveto
76.53532 42.51968 lineto
101.45114 51.58841 114.8381 81.73712 141.73225 85.03935 curveto
141.73225 0 lineto
 closepath fill
 0 0 0 setrgbcolor 0 0.5 dtransform truncate idtransform setlinewidth pop
 [3 3 ] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 76.53532 0 moveto
76.53532 42.51968 lineto
141.73225 42.51968 lineto
141.73225 0 lineto stroke
 0 0.8 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 28.34645 0 moveto
28.34645 22.67725 lineto
46.02068 23.60355 60.0421 36.51657 76.53532 42.51968 curveto
101.45114 51.58841 114.8381 81.73712 141.73225 85.03935 curveto
145.51276 85.50354 149.29224 85.97601 153.07066 86.45676 curveto stroke
newpath 0 0 moveto
155.90549 0 lineto stroke
newpath 152.21094 -1.53036 moveto
155.90549 0 lineto
152.21094 1.53036 lineto
 closepath
gsave fill grestore stroke
 0.8 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 0 moveto
0 99.21259 lineto stroke
 0 0.8 dtransform truncate idtransform setlinewidth pop
newpath 1.53061 95.51744 moveto
0 99.21259 lineto
-1.53061 95.51744 lineto
 closepath
gsave fill grestore stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
 [1.5 1.5 ] 0 setdash
newpath 141.73225 85.03935 moveto
0 85.03935 lineto stroke
newpath 76.53532 42.51968 moveto
0 42.51968 lineto stroke
45.99414 14.5174 moveto
(A) cmsy10 9.96265 fshow
53.94904 14.5174 moveto
(\() cmr10 9.96265 fshow
57.82344 14.5174 moveto
(x) cmmi10 9.96265 fshow
63.51733 14.5174 moveto
(\)) cmr10 9.96265 fshow
85.08563 32.77031 moveto
(A) cmsy5 4.98132 fshow
90.63223 32.77031 moveto
(\() cmr5 4.98132 fshow
93.33054 32.77031 moveto
(x) cmmi5 4.98132 fshow
98.90373 32.77031 moveto
(+) cmr5 4.98132 fshow
105.65323 32.77031 moveto
(h) cmmi5 4.98132 fshow
109.77553 32.77031 moveto
(\)) cmr5 4.98132 fshow
114.10344 32.77031 moveto
(\000) cmsy5 4.98132 fshow
121.12964 32.77031 moveto
(A) cmsy5 4.98132 fshow
126.67624 32.77031 moveto
(\() cmr5 4.98132 fshow
129.37453 32.77031 moveto
(x) cmmi5 4.98132 fshow
133.31813 32.77031 moveto
(\)) cmr5 4.98132 fshow
73.68837 -7.2895 moveto
(x) cmmi10 9.96265 fshow
-2.49065 -9.4204 moveto
(0) cmr10 9.96265 fshow
129.927 -9.9185 moveto
(x) cmmi10 9.96265 fshow
137.83481 -9.9185 moveto
(+) cmr10 9.96265 fshow
147.79741 -9.9185 moveto
(h) cmmi10 9.96265 fshow
25.7134 -7.2895 moveto
(a) cmmi10 9.96265 fshow
-22.3926 40.02902 moveto
(f) cmmi10 9.96265 fshow
-16.44269 40.02902 moveto
(\() cmr10 9.96265 fshow
-12.5683 40.02902 moveto
(x) cmmi10 9.96265 fshow
-6.87439 40.02902 moveto
(\)) cmr10 9.96265 fshow
-40.3091 82.54869 moveto
(f) cmmi10 9.96265 fshow
-34.35919 82.54869 moveto
(\() cmr10 9.96265 fshow
-30.4848 82.54869 moveto
(x) cmmi10 9.96265 fshow
-22.577 82.54869 moveto
(+) cmr10 9.96265 fshow
-12.6144 82.54869 moveto
(h) cmmi10 9.96265 fshow
-6.8743 82.54869 moveto
(\)) cmr10 9.96265 fshow
98.47775 70.14732 moveto
(C) cmsy10 9.96265 fshow
103.72336 68.65292 moveto
(f) cmmi7 6.97385 fshow
showpage
%%EOF
