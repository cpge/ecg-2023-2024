%!PS
%%BoundingBox: -128 -128 128 129 
%%HiResBoundingBox: -127.80904 -127.80904 127.80904 128.25903 
%%Creator: MetaPost 2.02
%%CreationDate: 2023.03.26:1733
%%Pages: 1
%*Font: cmsy10 9.96265 9.96265 00:8
%*Font: cmr10 9.96265 9.96265 30:f8
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0 1 setrgbcolor 0 1.4 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath -113.3858 -113.3858 moveto
0 0 lineto stroke
 0 4 dtransform truncate idtransform setlinewidth pop
newpath 0 28.34645 moveto 0 0 rlineto stroke
 0 1.4 dtransform truncate idtransform setlinewidth pop
newpath 0 28.34645 moveto
99.21259 127.55904 lineto stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath 2.83482 -2.83482 moveto
1.2692 -2.83482 0 -1.56563 0 0 curveto
0 1.56563 1.2692 2.83482 2.83482 2.83482 curveto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath -113.3858 -2.83482 moveto
-113.3858 2.83482 lineto stroke
-119.75085 -12.25522 moveto
(\000) cmsy10 9.96265 fshow
-112.00215 -12.25522 moveto
(4) cmr10 9.96265 fshow
newpath -85.03935 -2.83482 moveto
-85.03935 2.83482 lineto stroke
-91.4044 -12.25522 moveto
(\000) cmsy10 9.96265 fshow
-83.6557 -12.25522 moveto
(3) cmr10 9.96265 fshow
newpath -56.6929 -2.83482 moveto
-56.6929 2.83482 lineto stroke
-63.05795 -12.25522 moveto
(\000) cmsy10 9.96265 fshow
-55.30925 -12.25522 moveto
(2) cmr10 9.96265 fshow
newpath -28.34645 -2.83482 moveto
-28.34645 2.83482 lineto stroke
-34.7115 -12.25522 moveto
(\000) cmsy10 9.96265 fshow
-26.9628 -12.25522 moveto
(1) cmr10 9.96265 fshow
newpath 28.34645 -2.83482 moveto
28.34645 2.83482 lineto stroke
25.8558 -12.25522 moveto
(1) cmr10 9.96265 fshow
newpath 56.6929 -2.83482 moveto
56.6929 2.83482 lineto stroke
54.20226 -12.25522 moveto
(2) cmr10 9.96265 fshow
newpath 85.03935 -2.83482 moveto
85.03935 2.83482 lineto stroke
82.5487 -12.25522 moveto
(3) cmr10 9.96265 fshow
newpath 113.3858 -2.83482 moveto
113.3858 2.83482 lineto stroke
110.89516 -12.25522 moveto
(4) cmr10 9.96265 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -2.83482 28.34645 moveto
2.83482 28.34645 lineto stroke
-7.9813 25.13625 moveto
(1) cmr10 9.96265 fshow
newpath -2.83482 56.6929 moveto
2.83482 56.6929 lineto stroke
-7.9813 53.4827 moveto
(2) cmr10 9.96265 fshow
newpath -2.83482 85.03935 moveto
2.83482 85.03935 lineto stroke
-7.9813 81.82915 moveto
(3) cmr10 9.96265 fshow
newpath -2.83482 113.3858 moveto
2.83482 113.3858 lineto stroke
-7.9813 110.1756 moveto
(4) cmr10 9.96265 fshow
newpath -2.83482 -113.3858 moveto
2.83482 -113.3858 lineto stroke
-15.7301 -116.18091 moveto
(\000) cmsy10 9.96265 fshow
-7.9814 -116.18091 moveto
(4) cmr10 9.96265 fshow
newpath -2.83482 -85.03935 moveto
2.83482 -85.03935 lineto stroke
-15.7301 -87.83446 moveto
(\000) cmsy10 9.96265 fshow
-7.9814 -87.83446 moveto
(3) cmr10 9.96265 fshow
newpath -2.83482 -56.6929 moveto
2.83482 -56.6929 lineto stroke
-15.7301 -59.488 moveto
(\000) cmsy10 9.96265 fshow
-7.9814 -59.488 moveto
(2) cmr10 9.96265 fshow
newpath -2.83482 -28.34645 moveto
2.83482 -28.34645 lineto stroke
-15.7301 -31.14156 moveto
(\000) cmsy10 9.96265 fshow
-7.9814 -31.14156 moveto
(1) cmr10 9.96265 fshow
2.09999 -12.20557 moveto
(0) cmr10 9.96265 fshow
newpath -127.55904 0 moveto
127.55904 0 lineto stroke
newpath 123.86191 -1.53143 moveto
127.55904 0 lineto
123.86191 1.53143 lineto
 closepath
gsave fill grestore stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 -127.55904 moveto
0 127.55904 lineto stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath 1.53143 123.86191 moveto
0 127.55904 lineto
-1.53143 123.86191 lineto
 closepath
gsave fill grestore stroke
showpage
%%EOF
