%!PS
%%BoundingBox: -142 -28 142 13 
%%HiResBoundingBox: -141.98225 -27.13594 141.98225 12.96272 
%%Creator: MetaPost 2.02
%%CreationDate: 2023.03.26:1733
%%Pages: 1
%*Font: cmmi10 9.96265 9.96265 41:c00000000000018
%*Font: cmr10 9.96265 9.96265 28:c00004
%*Font: cmsy10 9.96265 9.96265 00:800000000000000000000000002
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0 0 setrgbcolor 0 0.5 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath -141.73225 0 moveto
141.73225 0 lineto stroke
newpath 138.03589 -1.53113 moveto
141.73225 0 lineto
138.03589 1.53113 lineto
 closepath
gsave fill grestore stroke
-67.15025 5.49072 moveto
(A) cmmi10 9.96265 fshow
-59.67825 5.49072 moveto
(\() cmr10 9.96265 fshow
-55.80385 5.49072 moveto
(x) cmmi10 9.96265 fshow
-50.10995 5.49072 moveto
(\)) cmr10 9.96265 fshow
 0 3 dtransform truncate idtransform setlinewidth pop
newpath -56.6929 0 moveto 0 0 rlineto stroke
74.2897 5.49072 moveto
(B) cmmi10 9.96265 fshow
82.3463 5.49072 moveto
(\() cmr10 9.96265 fshow
86.2207 5.49072 moveto
(x) cmmi10 9.96265 fshow
91.9146 5.49072 moveto
(\)) cmr10 9.96265 fshow
newpath 85.03935 0 moveto 0 0 rlineto stroke
 1 0 0 setrgbcolor 0 1.2 dtransform truncate idtransform setlinewidth pop
newpath -56.6929 -14.17323 moveto
85.03935 -14.17323 lineto stroke
newpath 81.34299 -15.70436 moveto
85.03935 -14.17323 lineto
81.34299 -12.6421 lineto
 closepath
gsave fill grestore stroke
newpath 85.03935 -14.17323 moveto
-56.6929 -14.17323 lineto stroke
newpath -52.99654 -12.6421 moveto
-56.6929 -14.17323 lineto
-52.99654 -15.70436 lineto
 closepath
gsave fill grestore stroke
 0 0 0 setrgbcolor
-21.11562 -24.64523 moveto
(j) cmsy10 9.96265 fshow
-18.34822 -24.64523 moveto
(y) cmmi10 9.96265 fshow
-10.89241 -24.64523 moveto
(\000) cmsy10 9.96265 fshow
-0.92981 -24.64523 moveto
(x) cmmi10 9.96265 fshow
4.76408 -24.64523 moveto
(j) cmsy10 9.96265 fshow
10.29889 -24.64523 moveto
(=) cmr10 9.96265 fshow
20.81499 -24.64523 moveto
(j) cmsy10 9.96265 fshow
23.58238 -24.64523 moveto
(x) cmmi10 9.96265 fshow
31.49019 -24.64523 moveto
(\000) cmsy10 9.96265 fshow
41.45279 -24.64523 moveto
(y) cmmi10 9.96265 fshow
46.69469 -24.64523 moveto
(j) cmsy10 9.96265 fshow
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath -113.3858 -2.83482 moveto
-113.3858 2.83482 lineto stroke
newpath -85.03935 -2.83482 moveto
-85.03935 2.83482 lineto stroke
newpath -28.34645 -2.83482 moveto
-28.34645 2.83482 lineto stroke
newpath 0 -2.83482 moveto
0 2.83482 lineto stroke
newpath 28.34645 -2.83482 moveto
28.34645 2.83482 lineto stroke
newpath 56.6929 -2.83482 moveto
56.6929 2.83482 lineto stroke
newpath 113.3858 -2.83482 moveto
113.3858 2.83482 lineto stroke
showpage
%%EOF
