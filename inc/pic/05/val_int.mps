%!PS
%%BoundingBox: -142 -86 171 103 
%%HiResBoundingBox: -141.98225 -85.73935 170.32828 102.29697 
%%Creator: MetaPost 2.02
%%CreationDate: 2023.03.26:1733
%%Pages: 1
%*Font: cmmi10 10.9091 9.96265 61:e420008
%*Font: cmr10 10.9091 9.96265 28:c00004
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0 0 setrgbcolor 0 0.5 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath -141.73225 0 moveto
170.07828 0 lineto stroke
newpath 166.38153 -1.53128 moveto
170.07828 0 lineto
166.38153 1.53128 lineto
 closepath
gsave fill grestore stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 -85.03935 moveto
0 102.04697 lineto stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath 1.53055 98.35199 moveto
0 102.04697 lineto
-1.53055 98.35199 lineto
 closepath
gsave fill grestore stroke
 0 0 1 setrgbcolor 0 1.4 dtransform truncate idtransform setlinewidth pop
newpath -113.38623 -49.98476 moveto
-106.2992 -60.13197 lineto
-99.21214 -68.79861 lineto
-92.1251 -75.77191 lineto
-85.03806 -80.87796 lineto
-77.951 -83.99348 lineto
-70.86397 -85.03935 lineto
-63.77692 -83.99219 lineto
-56.68988 -80.87537 lineto
-49.60283 -75.76802 lineto
-42.51578 -68.79343 lineto
-35.42874 -60.12677 lineto
-28.34169 -49.97827 lineto
-21.25465 -38.59834 lineto
-14.1676 -26.26857 lineto
-7.08055 -13.29129 lineto
0.00648 0.01167 lineto
7.09354 13.31595 lineto
14.18057 26.29192 lineto
21.26762 38.62039 lineto
28.35468 49.99773 lineto
35.44171 60.14365 lineto
42.52876 68.80771 lineto
49.6158 75.7784 lineto
56.70285 80.88315 lineto
63.7899 83.99608 lineto
70.87694 85.03935 lineto
77.96399 83.9896 lineto
85.05103 80.87018 lineto
92.13808 75.76024 lineto
99.22513 68.78435 lineto
106.31216 60.1151 lineto
113.39922 49.96399 lineto
120.48625 38.58406 lineto
127.5733 26.25299 lineto stroke
 1 0 0 setrgbcolor 0 0.5 dtransform truncate idtransform setlinewidth pop
 [3 3 ] 0 setdash
newpath -113.38623 68.03174 moveto
141.73225 68.03174 lineto stroke
 0 0 0 setrgbcolor
100.23222 73.15295 moveto
(y) cmmi10 10.9091 fshow
109.00243 73.15295 moveto
(=) cmr10 10.9091 fshow
120.51752 73.15295 moveto
(k) cmmi10 10.9091 fshow
 0 1.2 dtransform truncate idtransform setlinewidth pop
 [0 5 ] 2.5 setdash
newpath 14.17409 0 moveto
14.17409 26.28024 lineto
0 26.28024 lineto stroke
newpath 63.77908 0 moveto
63.77908 83.99219 lineto
0 83.99219 lineto stroke
 0 0.5 0 setrgbcolor 0.5
 0 dtransform exch truncate exch idtransform pop setlinewidth [] 0 setdash
newpath 0 26.28024 moveto
0 83.99219 lineto stroke
 0 0 0 setrgbcolor [3 3 ] 0 setdash
newpath 41.83455 0 moveto
41.83455 68.03174 lineto stroke
11.29088 -7.697 moveto
(a) cmmi10 10.9091 fshow
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 14.17409 0 moveto 0 0 rlineto stroke
61.43819 -10.5757 moveto
(b) cmmi10 10.9091 fshow
newpath 63.77908 0 moveto 0 0 rlineto stroke
39.47404 -7.697 moveto
(c) cmmi10 10.9091 fshow
newpath 41.83455 0 moveto 0 0 rlineto stroke
-23.7665 23.553 moveto
(f) cmmi10 10.9091 fshow
-17.2513 23.553 moveto
(\() cmr10 10.9091 fshow
-13.0089 23.553 moveto
(a) cmmi10 10.9091 fshow
-7.2425 23.553 moveto
(\)) cmr10 10.9091 fshow
newpath 0 26.28024 moveto 0 0 rlineto stroke
-22.6819 81.26494 moveto
(f) cmmi10 10.9091 fshow
-16.1667 81.26494 moveto
(\() cmr10 10.9091 fshow
-11.9243 81.26494 moveto
(b) cmmi10 10.9091 fshow
-7.2425 81.26494 moveto
(\)) cmr10 10.9091 fshow
newpath 0 83.99219 moveto 0 0 rlineto stroke
-8.1227 58.35605 moveto
(k) cmmi10 10.9091 fshow
newpath 0 68.03174 moveto 0 0 rlineto stroke
showpage
%%EOF
