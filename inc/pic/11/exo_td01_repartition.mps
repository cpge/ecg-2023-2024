%!PS
%%BoundingBox: -15 -8 441 250 
%%HiResBoundingBox: -14.82324 -7.49426 440.02023 249.70015 
%%Creator: MetaPost 2.02
%%CreationDate: 2023.11.28:1944
%%Pages: 1
%*Font: cmr10 6.97382 9.96265 30:ffc
%*Font: cmr7 3.48692 6.97385 30:ffc
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0 0 setrgbcolor 0 0.5 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath -14.17323 0 moveto
425.19699 0 lineto stroke
newpath 421.49922 -1.53171 moveto
425.19699 0 lineto
421.49922 1.53171 lineto
 closepath
gsave fill grestore stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 0 moveto
0 249.45015 lineto stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath 1.53093 245.75424 moveto
0 249.45015 lineto
-1.53093 245.75424 lineto
 closepath
gsave fill grestore stroke
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
newpath -14.17323 0 moveto
0 0 lineto stroke
 0 0 0 setrgbcolor
-1.74345 -7.49426 moveto
(0) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 69.2917 moveto
1.4174 69.2917 lineto stroke
-7.569 70.00764 moveto
(12) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 69.2917 moveto
-3.59775 69.2917 lineto stroke
-7.569 66.32858 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 0 69.2917 moveto
14.17323 69.2917 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 0 0 moveto
0 69.2917 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 0 69.2917 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
12.42978 -7.49426 moveto
(1) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 75.58939 moveto
1.4174 75.58939 lineto stroke
-7.569 76.30533 moveto
(12) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 75.58939 moveto
-3.59775 75.58939 lineto stroke
-7.569 72.62627 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 14.17323 75.58939 moveto
28.34647 75.58939 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 14.17323 0 moveto
14.17323 75.58939 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 14.17323 75.58939 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
26.60301 -7.49426 moveto
(2) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 88.18819 moveto
1.4174 88.18819 lineto stroke
-7.569 88.90413 moveto
(14) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 88.18819 moveto
-3.59775 88.18819 lineto stroke
-7.569 85.22507 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 28.34647 88.18819 moveto
42.5197 88.18819 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 28.34647 0 moveto
28.34647 88.18819 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 28.34647 88.18819 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
40.77625 -7.49426 moveto
(3) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 100.787 moveto
1.4174 100.787 lineto stroke
-7.569 101.50294 moveto
(16) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 100.787 moveto
-3.59775 100.787 lineto stroke
-7.569 97.82388 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 42.5197 100.787 moveto
56.69293 100.787 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 42.5197 0 moveto
42.5197 100.787 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 42.5197 100.787 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
54.94948 -7.49426 moveto
(4) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 119.68347 moveto
1.4174 119.68347 lineto stroke
-7.569 120.39941 moveto
(19) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 119.68347 moveto
-3.59775 119.68347 lineto stroke
-7.569 116.72035 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 56.69293 119.68347 moveto
70.86617 119.68347 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 56.69293 0 moveto
56.69293 119.68347 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 56.69293 119.68347 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
69.12271 -7.49426 moveto
(5) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 132.28229 moveto
1.4174 132.28229 lineto stroke
-7.569 132.99823 moveto
(21) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 132.28229 moveto
-3.59775 132.28229 lineto stroke
-7.569 129.31917 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 70.86617 132.28229 moveto
85.0394 132.28229 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 70.86617 0 moveto
70.86617 132.28229 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 70.86617 132.28229 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
83.29594 -7.49426 moveto
(6) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 144.88109 moveto
1.4174 144.88109 lineto stroke
-7.569 145.59703 moveto
(23) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 144.88109 moveto
-3.59775 144.88109 lineto stroke
-7.569 141.91797 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 85.0394 144.88109 moveto
99.21263 144.88109 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 85.0394 0 moveto
85.0394 144.88109 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 85.0394 144.88109 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
97.46918 -7.49426 moveto
(7) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 144.88109 moveto
1.4174 144.88109 lineto stroke
-7.569 145.59703 moveto
(23) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 144.88109 moveto
-3.59775 144.88109 lineto stroke
-7.569 141.91797 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 99.21263 144.88109 moveto
113.38586 144.88109 lineto stroke
 0 0 0 setrgbcolor
111.64241 -7.49426 moveto
(8) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 157.4799 moveto
1.4174 157.4799 lineto stroke
-7.569 158.19585 moveto
(25) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 157.4799 moveto
-3.59775 157.4799 lineto stroke
-7.569 154.51678 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 113.38586 157.4799 moveto
127.5591 157.4799 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 113.38586 0 moveto
113.38586 157.4799 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 113.38586 157.4799 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
125.81564 -7.49426 moveto
(9) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 163.78104 moveto
1.4174 163.78104 lineto stroke
-7.569 164.49698 moveto
(27) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 163.78104 moveto
-3.59775 163.78104 lineto stroke
-7.569 160.81792 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 127.5591 163.78104 moveto
141.73233 163.78104 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 127.5591 0 moveto
127.5591 163.78104 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 127.5591 163.78104 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
138.24539 -7.49426 moveto
(10) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 176.37637 moveto
1.4174 176.37637 lineto stroke
-7.569 177.09232 moveto
(28) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 176.37637 moveto
-3.59775 176.37637 lineto stroke
-7.569 173.41325 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 141.73233 176.37637 moveto
155.90556 176.37637 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 141.73233 0 moveto
141.73233 176.37637 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 141.73233 176.37637 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
152.41862 -7.49426 moveto
(11) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 176.37637 moveto
1.4174 176.37637 lineto stroke
-7.569 177.09232 moveto
(28) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 176.37637 moveto
-3.59775 176.37637 lineto stroke
-7.569 173.41325 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 155.90556 176.37637 moveto
170.0788 176.37637 lineto stroke
 0 0 0 setrgbcolor
166.59186 -7.49426 moveto
(12) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 188.97519 moveto
1.4174 188.97519 lineto stroke
-7.569 189.69113 moveto
(30) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 188.97519 moveto
-3.59775 188.97519 lineto stroke
-7.569 186.01207 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 170.0788 188.97519 moveto
184.25203 188.97519 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 170.0788 0 moveto
170.0788 188.97519 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 170.0788 188.97519 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
180.76509 -7.49426 moveto
(13) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 188.97519 moveto
1.4174 188.97519 lineto stroke
-7.569 189.69113 moveto
(30) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 188.97519 moveto
-3.59775 188.97519 lineto stroke
-7.569 186.01207 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 184.25203 188.97519 moveto
198.42526 188.97519 lineto stroke
 0 0 0 setrgbcolor
194.93832 -7.49426 moveto
(14) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 188.97519 moveto
1.4174 188.97519 lineto stroke
-7.569 189.69113 moveto
(30) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 188.97519 moveto
-3.59775 188.97519 lineto stroke
-7.569 186.01207 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 198.42526 188.97519 moveto
212.5985 188.97519 lineto stroke
 0 0 0 setrgbcolor
209.11156 -7.49426 moveto
(15) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 201.57399 moveto
1.4174 201.57399 lineto stroke
-7.569 202.28993 moveto
(32) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 201.57399 moveto
-3.59775 201.57399 lineto stroke
-7.569 198.61087 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 212.5985 201.57399 moveto
226.77173 201.57399 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 212.5985 0 moveto
212.5985 201.57399 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 212.5985 201.57399 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
223.28479 -7.49426 moveto
(16) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 207.87512 moveto
1.4174 207.87512 lineto stroke
-7.569 208.59106 moveto
(34) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 207.87512 moveto
-3.59775 207.87512 lineto stroke
-7.569 204.912 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 226.77173 207.87512 moveto
240.94496 207.87512 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 226.77173 0 moveto
226.77173 207.87512 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 226.77173 207.87512 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
237.45802 -7.49426 moveto
(17) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 207.87512 moveto
1.4174 207.87512 lineto stroke
-7.569 208.59106 moveto
(34) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 207.87512 moveto
-3.59775 207.87512 lineto stroke
-7.569 204.912 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 240.94496 207.87512 moveto
255.1182 207.87512 lineto stroke
 0 0 0 setrgbcolor
251.63126 -7.49426 moveto
(18) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 207.87512 moveto
1.4174 207.87512 lineto stroke
-7.569 208.59106 moveto
(34) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 207.87512 moveto
-3.59775 207.87512 lineto stroke
-7.569 204.912 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 255.1182 207.87512 moveto
269.29143 207.87512 lineto stroke
 0 0 0 setrgbcolor
265.80449 -7.49426 moveto
(19) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 207.87512 moveto
1.4174 207.87512 lineto stroke
-7.569 208.59106 moveto
(34) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 207.87512 moveto
-3.59775 207.87512 lineto stroke
-7.569 204.912 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 269.29143 207.87512 moveto
283.46466 207.87512 lineto stroke
 0 0 0 setrgbcolor
279.97772 -7.49426 moveto
(20) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 220.47394 moveto
1.4174 220.47394 lineto stroke
-7.569 221.18988 moveto
(36) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 220.47394 moveto
-3.59775 220.47394 lineto stroke
-7.569 217.51082 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 283.46466 220.47394 moveto
297.6379 220.47394 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 283.46466 0 moveto
283.46466 220.47394 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 283.46466 220.47394 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
294.15096 -7.49426 moveto
(21) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 220.47394 moveto
1.4174 220.47394 lineto stroke
-7.569 221.18988 moveto
(36) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 220.47394 moveto
-3.59775 220.47394 lineto stroke
-7.569 217.51082 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 297.6379 220.47394 moveto
311.81113 220.47394 lineto stroke
 0 0 0 setrgbcolor
308.32419 -7.49426 moveto
(22) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 220.47394 moveto
1.4174 220.47394 lineto stroke
-7.569 221.18988 moveto
(36) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 220.47394 moveto
-3.59775 220.47394 lineto stroke
-7.569 217.51082 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 311.81113 220.47394 moveto
325.98436 220.47394 lineto stroke
 0 0 0 setrgbcolor
322.49742 -7.49426 moveto
(23) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 220.47394 moveto
1.4174 220.47394 lineto stroke
-7.569 221.18988 moveto
(36) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 220.47394 moveto
-3.59775 220.47394 lineto stroke
-7.569 217.51082 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 325.98436 220.47394 moveto
340.1576 220.47394 lineto stroke
 0 0 0 setrgbcolor
336.67065 -7.49426 moveto
(24) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 220.47394 moveto
1.4174 220.47394 lineto stroke
-7.569 221.18988 moveto
(36) cmr7 3.48692 fshow
 0 0.19925 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -7.569 220.47394 moveto
-3.59775 220.47394 lineto stroke
-7.569 217.51082 moveto
(36) cmr7 3.48692 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 340.1576 220.47394 moveto
354.33083 220.47394 lineto stroke
 0 0 0 setrgbcolor
350.84389 -7.49426 moveto
(25) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 226.7716 moveto
1.4174 226.7716 lineto stroke
-5.58688 228.8716 moveto
(1) cmr10 6.97382 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
newpath 354.33083 226.7716 moveto
368.50406 226.7716 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 354.33083 0 moveto
354.33083 226.7716 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 354.33083 226.7716 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
365.01712 -7.49426 moveto
(26) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 226.7716 moveto
1.4174 226.7716 lineto stroke
-5.58688 228.8716 moveto
(1) cmr10 6.97382 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
newpath 368.50406 226.7716 moveto
382.67729 226.7716 lineto stroke
 0 0 0 setrgbcolor
379.19035 -7.49426 moveto
(27) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 226.7716 moveto
1.4174 226.7716 lineto stroke
-5.58688 228.8716 moveto
(1) cmr10 6.97382 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
newpath 382.67729 226.7716 moveto
396.85052 226.7716 lineto stroke
 0 0 0 setrgbcolor
393.36359 -7.49426 moveto
(28) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 226.7716 moveto
1.4174 226.7716 lineto stroke
-5.58688 228.8716 moveto
(1) cmr10 6.97382 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
newpath 396.85052 226.7716 moveto
411.02376 226.7716 lineto stroke
 0 0 0 setrgbcolor
407.53682 -7.49426 moveto
(29) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 226.7716 moveto
1.4174 226.7716 lineto stroke
-5.58688 228.8716 moveto
(1) cmr10 6.97382 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
newpath 411.02376 226.7716 moveto
425.19699 226.7716 lineto stroke
 0 0 0 setrgbcolor
421.71005 -7.49426 moveto
(30) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -1.4174 226.7716 moveto
1.4174 226.7716 lineto stroke
-5.58688 228.8716 moveto
(1) cmr10 6.97382 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
newpath 425.19699 226.7716 moveto
439.37022 226.7716 lineto stroke
showpage
%%EOF
