%!PS
%%BoundingBox: -11 -8 319 188 
%%HiResBoundingBox: -10.23341 -7.49426 318.13145 187.33762 
%%Creator: MetaPost 2.02
%%CreationDate: 2023.03.27:1800
%%Pages: 1
%*Font: cmr10 6.97382 9.96265 30:ffc
%*Font: cmr7 4.88167 6.97385 30:f6
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0 0 setrgbcolor 0 0.5 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 0 0 moveto
317.48145 0 lineto stroke
newpath 313.7846 -1.53131 moveto
317.48145 0 lineto
313.7846 1.53131 lineto
 closepath
gsave fill grestore stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 0 moveto
0 187.08762 lineto stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath 1.53056 183.3926 moveto
0 187.08762 lineto
-1.53056 183.3926 lineto
 closepath
gsave fill grestore stroke
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
newpath 0 0 moveto
22.67725 0 lineto stroke
 0 0 0 setrgbcolor
20.93379 -7.49426 moveto
(1) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -2.26787 0 moveto
2.26787 0 lineto stroke
-8.00665 1.00233 moveto
(0) cmr7 4.88167 fshow
 0 0.27895 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -9.39658 0 moveto
-3.83684 0 lineto stroke
-9.39658 -4.14833 moveto
(36) cmr7 4.88167 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 22.67725 0 moveto
45.35449 0 lineto stroke
 0 0 0 setrgbcolor
43.61104 -7.49426 moveto
(2) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -2.26787 4.72325 moveto
2.26787 4.72325 lineto stroke
-8.00665 5.72559 moveto
(1) cmr7 4.88167 fshow
 0 0.27895 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -9.39658 4.72325 moveto
-3.83684 4.72325 lineto stroke
-9.39658 0.57492 moveto
(36) cmr7 4.88167 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 45.35449 4.72325 moveto
68.03174 4.72325 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 45.35449 0 moveto
45.35449 4.72325 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 45.35449 4.72325 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
66.28828 -7.49426 moveto
(3) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -2.26787 14.17236 moveto
2.26787 14.17236 lineto stroke
-8.00665 15.1747 moveto
(3) cmr7 4.88167 fshow
 0 0.27895 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -9.39658 14.17236 moveto
-3.83684 14.17236 lineto stroke
-9.39658 10.02403 moveto
(36) cmr7 4.88167 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 68.03174 14.17236 moveto
90.70898 14.17236 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 68.03174 0 moveto
68.03174 14.17236 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 68.03174 14.17236 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
88.96553 -7.49426 moveto
(4) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -2.26787 28.34473 moveto
2.26787 28.34473 lineto stroke
-8.00665 29.34706 moveto
(6) cmr7 4.88167 fshow
 0 0.27895 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -9.39658 28.34473 moveto
-3.83684 28.34473 lineto stroke
-9.39658 24.1964 moveto
(36) cmr7 4.88167 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 90.70898 28.34473 moveto
113.38623 28.34473 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 90.70898 0 moveto
90.70898 28.34473 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 90.70898 28.34473 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
111.64278 -7.49426 moveto
(5) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -2.26787 47.24294 moveto
2.26787 47.24294 lineto stroke
-9.39658 48.24527 moveto
(10) cmr7 4.88167 fshow
 0 0.27895 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -9.39658 47.24294 moveto
-3.83684 47.24294 lineto stroke
-9.39658 43.0946 moveto
(36) cmr7 4.88167 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 113.38623 47.24294 moveto
136.06348 47.24294 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 113.38623 0 moveto
113.38623 47.24294 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 113.38623 47.24294 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
134.32002 -7.49426 moveto
(6) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -2.26787 70.8644 moveto
2.26787 70.8644 lineto stroke
-9.39658 71.86673 moveto
(15) cmr7 4.88167 fshow
 0 0.27895 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -9.39658 70.8644 moveto
-3.83684 70.8644 lineto stroke
-9.39658 66.71606 moveto
(36) cmr7 4.88167 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 136.06348 70.8644 moveto
158.74072 70.8644 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 136.06348 0 moveto
136.06348 70.8644 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 136.06348 70.8644 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
156.99727 -7.49426 moveto
(7) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -2.26787 99.21172 moveto
2.26787 99.21172 lineto stroke
-9.39658 100.21405 moveto
(21) cmr7 4.88167 fshow
 0 0.27895 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -9.39658 99.21172 moveto
-3.83684 99.21172 lineto stroke
-9.39658 95.06339 moveto
(36) cmr7 4.88167 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 158.74072 99.21172 moveto
181.41797 99.21172 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 158.74072 0 moveto
158.74072 99.21172 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 158.74072 99.21172 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
179.67451 -7.49426 moveto
(8) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -2.26787 122.83318 moveto
2.26787 122.83318 lineto stroke
-9.39658 123.83551 moveto
(26) cmr7 4.88167 fshow
 0 0.27895 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -9.39658 122.83318 moveto
-3.83684 122.83318 lineto stroke
-9.39658 118.68484 moveto
(36) cmr7 4.88167 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 181.41797 122.83318 moveto
204.09521 122.83318 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 181.41797 0 moveto
181.41797 122.83318 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 181.41797 122.83318 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
202.35176 -7.49426 moveto
(9) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -2.26787 141.73138 moveto
2.26787 141.73138 lineto stroke
-9.39658 142.73372 moveto
(30) cmr7 4.88167 fshow
 0 0.27895 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -9.39658 141.73138 moveto
-3.83684 141.73138 lineto stroke
-9.39658 137.58305 moveto
(36) cmr7 4.88167 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 204.09521 141.73138 moveto
226.77246 141.73138 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 204.09521 0 moveto
204.09521 141.73138 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 204.09521 141.73138 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
223.28552 -7.49426 moveto
(10) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -2.26787 155.90375 moveto
2.26787 155.90375 lineto stroke
-9.39658 156.90608 moveto
(33) cmr7 4.88167 fshow
 0 0.27895 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -9.39658 155.90375 moveto
-3.83684 155.90375 lineto stroke
-9.39658 151.75542 moveto
(36) cmr7 4.88167 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 226.77246 155.90375 moveto
249.4497 155.90375 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 226.77246 0 moveto
226.77246 155.90375 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 226.77246 155.90375 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
245.96277 -7.49426 moveto
(11) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -2.26787 165.35286 moveto
2.26787 165.35286 lineto stroke
-9.39658 166.3552 moveto
(35) cmr7 4.88167 fshow
 0 0.27895 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath -9.39658 165.35286 moveto
-3.83684 165.35286 lineto stroke
-9.39658 161.20453 moveto
(36) cmr7 4.88167 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
 1 setlinecap
newpath 249.4497 165.35286 moveto
272.12695 165.35286 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 249.4497 0 moveto
249.4497 165.35286 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 249.4497 165.35286 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
268.64001 -7.49426 moveto
(12) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -2.26787 170.07611 moveto
2.26787 170.07611 lineto stroke
-5.58688 172.1761 moveto
(1) cmr10 6.97382 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
newpath 272.12695 170.07611 moveto
294.8042 170.07611 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 272.12695 0 moveto
272.12695 170.07611 lineto stroke
 1 0 0 setrgbcolor
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 272.12695 170.07611 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
291.31726 -7.49426 moveto
(13) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -2.26787 170.07611 moveto
2.26787 170.07611 lineto stroke
-5.58688 172.1761 moveto
(1) cmr10 6.97382 fshow
 1 0 0 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
newpath 294.8042 170.07611 moveto
317.48145 170.07611 lineto stroke
showpage
%%EOF
