%!PS
%%BoundingBox: 12 -8 296 199 
%%HiResBoundingBox: 12.44383 -7.49426 295.0542 198.67343 
%%Creator: MetaPost 2.02
%%CreationDate: 2023.03.27:2010
%%Pages: 1
%*Font: cmr10 6.97382 9.96265 30:ffc
%*Font: cmr7 4.88167 6.97385 31:fc
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0 0 setrgbcolor 0 0.5 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 22.67725 0 moveto
294.8042 0 lineto stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 22.67725 0 moveto
22.67725 198.42343 lineto stroke
 0 0 1 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
newpath 43.08662 0 moveto
43.08662 28.33952 lineto
47.62236 28.33952 lineto
47.62236 0 lineto
 closepath
gsave fill grestore stroke
 0 0 0 setrgbcolor
43.61104 -7.49426 moveto
(2) cmr10 6.97382 fshow
 0 0 1 setrgbcolor
newpath 65.76387 0 moveto
65.76387 56.69463 lineto
70.2996 56.69463 lineto
70.2996 0 lineto
 closepath
gsave fill grestore stroke
 0 0 0 setrgbcolor
66.28828 -7.49426 moveto
(3) cmr10 6.97382 fshow
 0 0 1 setrgbcolor
newpath 88.44112 0 moveto
88.44112 85.03416 lineto
92.97685 85.03416 lineto
92.97685 0 lineto
 closepath
gsave fill grestore stroke
 0 0 0 setrgbcolor
88.96553 -7.49426 moveto
(4) cmr10 6.97382 fshow
 0 0 1 setrgbcolor
newpath 111.11836 0 moveto
111.11836 113.38927 lineto
115.6541 113.38927 lineto
115.6541 0 lineto
 closepath
gsave fill grestore stroke
 0 0 0 setrgbcolor
111.64278 -7.49426 moveto
(5) cmr10 6.97382 fshow
 0 0 1 setrgbcolor
newpath 133.79561 0 moveto
133.79561 141.72879 lineto
138.33134 141.72879 lineto
138.33134 0 lineto
 closepath
gsave fill grestore stroke
 0 0 0 setrgbcolor
134.32002 -7.49426 moveto
(6) cmr10 6.97382 fshow
 0 0 1 setrgbcolor
newpath 156.47285 0 moveto
156.47285 170.0839 lineto
161.00859 170.0839 lineto
161.00859 0 lineto
 closepath
gsave fill grestore stroke
 0 0 0 setrgbcolor
156.99727 -7.49426 moveto
(7) cmr10 6.97382 fshow
 0 0 1 setrgbcolor
newpath 179.1501 0 moveto
179.1501 141.72879 lineto
183.68584 141.72879 lineto
183.68584 0 lineto
 closepath
gsave fill grestore stroke
 0 0 0 setrgbcolor
179.67451 -7.49426 moveto
(8) cmr10 6.97382 fshow
 0 0 1 setrgbcolor
newpath 201.82735 0 moveto
201.82735 113.38927 lineto
206.36308 113.38927 lineto
206.36308 0 lineto
 closepath
gsave fill grestore stroke
 0 0 0 setrgbcolor
202.35176 -7.49426 moveto
(9) cmr10 6.97382 fshow
 0 0 1 setrgbcolor
newpath 224.5046 0 moveto
224.5046 85.03416 lineto
229.04033 85.03416 lineto
229.04033 0 lineto
 closepath
gsave fill grestore stroke
 0 0 0 setrgbcolor
223.28552 -7.49426 moveto
(10) cmr10 6.97382 fshow
 0 0 1 setrgbcolor
newpath 247.18184 0 moveto
247.18184 56.69463 lineto
251.71758 56.69463 lineto
251.71758 0 lineto
 closepath
gsave fill grestore stroke
 0 0 0 setrgbcolor
245.96277 -7.49426 moveto
(11) cmr10 6.97382 fshow
 0 0 1 setrgbcolor
newpath 269.85909 0 moveto
269.85909 28.33952 lineto
274.39482 28.33952 lineto
274.39482 0 lineto
 closepath
gsave fill grestore stroke
 0 0 0 setrgbcolor
268.64001 -7.49426 moveto
(12) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath 20.40938 28.33952 moveto
24.94511 28.33952 lineto stroke
14.6706 29.34186 moveto
(1) cmr7 4.88167 fshow
 0 0.27895 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath 13.28067 28.33952 moveto
18.84041 28.33952 lineto stroke
13.28067 24.1912 moveto
(36) cmr7 4.88167 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop 1 setlinecap
newpath 20.40938 56.69463 moveto
24.94511 56.69463 lineto stroke
14.6706 57.69696 moveto
(2) cmr7 4.88167 fshow
 0 0.27895 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath 13.28067 56.69463 moveto
18.84041 56.69463 lineto stroke
13.28067 52.5463 moveto
(36) cmr7 4.88167 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop 1 setlinecap
newpath 20.40938 85.03416 moveto
24.94511 85.03416 lineto stroke
14.6706 86.0365 moveto
(3) cmr7 4.88167 fshow
 0 0.27895 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath 13.28067 85.03416 moveto
18.84041 85.03416 lineto stroke
13.28067 80.88583 moveto
(36) cmr7 4.88167 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop 1 setlinecap
newpath 20.40938 113.38927 moveto
24.94511 113.38927 lineto stroke
14.6706 114.3916 moveto
(4) cmr7 4.88167 fshow
 0 0.27895 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath 13.28067 113.38927 moveto
18.84041 113.38927 lineto stroke
13.28067 109.24094 moveto
(36) cmr7 4.88167 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop 1 setlinecap
newpath 20.40938 141.72879 moveto
24.94511 141.72879 lineto stroke
14.6706 142.73112 moveto
(5) cmr7 4.88167 fshow
 0 0.27895 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath 13.28067 141.72879 moveto
18.84041 141.72879 lineto stroke
13.28067 137.58046 moveto
(36) cmr7 4.88167 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop 1 setlinecap
newpath 20.40938 170.0839 moveto
24.94511 170.0839 lineto stroke
14.6706 171.08623 moveto
(6) cmr7 4.88167 fshow
 0 0.27895 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath 13.28067 170.0839 moveto
18.84041 170.0839 lineto stroke
13.28067 165.93556 moveto
(36) cmr7 4.88167 fshow
showpage
%%EOF
