#!/opt/homebrew/bin/python3

# Librairies
from rich import print
from rich.console import Console
from rich.prompt import Prompt, Confirm, FloatPrompt
from rich.table import Table
from rich.progress import track
from rich.progress import Progress
import shutil, os, subprocess, re, sys, time, glob
from pyutil import filereplace
#import os, sys, subprocess, time, datetime, re, glob

console = Console()

CHECK_MARK=" "*12+u'[green]\u2713[/green]' 
ERROR_MARK=" "*12+u'[red]\u2718[/red]'

### Fonctions auxiliaires
def titre_chapitre(chapitre):
    ''' Fonction qui renvoie le titre du chapitre '''
    try:
        with open(f"./tex/{chapitre:02d}/Cours.tex", "r") as file:
            lignes = file.readlines()
            # On cherche le mot clé \chapter{}
            for ligne in lignes:
                if ligne.find(r'\chapter{') != 1:
                    titre = re.match(r'.*\{(.*?)\}.*',ligne).group(1)
                    return titre
            return ""
    except: return ""
    
def renvoie_num_exo(chap,exo):
    '''' Renvoie le numéro du fichier de l'exercice recherché '''
    fichier = f"./tex/{chap:02d}/Exo.tex"
    num = 0
    f = open(fichier, "r")
    lignes = f.readlines()
    for lig in lignes:
        if lig[0]=="%": continue
        if "input" in lig:
            num += 1
            e1 = lig.find(r"{")
            e1 = lig.find(r"-", e1)
            e2 = lig.find(r"}")
            exo_encours = lig[e1+1:e2]
            if num == exo: return exo_encours
        if "exotd" in lig:
            num += 1
            e1 = lig.find(r"{")
            e2 = lig.find(r"}")
            exo_encours = lig[e1+1:e2]
            if num == exo: return exo_encours
    return None

def affiche_titre_chapitre():
    ''' Fonction qui affiche tous les titres d'un chapitre'''
    for fichier in sorted(glob.glob('./tex/*/Cours.tex', recursive=True)):
        num_chap = int(fichier[6:8])
        console.print(f"Chapitre [red]{num_chap}[/red] : [blue]{titre_chapitre(num_chap)}[/blue]")

def titre_exo(chapitre, exo):
    ''' Fonction qui renvoie le titre de l'exercice '''
    try:
        with open(f"./inc/td/{chapitre:02d}/td-{exo:02d}.tex", "r") as file:
            lines = file.readlines()
            for line in lines:
                    # check if string present on a current line
                    if line.find(r"begin{exo") != -1 and line.find(r"[") != -1:
                        titre = re.match(r'.*\[(.*?)\].*',line).group(1)
                        return titre
            return ""
    except: return ""

def affiche_titre_exos_chap(chapitre):
    ''' Fonction qui affiche tous les titres d'un chapitre'''
    chapitre = int(chapitre)
    console.print(f"Exercices du chapitre {chapitre} : "+ titre_chapitre(chapitre))
    with open(f"./tex/{chapitre:02d}/Exo.tex", "r") as file:
        lines = file.readlines()
        i = 0
        for line in lines:
            if line[0]=="%": continue
            if line.find(r"\input{") != -1:
                i += 1
                num = re.match(r'.*td\-(\d*).*',line).group(1)
                titre = titre_exo(chapitre, int(num))
                console.print(f"Exercice {i} : {titre}")

   
def usage():
    console.print(r'''Usage : make \[instr] \[arg]
    * pdf  (chap)         : convertit en PDF le cours (prof, eleve, TD)
    * tout                : convertit tous les fichiers en PDF
    * edit (chap) (exo)   : édite le fichier de cours (chap) ou de l'exercice (chap) (exo)
    * titre (chap)        : affiche les titres des chapitres ou des exos du chapitre (chap)
    ''')
    sys.exit(0)
    
def cree_fichier_cours(chap, cours, prof, exo, correction):
    '''
    Créer le fichier à compiler 
    Arguments : 
        - chap : numéro du chapitre
        - cours : booléen True : cours, False : TD
        - prof : booléen True : prof, False , elève
        - exo : booléen True : oui, False : non
        - correction : booléen True : oui, False: non
    
    - modifie les nums de chapitre comme il le faut
    - modifie proftrue et correctiontrue si nécessaire
    Ne renvoie rien, mais le bon fichier prêt à compiler est présent
    '''
    # 1 : on copie
    if cours:
        shutil.copy('./tex/Chap.tex', 'temporaire.tex')
    else:
        shutil.copy('./tex/TD.tex', 'temporaire.tex')
        
    # 2 : on modifie
    
    # Les num de chapitre
    filereplace("temporaire.tex","##CHAP##",f"{chap:02d}")
    filereplace("temporaire.tex","##CHAPAVANT##",f"{chap-1:02d}")
    
    filereplace("temporaire.tex", "proftrue", f"prof{str(prof).lower()}")
    filereplace("temporaire.tex", "correctiontrue", f"correction{str(correction).lower()}")
    filereplace("temporaire.tex", "exotrue", f"exo{str(exo).lower()}")

    # Noir et blanc si pas prof
    if not prof: filereplace("temporaire.tex",r"\[maths",r"[maths,noir")
    return 0

def cree_fichier_exo(chap, exo):
    '''
    Créer le fichier à éditer 
    Arguments : 
        - chap : numéro du chapitre
        - exo : numero du tex de l'exo
    
    Ne renvoie rien, mais le bon fichier prêt à compiler est présent    
    '''
    shutil.copy('./tex/Exo.tex', 'temporaire.tex')
    filereplace("temporaire.tex","##CHAP##",f"{chap:02d}")
    filereplace("temporaire.tex","##EXO##",exo)
    return 0

### Fonction principale : compilation de tous les PDF possibles
def compile_tout_pdf():
    i = 1
    while os.path.isfile(f"tex/{i:02d}/Cours.tex"):
        compile_pdf(i, aff=False)
        i += 1

### Fonction principale : édition d'un cours ###
def edit_chapitre(num_chapitre):
    ''' Fonction qui édite un chapitre '''
    console.log(f"Edition du chapitre [red]{num_chapitre:02d}[/red] : [blue]{titre_chapitre(num_chapitre)}[/blue]")
    cree_fichier_cours(num_chapitre, cours=True, prof=True, exo=False, correction=False)
    os.system("open -a Texifier temporaire.tex")
    while (subprocess.getoutput("pgrep Texifier") != ""):
        time.sleep(1)
    console.log("Fin d'édition")
    fichiers_temp = glob.glob(os.path.join(".", "temporaire*"))
    for fichier in fichiers_temp: 
        if os.path.isfile(fichier): os.remove(fichier)

def edit_exo_chapitre(num_chapitre, num_exo):
    # On récupère le bon numéro de l'exo 
    tex_exo = renvoie_num_exo(num_chapitre, num_exo)
    cree_fichier_exo(num_chapitre, tex_exo)
    console.log(f"Affichage de l'exercice [red]{num_chapitre:02d}[/red]-[blue]{tex_exo}[/blue] (numéro [green]{num_exo}[/green])")
    os.system("open -a Texifier temporaire.tex")
    while (subprocess.getoutput("pgrep Texifier") != ""):
        time.sleep(1)
    console.log("Fin de l'édition")
    fichiers_temp = glob.glob(os.path.join(".", "temporaire*"))
    for fichier in fichiers_temp: 
        if os.path.isfile(fichier): os.remove(fichier)
    
    
    
### Fonction principale : compilation de PDF ###
def compile_pdf(num_chapitre, aff=True):
    '''
    Prend un argument (numéro du chapitre)
    Produit, si pas d'erreur, deux PDF (prof et élève)
    '''
    # On s'assure que les répertoires importants soient présents
    os.makedirs("pdf/TD", exist_ok=True)
    os.makedirs(f"pdf/{num_chapitre:02d}", exist_ok=True)
    os.makedirs(".temp", exist_ok=True)
    
    titre = titre_chapitre(num_chapitre)
    console.log(f"Traitement du chapitre [red]{num_chapitre:02d}[/red] : [blue]{titre}[/blue]")
    for type_cours in ["prof", "eleve", "TD"]:

        # On créé le fichier associé au type
        nom_final = f"Chapitre {num_chapitre:02d} - {titre}" if type_cours=="prof" else (f"Chapitre {num_chapitre:02d} - {titre}.eleve" if type_cours=="eleve" else f"TD{num_chapitre:02d} - {titre}")
        repertoire = f"pdf/{num_chapitre:02d}" if type_cours != "TD" else "pdf/TD"
        
        with console.status(f"Compilation du chapitre [red]{num_chapitre:02d}[/red] ([green]{type_cours}[/green])", spinner="circleQuarters"):
            # On créé le fichier 
            cree_fichier_cours(num_chapitre, cours=(False if type_cours=="TD" else True), prof=(True if type_cours=="prof" else False), exo=True, correction=(True if type_cours=="prof" else False))
            process=subprocess.Popen(f'latexmk -lualatex="lualatex --shell-escape" -auxdir=.temp -lualatex -outdir={repertoire} -jobname="{nom_final}" -quiet temporaire.tex', shell=True,stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
            result=process.communicate()
            # On vérifie le log
            ok = True
            with open(f".temp/{nom_final}.log", 'r') as fp:
                lines = fp.readlines()
                for line in lines:
                    if (line.find("Error") != -1 and line.find(r"{Exception}") == -1) or line.find("to be read again") != -1:
                        console.log("   [red]Erreur[/red] : "+line.rstrip())
                        ok = False
            if ok:  
                if aff: console.print(CHECK_MARK+ f" Version {type_cours}") 
            else: 
                console.print(ERROR_MARK + " Erreur de compilation. Veuillez corriger la (ou les) erreur(s)")
                return
                
            if os.path.isfile("temporaire.tex"): os.remove("temporaire.tex")
        
        

if __name__ == '__main__':
    console = Console()
    argu = sys.argv
    if len(argu) == 1: usage()
        
    action=argu[1]
    match action:
        case "pdf":
            if len(argu) == 2: usage()
            compile_pdf(int(argu[2]))
            sys.exit(0)
        case "tout":
            compile_tout_pdf()
            sys.exit(0)
        case "edit":
            if len(argu) == 2: usage()
            if len(argu) == 3:
                edit_chapitre(int(argu[2]))
            else:
                edit_exo_chapitre(int(argu[2]), int(argu[3]))
            sys.exit(0)
        case "titre":
            if len(argu) == 2:
                affiche_titre_chapitre()
            elif len(argu) == 3:
                affiche_titre_exos_chap(int(argu[2]))
            else: usage()
