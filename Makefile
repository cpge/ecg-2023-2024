# On gère tout avec principale

.PHONY: pdf 

GOAL   := $(firstword $(MAKECMDGOALS))
PARAMS := $(filter-out $(GOAL),$(MAKECMDGOALS))

$(GOAL):
	@python3 ./script/fait.py $(GOAL) $(PARAMS) #$(MAKECMDGOALS)

%:
	@true
