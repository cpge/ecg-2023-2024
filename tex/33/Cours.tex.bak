\chapter{Convergence et approximation}
%!TeX root=../../encours.nouveau.tex

\objectifintro{
\lettrine{D}{ans} ce dernier chapitre de l'année, nous revenons sur les probabilités en s'intéressant à la notion de convergence de variables aléatoires. Nous verrons qu'il y a plusieurs convergences, 
et nous reviendrons sur l'approximation d'une loi binommiale par une loi de Poisson.
}

%%%%%%%%%%%%%%%%%%%
%%%% Extrait.  %%%%
%%%%%%%%%%%%%%%%%%%
\begin{extrait}{Antoine de Rivarol  (1753--1801).}
Un livre, c'est pendant des semaines, pendant des mois, la pensée qui se recueille et se concentre : c'est ce corps qui fait converger ses muscles et ses nerfs vers un seul point... vers le bout de la 
plume.
\end{extrait}

\begin{objectifs}
\begin{numerote}
\item \lienobj{1}{Connaître les inégalités de Markov et de Bienaymé-Tchebychev}
\item \lienobj{1}{Savoir manipuler la convergence en probabilité}
\item \lienobj{1}{Connaître la loi faible des grands nombres}
\item \lienobj{1}{Connaître la définition de la la convergence en loi}
\item \lienobj{1}{Savoir manipuler la convergence en loi dans le cas des variables à valeurs entières}
\item \lienobj{1}{Savoir approximer une loi binomiale par une loi de Poisson}
\end{numerote}
\end{objectifs}


%%%%%%%%%%%%%%%%%%%
%%% Début du cours %%%
%%%%%%%%%%%%%%%%%%%

Dans ce chapitre, on suppose que toutes les variables aléatoires sont définies sur un espace probabilisé $(\Omega, \AA, \PP)$.

Soit $(X_n)_{n\in \N}$ une suite de variables aléatoires, et soit $X$ une variable aléatoires. On s'intéresse à la manière de définir la convergence de la suite $(X_n)$ vers $X$.

La plus intuitive serait de dire que $(X_n)$ converge vers $X$ si
\[  \PP\left( X_n \tendversen{n\to +\infty}X \right) = \PP\left( \left \{ \omega\in \Omega,\quad X_n(\omega)\tendversen{n\to +\infty} X(\omega)\right\}\right) = 1. \]

Intuitivement, cela veut dire que pour presque toute valeur de $\omega \in \Omega$, la suite réelle $(X_n(\omega))$ tend vers $X(\omega)$. On appelle cette convergence la convergence \textbf{presque 
sûre}. Cette convergence n'est pas au programme de Mathématiques approfondies. Deux autres convergences sont au programme de deuxième année, que nous allons introduire ici avec des résultats de 
probabilités classiques : la \textbf{convergence en loi} et la \textbf{convergence en probabilité}.

\section{Inégalités de Markov et de Bienaymé-Tchebychev}

\subsection{Fonction indicatrice}

Avant de commencer, on introduit une fonction très importante : la fonction indicatrice :

\begin{definition}[Fonction indicatrice]
                Soit $A$ une partie de $\R$. On appelle \textbf{indicatrice} de $A$, et on note $\1_A$, la fonction définie sur $\R$ par
                \[  \1_A (x) = \left \{ \begin{array}{cl} 1 & \text{si $x\in A$,}\\ 0 & \text{si $x\notin A$} \end{array}\right..\]
\end{definition}

\begin{exemple}
                La fonction $\1_{\R+}$ a pour courbe représentative :
        \begin{center}
                \begin{tikzpicture}
                \draw[ ->] (-4,0) -- (4,0) node[ right] {$x$};
                \draw[ ->] (0,-0.1) -- (0,3);
                \draw (0,-0.1) node[ below] {$0$};
                \draw (0.1,2)--(-0.1,2) node[ left] {$1$};

                \draw[ red, line width=1.5pt] (-4,0) -- (0,0);
                \draw[ red, line width=1.5pt] (0,2) -- (4,2);
                \draw[ red,line width=0.5pt,fill=red] (0,2) circle (2.5pt);
                \draw[ red,line width=0.5pt,fill=white] (0,0) circle (2.5pt);
                \end{tikzpicture}
                \end{center}
\end{exemple}

\subsection{Inégalité de Markov}

\begin{theoreme}[Inégalité de Markov]
        Soit $X$ une variable aléatoire réelle discrète \textbf{positive} admettant une espérance. On a alors
\[  \forall a>0,\quad \PP(X\geq a) \leq \frac{\esperance(X)}{a}.\]
\end{theoreme}

\preuve[10]{
Nous allons le démontrer dans le cas énoncé :
% TODO
}

\begin{remarque}
        En réalité, ce résultat est valable pour toute variable aléatoire (pas uniquement discrète). La démonstration est un peu plus abstraite mais intéressante :

\ifprof
Soit $f$ la fonction définie sur $\R$ par
\[ 
f(x) =
\left\{
\begin{array}{cl}
a & \text{si $x\geq a$,} \\
0 & \text{si $x < a$,}
\end{array}
\right.
\quad
\text{ce qui s'écrit encore}
\quad
f(x) = a \cdot \1_{[ a,+\infty[ }(x).
\]
Soit $g$ la fonction définie sur $\R$ par
\[ 
g(x) =
\left\{
\begin{array}{cl}
x & \text{si $x\geq 0$,} \\
0 & \text{si $x < 0$,}
\end{array}
\right.
\quad
\text{ce qui s'écrit encore}
\quad
g(x) = x \cdot \1_{[ 0,+\infty[ }(x).
\]
Voici, en rouge et pointillés, la courbe représentative de $f$, et en bleu et trait plein celle de~$g$.
\begin{center}
\begin{tikzpicture}
\draw[ ->] (-2,0) -- (4,0) node[ right] {$x$};
\draw[ ->] (0,-0.1) -- (0,4);
\draw (0,-0.1) node[ below] {$0$};
\draw[ dotted] (2,0) -- (2,2);
\node[ below] at (2,-0.15) {$a$};
\draw[ dotted] (0,2) -- (2,2);
\draw (0.1,2)--(-0.1,2) node[ left] {$a$};

\draw[ blue,line width=2.5pt] (-2,0) -- (0,0) -- (4,4);
\draw[ red, line width=1.5pt,dashed] (-2,0) -- (2,0);
\draw[ red, line width=1.5pt,dashed] (2,2) -- (4,2);
\draw[ red,line width=0.5pt,fill=red] (2,2) circle (2.5pt);
\draw[ red,line width=0.5pt,fill=white] (2,0) circle (2.5pt);


\end{tikzpicture}
\end{center}
Graphiquement, on constate que $f(x) \leq g(x)$, pour tout réel $x$, ce que nous prouvons de suite. Nous avons
\[ 
g(x) - f(x) =
\left\{
\begin{array}{cl}
0 & \text{si $x< 0$,} \\
x & \text{si $0 \leq x < a$,} \\
x-a & \text{si $ x \geq a$.} \\
\end{array}
\right.
\]
Ainsi, la fonction $g-f$ est bien à valeurs positives, ce qui prouve que $f(x) \leq g(x)$, pour tout réel $x$.

Introduisons à présent la variable aléatoire $Y=f(X)$ à valeurs dans $\{0,a\}$. La variable aléatoire $Y$ possède donc une espérance, et nous avons
\[ 
\esperance(Y) = a \cdot \PP(Y=a) = a \cdot \PP(f(X)= a) = a \cdot \PP(X\geq a).
\]
Ensuite, introduisons la variable aléatoire $Z=g(X)$ à valeurs positives. Nous avons $0 \leq g(X) = X$, car $X$ est à valeurs positives. Ceci montre que $Z$ admet une espérance puisque $X$ en admet 
une, et il vient
\[ 
\esperance(Z) = \esperance(X).
\]
Finalement, nous avons $0 \leq f(X) \leq g(X)$, ce qui prouve que $0\leq Y \leq Z$, ce qui entraîne d'après la positivité de l'espérance
\[ 
a \cdot \PP(X \geq a) = \esperance(Y) \leq \esperance(Z) = \esperance(X).
\]
On a démontré l'inégalité de Markov.
\else
\lignes{30}
\fi
\end{remarque}

\begin{corollaire}
\label{coro:Markov}
Soit $X$ une variable aléatoire possédant un moment d'ordre $m$. Alors, pour tout réel $a>0$, nous avons
\[ 
\PP\big( |X| \geq a \big) \leq \frac{\esperance\big(|X|^m\big)}{a^m}.
\]
\end{corollaire}

\preuve[2]{
En utilisant l'inégalité de Markov avec $|X|^m$, nous avons
\[ 
\PP\big( |X| \geq a \big) = \PP\big( |X|^m \geq a^m \big) \leq \frac{\esperance\big(|X|^m\big)}{a^m}.
\]
}

\subsection{Inégalité de Bienaymé-Tchebychev}

\begin{theoreme}[Inégalité de Bienaymé-Tchebychev, au programme]
        Soit $X$ une variable aléatoire possédant un moment d'ordre $2$. Alors, pour tout réel $a>0$, nous avons
        \[ 
        \PP\big( |X-\esperance(X)| \geq a \big) \leq \frac{\Var(X)}{a^2}.
        \]
\end{theoreme}

\preuve[4]{
        En utilisant le corollaire~\ref{coro:Markov} avec $m=2$ et $Y=X-\esperance(X)$, nous avons
        \[ 
        \PP\big( |X-\esperance(X)| \geq a \big) \leq \frac{\esperance\big(|X-\esperance(X)|^2\big)}{a^2} = \frac{\Var(X)}{a^2}.
        \]
}

\begin{remarque}
Nous pouvons démontrer l'inégalité de Bienaymé-Tchebychev en adaptant la preuve de l'inégalité de Markov. Notons $\mu=\esperance(X)$. Soit $f$ la fonction définie sur $\R$ par
        \[ 
        f(x) =
        \left\{
        \begin{array}{cl}
        a^2 & \text{si $|x-m|\geq a$,} \\
        0 & \text{si $|x-m| < a$.}
        \end{array}
        \right.
        \]
        Soit $g$ la fonction définie sur $\R$ par
        \[ 
        g(x) = (x-m)^2.
        \]
        Voici, en rouge et pointillés, la courbe représentative de $f$, et en bleu et trait plein celle de~$g$.
        \begin{center}
        \begin{tikzpicture}



        \draw[ ->] (-4,0) -- (4,0) node[ right] {$x$};
        \draw[ ->] (0,-0.1) -- (0,6.5);
        \draw (0,-0.1) node[ below] {$m$};
        \draw[ dotted] (2,0) -- (2,2);
        \node[ below] at (2,-0.15) {$m+a$};
        \node[ below] at (-2,-0.15) {$m-am$};
        \draw[ dotted] (0,4) -- (2,4);
        \draw (0.1,4)--(-0.1,4) node[ left] {$a^2$};

        \draw[ line width=2.5pt,color=blue, smooth,samples=100,domain=-2.5:2.5] plot(\x,\x*\x);
        \draw[ red, line width=1.5pt,dashed] (-2,0) -- (2,0);
        \draw[ red, line width=1.5pt,dashed] (2,4) -- (4,4);
        \draw[ red, line width=1.5pt,dashed] (-2,4) -- (-4,4);
        \draw[ red,line width=0.5pt,fill=red] (2,4) circle (2.5pt);
        \draw[ red,line width=0.5pt,fill=red] (-2,4) circle (2.5pt);
        \draw[ red,line width=0.5pt,fill=white] (2,0) circle (2.5pt);
        \draw[ red,line width=0.5pt,fill=white] (-2,0) circle (2.5pt);

        \end{tikzpicture}
        \end{center}
        Graphiquement, on constate que $f(x) \leq g(x)$, pour tout réel $x$, ce que nous prouvons de suite. Nous avons
        \[ 
        g(x) - f(x) =
        \left\{
        \begin{array}{cl}
        (x-m^2)-a^2 & \text{si $|x-m|\geq a$,} \\
        (x-m)^2 & \text{si $|x-m|< a$.}
        \end{array}
        \right.
        \]
        Ainsi, la fonction $g-f$ est bien à valeurs positives, ce qui prouve que $f(x) \leq g(x)$, pour tout réel $x$.

        Introduisons à présent la variable aléatoire $Y=f(X)$ à valeurs dans $\{0,a^2\}$. La variable aléatoire $Y$ possède donc une espérance, et nous avons
        \[ 
        \esperance(Y) = a^2 \cdot \PP(Y=a^2) = a^2 \cdot \PP(f(X)= a^2) = a^2 \cdot \PP(|X-m|\geq a).
        \]
        Ensuite, introduisons la variable aléatoire $Z=g(X)$. Puisque $X$ admet un moment d'ordre $2$, $Z$ admet une espérance , et il vient
        \[ 
        \esperance(Z) = \esperance\big((X-m)^2 \big) = \Var(X).
        \]
        Finalement, nous avons $0 \leq f(X) \leq g(X)$, ce qui prouve que $0\leq Y \leq Z$, ce qui entraîne d'après la positivité de l'espérance
        \[ 
        a^2 \cdot \PP(|X-m|\geq a) = \esperance(Y) \leq \esperance(Z) = \Var(X).
        \]
\end{remarque}

\begin{exemple}
On interroge $1000$ personnes dans la rue (de façon indépendante) et on leur demande s'ils ont voté  pour le candidat A ou le candidat B. On sait que le candidat A a gagné avec un score de 66\%.

On note $X$ la variable aléatoire désignant le nombre de personnes interrogées ayant voté pour A. On suppose que, par équiprobabilité des sondages, $X$ suit une loi binomiale de paramètre $n=1000$ et 
$p=0,66$.

On a $\esperance(X) = np = 660$ et $\Var(X) = np(1 - p) = 224,4$. L'inégalité de Bienaymé-Tchebychev implique que
\begin{align*}
\PP(600 < X < 720) &= \PP(-60 < X - 660 < 60) = \PP(|X - \esperance(X)| < 60)\\
&=  1 - \PP(|X - \esperance(X )| \geq 60) \geq 1-\frac{\Var(X)}{60^2} \approx 0, 938.
\end{align*}
On peut alors en déduire que la probabilité que le nombre de personnes interrogées ayant voté pour A se situe entre $600$ et $720$ est supérieure à $0,938$.
\end{exemple}

\begin{exo}
Soit $p$ la proportion (inconnue) de foyers d'un département  disposant d'un poste de télévision et désireux de recevoir les émissions par câble.

Ne voulant pas procéder à un recensement complet de la population, on se propose d'estimer cette proportion $p$ via un sondage.

On construit un échantillon de taille $n$ de la population en piochant $n$ fois au hasard dans l'annuaire un foyer du département. On suppose qu'un même foyer peut être sondé plusieurs fois, ce qui 
correspond à un tirage avec remise, et que les tirages sont indépendants. On note $X_i$  la variable aléatoire qui vaut $1$ si le $i$-ème foyer sondé possède un téléviseur et est intéressé par le 
cable, et $0$ sinon. Enfin, on note
\[ 
\overline{X}_n = \frac{X_1+\dots+X_n}{n}.
\]
\begin{enumerate}
\item
Quelle est la loi de $X_i$, pour tout $i$ de $\ll1,n\rr$ ? Déterminer son espérance et sa variance.
\item
Préciser l'espérance et la variance de $\overline{X}_n$.
\item
Justifier que $p(1-p) \leq \frac14$.
\item
Déduire de ce qui précède et de l'inégalité de Bienaymé-Tchbychev que, pour tout $\alpha>0$,
\[ 
\PP\big( |\overline{X}_n - p| \geq \alpha\big) \leq \frac{1}{4 \alpha^2 n}.
\]
\item
Sur $n=500$ foyers sondés, $60\%$ d'entre eux sont possesseurs d'un téléviseur et intéressés par le câble.
\begin{enumerate}
\item
Vérifier que  le réel $\alpha_0 = 0.1$ est solution de
\[ 
\frac{1}{4 \alpha_0^2 n} = 0.05.
\]
\item
En déduire qu'avec une probabilité d'au moins 95\%, la proportion de foyers disposant d'un poste de télévision et désireux de recevoir les émissions par cable, se trouve dans l'intervalle
\[ 
]50\%,70\%[ .
\]
\end{enumerate}
\end{enumerate}
%%
\end{exo}

\solution[20]{
\begin{enumerate}
\item
La variable aléatoire $X_i$ suit une loi de Bernoulli de paramètre $p$. Nous avons $\esperance(X_i)=p$ et $\Var(X_i) = p(1-p)$.
\item
Nous avons, par linéarité de l'espérance,
\[ 
\esperance \big(\overline{X}_n \big) = \esperance\left(\frac{X_1+\dots+X_n}{n} \right) = \frac1n \esperance(X_1 + \dots + X_n) = \frac1n [ \esperance(X_1) + \dots \esperance(X_n) ] =p.
\]
Par indépendance des variables aléatoires $(X_i)_{1\leq i\leq n }$ et les propriétés de la variance, il vient
\[ 
\Var\big(\overline{X}_n\big) = \Var\left(\frac{X_1+\dots+X_n}{n} \right) = \frac1{n^2} \Var(X_1 + \dots + X_n) = \frac1{n^2} [ \Var(X_1) + \dots \Var(X_n) ] = \frac{p(1-p)}{n}.
\]
\item
La fonction $p \donne p(1-p)$ est une fonction trinôme dont la dérivée est $1-2p$. On a alors le tableau de variations suivant
\begin{center}
        \begin{tikzpicture}
           \tkzTabInit{$p$ / 1 , $1-2p$ / 1, $p(1-p)$ / 1.5}{$0$, $\frac{1}{2}$, $1$}
           \tkzTabLine{, +, z, -, }
           \tkzTabVar{-/ 0, +/ $\frac14$, -/ 0}
        \end{tikzpicture}
\end{center}

On a bien $p(1-p)\leq 1/4$.
\item
La variable aléatoire $\overline{X}_n$ possède un moment d'ordre 2. Par Bienaymé-Tchebychev, il vient
\[ 
\PP\big( |\overline{X}_n - p| \geq \alpha\big) \leq \frac{\Var\big(\overline{X}_n\big)}{\alpha^2}
= \frac{p(1-p)}{n \alpha^2} \leq \frac{1}{4 \alpha^2 n}.
\]
\item
\begin{enumerate}
\item
On a $\alpha_0^2 =0.01$ puis $4\alpha_0^2 n =20$.
\item
On a alors
 \[ 
\PP\big( |\overline{X}_n - p| \geq \alpha_0\big) \leq \frac{1}{4 \alpha_0^2 n} = 0.05,
\]
d'où
\[ 
\PP\big( |\overline{X}_{500} - p| < 0.1 \big) \geq 95\%.
\]
Or, on a
\[ 
\big\{|\overline{X}_{500} - p| < 0.1 \big\} = \big\{ \overline{X}_{500} - 0.1 < p < \overline{X}_{500} + 0.1 \big\}
= \big\{ p \in \big]\overline{X}_{500} - 0.1, \overline{X}_{500} + 0.1 \big[  \}.
\]
Avec une probabilité d'au moins 95\%, la proportion de foyers disposant d'un poste de télévision et désireux de recevoir les émissions par cable, se trouve dans l'intervalle
\[ 
\interoo{{\overline{X}_{500} - 0.1} \overline{X}_{500} + 0.1} = \interoo{0,5, 0,7}.
\]

\end{enumerate}
%%
\end{enumerate}
}

\begin{theoreme}[Inégalité de Jensen, hors programme]
Soient $X$ une variable aléatoire admettant une espérance et $f$ une fonction telle que $f(X)$ admette une espérance. Si la fonction $f$ est \textbf{convexe}, alors

\[ 
f\big(\esperance(X)\big) \leq \esperance\big(f(X)\big).
\]
\end{theoreme}

\preuve[10]{
Soit $\mu=\esperance(X)$ un réel. Puisque $f$ est convexe, il existe un réel $\lambda$ tel que
\[ 
f(x) \geq f(\mu) + \lambda (x-\mu), \quad \text{pour tout réel $x$.}
\]
Par exemple, dans le cas où $f$ est dérivable en $\mu$, nous avons $\lambda=f'(\mu)$, puisque $f$ est au-dessus de sa tangente en $\mu$. En passant à l'espérance, nous obtenons
\[ 
\esperance\big(f(X)\big) \geq f(\mu) + \lambda \big(\esperance(X) - \mu \big) = f(\mu).
\]
}

\section{Loi faible des grands nombres}

Nous considérons une suite $(X_n)_{n\geq1}$ de variables aléatoires définies sur un espace de probabilité $(\Omega,\AA,\PP)$, et une variable aléatoire \og limite \fg \  $X$ définie sur le même espace 
de probabilité.

\subsection{Convergence en probabilité}

Nous rappelons qu'une suite $(u_n)$ à valeurs réelles converge vers le réel $\ell$ si
\[ 
\forall \eps >0, \quad \exists n_0 \in \N \quad : \quad \forall n \geq n_0, \quad |u_n-\ell | < \eps.
\]
Nous avons alors
\[ 
\lim_{n \to +\infty} | u_n - \ell | = 0 \qeq \lim_{n \to +\infty} u_n = \ell.
\]
Nous allons à présent définir une notion de convergence pour une suite de variables aléatoires: la convergence \emph{en probabilité}.

\begin{definition}[Convergence en probabilité]
\vspace*{.2cm}
On dit que la suite $(X_n)$ \textbf{converge en probabilité} vers $X$, et on note $X_n \convergeen{\PP} X$, si
\[ 
\forall \eps>0, \quad \lim_{n \to +\infty} \PP\big( | X_n - X | \geq \eps \big) = 0.
\]
\end{definition}

\begin{exemple}
Soit $(X_n)_{n\geq1}$ une suite de variables aléatoires de Bernoulli telles que
\[ 
\PP(X_n=1) = \frac1n \qeq \PP(X_n=0) = 1 - \frac1n.
\]
\begin{enumerate}
\item
Montrer que $(X_n)$ converge en probabilité vers $0$.
\item
La suite $\big( \esperance(X_n) \big)$ est-elle convergente ?
\end{enumerate}
%%
\end{exemple}

\solution[10]{
\begin{enumerate}
\item
Soit $\eps>1$. Puisque pour tout entier $n\geq 1$, nous avons $X_n$ à valeurs dans $\{0,1\}$, on en déduit que $\{| X_n - 0 | \geq \eps\} = \vide$, et que $\PP(| X_n - 0 | \geq \eps) = 0$. En 
particulier, on a \( \lim\limits_{n \to +\infty} \PP\big( | X_n - 0 | \geq \eps \big) = 0\).

Soit $\eps$ dans $]0,1$. Nous avons alors $\{| X_n - 0 | \geq \eps\} = \{X_n=1\}$, pour tout entier $n$, et on en déduit que $\PP(| X_n - 0 | \geq \eps) = \frac1n$. En particulier, on a \( 
\lim\limits_{n \to +\infty} \PP\big( | X_n - 0 | \geq \eps \big) = 0\).

On a bien prouvé que
\[ 
\forall \eps>0, \quad \lim_{n \to +\infty} \PP\big( | X_n - 0 | \geq \eps \big) = 0.
\]
\item
Nous avons $\esperance(X_n)= \frac1n$, ce qui entraîne que la suite $\big( \esperance(X_n) \big)$ converge vers $0$. \end{enumerate}
}

\begin{attention}
Si la suite $(X_n)$ converge en probabilité vers $X$, la suite $\big( \esperance(X_n) \big)$ ne converge pas toujours vers $\esperance(X)$.
\end{attention}

\begin{exemple}
Soit $(X_n)_{n\geq1}$ une suite de variables aléatoires telles que
\[ 
\PP(X_n=n^2) = \frac1n \qeq \PP(X_n=0) = 1 - \frac1n.
\]
\begin{enumerate}
\item
Montrer que $(X_n)$ converge en probabilité vers $0$.
\item
La suite $\big( \esperance(X_n) \big)$ est-elle convergente ?
\end{enumerate}
%%
\end{exemple}

\solution[10]{
\begin{enumerate}
\item
Soit $\eps>0$. Pour tout entier $n\geq \sqrt{\eps}$, nous avons $\{| X_n - 0 | \geq \eps\} = \{X_n=n^2\}$, et on en déduit que $\PP(| X_n - 0 | \geq \eps) = \frac1n$. En particulier, nous avons $\ds 
\lim_{n \to +\infty} \PP\big( | X_n - 0 | \geq \eps \big) = 0$.
\item
Nous avons $\esperance(X_n)= n^2 \cdot\frac1n=n$, ce qui entraîne que la suite $\big( \esperance(X_n) \big)$ tend vers $+\infty$ et n'est donc pas convergente.
\end{enumerate}
}

\begin{exemple}
Montrer que si les variables aléatoires  $X_n$ et $X$ admettent une espérance et que
\[ 
\esperance\big( | X_n - X | \big) \to 0, \quad \text{quand $n \to +\infty$,}
\]
alors $(X_n)$ converge en probabilité vers $X$.
\end{exemple}

\solution[5]{
Soit $\eps>0$, d'après l'inégalité de Markov, nous avons
\[ 
\PP \big(  | X_n - X | \geq \eps  \big) \leq \frac{\esperance\big( | X_n - X | \big)}{\eps} \tendversen{n\to +\infty} 0.
\]
}


\subsection{Loi faible des grands nombres}

\begin{theoreme}[Loi faible des grands nombres]
Soit $(X_n)_{n\geq1}$ une suite de variables aléatoires \textbf{indépendantes}, et admettant une espérance $\mu$ et une variance $\sigma^2$ \textbf{communes}.

Alors la suite $(\overline{X}_n)$, définie pour tout entier $n\geq 1$ par
\[ 
\overline{X}_n = \frac1n \sum_{k=1}^n X_k = \frac{X_1 + \dots + X_n}{n}
\]
 converge en probabilité vers $\mu$.
\end{theoreme}

\preuve[20]{
Puisque les variables aléatoires $X_n$ admettent un moment d'ordre 2, on en déduit que $ \overline{X}_n$ admet un moment d'ordre 2. Par ailleurs, nous avons
\[ 
\esperance\big(\overline{X}_n\big) =  \esperance\left(\frac1n \sum_{k=1}^n X_k \right) = \frac1{n}  \sum_{k=1}^n \esperance(X_k) = \mu,
\]
et par indépendance des $X_k$,
\[ 
\Var\big(\overline{X}_n\big) = \Var\left(\frac1n \sum_{k=1}^n X_k \right) = \frac1{n^2} \sum_{k=1}^n \Var(X_k) = \frac{\sigma^2}{n}.
\]
Soit $\eps>0$. En appliquant l'inégalité de Bienaymé-Tchebychev sur $\overline{X}_n $, il vient
\[ 
\PP \big(  \left| \overline{X}_n - \mu \right| \geq \eps  \big) \leq \frac{\Var\big(\overline{X}_n\big)}{\eps^2} = \frac{\sigma^2}{\eps^2 n}\tendversen{n\to +\infty} 0.
\]
}

\begin{experiencehistorique}
        \begin{tabular}{p{1cm}p{12cm}p{1cm}}
                \lien{https://fr.wikipedia.org/wiki/Simeon_Denis_Poisson} &
\begin{minipage}[ t]{0.8\textwidth}     Ce résultat a été conjecturé par Jacques Bernoulli (1654--1705) dans son livre \emph{Ars Conjectandi} (Arts de la conjecture), et démontré par Siméon Denis 
Poisson (1781--1840), dans \emph{Recherches sur les probabilités des jugements}, dans le cas où les $(X_n)$ suivent toutes une loi de Bernoulli de paramètre $p$.\end{minipage} & 
\lien{https://fr.wikipedia.org/wiki/Jacques_Bernoulli} \\
        \lien{https://fr.wikipedia.org/wiki/Alexandre_Khintchine} &
        \begin{minipage}[ b]{0.8\textwidth}\vspace*{.1cm}Il a ensuite été démontré, tel qu'énoncé précédememnt, au début du XX\ieme siècle grâce à l'inégalité de Markov, entre autre par Kolmogorov et 
Tchebychev.

         Alexandre Khintchine (1894--1959), mathématicien russe, a montré que l'hypothèse d'existence d'une variance était inutile. Cet intitulté est hors-programme.\end{minipage}
         & \lien{https://fr.wikipedia.org/wiki/Pafnouti_Tchebychev} \vspace*{.6cm}
\end{tabular}
\end{experiencehistorique}

\begin{exemple}
Soit $p$ dans $\interoo{0 1}$ et soit $(X_n)_{n\geq1}$ une suite de variables aléatoires indépendantes suivant toutes une loi de Bernoulli de paramètre $p$.

Montrer que $(\overline{X}_n)$ converge en probabilité vers $p$.
\end{exemple}

\solution[3]{
Une loi de Bernoulli de paramètre $p$ admet une espérance qui vaut $p$ et une variance qui vaut $p(1-p)$. D'après la loi faible des grands nombres, on a bien $(\overline{X}_n)$ qui converge en 
probabilité vers $p$.
}

\subsection{Application en Python}

        Voici un programme pour mettre en évidence la convergence en probabilité de la moyenne empirique de Bernoulli vers leur paramètre. On répète 2000 fois l'expérience consistant à effectuer 15 000
pile ou face, et on observe l'évolution de $(\overline{X}_n)_n$.

\pythonfile[ 1]{loi_faible.py}

ce qui donne :

\begin{center}
        \vspace*{-.5cm}\includegraphics{loi_faible_grands_nombres.pdf}
\end{center}

\section{Convergence en loi et approximation}

Nous considérons une suite $(X_n)_{n\geq1}$ de variables aléatoires (pouvant être définies sur des espaces de probabilité différents) et une variable aléatoire \guill{limite} $X$. Nous notons $F_n$ la 
fonction de répartition de $X_n$, pour tout entier $n\geq1$, et $F$ celle de $X$.

\subsection{Convergence en Loi}

Rappelons le résultat suivant.
\begin{proposition}
Deux variables aléatoires ont la même loi si, et seulement si, leurs fonctions de répartition coïncident sur $\R$.
\end{proposition}

La fonction de répartition caractérise donc la loi d'une variable aléatoire. Ce n'est pas l'unique objet qui possède cette propriété, mais c'est le seul au programme.

\begin{definition}
        \vspace*{.2cm}
On dit que la suite $(X_n)$ \textbf{converge en loi} vers $X$, et on note $X_n \convergeen{\LL} X$, si, pour tout point $x$ où $F$ est \textbf{continue}, nous avons
\[ 
\lim_{n \to +\infty} F_n(x) = F(x).
\]
\end{definition}

\begin{proposition}
Si $X_n$ est à valeurs \textbf{entières}, pour tout entier $n$, ainsi que $X$, la suite $(X_n)$ converge en loi vers $X$ si, et seulement si,
\[ 
\forall k\in \N,\quad \lim_{n \to +\infty} \PP(X_n=k) = \PP(X=k).
\]
\end{proposition}

\preuve[15]{
Puisque $X$ est à valeurs entières, sa fonction de répartition $F$ est continue sur $\R \setminus \N$. Si $x<0$, nous avons $F_n(x) = 0 = F(x)$, ce qui prouve que
\[ 
\lim_{n \to +\infty} F_n(x) = F(x), \quad \text{ pour tout réel $x<0$.}
\]
Soit $x>0$ un réel non entier. Nous avons
\[ 
F_n(x) = \sum_{k=0}^{\lfloor x \rfloor} \PP(X_n=k)
\qeq
F(x) = \sum_{k=0}^{\lfloor x \rfloor} \PP(X=k).
\]
Ainsi, si $\lim\limits_{n \to +\infty} \PP(X_n=k) = \PP(X=k)$, pour tout entier $k$, On en déduit que
\[ 
\lim_{n \to +\infty} F_n(x) = \lim_{n \to +\infty} \left(\sum_{k=0}^{\lfloor x \rfloor} \PP(X_n=k) \right)
= \sum_{k=0}^{\lfloor x \rfloor}\left( \lim_{n \to +\infty}  \PP(X_n=k) \right) = \sum_{k=0}^{\lfloor x \rfloor}  \PP(X=k) = F(x).
\]
Réciproquement, si pour tout point $x>0$ non entier, nous avons
\[ 
\lim_{n \to +\infty} F_n(x) = F(x),
\]
en remarquant que
\[ 
\PP(X_n=k) = F_n\left(k+\frac12\right) - F_n\left(k-\frac12 \right), \quad \text{pour tout entier $k$,}
\]
il vient alors
\[ 
\lim_{n \to +\infty} \PP(X_n=k) = \lim_{n \to +\infty}F_n\left(k+\frac12\right) - \lim_{n \to +\infty} F_n\left(k-\frac12 \right) = F\left(k+\frac12\right) - F\left(k-\frac12 \right) = \PP(X=k)\].
}

\begin{exemple}
Soit $(X_n)_{n\geq1}$ une suite de variables aléatoires de Bernoulli telles que
\[ 
\PP(X_n=1) = \frac1n \qeq \PP(X_n=0) = 1 - \frac1n.
\]
Montrer que $(X_n)$ converge en loi vers $0$.
\end{exemple}

\solution[5]{
Chaque variable aléatoire $X_n$ est à valeurs entières et nous avons
\[ 
\lim_{n \to +\infty} \PP(X_n=0) = 1,
\qeq
\lim_{n \to +\infty} \PP(X_n=k) = 0,
\quad  \text{pour tout entier naturel $k\geq1$.}
\]
Ceci prouve que $(X_n)$ converge en loi vers $0$.
}

\begin{exemple}
Soit $(X_n)_{n\geq1}$ une suite de variables aléatoires telles que
\[ 
\PP(X_n=n^2) = \frac1n \qeq \PP(X_n=0) = 1 - \frac1n.
\]
Montrer que $(X_n)$ converge en loi vers $0$.
\end{exemple}

\solution[7]{
Chaque variable aléatoire $X_n$ est à valeurs entières. Soit $k$ un entier non nul. Nous avons
$\PP(X_n=k)=0$, pour tout entier $n > \sqrt{k}$, ceci prouve que
\[ 
\lim_{n \to +\infty} \PP(X_n=k) = 0, \quad  \text{pour tout entier naturel $k\geq1$.}
\]
D'autre part, nous avons
\[ 
\lim_{n \to +\infty} \PP(X_n=k) = 0.
\]
Tout ceci prouve que $(X_n)$ converge en loi vers $0$.
}

\begin{exo}
Soit un réel $\lambda>0$ et un entier $n\geq1$ tels que $0 < \lambda < n$. On considère $n$ variables  aléatoires $(U_j)_{1\leq j \leq n}$ suivant une loi de Bernoulli de paramètre $\lambda/n$.

On a donc, pour tout entier $j$ de $\ll1,n\rr$.
\[ 
\PP(U_j=1) = \frac{\lambda}{n} \qeq \PP(U_j=0) = 1-\frac{\lambda}{n},  \quad \text{pour tout entier $i \geq 2$.}
\]
\begin{enumerate}
\item
Quelle est la loi de $\ds X_n= \sum_{j=1}^n U_j$ ?
\item
Soit $k$ un entier naturel.
\begin{enumerate}
        \item
        Donner l'expression de $\PP(X_n = k)$, pour tout $n \geq k$.
        \item
        \'Etudier la limite de $\left(1 - \frac{\lambda}n\right)^n$ quand $n$ tend vers l'infini.
        \item
        \'Etudier la limite de $\binom{n}{k} \left( \frac{\lambda}{n} \right)^k$, quand $n$ tend vers l'infini.
        \item
        En déduire que $(X_n)$ converge en loi vers $X$, où $X \suit \Pcal(\lambda)$.                                   \end{enumerate}
\end{enumerate}
\end{exo}

\solution[20]{
\begin{enumerate}
\item
La variable aléatoire $X_n$ suit une loi binomiale de paramètre $n$ et $\frac{\lambda}n$.
\item
\begin{enumerate}
\item
Nous avons
\[ 
\PP(X_n = k) = \binom{n}{k} \left( \frac{\lambda}{n} \right)^k \left(1- \frac{\lambda}{n} \right)^{n-k} , \quad \text{pour tout $n \geq k$.}
\]
\item
Remarquons que
\[ 
 n \ln \left(1- \frac{\lambda}{n} \right) \eq{n\to +\infty} -\lambda.
\]
Ceci montre, par composition de limites, que
\[ 
\left(1- \frac{\lambda}{n} \right)^{n} = \exp \left[  n \ln \left(1- \frac{\lambda}{n} \right) \right] \to \eu{-\lambda}, \quad \text{quand $n \to +\infty$.}
\]
\item
Nous avons
\[ 
\binom{n}{k} \left( \frac{\lambda}{n} \right)^k = \frac{n!}{k!(n-k)!} \left( \frac{\lambda}{n} \right)^k = \frac{1}{k!}  \left( \frac{\lambda}{n} \right)^k  \prod_{i=0}^{k-1} (n-i) = \frac{1}{k!} 
\prod_{i=0}^{k-1}\left[ \lambda \frac{n-i}n  \right].
\]
\`A $i$ fixé, nous avons $\frac{n-i}n \to 1$ quand $n \to + \infty$, et on en déduit que
\[ 
 \prod_{i=0}^{k-1}\left[ \lambda \frac{n-i}n  \right] \to \lambda^k, \quad \text{quand $n \to +\infty$.}
\]
On en déduit que
\[ 
\binom{n}{k} \left( \frac{\lambda}{n} \right)^k \to \frac{\lambda^k}{k!}, \quad \text{quand $n \to +\infty$.}
\]
\item
Par produit, nous avons
\[ 
\PP(X_n=k) = \binom{n}{k} \left( \frac{\lambda}{n} \right)^k \left(1- \frac{\lambda}{n} \right)^{n-k} \to \eu{-\lambda} \frac{\lambda^k}{k!}, \quad \text{quand $n \to +\infty$.}
\]
Si $X$ suit une loi de Poison de paramètre $\lambda$, nous avons
\[ 
\lim_{n \to +\infty} \PP(X_n=k) = \PP(X=k), \quad \text{pour tout entier $k$,}
\]
ce qui prouve que $(X_n)$ converge en loi vers $X$.
\end{enumerate}
\end{enumerate}
}

\begin{attention}
Toute suite qui converge en loi vers $X$ ne converge pas forcément en probabilité vers $X$.
\end{attention}

\begin{exemple}
Soit $(X_n)$ une suite de variables aléatoires i.i.d. suivant la loi de Rademacher, c'est à dire telle que
\[ 
\PP(X_n=1)=\PP(X_n=-1)=\frac12.
\]
Soit $X$ une variable aléatoire de Rademacher indépendante des $(X_n)$

Montrer que $(X_n)$ converge en loi vers $X$, mais pas en probabilité.
\end{exemple}


\solution[10]{
Remarquons que $X_n$ et $X$ ont la même loi, ce qui signifie que $F_n$ et $F$ coïncide sur $\R$, pour tout entier $n$. On a en particulier
\[ 
\lim_{n \to +\infty} F_n(x) = F(x),
\]
pour tout point $x$ où $F$ est continue.

Montrons à présent que $(X_n)$ ne converge pas en probabilité vers $X$. Pour cela, il nous faut exhiber un $\eps>0$ tel que
\[ 
\lim_{n \to +\infty} \PP\big( |X_n-X| \geq \eps \big) \neq 0.
\]
Remarquons que du fait que $X_n$ et $X$ sont à valeurs dans $\{-1,1\}$, nous avons $|X_n-X|$ à valeurs dans $\{0,2\}$, et
\[ 
\PP\big(|X_n-X| =2\big) = \PP(X_n=1,X=-1) + \PP(X_n=-1,X=1) = \frac12,  \quad \text{pour tout entier $n$.}
\]
Ainsi, nous avons, avec $\eps=2$,
\[ 
\PP\big( |X_n-X| \geq \eps \big) = \frac12, \quad \text{pour tout entier $n$,}
\]
ce qui prouve que
\[ 
\lim_{n \to +\infty} \PP\big( |X_n-X| \geq \eps \big) =\frac12 \neq 0.
\]
}

\begin{remarque}
        En revanche, si la suite $(X_n)$ converge en probabilité vers $X$, alors elle converge en loi. Ce résultat est hors-programme, et fait l'objet de l'exercice \lienexo{23}.
\end{remarque}

\subsection{Approximation d'une loi binomiale par une loi de Poisson}

On va démontrer un résultat qui va justifier, a posteriori, un résultat vu précédemment : sous certaines circonstances, on peut approcher une loi binomiale par une loi de Poisson :

\begin{theoreme}[Approximation binomiale / Poisson]
        Soit $\left(p_{n}\right)_{n \in \N*}$ une suite de réels de $\interoo{0 1}$ telle que $n p_{n}\tendversen{n\to +\infty}\lambda>0$.

        Soit $\left(X_{n}\right)_{n\in \N*}$ une suite de variables aléatoires telle que, pour tout $n \in \N*$, $X_{n} \suit \mathcal{B}\left(n, p_{n}\right)$.

        Alors $\left(X_{n}\right)_{n \in \N}$ converge en loi vers une variable aléatoire suivant une loi $\mathcal{P}(\lambda)$.
\end{theoreme}

\preuve[15]{
Soit $X$ une variable aléatoire suivant une loi $\mathcal{P}(\lambda)$. Toutes les variables aléatoires en jeu sont à valeurs dans $\N$. On peut donc utiliser le critère du paragraphe précédent.

Soit $k\in \N$ et $n\in \N*$. On a
\[ 
\PP\left(X_{n}=k\right)= \binom{n}{k}p_{n}^{k}\left(1-p_{n}\right)^{n-k}
\]
On a :
\begin{itemize}
\item $\binom{n}{k} = \frac{n(n-1) \cdots(n-k+1)}{k !} \eq{n\to +\infty} \frac{n^{k}}{k !}$
\item $p_{n} \eq{+\infty} \frac{\lambda}{n}$ donc par exponentiation, $p_{n}^{k} \eq{n\to +\infty} \frac{\lambda^{k}}{n^{k}}$.
\item $\left(1-p_{n}\right)^{n-k}=\eu{(n-k) \ln \left(1-p_{n}\right)}$.Puisque $-p_{n} \tendversen{+\infty} 0$, on a
\[ 
(n-k) \ln \left(1-p_{n}\right) \eq{n\to+\infty} (n-k)\left(-p_{n}\right) \eq{n\to +\infty} -n p_{n} \tendversen{n\to +\infty} -\lambda
\]
\end{itemize}
Comme exp est continue en $-\lambda$, on obtient $\left(1-p_{n}\right)^{n-k} \tendversen{n\to +\infty} \eu{-\lambda}$.
Finalement
\[ 
\PP\left(X_{n}=k\right)  \eq{n\to +\infty} \frac{n^{k}}{k !} \frac{\lambda^{k}}{n^{k}} \eu{-\lambda}=\eu{-\lambda} \frac{\lambda^{k}}{k !}=\PP(X=k)
\]
Ainsi $X_{n} \convergeen{\LL} X$.
}

En utilisant un programme Python, on peut mettre en exergue ce résultat :

\pythonfile[ 1]{conv_bino_poi.py}

On voit alors bien la convergence en loi :
\begin{center}
\begin{tabular}{cc}
\includegraphics[ width=0.5\textwidth]{conv_bino_poi_20.pdf} & \includegraphics[ width=0.5\textwidth]{conv_bino_poi_100.pdf} \\ \includegraphics[ width=0.5\textwidth]{conv_bino_poi_500.pdf} 
&\includegraphics[ width=0.5\textwidth]{conv_bino_poi_1000.pdf}
\end{tabular}
\end{center}

\begin{remarque}
        En pratique, En pratique, si $X$ est une variable aléatoire suivant la loi binomiale $\BB(n,p)$, avec $n\geq 30$, $p\leq 0,1$ et $np\leq 15$, on peut approximer la loi de $X$ par une loi de 
Poisson de paramètre $\lambda=np$.
\end{remarque}

%%% Fin du cours %%%
