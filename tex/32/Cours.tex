\chapter{Fonctions convexes}
%!TeX root=../../encours.nouveau.tex

\objectifintro{
        \lettrine[lines=2, lhang=0.33, loversize=0.25]{N}{ous} allons introduire la notion de convexité de fonctions. Cette notion nous permettra de représenter plus 
fidèlement des fonctions, mais aussi d'obtenir des inégalités intéressantes.
}

%%%%%%%%%%%%%%%%%%%
%%%% Extrait.  %%%%
%%%%%%%%%%%%%%%%%%%
\begin{extrait}{Karl Kraus (1874--1936).}
La différence entre les psychiatres et les autres malades mentaux, c'est un peu le rapport entre la folie convexe et la folie concave.
\end{extrait}

\begin{objectifs}
\begin{numerote}
\item \lienobj{01}{connaître la définition}
\item \lienobj{02}{savoir démontrer une convexité dans le cas $\CC^1$}
\item \lienobj{03}{savoir démontrer une convexité dans le cas $\CC^2$}
\item \lienobj{04}{savoir déterminer des inégalités de convexité}
\item \lienobj{05}{connaître l'inégalité des trois pentes et la croissance des pentes}
\end{numerote}
\end{objectifs}


%%%%%%%%%%%%%%%%%%%
%%% Début du cours %%%
%%%%%%%%%%%%%%%%%%%

Dans l'ensemble de ce chapitre, $I$ désigne un intervalle de $\R$ non vide et non réduit à un point.

\section{Paramétrisation}

Soient $a$ et $b$ deux réels de $I$ tels que $a<b$. Soit $f$ une fonction définie sur $I$. On munit le plan d'un \ron, et on note 
$\CC_f$ la courbe représentative de $f$.

On peut \textit{paramétriser} un segment :

\begin{proposition}
  L'ensemble des réels de la forme $(1-t)a+tb$, où $t$ parcourt $\interff{0 1}$, est le segment \interff{a b}.
\end{proposition}

\preuve{ %[5]{
On procède par double inclusion :
\begin{itemize}
\item Si $t\in \interff{0 1}$, alors $(b-a)t\in \interff{0 b-a}$ et finalement $(1-t)a+tb=a + t(b-a) \in \interff{a b}$.
\item Réciproquement, si $u\in \interff{a b}$, alors $t=\frac{u-a}{b-a}$ vérifie
  \[  (1-t)a+tb = \frac{b-u}{b-a} a + \frac{u-a}{b-a}b=u\frac{b-a}{b-a}=u. \]
\end{itemize}
}

\begin{remarque}
De la même manière, $\left \{ t a + (1-t)b,\, t\in \interff{0 1}\right \} = \interff{a b}$ également.
\end{remarque}

De la même manière, on a :
\begin{itemize}
\item $u\in f\left(\interff{a b}\right)$ si et seulement s'il existe $t\in \interff{0 1}$ tel que $u = f((1-t)a+tb)$.
\item $u \in \interff{f(a) f(b)}$ si et seulement s'il existe $t\in \interff{0 1}$ tel que $u=(1-t)f(a)+tf(b)$, en considérant que si 
$f(a)>f(b)$, $\interff{f(a) f(b)}=\interff{f(b) f(a)}$.
\end{itemize}

\begin{definition}
Soient $A$ et $B$ les deux points du plan, de coordonnées respectives $(a, f(a))$ et $(b, f(b))$.

On appelle \textbf{arc} de $\CC_f$ entre $a$ et $b$ la portion de $\CC_f$ comprise entre $A$ et $B$.
On appelle \textbf{corde} de cet arc le segment $\interff{A B}$.
\begin{center}
  \includegraphics[ width=10cm]{arc-corde.mps}
\end{center}
\end{definition}

\section{Convexité, concavité}

\subsection{Définition}

\begin{definition}
  Soit $I$ un intervalle, $f:I\rightarrow \R$ une fonction, et $\CC_f$ sa courbe représentative dans un repère orthonormé.
\begin{itemize}
\item On dit que $f$ est \textbf{convexe} si tout arc de $\CC_f$ est en dessous de sa corde.
\item On dit que $f$ est \textbf{concave} si tout arc de $\CC_f$ est au dessus de sa corde.
\end{itemize}
\end{definition}

\begin{center}
\begin{tabular}{cc}
        \includegraphics[ width=7cm]{convexe}&\includegraphics[ width=7cm]{concave}\\ \textit{Fonction convexe} & \textit{Fonction 
concave}
\end{tabular}
\end{center}


En utilisant la paramétrisation des arcs et cordes:

\begin{proposition}[Convexité, concavité]
  Soit $I$ un intervalle, et $f:I \rightarrow \R$ une fonction.
\begin{itemize}
\item La fonction $f$ est convexe sur $I$, si, pour tout $a,b \in I$ tels que $a<b$ on a
\[ \forall t \in \interff{0 1},\quad f((1-t)a+tb) \leq (1-t) f(a)+tf(b)\]
\item La fonction $f$ est concave sur $I$, si, pour tout $a,b \in I$ tels que $a<b$ on a
\[ \forall t \in \interff{0 1},\quad f((1-t)a+tb) \geq (1-t) f(a)+tf(b)\]

\end{itemize}
\end{proposition}

\begin{remarque}
De même, $f$ est convexe sur $I$ si, pour tout $(a, b)\in I^2$, tels que $a<b$, on a
\[  \forall t\in \interff{0 1},\quad f(ta + (1-t)b) \leq tf(a) + (1-t)f(b).\]
\end{remarque}

\begin{exemple}
  La fonction $f:x\mapsto x^2$ est convexe sur $\R$.
\end{exemple}

\preuve[7]{
        En effet, soient $a$ et $b$  deux réels tels que $a<b$ et  $t \in \interff{0 1}$. On a
\begin{align*}
  f(ta+(1-t)b)&=(ta+(1-t)b)^2=t^2a^2+2t(1-t)ab+(1-t)^2b^2
\end{align*}
On a alors
\begin{align*}
  f(ta+(1-t)b)-(tf(a)+(1-t)f(b) )&= t^2a^2+2t(1-t)ab+(1-t)^2b^2-ta^2-(1-t)b^2\\
        &= t(t-1)a^2+t(1-t)2ab+(t-1)tb^2\\&=t(t-1)(a-b)^2 \leq 0 \textrm{ puisque } t \in \interff{0 1}
\end{align*}
}

\begin{exo}
        Démontrer que la fonction valeur absolue est convexe sur $\R$.
\end{exo}

\solution[5]{
Soient $(a,b)\in \R^2$ tels que $a<b$, et $t\in \interff{0 1}$. On a, par inégalité triangulaire :
\begin{align*}
        \left| (1-t)a+tb\right| &\leq |(1-t)a| + |tb| \\
        &\leq |1-t|\cdot |a| + |t| \cdot|b| = (1-t)|a| + t|b| \text{ puisque } t\geq 0 \text{ et } 1-t\geq 0.
\end{align*}
Ainsi, la fonction valeur absolue est convexe.
}

\begin{remarque}
        Une fonction $f$ est concave si et seulement si $-f$ est convexe.
\end{remarque}

\begin{attention}
        Une fonction peut tout à fait être ni convexe, ni concave !
\end{attention}

\subsection{Convexité, continuité et dérivabilité}

\begin{theoreme}
Une fonction convexe (ou concave) sur un intervalle ouvert $I$ est continue, et admet des dérivées à droite et à gauche en tout point.
\end{theoreme}

\begin{demonstration}
Résultat admis.
\end{demonstration}

\begin{remarque}
Ainsi, par contraposée du théorème précédent, si une fonction n'est pas continue sur un intervalle $I$, elle ne peut \textit{a fortiori}
pas être convexe sur cet intervalle
\end{remarque}

\subsection{Propriétés}

\subsubsection{Inégalités de convexité}

\begin{theoreme}[Inégalité de convexité]
Soit $f: I \to \R$ une application.

$f$ est convexe sur $I$ si et seulement si pour tout $n \geq 2$, pour tous $\left(x_{1}, \hdots, x_{n}\right) \in I^{n}$ et pour tous 
$\left(\lambda_{1}, \ldots, \lambda_{n}\right) \in \interff{0 1}^{n}$ vérifiant $\lambda_{1}+\cdots+\lambda_{n}=1$, on a
        \[ 
        f\left(\sum_{i=1}^{n} \lambda_{i} x_{i}\right) \leqslant \sum_{i=1}^{n} \lambda_{i} f\left(x_{i}\right)
        \]
\end{theoreme}

\preuve{ %[20]{
Le sens réciproque est immédiat, en prenant $n=2$, $\lambda_1=1-t$ et $\lambda_2=t$. On montre le sens direct par récurrence sur $n$.
\begin{itemize}
\item Pour $n=2$, il s'agit de la définition de la convexité.
\item Supposons la proposition $P_n$ vraie pour un certain $n\geq 2$ fixé. Soient $(x_1,\hdots,x_{n+1})\in I^{n+1}$ et 
$(\lambda_1,\hdots, \lambda_{n+1}) \in \interff{0 1}^{n+1}$ vérifiant $\lambda_1+\hdots+\lambda_{n+1}=1$. Si $\lambda_{n+1}=1$, 
l'inégalité est immédiate. Sinon, on pose $t_n=\lambda_1+\hdots+\lambda_n$ et \[  \alpha_n= \frac{1}{t_n}\sum_{i=1}^n \lambda_i x_i. \]
        $f$ étant convexe, et $t_n\in \interof{0 1}$, on a
        \[  f( \lambda_{n+1}x_{n+1} + (1-\lambda_{n+1})\alpha_n) \leq \lambda_{n+1}f(x_{n+1}) + (1-\lambda_{n+1}) f(\alpha_n) \]
        soit, en remarquant que $1-\lambda_{n+1} = t_n$
        \[  f(\lambda_{n+1}x_{n+1} + t_n \alpha_n) \leq \lambda_{n+1} f(x_{n+1}) + t_n f(\alpha_n) \]
        soit encore, par définition de $\alpha_n$
        \[  f\left(\lambda_{n+1}x_{n+1}+ \sum_{i=1}^n \lambda_ix_i\right) \leq \lambda_{n+1} + t_n f(\alpha_n). \]
        On peut appliquer l'hypothèse de récurrence à la famille $(x_1,\hdots, x_n)$ et $\left(\frac{x_1}{t_n},\hdots, 
\frac{x_n}{t_n}\right)$ puisque, par définition de $t_n$, $\sum\limits_{i=1}^n \frac{\lambda_i}{t_n} = 1$. Ainsi,
        \[  f(\alpha_n) \leq \sum_{i=1}^n \frac{\lambda_i}{t_n} x_i. \]
        On remplace alors dans l'inégalité précédente :
        \[  f\left(\sum_{i=1}^{n+1} \lambda_ix_i\right) \leq \lambda_{n+1}x_{n+1} + t_n\sum_{i=1}^n \frac{\lambda_i}{t_n}x_i = 
\sum_{i=1}^{n+1} \lambda_i x_i. \]
\end{itemize}
Le principe de récurrence permet de conclure que la propriété est vraie pour tout $n\geq 2$.
}

On dispose d'un corollaire qui est un cas particulier usuel :

\begin{corollaire}[ Image de la moyenne]
Soit $f:I\to \R$ une application convexe. Alors, pour tout $n\in \N*$,
\[  \forall (x_1,\hdots, x_n)\in I^n,\quad f\left(\frac{x_1+\hdots+x_n}{n}\right)\leq \frac{f(x_1)+\hdots +f(x_n)}{n}. \]
\end{corollaire}

\begin{exo}
Soient $x_1,\hdots,x_n$ des réels. Montrer que \[  \left(x_1+\hdots+x_n\right)^2\leq n\left(x_1^2+\hdots +x_n^2\right). \]
\end{exo}

\solution[5]{
La fonction carrée est convexe sur $\R$. Pour tout réels $x_1,\hdots,x_n$ on a alors
\[  \left(\frac{x_1+\hdots+x_n}{n}\right)^2 \leq \frac{x_1^2+\hdots + x_n^2}{n} \]
ce qui donne le résultat.
}

\subsubsection{Inégalité des trois pentes}

Les fonctions convexes vérifient une inégalité visuellement très compréhensible :

\begin{theoreme}[Inégalité des trois pentes]
Soit $f$ une fonction convexe sur $I$. Soient $(a, b, c) \in I^{3}$ tels que $a<c<b$.
Alors
        \[ 
        \frac{f(c)-f(a)}{c-a} \leqslant \frac{f(b)-f(a)}{b-a} \leqslant \frac{f(b)-f(c)}{b-c} .
        \]
\end{theoreme}

\preuve{ %[5]{
Soit $(a, b, c) \in I^{2}$ tel que $a<c<b$. On pose $t=\frac{c-a}{b-a} \in \interoo{0 1}$; ainsi, $c=(1-t) a+t b$. Puisque $f$ est 
convexe, on a $f(c)=f((1-t) a+t b) \leq (1-t) f(a)+t f(b)$. Or $c-a>0$, donc
\[ 
\frac{f(c)-f(a)}{c-a} \leqslant \frac{(1-t) f(a)+t f(b)-f(a)}{c-a}=\frac{t(f(b)-f(a))}{c-a}=\frac{f(b)-f(a)}{b-a}
\]
D'où la première inégalité. La deuxième se montre de la même manière, avec $t=\frac{b-c}{b-a}$.
}

\begin{remarque}
        Graphiquement, en notant $A$ le point de coordonnées $(a, f(a))$, $B$ de coordonnées $(b,f(b))$ et $C$ de coordonnées $(c, 
f(c))$ alors
\begin{center}
 pente de $(AC)$ $\leq$ pente de $(AB)$ $\leq$ pente de $(BC)$
\end{center}
\end{remarque}

On en déduit un résultat qui nous servira dans la section suivante :

\begin{proposition}[Croissance des pentes]
        Soit $f:I\to \R$ une fonction. $f$ est convexe si et seulement si pour tout $x_0\in I$, la fonction $\ds{x\donne 
\frac{f(x)-f(x_0)}{x-x_0}}$ est croissante sur $I\setminus \{x_0\}$.
\end{proposition}

\preuve{ %[10]{
\begin{itemize}
\item Si $f$ est convexe sur $I$, alors la croissance de $T_{x_{0}}: x \in I \backslash\left\{x_{0}\right\} \longmapsto 
\frac{f(x)-f\left(x_{0}\right)}{x-x_{0}}$ est une conséquence directe de l'inégalité des pentes.
\item Réciproquement supposons que, pour tout $x_{0} \in I$, la fonction $T_{x_{0}}$ est croissante sur $I 
\backslash\left\{x_{0}\right\}$. Soient $t \in[ 0,1]$ et $(x, y) \in I^{2}$ tel que $x<y$ (le cas $x=y$ est immédiat et le cas $x>y$ est
analogue).
\begin{itemize}
\item Si $t=0$ ou $t=1$, l'inégalité de convexité est immédiate.
\item Si $t \in \interoo{0 1}$, alors $x<(1-t) x+t y<y$ et donc
\[ 
\frac{f((1-t) x+t y)-f(x)}{t(y-x)}=\frac{f((1-t) x+t y)-f(x)}{(1-t) x+t y-x}=T_{x}((1-t) x+t y) \leqslant T_{x}(y)=\frac{f(y)-f(x)}{y-x}
\]
Puisque $t>0$ et $y-x>0$, nous obtenons $f((1-t) x+t y)-f(x) \leqslant t(f(y)-f(x))$. D'où l'inégalité de convexité.
\end{itemize}
Ainsi $f$ est convexe sur $I$.
\end{itemize}
}

\section{Cas des fonctions dérivables}

\subsection{Convexité et dérivée}

\begin{theoreme}
  Soit $I$ un intervalle ouvert, et $f:I\rightarrow \R$ une fonction dérivable sur $I$.\\
Alors $f$ est convexe (respectivement concave) sur $I$ si, et seulement si, $f'$ est croissante (resp. décroissante).
\end{theoreme}


\preuve{ %[15]{
\begin{itemize}
\item Supposons $f$ convexe. Soient $(x,y) \in I^2$ tels que $x< y$. Pour tout $z\in \interoo{x y}$, d'après l'inégalité des trois 
pentes :
        \[  \frac{f(z)-f(x)}{z-x} \leq \frac{f(y)-f(x)}{y-x}\leq \frac{f(y)-f(z)}{y-z}. \]
        En faisant tendre $z$ vers $x$ dans la première partie d'inégalité, on obtient
        \[  f'(x) \leq \frac{f(y)-f(x)}{y-x}. \]
        En faisant tendre $z$  vers $y$ dans la deuxième partie d'inégalité, on obtient
        \[  \frac{f(y)-f(x)}{y-x} \leq f'(y). \]
        Ainsi, $f'(x)\leq f'(y)$ : $f'$ est bien croissante.
\item Réciproquement, supposons $f'$ croissante. Soient $(a,b)\in I$ tels que $a<b$ et $t\in \interff{0 1}$. Si $t=0$ ou $t=1$, le 
résultat est immédiat. Supposons $t\in \interoo{0 1}$. Soit $z=(1-t)a+tb \in \interoo{a b}$. On applique l'inégalité des accroissements 
finis sur $\interoo{a z}$ et sur $\interoo{z b}$ : il existe $x\in \interoo{a z}$ et $y\in \interoo{z b}$ tels que
        \[  \frac{f(z)-f(a)}{z-a} = f'(x) \qeq \frac{f(b)-f(z)}{b-z} = f'(y). \]
        Puisque $x<y$, par croissance de $f'$, on a
        \[  \frac{f(z)-f(a)}{z-a} \leq \frac{f(b)-f(z)}{b-z}\]
        soit, en revenant à la définition de $z=(1-t)a+tb$ :
        \begin{align*}
                 \frac{f(z)-f(a)}{t(b-a)} \leq \frac{f(b)-f(z)}{(1-t)(b-a)} &\implies  (1-t)(f(z)-f(a)) \leq t(f(b)-f(z)) \\
                 &\implies f(z) \leq (1-t)f(a) + t f(b) \\
                 &\implies f((1-t)a+tb) \leq (1-t)f(a)+tf(b).
        \end{align*}
        Ainsi, $f$ est convexe.
\end{itemize}
}

\begin{exemple}
\begin{itemize}
\item La fonction $\exp$ est convexe sur $\R$. En effet, $\exp'=\exp$ qui est bien une fonction croissante.
\item La fonction $\ln$ est concave sur $\R^+_*$. En effet, pour tout $x>0$, $\ln'(x)=\frac{1}{x}$ et la fonction inverse est 
décroissante sur $\R^*_+$.
\end{itemize}
\end{exemple}

\subsection{Inégalité de convexité}

\begin{theoreme}
  Une fonction dérivable est convexe si et seulement si elle est au dessus de chacune de ses tangentes. Elle est concave si et seulement
si elle est en dessous de chacune ses tangentes.
\end{theoreme}

\preuve{ %[10]{
\begin{itemize}
\item Supposons $f$ convexe. D'après l'inégalité des trois pentes appliquées à  $x<z<x_0$ :
        \[  \frac{f(z)-f(x)}{z-x} \leq \frac{f(x_0)-f(x)}{x_0-x} \leq \frac{f(x_0)-f(z)}{x_0-z}. \]
        En faisant tendre $z$ vers $x_0$ dans la partie droite :
        \[  \frac{f(x_0)-f(x)}{x_0-x} \leq f'(x_0) \implies f(x) \geq f'(x_0)(x-x_0)+f(x_0). \]
        Ainsi, la courbe de $f$ est au dessus de sa tangente en $x_0$ à gauche. Le même raisonnement avec $x_0 < z < x$ montre que la 
courbe est au-dessus de sa tangente en $x_0$ à droite.
\item Supposons que la courbe de $f$ est au-dessus de ses tangentes. Soient $a$ et $b$ deux éléments de $I$ tels que $a<b$. Alors,
        \[  f(b) \geq f'(a)(b-a)+f(a) \qeq f(a) \geq f'(b)(a-b)+f(b) \]
        soit
        \[     f'(a) \leq   \frac{f(b)-f(a)}{b-a} \leq f'(b). \]
        Ainsi, $f'$ est croissante, et donc $f$ est convexe.
\end{itemize}
}

\begin{center}
    \includegraphics[ width=10cm]{convexite2}
\end{center}

\begin{application}
  Comme la fonction $\exp$ est convexe, la courbe de la fonction $\exp$ est toujours au dessus de ses tangentes. En particulier, elle 
est au-dessus de la tangente en $0$, d'équation $y=x+1$. Ainsi, \[ \forall x,~\mathrm{e}^x \geq 1+x\]
De même, la fonction $f:x\mapsto \ln(x+1)$ est concave sur $]-1;+\infty[ $, donc la courbe de $f$ est toujours en dessous de ses 
tangentes, et en particulier sa tangente en $0$, d'équation $y=x$. Ainsi,
\[ \forall x\in ]-1;+\infty[ ,~\ln(x+1)\leq x\]
\end{application}

\begin{exo}
Montrer que, pour tout $x\in \interff{0 \pi}$, $\sin(x)\leq x$.
\end{exo}

\solution[5]{
Remarquons que, $\sin$ est dérivable et $\sin'=\cos$. Ainsi, $\sin'$ est décroissante sur $\interff{0 \pi}$ : $\sin$ est concave sur 
$\interff{0 \pi}$. Déterminons l'équation de la tangente au point d'abscisse $0$ :
\[  y = \sin'(0)(x-0)+\sin(0) = x. \]
Ainsi, par concavité : \[  \forall x\in \interff{0 \pi},\quad \sin(x)\leq x. \]
}


\subsection{Convexité et signe de $f''$}

\begin{theoreme}
  Soit $f$ une fonction deux fois dérivable sur un intervalle ouvert $I$. Alors $f$ est convexe (respectivement concave) si et seulement
si $f''$ est positive (resp. négative).
\end{theoreme}

\begin{remarque}
        Le programme officiel ne précise ce théorème que sur des fonctions de classe $\CC^2$, mais il est vrai sans que la fonction soit
$\CC^2$, mais simplement $\mathcal{D}^2$.
\end{remarque}

\begin{demonstration}
Supposons la fonction $f$ deux fois dérivable sur $I$ et convexe. D'après le théorème précédent, la fonction $f'$ est donc croissante. 
Puisque $f'$ est elle-même dérivable, $f'$ est croissante si et seulement si $f''$ est positive.
\end{demonstration}

\begin{exemple}
  Soit $f$ la fonction définie sur $\R^*_+$ par $f(x)=x^2-\ln(x)$. Alors, $f$ est deux fois dérivables sur $\R^*_+$, et on a
\[ f''(x)=2+\frac{1}{x^2}\]
La dérivée seconde étant positive sur $\R^*_+$, la fonction $f$ est convexe sur $\R^*_+$.
\end{exemple}

\begin{methode}
  Pour montrer qu'une fonction est convexe, ou concave, tout dépend de sa régularité :
\begin{itemize}
\item si elle est de classe $\CC^2$ (ou, au moins, deux fois dérivable), on calcule sa dérivée seconde, et on s'intéresse à son signe.
\item si elle n'est pas deux fois dérivable, mais au moins dérivable, on la dérive et on vérifie le sens de variation de sa dérivée.
\item si elle n'est pas dérivable, on part sur la définition de base, ou on se ramène à des fonctions connues.
\end{itemize}
\end{methode}

\subsection{Point d'inflexion}

\begin{definition}
  Un \textbf{point d'inflexion} de la courbe $\CC$ est un point où la courbe $\CC$ traverse sa tangente en ce point. Lorsque sa courbe 
franchit un point d'inflexion, la convexité change de sens.
\end{definition}


\begin{exemple}
  Soit $f:x\mapsto x^3$. Alors la tangente au point d'abscisse $0$ coupe la courbe donc $0$ est un point d'inflexion.
\begin{center}
    \includegraphics[ width=8cm]{cube}
\end{center}
\end{exemple}

\begin{theoreme}
  Soit $f$ une fonction de classe $\CC^2$ sur $I$. Si la dérivée seconde de $f$ s'annule en changeant de signe en $x_0$, alors le point 
de $\CC$ d'abscisse $x_0$ est un point d'inflexion.
\end{theoreme}


\begin{attention}
 Le point d'inflexion peut exister sans que la fonction soit de classe $\CC^2$ : c'est donc une condition suffisante, mais pas 
nécessaire.
\end{attention}


\begin{methode}
  Pour déterminer l'existence potentielle d'un point d'inflexion, si la fonction est de classe $\CC^2$ :
\begin{itemize}
\item on détermine la dérivée seconde de la fonction,
\item on dresse le tableau de signe de la dérivée seconde
\item on conclut, en cherchant les réels pour lesquels la dérivée seconde s'annule en changeant de signe.
\end{itemize}
\end{methode}

\begin{exemple}
  Soit $f$ la fonction définie sur $\R$ par $f(x)=x^4-6x^2$. Déterminer les éventuels points d'inflexion de $f$.
\end{exemple}

\solution[6]{
$f$ est de classe $\CC^2$ sur $\R$ en tant que polynôme, et on a \[ f''(x)=12x^2-12=12(x-1)(x+1)\]
En dressant le tableau de signe de $f''$, on constate que la dérivée seconde s'annule en changeant de signe en $-1$ et en $1$. Ainsi, la
courbe de $f$ admet deux points d'inflexion.
}

%%% Fin du cours %%%
