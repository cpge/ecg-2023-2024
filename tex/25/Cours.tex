\chapter{Sommes d'espaces vectoriels}
%!TeX root=../../encours.nouveau.tex

\objectifintro{
\lettrine[lines=2, lhang=0.33, loversize=0.25]{A}{près} avoir étudié les espaces vectoriels, nous allons nous intéresser à un cas particulier de sous-espace vectoriel : la 
somme de deux (ou plusieurs) sous-espaces vectoriels. Nous étudierons un cas particulier, le cas où la somme est directe, et nous 
verrons le lien entre les bases des sous-espaces vectoriels et une base de la somme directe.
}

%%%%%%%%%%%%%%%%%%%
%%%% Extrait.  %%%%
%%%%%%%%%%%%%%%%%%%
\begin{extrait}{Victor Hugo (1802 -- 1885). \emph{Choses vues}}
Être, c'est être la somme de tout ce qu'on a été. L'homme ne comprend et n'accepte l'immortalité qu'à condition de se souvenir. Être, 
pour la créature intelligente, c'est comparer perpétuellement ce qu'on a été avec ce qu'on est.
\end{extrait}

\begin{objectifs}
\begin{numerote}
\item \lienobj{1}{Savoir déterminer la somme de deux (ou plusieurs) sous-espaces vectoriels}
\item \lienobj{2}{Savoir démontrer qu'une somme est directe}
\item \lienobj{3}{Savoir démontrer que deux sous-espaces vectoriels sont supplémentaires}
\item \lienobj{4}{Déterminer une base (ou une famille génératrice) d'une somme connaissant une base (ou une famille génératrice) des 
s.e.v.}
\item \lienobj{5}{Savoir déterminer l'expression d'une projection sur $F$ parallèlement à $G$}
\item \lienobj{6}{Connaissant un projecteur, savoir déterminer sur quel s.e.v et parallèlement à quel s.e.v.}
\end{numerote}
\end{objectifs}


%%%%%%%%%%%%%%%%%%%
%%% Début du cours %%%
%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Somme de sous-espaces vectoriels}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Définition}

\begin{definition}[Somme de deux sous-espaces vectoriels]
        Soient $E$ un $\R$-espace vectoriel, et $F$ et $G$ deux sous-espaces vectoriels de $E$. On appelle \textbf{somme} de $F$ et $G$,
et on note $F+G$ l'ensemble
\[  F+G = \left \{ u+v,\quad u\in F,\quad v\in G \right \} \]
Ainsi, un élément de $F+G$ est la somme d'un élément de $F$ et d'un élément de $G$.
\end{definition}

\begin{exemple}
On a par exemple $\R u_1+\R u_2 = \Vect(u_1,u_2)$, en notant $\R u_1$ la droite vectorielle engendrée par $u_1$.
\end{exemple}

\begin{proposition}
Soient $E$ un $\R$-espace vectoriel, et $F$ et $G$ deux sous-espaces vectoriels de $E$. Alors, $F+G$ est un sous-espace vectoriel de 
$E$, contenant $F$ et $G$.
\end{proposition}


\preuve[10]{
Tout d'abord, puisque $F$ et $G$ sont des sous-espaces vectoriels de $E$, $0_E$ appartient à $F$ et $G$. Mais alors $0_E+0_E \in F+G$, 
et donc $0_E\in F+G$.\\
Soient ensuite $u$ et $v$ deux éléments de $F+G$, et $\lambda \in \R$. On écrit $u=u_F+u_G$ et $v=v_F+v_G$, avec $(u_F, v_F)\in F^2$ et 
$(u_G, v_G) \in G^2$. Mais alors, puisque $F$ et $G$ sont des sous-espaces vectoriels de $E$ : \[  \lambda u+v = \lambda 
(u_F+u_G)+(v_F+v_G)= \underbrace{(\lambda u_F+v_F)}_{\in F} + \underbrace{(\lambda u_G+v_G)}_{\in G} \in F+G \]
Ainsi, $\lambda u + v \in F+G$ : $F+G$ est bien un sous-espace  vectoriel de $E$.

Enfin, pour tout $u\in F$ et $v\in G$, on constate par définition que $u=u+0_G\in F+G$ et $v=0_F+v \in F+G$. Ainsi, $F\subset F+G$ et 
$G\subset F+G$.
}

On peut généraliser à la somme de plusieurs sous-espaces vectoriels :

\begin{definition}[Somme de $n$ sous-espaces vectoriels]
Soit $E$ un $\R$-espace vectoriel, et $F_1, \hdots, F_n$ des sous-espaces vectoriels de $E$. On appelle \textbf{somme} des $F_i$, 
l'ensemble
\begin{align*}
        \sum_{i=1}^n F_i = F_1+\hdots +F_n &= \{ x_1+\hdots + x_n,\quad x_1\in F_1,\,\hdots,\,x_n\in F_n \} \\
        &= \{ x\in E,\quad \exists\,(x_1,\hdots,x_n)\in F_1\times \hdots \times F_n,\, x=x_1+\hdots+x_n\}.
\end{align*}
C'est un sous-espace vectoriel de $E$, contenant les $F_i$.
\end{definition}

\begin{methode}
        Pour montrer que $x\in F+G$, on raisonne en général par analyse et synthèse : on suppose que $x=f+g$ avec $f\in F$ et $g\in G$ 
et on cherche $f, g$ en fonction de $x$.
\end{methode}

\begin{exemple}\logorefaire
\label{exe_prem_somme}
On note $F = \{ (x,y,z)\in \R^3,\, y=z=0 \}$ et $G=\{ (x,y,z)\in \R^3, \, x=z=0\}$. Montrer que $(2,3,0) \in F+G$.
\end{exemple}

\solution[5]{
On note $e=(2,3,0)$. On cherche $(f,g)\in F\times G$ tel que $x=f+g$. $f \in F$ donc $f=(x,0,0)$ pour $x\in \R$. De même, $g\in G$ donc 
$g=(0,y,0)$ pour $y\in \R$. Mais alors
\[  e = f+g \iff (2,3,0)=(x,y,0) \iff x=2\text{ et } y =3. \]
Ainsi, $e=\underbrace{(2,0,0)}_{\in F} + \underbrace{(0,3,0)}_{\in G} \in F+G$.
}

\begin{exo}
Soient $(a,b)$ et $(c,d)$ deux vecteurs non colinéaires de $\R^2$. Montrer que $\Vect( (a,b)) + \Vect((c,d)) = \R^2$.
\end{exo}

\solution[10]{
On raisonne de même. Soit $(x,y)\in \R^2$. On cherche $f\in \Vect((a,b))$ et $g\in \Vect((c,d))$ tel que $(x,y)=f+g$.

$f\in \Vect((a,b))$ donc il existe $\lambda$ tel que $f=\lambda(a,b)=(\lambda a,\lambda b)$; de même, il existe $\mu$ tel que 
$g=\mu(c,d) =(\mu c, \mu d)$. Ainsi :
\begin{align*}
        (x,y)=f+g &\iff (x,y) = (\lambda a+\mu c, \lambda b + \mu d)\\
        &\iff \systeme[\lambda\mu]{a\lambda + c\mu=x,b\lambda+d\mu=y}
\end{align*}

Deux méthodes pour conclure :
\begin{itemize}
	\item On écrit matriciellement : 
\[ \systeme[\lambda\mu]{a\lambda + c\mu=x,b\lambda+d\mu=y}\iff \matrice{a&c\\b&d}\matrice{\lambda\\\mu} = \matrice{x\\y}.\]
La matrice $A=\matrice{a&c\\b&d}$ est inversible, puisque $\det(A)=ad-bc\neq 0$, les vecteurs $(a,b)$ et $(c,d)$ n'étant pas colinéaires. Le système admet donc (au moins) une solution 
\[ \matrice{\lambda\\mu} = A^{-1}\matrice{x\\y} \iff \systeme*[\lambda\mu]{\lambda=\dfrac{dx-cy}{ad-bc},\mu=\dfrac{ay-bx}{ad-bc}}. \]
	\item Ou alors on résout le système, mais en raisonnant par implication (et on vérifie à la synthèse que le résultat obtenu convient). Dans ce cas, on peut multiplier par n'importe quel réel. Par la méthode du pivot :
\begin{align*}
	\systeme[\lambda\mu]{a\lambda + c\mu=x,b\lambda+d\mu=y} &\implies 
	\systeme[\lambda\mu]{a\lambda + c\mu=x,(ad\-bc)\mu=ay-bx}\\
	&\implies \systeme*[\lambda\mu]{\lambda=\dfrac{dx-cy}{ad-bc},\mu=\dfrac{ay-bx}{ad-bc}}.
\end{align*}
On remarque que $\lambda$ et $\mu$ ont un sens puisque $ad-bc\neq 0$ : les deux vecteurs $(a,b)$ et $(c,d)$ ne sont pas colinéaires.

\end{itemize}
On passe à la synthèse : on constate que
\[  (x,y) = \dfrac{dx-cy}{ad-bc}(a,b) + \dfrac{ay-bx}{ad-bc}(c,d) \in \Vect((a,b))+\Vect((c,d)).\]
Ainsi, $\Vect((a,b))+\Vect((c,d))=\R^2$.
}

\begin{remarque}
        En général, pour montrer que $E=F+G$, on procède par double inclusion.
\end{remarque}

\begin{exemple}\logorefaire
        En reprenant les espaces de l'exemple \thechapter.\ref{exe_prem_somme}, montrer que $F+G = \{ (x,y,z)\in \R^3, \, z=0\}$.
\end{exemple}

\solution[7]{
Raisonnons par double inclusion et notons $H= \{ (x,y,z)\in \R^3, \, z=0\}$.
\begin{itemize}
\item Si $u\in F+G$, il existe $f\in F$ et $g\in G$ tel que $u=f+g$. Puisque $f\in F$, $f$ s'écrit $(x,0,0)$, et puisque $g\in G$, $g$ 
s'écrit $(0,y,0)$. Mais alors
        \[  u=(x,0,0)+(0,y,0)=(x,y,0) \in H. \]
        Ainsi, $F+G\subset H$.
\item Réciproquement, soit $u\in H$. Il s'écrit $u=(x,y,0)$ avec $(x,y)\in \R^2$. Mais alors
        \[  u = \underbrace{(x,0,0)}_{\in F} + \underbrace{(0,y,0)}_{\in G} \in F+G. \]
        Ainsi, $H \subset F+G$.
\end{itemize}
}

\subsection{Somme directe}

        De manière générale, si $u\in F+G$, $u$ peut avoir plusieurs écritures sous la forme $f+g$, avec $f\in F$ et $g\in G$ : il n'y a
pas forcément unicité.

\begin{definition}
        Soient $E$ un $\R$-espace vectoriel, et $F$ et $G$ deux sous-espaces vectoriels de $E$.

On dit que la somme $F+G$ est \textbf{directe} si pour tout $u \in F+G$, il existe un unique couple $(f,g)\in F\times G$ tel que 
$u=f+g$. Dans ce cas, on notera la somme $F \oplus G$ au lieu de $F+G$.
\end{definition}

\begin{remarque}
        On peut généraliser à la somme de $n$ sous-espaces vectoriels : la somme $F_1+\hdots + F_n$ est \textbf{directe} si, pour tout 
$x\in F_1+\hdots +F_n$, il existe un unique $n$-uplet $(x_1,\hdots,x_n)\in F_1\times \hdots \times F_n$ tel que $x=x_1+\hdots+x_n$.

        On note alors $F_1\oplus \hdots \oplus F_n$ ou bien $\bigoplus\limits_{i=1}^n F_i$.
\end{remarque}

\begin{proposition}
        Soient $(F_1,\hdots, F_n)$ $n$ sous-espaces vectoriels de $E$. La somme $F_1+\hdots +F_n$ est directe si et seulement si
        \[  \forall (x_1,\hdots,x_n)\in F_1\times \hdots \times F_n,\quad \left(x_1+\hdots+x_n=0\implies x_1=\hdots = x_n=0\right). \]
\end{proposition}

\preuve{ %[10]{
On procède par double implication.
\begin{itemize}
\item Supposons $F_1+\hdots+F_n$ directe. Soient $(x_1,\hdots, x_n)\in F_1\times \hdots \times F_n$ tels que $x_1+\hdots +x_n=0$. 
Remarquons que \[  \underbrace{0}_{\in F_1}+\hdots +\underbrace{0}_{\in F_n} = 0. \]
        Par unicité de la décomposition (car la somme est directe), on en déduit que $x_1=0, \hdots, x_n=0$.
\item Réciproquement, supposons que si $x_1+\hdots + x_n=0$ avec $(x_1,\hdots, x_n)\in F_1\times \hdots \times F_n$ alors $x_1=\hdots 
=x_n=0$.

        Soit $x\in F_1+\hdots + F_n$. On prend deux décompositions de $x$ : $x=x_1+\hdots +x_n = y_1+\hdots +y_n$ avec $(x_1,\hdots, 
x_n)\in F_1\times \hdots \times F_n$ et $(y_1,\hdots, y_n)\in F_1\times \hdots \times F_n$. Par soustraction :
        \[  0 = x_1+\hdots + x_n) - (y_1+\hdots +y_n) = \underbrace{(x_1-y_1)}_{\in F_1}+\hdots + \underbrace{(x_n-y_n)}_{\in F_n}. \]
        D'après la propriété supposée, on dispose d'une décomposition de $0$, qui est donc nulle :
        \[  x_1-y_1=\hdots = x_n-y_n = 0 \implies x_1=y_1,\hdots , x_n=y_n. \]
        Ainsi, la somme $F_1+\hdots +F_n$ est directe.
\end{itemize}
}

Dans le cas de la somme de deux sous-espaces vectoriels, on dispose d'un résultat plus simple :

\begin{proposition}
Soient $E$ un $\R$-espace vectoriel, et $F$ et $G$ deux sous-espaces vectoriels de $E$.
 $F+G$ est directe si et seulement si $F\cap G=\{0\}$.
\end{proposition}

\preuve{ %[10]{
Supposons $F\cap G=\{0\}$. Soit $u$ un élément de $F+G$, et deux décompositions $u=f_1+g_1=f_2+g_2$, avec $(f_1,f_2)\in F^2$ et 
$(g_1,g_2)\in G^2$. Mais alors
\[  u - u = (f_1+g_1)-(f_2+g_2) = (f_1-f_2)+(g_1-g_2) = 0 \]
et donc $f_1-f_2=g_2-g_1$. Or, $f_1-f_2\in F$ et $g_2-g_1\in G$. Puisqu'ils sont égaux, on en déduit que $f_1-f_2\in F\cap G$ et 
$g_2-g_1\in F\cap G$, c'est-à-dire $f_1-f_2=0$ et $g_2-g_1=0$ : ainsi, $f_1=f_2$ et $g_1=g_2$ et la décomposition est unique.\\
Réciproquement, supposons la somme directe. Soit $w\in F\cap G$. Remarquons qu'on peut écrire $w = w+0 = 0+w$ puisque $w\in F$ et $w\in 
G$. Par unicité de la décomposition, on en déduit que $w=0$ : ainsi, $F+G=\{0\}$.
}


\begin{methode}
Pour montrer qu'une somme $F+G$ est directe, on montre que $F\cap G=\{0\}$.

Pour montrer qu'une somme $F_1+\hdots+F_n$ est directe, on prend une décomposition de $0$, sous la forme $x_1+\hdots+x_n=0$, avec $x_1\in F_1,\hdots,x_n\in F_n$, et on 
démontre que $x_1=\hdots=x_n=0$.
\end{methode}

\begin{exemple}\logorefaire
\label{exe_trois_poly}
        Soient $F$, $G$ et $H$ trois sous-ensembles de $\R[ -3]$ définis par
        \[  F = \left\{P \in \R[ -3],\, P(X) = P(-X)\right\},\quad G = \left\{P \in \R[ -3],\, P(0) = P(1) = P(2) = 0\right\}\]
        \[  \qeq H=\left \{P \in \R[ -3],\, P(1)=P(2)=P(3)=0\right\}. \]
        Montrer que $F, G$ et $H$ sont des sous-espaces vectoriels de $\R[ -3]$, et que la somme $F+G+H$ est directe.
\end{exemple}

\solution[20]{
Tout d'abord, $F, G$ et $H$ sont bien des sous-espaces vectoriels de $\R[ -3]$. Ils sont bien inclus dans $\R[ -3]$ et si $(P, Q)\in 
F^2$ et $\lambda \in \R$, alors,
\[  (\lambda P+Q)(X) = \lambda P(X) + Q(X) = \lambda \underbrace{P(-X)}_{\text{car }P\in F}+\underbrace{Q(-X)}_{\text{car }Q\in 
F}=(\lambda P+Q)(-X) \]
Donc $\lambda P+Q \in F$ et $F$ est un s.e.v. de $\R[ -3]$ (le même raisonnement permet de justifier que $G$ et $H$ le sont également).

Montrons que la somme $F+G+H$ est directe. Soient $(P, Q,R)\in F\times G \times H$ tels que $P+Q+R=0$. Montrons que $P=Q=R=0$.  On peut 
écrire $P = -Q-R$. Puisque $Q\in G$, $Q$ admet $1$ et $2$ comme racines. De même, $R\in H$ admet également $1$ et $2$ comme racines.

En appliquant en $1$ et en $2$, on en déduit que
\[  P(1)=-Q(1)-R(1)=0 \qeq P(2)=-Q(2)-R(2)=0. \]
Mais $P(X)=P(-X)$ donc $P(-1)=P(1)=0$ et $P(-2)=P(2)=0$. Ainsi, $P$ est de degré $3$ et admet au moins $4$ racines distinctes : il est 
nul, et $P=0$. Il nous reste $Q+R=0$. $Q$ s'écrit $\lambda X (X-1)(X-2)$ et $Q$ s'écrit $\mu (X-1)(X-2)(X-3)$ (car on connait ses racines 
et son degré max). Ainsi
\[  \lambda X(X-1)(X-2) + \mu (X-1)(X-2)(X-3)=0. \]
En appliquant en $0$, on obtient $\mu=0$ et donc $R=0$ et en appliquant en $3$, on obtient $\lambda =0$ soit $Q=0$.

Ainsi, la somme est directe.
}

\begin{attention}
Si $F_1+\hdots+F_n$ est directe, alors pour tout $(i, j)\in \interent{1 n}^2$, avec $i\neq j$, $F_i\cap F_j =\{0\}$. Mais la réciproque 
n'est valable que pour la somme de deux espaces vectoriels, pas plus.

Par exemple, si $F=\Vect((1,0))$, $G=\Vect((0,1))$ et $H=\Vect((1,1))$, on vérifie rapidement que $F\cap G=F\cap H = G\cap H = \{0\}$. 
Cependant, la somme $F+G+H$ n'est pas directe puisque
\[  (1,1) = \underbrace{(0,0)}_{\in F}+\underbrace{(0,0)}_{\in G}+\underbrace{(1,1)}_{\in H} = \underbrace{(1,0)}_{\in F} + 
\underbrace{(0,1)}_{\in G} + \underbrace{(0,0)}_{\in H} \]
et la décomposition n'est donc pas unique.
\end{attention}

\subsection{Théorème de concaténation des bases}

Si on connait une base de $F$ et une base de $G$, et si la somme $F+G$ est directe, on en déduit une base de $F+G$ en \guill{réunissant}
les bases respectives de $F$ et $G$ : c'est ce qu'on appelle la concaténation.

\begin{definition}[Concaténation]
        Soient $\mathcal{F}=(x_1,\hdots x_n)$ et $\mathcal{G}=(y_1,\hdots, y_p)$ deux familles d'un espace vectoriel $E$. On appelle 
\textbf{concaténation} des familles $\mathcal{F}$ et $\mathcal{G}$, la famille $(x_1,\hdots, x_n, y_1,\hdots, y_p)$, que l'on note 
$(\mathcal{F}, \mathcal{G})$.
\end{definition}

Commençons par un premier résultat :

\begin{proposition}[Famille génératrice d'une somme]
        Soient $F$ et $G$ deux sous-espaces vectoriels de $E$. On suppose que $F$ et $G$ admettent des familles génératrices, 
respectivement notées $\mathcal{F}$ et $\mathcal{G}$. Alors la famille $(\mathcal{F}, \mathcal{G})$ engendre $F+G$.
\end{proposition}

\preuve{ %[10]{
Notons $\mathcal{F}=(x_1,\hdots, x_n)$ et $\mathcal{G}=(y_1,\hdots, y_p)$. Soit $u\in F+G$. Il existe $(f,g)\in F\times G$ tels que 
$u=f+g$. Puisque $\mathcal{F}$ et $\mathcal{G}$ sont génératrices respectivement de $F$ et $G$, on peut écrire
\[  f=\sum_{i=1}^n \lambda_i x_i \qeq g=\sum_{j=1}^p \mu_j y_j \]
où $(\lambda_1,\hdots, \lambda_n,\mu_1,\hdots, \mu_p)$ sont des réels. Mais alors
\[  u = f+g = \sum_{i=1}^n \lambda_ix_i + \sum_{j=1}^p \mu_jy_j \in \Vect( (x_1,\hdots,x_n,y_1,\hdots,y_p)). \]
Ainsi, $(\mathcal{F}, \mathcal{G})$ est génératrice de $F+G$.
}

\begin{exemple}
Soit $(x_1,\hdots, x_p)$ est une famille de vecteurs de $E$, alors pour tout $k\in \interent{1 p}$ \[  \Vect(x_1,\hdots, x_p) = 
\Vect(x_1,\hdots, x_k)+ \Vect(x_{k+1},\hdots, x_p). \]
Ainsi, \[  \Vect(x_1,\hdots, x_p) = \Vect(x_1)+\hdots + \Vect(x_p). \]
\end{exemple}

On en déduit alors le théorème important :

\begin{theoreme}[Concaténation de bases]
        Soient $E$ un espace vectoriel, $F$ et $G$ deux sous-espaces vectoriels de $E$, admettant des bases, notées respectivement 
$\mathcal{F}$ et $\mathcal{G}$.

        $F$ et $G$ sont en somme directe si et seulement si la concaténation des bases $\mathcal{F}$ et $\mathcal{G}$ est une base de 
$F+G$.
\end{theoreme}

\begin{remarque}
        D'après la proposition précédente, il faut retenir que la somme est directe si et seulement la concaténation des deux bases est 
libre.
\end{remarque}

\preuve{ %[15]{
Raisonnons par double implication. On note $\mathcal{F}=(x_1,\hdots,x_n)$ et $\mathcal{G}=(y_1,\hdots, y_p)$.
\begin{itemize}
\item Supponsons la somme directe. On note $\mathcal{B}=(\mathcal{F}, \mathcal{G})$. Montrons que cette famille est libre (elle sera 
donc une base de $F+G$, puisqu'elle est génératrice). Soient $(\lambda_1,\hdots, \lambda_n, \mu_1,\hdots, \mu_p)$ des réels tels que \[ 
\sum_{i=1}^n \lambda_ix_i + \sum_{j=1}^p \mu_i y_i = 0. \]
        On constate que $\sum\limits_{i=1}^n \lambda_ix_i\in F$ et $\sum\limits_{j=1}^p \mu_iy_i\in G$. La somme étant directe, on en 
déduit que
        \[  \sum_{i=1}^n \lambda_ix_i = \sum_{j=1}^p \mu_i y_i = 0. \]
        Les familles $\mathcal{F}$ et $\mathcal{G}$ formant des bases, elles sont libres. Ainsi, la relation précédente implique que 
$\lambda_1=\hdots=\lambda_n=0$ et $\mu_1=\hdots+\mu_p=0$ : la famille $(\mathcal{F}, \mathcal{G})$ est libre, et est donc une base de 
$F+G$.
\item Réciproquement, on suppose que $(\mathcal{F},\mathcal{G})$ forme une base de $F+G$. Montrons que la somme est directe. Soit 
$(f,g)\in F\times G$ tel que $f+g=0$. Puisque $\mathcal{F}$ et $\mathcal{G}$ sont des bases respectives de $F$ et $G$, on peut écrire 
$f=\sum\limits_{i=1}^n \lambda_i x_i$ et $g=\sum\limits_{j=1}^p \mu_jy_j$. Mais alors
        \[  f+g = \sum\limits_{i=1}^n \lambda_i x_i+\sum\limits_{j=1}^p \mu_jy_j = 0 \]
        La famille $(\mathcal{F},\mathcal{G})$ étant une base de $F+G$, elle est libre : la relation précédente implique que 
$\lambda_1=\hdots=\lambda_n = \mu_1=\hdots=\mu_p=0$ : donc $f=g=0$ : la somme est directe.
\end{itemize}
}

On peut généraliser le résultat à une famille de sous-espaces vectoriels :

\begin{theoreme}[Concaténation de bases]
        Soient $E$ un espace vectoriel, $F_1,\hdots, F_n$ des sous-espaces vectoriels de $E$, admettant des bases, notées respectivement
$\mathcal{F}_1,\hdots \mathcal{F}_n$.

        La somme $F_1+\hdots +F_n$ est directe si et seulement si la concaténation des bases $\mathcal{F}_1,\hdots, \mathcal{F}_n$ est 
une base de $F_1+\hdots + F_n$.
\end{theoreme}

\begin{exemple}
Reprendre l'exemple \thechapter.\ref{exe_trois_poly} en utilisant le résultat précédent.
\end{exemple}

\solution[20]{
Déterminons tout d'abord une base de $F, G$ et $H$. Rapidement, nous avons vu que
\[  G = \Vect( X(X-1)(X-2)) \qeq H = \Vect( (X-1)(X-2)(X-3)). \]
Chaque polynôme étant non nul, $(X(X-1)(X-2))$ forme une base de $G$ et $( (X-1)(X-2)(X-3))$ une base de $H$.

Soit $P\in F$. $P$ s'écrit $aX^3+bX^2+cX+d$. Puisque $P(-X)=P(X)$, on peut écrire
\[  a(-X)^3+b(-X)^2+c(-X)+d=aX^3+bX^2+cX+d \iff 2aX^3+2cX = 0 \iff a=c=0. \]
Ainsi, $P$ s'écrit $bX^2+d$ et donc $F= \Vect(1, X^2)$. Cette famille est libre (car les polynômes sont échelonnés en degré et non nuls)
donc forme une base de $F$.

Concaténons les bases : on note $\mathcal{B}=(X(X-1)(X-2), (X-1)(X-2)(X-3), 1, X^2)$. Elle est génératrice de $F+G+H$. Montrons qu'elle 
est libre. Soient $(a,b,c,d)\in \R^4$ tels que $aX(X-1)(X-2)+b(X-1)(X-2)(X-3)+c+dX^2=0$. En évaluant en $0, 1, 2$ et $3$, on en déduit
\[  \systeme{-6b+c=0, c+d=0, c+4d=0, 6a+c+9d=0} \iff \systeme*{a=0,b=0,c=0,d=0}\]
La famille est donc libre et est une base de $F+G+H$ : cette somme est directe.
}

\afaire{Exercice \lienexo{05}.}

\section{Supplémentaire et projections}

\subsection{Supplémentaire}

\begin{definition}
        Soient $E$ un espace vectoriel, et $F, G$ deux sous-espaces vectoriels de $E$.

 On dit que $F$ et $G$ sont \textbf{supplémentaires} s'ils sont en somme directe, et que leur somme donne l'espace $E$ tout entier, 
c'est-à-dire \[  F\oplus G=E\]
\end{definition}

\begin{remarque}
        Ainsi, deux espaces $F$ et $G$ sont supplémentaires si tout élément de $E$ s'écrit de manière unique $f+g$ avec $f\in F$ et 
$g\in G$.
\end{remarque}

\begin{proposition}
        Soient $E$ un espace vectoriel, et $F, G$ deux sous-espaces vectoriels de $E$.

$F$ et $G$ sont supplémentaires dans $E$ si et seulement si $F\cap G=\{0\}$ et $F+G=E$.
\end{proposition}

\preuve{
Cela découle de la propriété vue précédemment pour la somme directe.
}

\begin{methode}
        Pour montrer que $F\oplus G=E$, on vérifie que $F\cap G=\{0\}$, et que pour tout élément $u\in E$, il existe au moins un couple 
$(f,g)\in F\times G$ tel que $u=f+g$.

        On raisonne alors par analyse et synthèse : on suppose l'existence de la décomposition, que l'on trouve (analyse). On vérifie 
ensuite qu'elle convient (synthèse)
\end{methode}

\begin{exemple}\logorefaire
        Soient $F=\left \{ \matrice{t\\0},\quad t\in \R \right \}$ et $G=\left \{ \matrice{t\\t},\quad t\in \R\right \}$. Montrer que 
$F$ et $G$ sont des sous-espaces vectoriels de $\MM_{2,1}(\R)$, puis que $F\oplus G=\MM_{2,1}(\R)$.
\end{exemple}

\solution[20]{
Tout d'abord, $F$ et $G$ sont des sous-espaces vectoriels de $\MM_{2,1}(\R)$, par exemple parce que $F=\Vect\left(\matrice{1\\0}\right)$ et $G=\Vect\left(\matrice{1\\1}\right)$.
\begin{itemize}
\item \textbf{Somme directe} : Soit $u \in F\cap G$. Il existe donc $(t,t')\in \R^2$ tels que $u=\matrice{t\\0}$ et 
$u=\matrice{t'\\t'}$. Mais alors, \[  \matrice{t\\0} = \matrice{t'\\t'} \Leftrightarrow t=t'=0 \]
Ainsi, $u=\matrice{0\\0}$ et $F\cap G = \{0\}$ : la somme est directe.
\item
\textbf{Analyse} : soit $\matrice{x\\y}\in \MM_{2,1}(\R)$. On cherche $t$ et $t'$ tels que
\[  \matrice{x\\y} = \matrice{t\\0} + \matrice{t'\\t'} \]
soit $t'=y$ et $t=x-y$.\\
\textbf{Synthèse} : soit $\matrice{x\\y} \in \MM_{2,1}(\R)$. On constate que
\[  \matrice{x\\y} = \underbrace{\matrice{x-y\\0}}_{\in F} + \underbrace{\matrice{y\\y}}_{\in G} \]
Ainsi, $\matrice{x\\y} \in F+G$.
\end{itemize}
Bilan : $F$ et $G$ sont supplémentaires dans $\MM_{2,1}(\R)$.
}

\begin{exo}\logorefaire
On note $P(\R)$ (respectivement $I(\R)$) l'ensemble des fonctions paires (respectivement impaires) de $\R$ dans $\R$. Montrer que 
$P(\R)$ et $I(\R)$ sont supplémentaires dans $\mathcal{F}(\R, \R)$.
\end{exo}

\solution[15]{
Tout d'abord, $P(\R)$ et $I(\R)$ sont des sous-espaces vectoriels de $\mathcal{F}(\R, \R)$. En effet, si $(f,g)\in P(\R)^2$, et $\lambda\in \R$, remarquons que $\lambda f+g$ est définie sur $\R$, symétrique par rapport à $0$, et 
\[ \forall x\in \R,\quad (\lambda f+g)(-x)=\lambda  f(-x)+g(-x)=\lambda f(x)+g(x)=(\lambda f+g)(x).\]
Ainsi, $\lambda f+g$ est bien paire. Le raisonnement est similaire pour $I(\R)$.
\begin{itemize}
	\item \textbf{Somme directe}: soit $f\in P(\R)\cap I(\R)$. Ainsi, pour tout réel $x$ :
\[ f(-x) = f(x) \qeq f(-x)=-f(x) \implies f(x)=-f(x) \implies f(x)=0.\]
Ainsi, $f=0$ et $P(\R)\cap I(\R) =\{ 0\}$. La somme est donc directe.
\item \textbf{Analyse}. Soit $f\in \mathcal{F}(\R,\R)$. On cherche $p\in P(\R)$ et $i\in I(\R)$ telles, pour tout réel $x$
	\[ f(x) = p(x) + i(x). \]
Appliquons en $-x$ : pour tout réel $x$
\[ f(-x) = p(-x) + i (-x) = p(x) - i(x)\]
par parité de $p$ et imparité de $i$. Mais alors
\begin{align*}
\systeme[p(x)i(x)]{p(x)+i(x)=f(x), p(x)-i(x)=f(-x)} &\implies \systeme*{p(x)=\frac{f(x)+f(-x)}{2}, i(x)=\frac{f(x)-f(-x)}{2}}.	
\end{align*}
	\item \textbf{Synthèse}. Soit $f\in \mathcal{F}(\R,\R)$. Posons, pour tout réel $x$ 
\[ p(x)=\frac{f(x)+f(-x)}{2}\qeq i(x)=\frac{f(x)-f(-x)}{2}.\]
Remarquons tout d'abord que 
\[ \forall x\in \R,\quad p(x)+i(x) = f(x).\]
Montrons que $p$ est paire. $p$ est définie sur $\R$ et pour tout réel $x$:
\[ p(-x)=\frac{f(-x)+f(-(-x))}{2}=\frac{f(x)+f(-x)}{2}=p(x).\]
Ainsi, $p$ est paire. De même, pour tout réel $x$ :
\[ i(-x) = \frac{f(-x)-f(-(-x))}{2}=\frac{f(-x)-f(x)}{2}=\frac{-(f(x)-f(-x))}{2}=-i(x).\]
Ainsi $i$ est impaire.

On a finalement montré que $f$ s'écrit $p+i$, avec $p\in P(\R)$ et $i\in I(\R)$ : $P(\R)+I(\R) = \mathcal{F}(\R,\R)$ , et la somme étant directe
\[ P(\R)\oplus I(\R) = \mathcal{F}(\R,\R)\]
\end{itemize}
% TODO: corrigé
}

\begin{remarque}
        En vertu du théorème de concaténation des bases, si $F$ et $G$ admettent des bases $\mathcal{F}$ et $\mathcal{G}$, alors $F$ et 
$G$ sont supplémentaire si et seulement si la concaténation de $\mathcal{F}$ et $\mathcal{G}$ forme une base de $E$.
\end{remarque}

\begin{attention}
Il n'y a pas unicité du supplémentaire ! Par exemple, si $F=\Vect((1,0))$, on peut remarquer que $\Vect((0,1))$ et $\Vect((1,1))$ sont 
des supplémentaires de $F$ dans $\R^2$.
\end{attention}

\afaire{Exercice \lienexo{04}.}

\subsection{Projections sur un supplémentaire}

\begin{definition}[Projection vectorielle]\label{def:proj_vect}
Soient $E$ un espace vectoriel, $F$ et $G$ deux sous-espaces vectoriels de $E$ tels que $E=F\oplus G$. Ainsi, pour tout $x\in E$, il 
existe un unique couple $(u_x, v_x)\in F\times G$ tel que $x=u_x+v_x$.
\begin{itemize}
\item L'application $p:E\to E$ définie par $p:x\mapsto u_x$ est appelée \textbf{projection vectorielle sur $F$ parallèlement à $G$}.
\item L'application $q:E\to E$ définie par $q:x\mapsto v_x$ est appelée \textbf{projection vectorielle sur $G$ parallèlement à $F$}.
\end{itemize}
\end{definition}

\afaire{Exercices \lienexo{01}, \lienexo{02} et \lienexo{03}.}

Les projections vectorielles sont les applications \guill{naturelles} qui permettent de récupérer la composante sur l'un des 
supplémentaires. Comme leur nom l'indique, ce sont des projections. On démontre tout d'abord un lemme utile.

\begin{lemme}
        On reprend les mêmes notations que la définition précédente.

Soit $p$ la projection sur $F$ parallèlement à $G$. Alors
\[  \forall x\in F, \, p(x)=x \qeq \forall y\in G,\, p(y)=0_E. \]
\end{lemme}

\preuve[5]{
Par unicité de la décomposition sur $F\oplus G$ :
\begin{itemize}
\item Si $x\in F$, alors $x=\underbrace{x}_{\in F} + \underbrace{0}_{\in G}$ et donc $p(x)=x$.\vspace*{.2cm}
\item Si $y\in G$, alors $y= \underbrace{0}_{\in F} + \underbrace{y}_{\in G}$ et donc $p(x)=0$.
\end{itemize}
}

\begin{proposition}
        On reprend les mêmes notations que la définition \ref{def:proj_vect}. Les projections $p$ sur $F$ parallèlement à $G$, et $q$ 
sur $G$ parallèlement à $F$ sont des projections. De plus :
\begin{itemize}
\item $\Ker(p)=G$ et $\Image(p)=F$,
\item $\Ker(q)=F$ et $\Image(q)=G$.
\end{itemize}
\end{proposition}

\preuve[10]{
Soit $x\in E$. On écrit $x=u_x+v_x$ avec $(u_x, v_x)\in F\times G$. On montre rapidement que $p$ est linéaire.

Par définition, $p(x)=u_x\in F$ et d'après le lemme précédent, $p(p(x))=p(u_x)=u_x=p(x)$.

Ainsi, pour tout $x\in E$, $p\circ p(x)=p(x)$ : $p$ est bien une projection, et de plus $\Image(p)\subset F$ puisque $p(x)=u_x\in F$.

Réciproquement, si $x\in F$, d'après le lemme, $p(x)=x$ et donc $x\in \Image(p)$.

Enfin, si $x\in G$, d'après le lemme, $p(x)=0$ : $x\in \Ker(p)$. Réciproquement, si $x\in \Ker(p)$, on $p(x)=0$. On écrit $x=u_x+v_x$ 
avec $(u_x,v_x)\in F\times G$. Alors $p(x)=0\implies u_x=0$ et finalement $x=v_x\in G$ : $\Ker(p)\subset G$.

Le raisonnement est identique pour $q$.
}

Ainsi, une projection vectoriel est un projecteur particulier. Mais il y a une réciproque :

\begin{theoreme}
        Soit $E$ un espace vectoriel, et $p$ un projecteur de $E$. Alors :
\begin{itemize}
\item $E=\Ker(p)\oplus \Image(p)$,
\item $p$ est la projection sur $\Image(p)$, parallèlement à $\Ker(p)$.
\end{itemize}
\end{theoreme}

\preuve[15]{
Démontrons le premier point. Soit $x\in \Ker(p)\cap \Image(p)$. Il existe $y\in E$ tel que $x=p(y)$. Mais alors $p(x)=p\circ p(y)=p(y)$ 
car c'est un projecteur. Or, $x\in \Ker(p)$, donc $p(x)=0$. Ainsi, $p(y)=0$, c'est-à-dire $x=0$ : $\Ker(p)\cap \Image(p)=\{0\}$.

Raisonnons par analyse et synthèse. Soit $x\in E$. On cherche $y\in \Ker(p)$ et $z\in \Image(p)$ tel que $x=y+z$. $z\in \Image(p)$ donc 
il existe $u\in E$ tel que $z=p(u)$. Alors, en appliquant $p$ :
\[  p(x)=p(y+z)=p(y)+p(z)=0 + p\circ p(u)=p(u) =z. \]
Ainsi, $z=p(x)$ et $y=x-p(x)$.

On passe à la synthèse. Soit $x\in E$. On note $y=x-p(x)$ et $z=p(x)$. Par définition, $z\in \Image(p)$. Enfin, 
$p(y)=p(x-p(x))=p(x)-p\circ p(x)=p(x)-p(x)=0_E$. Ainsi, $y\in \Ker(p)$.

Ainsi, $E=\Ker(p)\oplus \Image(p)$.

Pour le deuxième point, il suffit de constater que, si $x\in E$, alors \[ x=\underbrace{p(x)}_{\in \Image(p)}+\underbrace{(x-p(x))}_{\in
\Ker(p)}\] d'après la décomposition précédente, et finalement $p$ est bien la projection sur $\Image(p)$ parallèlement à $\Ker(p)$.
}

\afaire{Exercices \lienexo{10}, \lienexo{11} et \lienexo{12}.}


%%% Fin du cours %%%
