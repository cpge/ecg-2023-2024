\chapter{Couples de variables aléatoires}
%!TeX root=../../encours.nouveau.tex

\objectifintro{
\lettrine[lines=2, lhang=0.33, loversize=0.25]{N}{ous} allons nous intéresser, dans ce chapitre, au comportement de deux (ou plusieurs) variables aléatoires qui peuvent être
correlées ou non. Lorsqu'on connait leur comportement de couple, on en extraiera le comportement individuel. Enfin, nous terminerons par
des théorèmes de stabilités sur la somme de variables aléatoires suivant certaines lois.
}

%%%%%%%%%%%%%%%%%%%
%%%% Extrait.  %%%%
%%%%%%%%%%%%%%%%%%%
\begin{extrait}{Adrien Decourcelle (1821--1892).}
Le couple est une réunion de deux personnes qui font rarement la paire.
\end{extrait}

\begin{objectifs}
\begin{numerote}
\item \lienobj{01}{Savoir déterminer la loi conjointe d'un couple}
\item \lienobj{02}{Savoir utiliser la loi conjointe d'un couple pour déterminer les lois marginales}
\item \lienobj{03}{Savoir déterminer et utiliser des lois conditionnelles}
\item \lienobj{04}{Savoir utiliser ou justifier des indépendances de variables aléatoires}
\item \lienobj{05}{Savoir utiliser la formule de transfert dans le cas d'une fonction d'un couple de variables aléatoires}
\item \lienobj{06}{Savoir déterminer la loi d'une somme de variables aléatoires à l'aide du produit de convolution discret}
\item \lienobj{07}{Connaître les résultats concernant la somme de $2$ lois binomiales ou de Poisson, et leurs démonstrations}
\end{numerote}

\begin{remarque}
Les résultats concernant la covariance, le coefficient de corrélation linéaire et les suites de variables aléatoires sont officiellement
au programme de $2$\ieme année.
\end{remarque}
\end{objectifs}

%%%%%%%%%%%%%%%%%%%
%%% Début du cours %%%
%%%%%%%%%%%%%%%%%%%

\section{Couple de variables aléatoires discrètes}

Dans toute la suite, $X$ et $Y$ sont deux variables aléatoires discrètes, définies sur le même espace de probabilité $(\Omega,\AA,\PP)$ 
avec $\Omega$ fini ou dénombrable.

On note $F=(x_i)_{i \in I}$ et $G=(y_j)_{j \in J}$ les valeurs respectivement prises par $X$ et $Y$, avec $I$ et $J$ finis ou 
dénombrables. On admet que l'ensemble $F\times G$ est également fini ou dénombrable.


\subsection{Loi conjointe et lois marginales}

Considérons deux variables aléatoires $X_1$ et $X_2$ suivant la loi dite de Rademacher, c'est-à-dire que $X_1(\Omega)=\{-1,1\}$, 
$X_2(\Omega)=\{-1,1\}$ et
\[ 
\PP(X_1=1) = \PP(X_1=-1) = \frac 12 \qeq \PP(X_2=1) = \PP(X_2=-1) = \frac 12
\]
La variable aléatoire $Y_1=-X_1$ est elle aussi une variable aléatoire de Rademacher, puisque
\[ 
\PP(Y_1=1)=\PP(X_1=-1)=\frac12 = \PP(X_1=1) = \PP(Y_1=-1).
\]
Les variables aléatoires $X_1$ et $Y_1$ ont donc la même loi. Si nous définissons la variable aléatoire $Y_2$ comme $Y_2=X_2$, alors 
$X_2$ et $Y_2$ ont également la même loi. En revanche, les couples $Z_1=(X_1,Y_1)$ et $Z_2=(X_2,Y_2)$ n'ont pas la même loi. En effet, 
la variable aléatoire $Z_1$ est à valeurs dans $\{(1,-1),(-1,1)\}$ tandis que $Z_2$ est à valeurs dans $\{(1,1),(-1,-1)\}$.

On peut donc en conclure que la loi conjointe de deux variables aléatoires contient beaucoup plus d'information que les lois de chacune 
des deux variables aléatoires prises séparément.

\subsubsection{Loi conjointe}
\labelobj{01}

\begin{definition}[Loi conjointe]
La loi  du couple $Z=(X,Y)$, appelée \textbf{loi conjointe} de $X$ et $Y$, est  $\PP_Z=(p^Z_{i,j})_{(i,j) \in I\times J}$ définie par
\[ 
p^Z_{i,j} = \PP( (X=x_i)\cap(Y=Y_j)), \quad \text{pour tout $i$ dans $I$ et tout $j$ dans $J$.}
\]
\end{definition}

\begin{remarque}
        Pour plus de simplicité, on notera $ \PP(X=x_i,Y=y_j)$ à la place de $\PP( (X=x_i)\cap(Y=Y_j))$.
\end{remarque}

\begin{exemple}
On lance deux dés équilibrés, l'un rouge et l'autre bleu, et on appelle $X$ le résultat du dé rouge et $Y$ le résultat du dé bleu. 
Déterminer la loi du couple $(X,Y)$.
\end{exemple}

\solution[5]{
Nous avons
\[ 
\PP(X=i,Y=j) = \frac 1 {36}, \quad \text{pour tout $(i,j)$ dans $\interent{1 6}^2$.}
\]
}

\begin{exo}
On lance deux dés équilibrés, l'un rouge et l'autre bleu, et on appelle $X$ le plus petit résultat des deux dés et $Y$ le plus grand. 
Déterminer la loi du couple $(X,Y)$.
\end{exo}

\solution[10]{
Pour déterminer la  loi du couple $(X,Y)$ nous allons  représenter les valeurs de
$(X,Y)$ en fonction des valeurs des deux dés.
%%
\begin{center}
\begin{tabular}{|c|c|c|c|c|c|c|c|}
\cline{3-8}
\multicolumn{2}{c|}{} & \multicolumn{6}{c|}{Dé bleu} \\
\cline{3-8}
\multicolumn{2}{c|}{} & 1 & 2 & 3 & 4 & 5 & 6 \\
\hline
\multirow{6}{*}{\begin{sideways} Dé rouge~ \end{sideways}} & 1 & (1,1) & (1,2) & (1,3)
& (1,4) & (1,5) & (1,6) \\
\cline{2-8}
& 2 & (1,2) & (2,2) & (2,3) & (2,4) & (2,5) & (2,6) \\
\cline{2-8}
& 3 & (1,3) & (2,3) & (3,3) & (3,4) & (3,5) & (3,6) \\
\cline{2-8}
& 4 & (1,4) & (2,4) & (3,4) & (4,4) & (4,5) & (4,6) \\
\cline{2-8}
& 5 & (1,5) & (2,5) & (3,5) & (4,5) & (5,5) & (5,6) \\
\cline{2-8}
& 6 & (1,6) & (2,6) & (3,6) & (4,6) & (5,6) & (6,6)\\
\hline
\end{tabular}
\end{center}
On a donc
\[ 
\PP(X=i,Y=j) = \left\{
\begin{array}{ll}
\frac 1 {36} & \text{si $i=j$,} \\
0 & \text{si $i>j$,} \\
\frac{2}{36} & \text{si $i < j$.}
\end{array}
\right.
\]
}

\begin{proposition}[Existence d'un coupe de loi fixée]
        Soit $\left(p_{i, j}\right)_{(i, j) \in \N^{2}}$  une famille de réels positifs dont la série double converge, de somme $1$.

        Il existe alors un espace probabilisé $(\Omega, \AA, \PP)$ et deux variables aléatoires $X$ et $Y$ définies sur cet espace 
telles que $X(\Omega)=Y(\Omega)=\mathbb{N}$ et :
        \[ 
        \forall(i, j) \in \N^2, \quad \mathbb{P}([ X=i] \cap[ Y=j])=p_{i, j}
        \]
\end{proposition}

\preuve{Proposition admise.}

\subsubsection{Lois marginales}
\labelobj{02}


\begin{definition}[Lois marginales]
Soit $(X,Y)$ un couple de variables aléatoires discrètes à valeurs dans $F \times G$. On appelle \textbf{lois marginales} les lois de 
$X$ et $Y$. Elles sont données par les formules suivantes
\begin{align*}
\PP(X=x_i) &= \sum_{j \in J} \PP(X=x_i,Y=y_j), \quad \text{pour tout $x_i$ dans $F$,} \\
\PP(Y=y_j) &= \sum_{i \in I} \PP(X=x_i,Y=y_j), \quad \text{pour tout $y_j$ dans $G$.} \\
\end{align*}
\end{definition}

\preuve{ %[3]{
Il s'agit de la formule des probabilités totales, appliquée au système complet d'événéments associé aux variables aléatoires $Y$ dans la
première formule, et $X$ dans la deuxième.
}

\begin{exemple}
\label{exem:proba_de_minmax}
On lance deux dés équilibrés, l'un rouge et l'autre bleu, et on appelle $X$ le plus petit résultat des deux dés et $Y$ le plus grand. 
Déterminer les lois de $X$ et $Y$.
\end{exemple}

\solution[10]{
On a vu que
\[ 
\PP(X=i,Y=j) = \left\{
\begin{array}{ll}
\frac 1 {36} & \text{si $i=j$,} \\
0 & \text{si $i>j$,} \\
\frac{2}{36} & \text{si $i < j$.}
\end{array}
\right.
\]
Ainsi, on a pour tout $i$ dans $\{1,\dots,6\}$,
\[ 
\PP(X=i) = \sum_{j=1}^6 \PP(X=i,Y=j) = \frac{1+2(6-i)}{36},
\]

\begin{center}
\vspace*{-.5cm}
        \includegraphics[ width=0.7\textwidth]{minmax1.eps}
\end{center}
et pour tout $j$ dans $\{1,\dots,6\}$,
\[ 
\PP(Y=j) = \sum_{i=1}^6 \PP(X=i,Y=j) = \frac{1+2(j-1)}{36}.
\]
\begin{center}
        \vspace*{-.35cm}
        \includegraphics[ width=0.7\textwidth]{minmax2.eps}
\end{center}
}


\begin{attention}
La loi des marginales ne permet pas d'obtenir la loi du couple comme vu dans l'exemple introductif.
\end{attention}

\subsection{Lois conditionnelles}
        \labelobj{03}

On considère l'expérience suivante : on lance un dé rouge et un dé bleu et on pose $X$ le résultat du dé rouge et $Y$ la somme des deux 
dés. On a déjà vu que la valeur de $X$, c'est-à-dire le résultat du dé rouge, influence la valeur de $Y$. Il est donc naturel de 
s'intéresser à la loi de $Y$ avec l'information a priori que $X=x_i$.

\begin{definition}[Loi conditionnelle]
Soit $x_i$ dans $F$ tel que $\PP(X=x_i)>0$. on appelle \textbf{loi conditionnelle} de $Y$ sachant $X=x_i$, la probabilité sur $G$ 
définie par
\[ 
\PP_{[ X=x_i]}(Y=y_j)=\PP(Y=y_j \, | \, X=x_i) = \frac{\PP(X=x_i,Y=y_j)}{\PP(X=x_i)}, \quad \text{pour tout $y_j$ dans $G$.}
\]
\end{definition}

\begin{remarque}
Puisque $\PP(X=x_i) = \sum\limits_{j \in J} \PP(X=x_i,Y=y_j)$, la loi conditionnelle de Y sachant $X=x_i$ est complètement déterminée 
par la loi du couple $(X,Y)$ et
\[ 
\frac{\PP(X=x_i,Y=y_j)}{\sum\limits_{j \in J} \PP(X=x_i,Y=y_j)} = \frac{p^{(X,Y)}_{i,j}}{ \sum\limits_{j \in J} p^{(X,Y)}_{i,j}} .
\]
\end{remarque}

\begin{exemple}
On lance deux dés équilibrés, l'un rouge et l'autre bleu, et on appelle $X$ le plus petit résultat des deux dés et $Y$ le plus grand. 
Déterminer la loi de $Y$ sachant $X=i$, pour tout $i$ dans $\{1,\dots,6\}$.
\end{exemple}

\solution[10]{
Nous avons
\[ 
\PP(Y=j \, | \, X=i) =  \frac{\PP(X=i,Y=j)}{\PP(X=i)} = \left\{
\begin{array}{ll}
\frac{1}{1+2(6-i)} & \text{si $j=i$,}\\
0 & \text{si $j<i$,}\\
 \frac{2}{1+2(6-i)} & \text{si $j>i$.}
\end{array}
\right.
\]
\begin{center}
\includegraphics[ width=0.7\textwidth]{minmaxcond.eps}
\end{center}
}

\begin{theoreme}
Si l'on connaît la loi marginale de $X$ (respectivement $Y$) et la loi conditionnelle de $Y$ sachant $X=x_i$ pour tout $x_i$ dans $F$ 
(respectivement  la loi conditionnelle de $X$ sachant $Y=y_j$ pour tout $y_j$ dans $G)$, alors la loi de $Y$ (respectivement de $X$) est
déterminée par
\[ 
\PP(Y=y_j) = \sum_{x_i \in F} \PP(Y=y_j \, | \, X=x_i) \PP(X=x_i), \quad \text{pour tout $y_j$ dans $G$,}
\]
(respectivement)
\[ 
\PP(X=x_i) = \sum_{y_j \in G} \PP(X=x_i \, | \, Y=y_j) \PP(Y=x_j), \quad \text{pour tout $x_i$ dans $F$.}
\]
\end{theoreme}

\begin{demonstration}
Il s'agit à nouveau de la formule des probabilités totales appliquées aux systèmes complets d'événements associés aux variables 
aléatoires.
\end{demonstration}

\subsection{Indépendance}
\labelobj{04}

\begin{rappel}
Les variables aléatoires $X$ et $Y$ sont dites \textbf{indépendantes} si pour toutes parties $A \subset F$ et $B \subset G$, elles 
vérifient
\[ 
\PP(X \in A,Y\in B) = \PP(X \in A) \times \PP(Y \in B).
\]
\end{rappel}

\begin{proposition}
Les propositions suivantes sont équivalentes.
\begin{enumerate}
\item
Les variables aléatoires $X$ et $Y$ sont indépendantes de lois respectives $p^X$ et $p^Y$.
\item
On a, pour tous $(x_i,y_j)$ dans $F \times G$,
\[ 
p^{(X,Y)}_{i,j}=\PP(X=x_i,Y=y_j) = \PP(X=x_i) \PP(Y=y_j) = p_i^X p_j^Y. 
\]
\item Pour tout $y_j \in G$, et $x_i \in F$ tel que $\PP(X=x_i)>0$, on a \[\PP(Y=y_j | X=x_i) = \PP(Y=y_j).\]
\end{enumerate}
%%
\end{proposition}


\begin{exo}
Considérons les lancers successifs et indépendants d'un même dé à six faces. On note $A_n$ l'évènement  \og on obtient 6 au $n$-ième 
lancer \fg \  et  $B_n$ l'évènement \og on obtient un nombre différent de 6 au $n$-ième lancer \fg. On note $S_1=T_1$ le temps d'attente
du premier $6$, $T_2$ le temps d'attente du second $6$ et $S_2=T_2-T_1$ le temps écoulé entre le premier $6$ obtenu et le second.

Le but de cet exercice est de déterminer la loi de $T_2$, puis de montrer que $S_1$ et $S_2$ sont indépendantes et de même loi.
\begin{enumerate}
\item
On suppose dans cette question uniquement que les premiers lancers nous donnent
\[ 
(1,3,5,2,6,2,4,6,2,\dots).
\]
Donner les valeurs de $T_1$,  $T_2$ et $S_2$.
\item
Rappeler la loi de $T_1$.
\item
Que vaut l'évènement $\{T_1=n\}\cap \{T_2=m\}$ si $n \geq m$ ?
\item
Soient $n$ et $\ell$ deux entiers non nuls. Exprimer l'évènement $\{T_1=n\}\cap \{T_2=n+\ell \}$ en fonction de $(A_k)$ et $(B_k)$.
\item
En déduire la loi du couple $(T_1,T_2)$.
\item
En déduire la loi de $T_2$ puis la loi de $(S_1,S_2)$.
\item
En déduire la loi de $S_2$ puis que $S_1$ et $S_2$ sont indépendantes.
\end{enumerate}
%%
\end{exo}

\solution[30]{
\begin{enumerate}
\item
Rapidement, $T_1=5$, $T_2=8$ et $S_2=3$.
\item
La variable aléatoire $T_1$ suit une loi géométrique de paramètre $p=\frac 16$ et nous avons
\[ 
\PP(T_1=n) = p(1-p)^{n-1}, \quad \text{pour tout entier $n$ non nul.}
\]
\item
On a $\{T_1=n\}\cap \{T_2=m\}=\vide$ si $n\geq m$, car $T_2 > T_1$.
\item
On a
\[ 
\{T_1=1\}\cap \{T_2=2\} = A_1 \cap A_2,
\]
\[ 
\{T_1=1\} \cap \{T_2=1+\ell\} = A_1 \cap \left( \bigcap_{k=2}^\ell B_k \right) \cap A_{1+\ell}, \quad \text{pour tout $\ell \geq 2$,}
\]
\[ 
\{T_1=n\} \cap \{T_2=n+1\} = \left( \bigcap_{k=1}^{n-1} B_k \right) \cap A_n \cap A_{n+1}, \quad \text{pour tout $n\geq 2$,}
\]
et enfin
\[ 
\{T_1=n\} \cap \{T_2=n+\ell\} =  \left( \bigcap_{k=1}^{n-1} B_k \right) \cap A_n \cap \left( \bigcap_{k=n+1}^{n+\ell-1} B_k \right) \cap
A_{n+\ell}, \quad \text{pour tous $n,\ell \geq 2$.}
\]
\item
D'après les questions précédentes et en utilisant l'indépendance des lancers, il vient
\[ 
\PP(T_1=n,T_2=m) = \left\{
\begin{array}{ll}
0 & \text{si $n\geq m$,} \\
p^2 (1-p)^{m-2} &  \text{si $n<m$.}
\end{array}
\right.
\]
\item
On a pour tout entier $m\geq 2$
\[ 
\PP(T_2=m) = \sum_{n=1}^{+\infty} \PP(T_1=n,T_2=m) = \sum_{n=1}^{m-1} p^2 (1-p)^{m-2} = p^2 (m-1) p^{m-2}.
\]
Or, $\{S_1=n\}\cap \{S_2=m\} = \{T_1=n\} \cap \{T_2=n+m\}$, ce qui entraîne
\[ 
\PP(S_1=n,S_2=m) = \PP(T_1=n,T_2=n+m) = p^2 (1-p)^{n+m-2} = p(1-p)^{n-1} \times p (1-p)^{m-1}.
\]
\item
On a
\[ 
\PP(S_2=m) = \sum_{n=1}^{+\infty} \PP(S_1=n,S_2=m) =p(1-p)^{m-1} \sum_{n=1}^{+\infty} p(1-p)^{n-1} = p(1-)^{m-1}.
\]
Ainsi $S_2$ suit une loi géométrique de paramètre $p$ (comme $S_1$) et on a bien
\[ 
\PP(S_1=n,S_2=m) = \PP(S_1=n) \PP(S_2=m).
\]
\end{enumerate}
}

\subsection{Fonction d'un couple de variable aléatoires discrètes.}

Nous avons vu précédemment que le temps d'attente du second succès dans une répétition d'expériences de Bernoulli indépendantes est la 
somme de deux lois géométriques de même paramètre, et indépendantes.

Dans beaucoup de problèmes de modélisation aléatoire, on est confronté à la gestion de plusieurs variables aléatoires, et notamment à 
des fonctions d'entre elles (minimum, maximum, somme, produit, etc.). Le saut conceptuel a déjà lieu avec deux variables aléatoires et 
c'est ce sur quoi nous nous concentrons dans cette partie.

Voici deux résultats préliminaires.

\begin{theoreme}[Formule de transfert]
\labelobj{05}
Soient $X$ et $Y$ deux variables aléatoires discrètes à valeurs dans $F$ et $G$ et  soit $\phi$ une application de $F \times G$ dans 
$\R$. Alors $\phi(X,Y)$ est une variable aléatoire discrète.

De plus, si $\phi(X,Y)$ admet une espérance, alors
\begin{equation} \label{equa:proba_transfert}
\esperance[ \phi(X,Y)] = \sum_{(i,j) \in I \times J} \phi(x_i,y_j) \PP(X=x_i,Y=y_j).
\end{equation}
\end{theoreme}

\begin{proposition}
Si $X$ et $Y$ sont deux variables aléatoires discrètes alors $X+Y$, $X \times Y$, $\max(X,Y)$ et $\min(X,Y)$ sont des variables 
aléatoires discrètes.
\end{proposition}

\begin{remarque}
Une propriété importante et utilse pour étudier la loi du $\max$ ou du $\min$ de deux variables aléatoires :
\[  [ \max(X,Y)\leq n] = (X\leq n)\cap(Y\leq n) \qeq [ \min(X,Y) \geq n]=(X\geq n)\cap (Y\geq n). \]
Ainsi, si on connait la loi de $(X,Y)$, on peut en déduire $\PP(\max(X,Y)\leq n)$ ou $\PP(\min(X,Y)\geq n)$ et donc la loi de 
$\min(X,Y)$ ou $\max(X,Y)$, en utilisant, par exemple,
\[  \PP(M=n) = \PP(M\leq n)-\PP(M\leq n-1).\]
\end{remarque}

\paragraph{Somme de deux variables aléatoires discrètes.}

Commençons par un exemple pour appréhender la notion de somme de variables aléatoires.

\begin{exemple}
On lance simultanément deux dés (un  rouge et un  bleu) et on pose $X$ comme le résultat du dé rouge et $Y$ le résultat du dé bleu. On 
définit $Z=X+Y$ comme la somme des deux dés.
\begin{enumerate}
\item
Déterminer les ensembles $F$ et $G$ des valeurs prises par $X$ et $Y$ respectivement.
\item
Déterminer l'ensemble $H$ des valeurs prises par $Z$.
\item
Déterminer l'ensemble $H_k=\{(i,j) \in F \times G \, : \, i+j=k\}$, pour tout $k$ dans $H$.
\item
Calculer $\PP(Z=k)$, pour tout $k$ dans $H$.
\end{enumerate}
%%
\end{exemple}

\solution[20]{
\begin{enumerate}
\item $F$ et $G$ valent tous les deux $\interent{1 6}$ puisque $X$ et $Y$ sont les résultats d'un dé à $6$ faces.
\item Par somme, $Z(\Omega)=\interent{2 12}$.
\item Il faut déterminer $H_k$ pour $k\in \interent{2 12}$. On constate que :
        \[  H_2=\{(1,1)\},\quad H_3=\{ (1,2), (2,1)\},\quad H_4=\{ (1,3), (2,2), (3,1)\}, \quad H_5=\{(1,4),(2,3), (3,2), (4,1)\} \]
        et on continue ainsi. Trouvons une méthode plus simple.
        
 $(i, j) \in H_k$ si et seulement si 
 \begin{itemize}
 	\item $i\in \interent{1 6}$,
 	\item $j\in \interent{1 6}$,
 	\item et $i+j=k$, soit $j=k-1$
 \end{itemize}
Ainsi, tout élément de $H_k$ s'écrit $(i, k-i)$ , avec 
\[ 1\leq i\leq 6 \qeq 1\leq k-i \leq 6 \]
soit 
\[ 1\leq i\leq 6 \qeq k-6\leq i\leq k-1\]
ou encore 
\[ \max(1,k-6) \leq i \leq \min(6, k-1).\]
Ainsi
\[ H_k = \left \{ (i, k-i),\quad \max(1,k-6) \leq i \leq \min(6, k-1).\right\}\]
\item Déterminons le cardinal de $H_k$. Distinguons deux cas :
\begin{itemize}
	\item Si $1\leq k\leq 6$, $\max(1,k-6)=1$ et $\min(6, k-1)=k-1$. Ainsi
	\[ \Card(H_k) = (k-1) - 1 + 1 = k-1.\]
	\item Si $7\leq k\leq 12$, $\max(1,k-6)=k-6$ et $\min(6,k-1)=6$. Ainsi
	\[ \Card(H_k) = 6 - (k-6) + 1 = 13-k.\]	
\end{itemize}
Utilisons la formule de convolution discret
        \begin{align*}
                \forall k\in \interent{2 12},\, \PP(Z=k) &= \sum_{(i,j)\in H_k} \PP(X=i, Y=j) \\
                &= \sum_{(i,j)\in H_k} \PP(X=i)\PP(Y=j)\text{ par indépendance}\\
                &=\sum_{(i,j)\in H_k} \frac{1}{6}\frac{1}{6} = \frac{\Card(H_k)}{36}.
        \end{align*}
        On en déduit le résultat déjà vu dans un chapitre précédent :
\[      \begin{array}{|c|ccccccccccc|}\hline
x_i & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 &  12\\\hline
\PP(Z=x_i) &= \frac{1}{36} & \frac{2}{36} & \frac{3}{36} & \frac{4}{36} & \frac{5}{36} & \frac{6}{36} & \frac{5}{36} & \frac{4}{36} & 
\frac{3}{36} & \frac{2}{36} & \frac{1}{36} \\\hline\end{array}\]
\end{enumerate}
%TODO : corrigé
}

\begin{theoreme}[Produit de convolution discret]
\labelobj{06}
Soient $X$ et $Y$ deux variables aléatoires discrètes à valeurs dans $F$ et $G$ respectivement.
La loi de $Z=X+Y$ est donnée par
\[ 
\PP(Z=z_k) = \sum_{(i,j) \in H_k} \PP(X=x_i,Y=y_j), \quad \text{avec $H_k = \{ (i,j) \in I \times J \, : \, x_i+y_j=z_k\}$.}
\]
Dans le cas où $X$ et $Y$ sont à valeurs \textbf{entières}, on a
\[ 
\PP(Z=k) = \sum_{i \in \N} \PP(X=i,Y=k-i)=\sum_{j \in \N} \PP(X=k-j,Y=j),
\]
où seul un nombre fini de termes sont non nuls. En particulier, si $X$ et $Y$ sont  \textbf{à valeurs entières et  indépendantes}
\[ 
\PP(Z=k) = \sum_{i \in \N} \PP(X=i)\PP(Y=k-i) = \sum_{j \in \N} \PP(X=k-j) \PP(Y=j).
\]
\end{theoreme}

On va utiliser ce résultat pour démontrer deux résultats classiques :

\begin{proposition}[Somme de deux lois binomiales]
\labelobj{07}
Soit $X \suit \BB(n,p)$ et $Y \suit \BB(m,p)$ indépendantes. Alors $X+Y$ suit une loi $\BB(n+m,p)$.
\end{proposition}

\preuve[15]{
Comme $X$ et $Y$ sont respectivement comprises entre $0$ et $n$, et $0$ et $m$, on en déduit que $Z$ est compris entre $0$ et $n+m$. 
Soit $k$ dans $\interent{0  n+m}$. On a
\[ 
\PP(Z=k) = \sum_{i=0}^k \PP(X=i,Y=k-i) =  \sum_{i=0}^k \PP(X=i)\PP(Y=k-i).
\]
Or on a
\[ 
 \PP(X=i)\PP(Y=k-i) = \binom{n}{i} p^i (1-p)^{n-i} \binom{m}{k-i} p^{k-i} (1-p)^{m-(k-i)} = \binom{n}{i} \binom{m}{k-i} p^k 
(1-p)^{n+m-k},
\]
ce qui entraîne
\[ 
\PP(Z=k) = p^k (1-p)^{n+m-k} \sum_{i=0}^k \binom{n}{i} \binom{m}{k-i}.
\]
Or, d'après la formule de Vandermonde,
\[ 
\sum_{i=0}^k \binom{n}{i} \binom{m}{k-i} = \binom{n+m}{k},
\]
et on obtient finalement
\[ 
\PP(Z=k) =  \binom{n+m}{k} p^k (1-p)^{n+m-k}.
\]
Ainsi, $Z \suit \BB(n+m,p)$.
}

\begin{proposition}[Somme de deux lois de Poisson]
Soit $X$ et $Y$ deux variables aléatoires indépendantes suivant respectivement une loi de Poisson de paramètre $\alpha>0$ et une loi de 
Poisson de paramètre $\beta >0$. Démontrer que $X+Y\suit \mathcal{P}(\alpha+\beta)$.
\end{proposition}

\preuve[15]{
Nous avons, pour $i\in \N$ et $j\in \N$ :
\[ 
\PP(X=i) = \eu{-\alpha} \frac{\alpha^i}{i!} \qeq \PP(Y=j)=\eu{-\beta} \frac{\beta^j}{j!}.
\]
Il vient
{\allowdisplaybreaks
\begin{align*}
\PP(Z=k) &= \sum_{i=0}^k \PP(X=i)\PP(Y=k-i) \\
&= \eu{-(\alpha+\beta)} \sum_{i=0}^k \frac{\alpha^i}{i!} \frac{\beta^{k-i}}{(k-i)!} \\
&= \eu{-(\alpha+\beta)} \frac{1}{k!} \sum_{i=0}^k \binom{k}{i} \alpha^i  \beta^{k-i} \\
&= \eu{-(\alpha+\beta)} \frac{(\alpha+\beta)^k}{k!}.
\end{align*}
}
Ainsi $Z$ suit une loi de Poisson de paramètre $\alpha+\beta$.
}

Enfin, voici un résultat que nous avons déjà vu mais que l'on peut démontrer désormais :

\begin{proposition}
Soient $X$ et $Y$ deux variables aléatoires discrètes admettant une espérance. Alors $X+Y$ admet une espérance et
\[ 
\esperance(X+Y) = \esperance(X) + \esperance(Y).
\]
\end{proposition}

\preuve{ %[15]{
\allowdisplaybreaks
Nous allons utiliser le théorème de transfert pour le démontrer. On a, d'après l'équation~\eqref{equa:proba_transfert},
\begin{align*}
\esperance(X+Y) &= \sum_{(i,j) \in I \times J} (x_i+y_j) \PP(X=x_i,Y=y_j) \\&= \sum_{(i,j) \in I \times J} x_i\PP(X=x_i,Y=y_j) +   
\sum_{(i,j) \in I \times J} y_j \PP(X=x_i,Y=y_j).
\end{align*}
Or, on a
\begin{align*}
\sum_{(i,j) \in I \times J} x_i\PP(X=x_i,Y=y_j) &= \sum_{i \in I} \sum_{j \in J} x_i\PP(X=x_i,Y=y_j)\\ &= \sum_{i \in I} x_i \sum_{j \in
J} \PP(X=x_i,Y=y_j)\\ &= \sum_{i \in I} x_i \PP(X=x_i) =\esperance(X),
\end{align*}
et de même
\[ 
\sum_{(i,j) \in I \times J} y_j \PP(X=x_i,Y=y_j) = \sum_{j \in J} y_j \PP(Y=y_j)=\esperance(Y).
\]
Ceci achève la preuve.
}

Le cas du maximum et du minimum  de deux variables aléatoires a été effleuré dans l'exemple \thechapter.\ref{exem:proba_de_minmax} et 
sera approfondi en exercice. Le cas du produit de deux variables aléatoires est évoqué dans la sous-partie suivante.

\subsection{Covariance et coefficient de corrélation linéaire}

On ne donnera pas la loi générale d'un produit de variables aléatoires.

\begin{proposition}
Soient $X$ et $Y$ deux variables aléatoires discrètes admettant un moment d'ordre $2$. Alors $XY$ admet une espérance. Si de plus $X$ et
$Y$ sont \textbf{indépendantes} alors
\[ 
\esperance(XY)=\esperance(X)\esperance(Y).
\]
\end{proposition}

\preuve{ %[15]{
Remarquons que
\[ 
(X+Y)^2 = X^2 + Y^2 +2XY \geq 0 \qeq (X-Y)^2 = X^2 + Y^2 -2 XY \geq 0.
\]
On a donc $|XY| \leq \frac{X^2+Y^2}{2}$, ce qui montre que si $X$ et $Y$ admettent un moment d'ordre $2$, alors $XY$ admet une 
espérance. Par ailleurs, d'après l'équation~\eqref{equa:proba_transfert}, nous avons
\[ 
\esperance(XY) =  \sum_{(i,j) \in I \times J} x_i y_j  \PP(X=x_i,Y=y_j)
\]
Si $X$ et $Y$ sont indépendantes, alors $  \PP(X=x_i,Y=y_j)= \PP(X=x_i) \PP(Y=y_j) $ et
\[ 
\esperance(XY) =  \sum_{(i,j) \in I \times J} x_i y_j  \PP(X=x_i) \PP(Y=y_j)= \sum_{i \in I} x_i \PP(X=x_i) \times \sum_{j \in J} y_j 
\PP(Y=y_j) = \esperance(X) \esperance(Y).
\]
}


\begin{definition}
Soient $X$ et $Y$ deux variables aléatoires discrètes admettant un moment d'ordre $2$. On définit la \textbf{covariance} de $X$ et $Y$ 
comme la quantité
\[ 
\Cov(X,Y) = \esperance\big[  \big(X - \esperance(X) \big) \big(Y - \esperance(Y) \big) \big] = \esperance(XY) - 
\esperance(X)\esperance(Y).
\]
\end{definition}

\begin{remarque}
On a $\Var(X) = \Cov(X,X)$.
\end{remarque}

\begin{proposition}
Soient $X$ et $Y$ deux variables aléatoires discrètes admettant un moment d'ordre $2$. Alors $X+Y$ admet un moment d'ordre $2$ et
\[ 
\Var(X+Y) = \Var(X) + \Var(Y) + 2\Cov(X,Y).
\]
En particulier, si $X$ et $Y$ sont indépendantes, on a
\[ 
\Cov(X,Y) =0 \qeq \Var(X+Y)=\Var(X) + \Var(Y).
\]
\end{proposition}

\preuve{ %[7]{
D'après la formule de Koenig-Huygens, on a
\[ 
\Var(X+Y) = \esperance[ (X+Y)^2] - \esperance(X+Y)^2.
\]
En développant, il vient
\[ 
\Var(X+Y) = \esperance(X^2 + Y^2 + 2 XY) - \esperance(X)^2 - \esperance(Y)^2 - 2\esperance(X)\esperance(Y) = \Var(X) + \Var(Y) + 
2\Cov(X,Y).
\]
Si $X$ et $Y$ sont indépendantes alors $\esperance(XY) = \esperance(X)\esperance(Y)$, et donc $\Cov(X,Y)=0$,  et finalement $\Var(X+Y) =
\Var(X) + \Var(Y)$.
}

\begin{attention}
On peut avoir $\Cov(X,Y)=0$ sans que $X$ et $Y$ ne soient indépendantes.
\end{attention}

\begin{exo}
Soient $X$ une variable aléatoire telle que
\[ 
\PP(X=1)=\PP(X=-1)=\PP(X=0)=\frac 13.
\]
Soit $Y$ la variable aléatoire définie par $\mathbb{1}_{\{0\}}(X)$, c'est-à-dire :
\[  \forall \omega \in \Omega,\quad Y(\omega)=\left \{ \begin{array}{lll}1 & \text{si} & 
X(\omega)=0\\0&\text{sinon}&\end{array}\right..\]
\begin{enumerate}
\item
Donner la loi de $Y$.
\item
Calculer $\Cov(X,Y)$.
\item
Montrer que $X$ et $Y$ ne sont pas indépendantes.
\end{enumerate}
\end{exo}

\solution[20]{
\begin{enumerate}
\item
La variable aléatoire $Y$ prend les valeurs $0$ ou $1$, c'est donc une variable aléatoire de Bernoulli, et il nous reste à déterminer 
son paramètre. Nous avons $\{Y=1\}=\{X=0\}$, d'où
\[ 
\PP(Y=1)=\PP(X=0)=\frac 13.
\]
\item
Remarquons que $XY=0$. En effet, si $X\neq 0$ alors $Y=0$ et $XY=0$, et si $X=0$ alors $XY=0$. On a donc 
$\esperance(XY)=\esperance(0)=0$. Par ailleurs, on a $\esperance(X)=0$. On a donc $\Cov(X,Y)=0$.
\item
Nous avons
\[ 
\PP(X=1,Y=1) = \PP(X=1,X=0) = 0 \qeq \PP(X=1)\PP(Y=1) = \frac 13 \frac 13 >0.
\]
Ainsi, $X$ et $Y$ ne sont pas indépendantes.
\end{enumerate}
}

\begin{definition}
Soient $X$ et $Y$ deux variables aléatoires discrètes admettant un moment d'ordre $2$ et non constantes. On définit le 
\textbf{coefficient de corrélation linéaire} de $X$ et $Y$ par
\[ 
\rho(X,Y) = \frac{\Cov(X,Y)}{\sigma_X \sigma_Y}.
\]
\end{definition}

\begin{theoreme}
Soient $X$ et $Y$ deux variables aléatoires discrètes admettant un moment d'ordre $2$ et non constantes.
\begin{itemize}
\item
$-1 \leq \rho(X,Y) \leq 1$;
\item
$|\rho(X,Y)|=1$ si, et seulement si, il existe deux réels $a\neq 0$ et $b$ tels que $Y=aX+b$.
\end{itemize}
\end{theoreme}

\preuve{ %[20]{
Soit $\phi$ l'application de $\R$ dans $\R$ définie par
\[ 
\phi(t) = \Var(tX+Y), \quad \text{pour tout réel $t$.}
\]
La fonction $\phi$ est alors à valeurs positives. Montrons qu'elle s'écrit sous la forme d'un trinôme du second degré. Nous avons
\[ 
\phi(t) = \Var(tX+Y) = \Var(tX) + \Var(Y) + 2\Cov(tX,Y) = \Var(X) t^2 + 2\Cov(X,Y) t + \Var(Y).
\]
Le discriminant de $\phi$ est donné par
\[ 
\Delta = 4 \Cov(X,Y)^2 - 4 \Var(X) \Var(Y) = 4 [  \Cov(X,Y)^2 - \Var(X) \Var(Y)].
\]
Or $\phi$ est à valeurs positives, on en déduit donc que $\phi$ ne possède pas de racine ou bien possède une racine double. On a donc 
$\Delta \leq 0$, ce qui entraîne
\[ 
 \Cov(X,Y)^2 \leq \Var(X) \Var(Y),
\]
et donc
\[ 
|\Cov(X,Y) \leq \sigma_X \sigma_Y,
\]
et finalement
\[ 
|\rho(X,Y)| \leq 1.
\]
On remarque que le cas d'égalité a lieu quand $\Delta=0$. Dans ce cas, la fonction $\phi$ s'annule en $t_0= - 
\frac{2\Cov(X,Y)}{2\Var(X)} = - \frac{\Cov(X,Y)}{\Var(X)}$ et on a alors
\[ 
\phi(t_0)=0=\Var(t_0 X +Y).
\]
Ceci entraîne qu'il existe une constante $b$ telle que $t_0X + Y=b$ et on a bien $Y=aX+b$ en prenant $a=-t_0$.
}

\begin{exo}
Les enseignants d'E1B, au nombre de dix, organisent une petite sauterie pour les fêtes et à cette occasion apporte chacun un cadeau. 
L'enseignant $1$ apporte le cadeau $1$, l'enseignant $2$ apporte le cadeau $2$, etc. Lors de la soirée, on procède à un tirage au sort 
pour répartir les cadeaux de la manière suivante:  on place 10 jetons identiques numérotés de 1 à 10 dans une urne et chaque enseignant 
pioche à tour de rôle un jeton sans remise.
\begin{enumerate}
\item
Quelle est la probabilité que l'enseignant $1$ reparte avec le \textit{Discours de la méthode} dont il comptait se débarrasser ?
\item
Quelle est la probabilité que l'enseignant $1$ reparte avec le \textit{Discours de la méthode} dont il comptait se débarrasser et que 
l'enseignant $2$ reparte avec un porte clé à l'effigie d'une marque de bière qu'il avait en double et dont il comptait également se 
débarrasser ?
\item
Pour tout $i$ dans $\{1,\dots,10\}$, on note $X_i$ la variable aléatoire qui vaut $1$ si l'enseignant $i$ repart avec le cadeau $i$ (son
propre cadeau), et $0$ sinon.
\begin{enumerate}
\item
Déterminer la loi de la variable aléatoire $X_i$, pour tout $i$ dans $\{1,\dots,10\}$, puis calculer $\esperance(X_i)$ et $\Var(X_i)$.
\item
Déterminer la loi de la variable aléatoire $X_i X_j$, pour tous $i$ et $j$ dans $\{1,\dots,10\}$ avec $i \neq j$.
\item
Calculer $\esperance(X_i X_j)$, pour tout $i$ et $j$ dans $\{1,\dots,10\}$ avec $i \neq j$.
\item
En déduire la covariance de $X_i$ et $X_j$, pour tout $i$ et $j$ dans $\{1,\dots,10\}$ avec $i \neq j$, puis leur coefficient de 
corrélation linéaire.
\end{enumerate}
%%
\item
On pose $X=X_1 + \cdots + X_{10}$.
\begin{enumerate}
\item
Que représente $X$ ?
\item
Calculer $\esperance(X)$.
\item
Exprimer $\Var(X)$ en fonction de $\Var(X_i)$ et $\Cov(X_i,X_j)$.
\item
En déduire $\Var(X)$.
\end{enumerate}
%%
\end{enumerate}
%%
\end{exo}

\solution[40]{
\begin{enumerate}
\item
Nous sommes dans le cas d'un tirage sans remise de $10$ jetons parmi $10$. L'espace de probabilité sous-jacent est l'ensemble des suites
à $10$ éléments distincts parmi $10$ et le nombre de tirages possibles est $10!$ (10 choix pour le premier tirage, 9 choix pour le 
second, etc.).

Le nombre de cas favorables à l'évènement \og L'enseignant $1$ repart avec son propre cadeau \fg \ est $9!$. En effet, un des éléments 
de la suite à $10$ éléments distincts est imposé, et il reste donc $9!$ possibilités de répartir les $9$ autres éléments.  La 
probabilité recherchée est donc $\frac{9!}{10!}=\frac{1}{10}$
\item
L'espace de probabilité sous-jacent n'a pas changé. Le nombre de cas favorables à la réalisation simultané de ces deux évènements est 
$8!$. En effet, deux des éléments de la suite à $10$ éléments distincts sont imposés, et il reste donc $8!$ possibilités de répartir les
$8$ autres éléments.  La probabilité recherchée est donc $\frac{8!}{10!}=\frac{1}{90}$.
\item
\begin{enumerate}
\item
Pour tout $i$ dans $\{1,\dots,10\}$, la variable aléatoire $X_i$ suit une loi de Bernoulli de paramètre $\frac{1}{10}$. On a alors
\[ 
\esperance(X_i) = \frac{1}{10} \qeq \Var(X_i) = \frac{9}{100}, \quad \text{pour tout $i$ dans $\{1,\dots,10\}$.}
\]
\item
Soient $i \neq j$. La variable aléatoire $X_i X_j$ vaut $1$ si $X_i=X_j=1$, et $0$ sinon. C'est donc une loi de Bernoulli, et il nous 
reste à déterminer son paramètre. Nous avons vu que la probabilité que deux enseignants repartent avec leur propre cadeau est donné par 
$\frac{1}{90}$, et on en déduit que $X_i X_j$ suit une loi de Bernoulli de paramètre $\frac{1}{90}$.
\item
On a
\[ 
\esperance(X_i X_j) = \frac{1}{90}, \quad \text{pour tout $i$ et $j$ dans $\{1,\dots,10\}$ avec $i \neq j$.}
\]
\item
On a  donc, pour tout $i$ et $j$ dans $\{1,\dots,10\}$ avec $i \neq j$,
\[ 
\Cov(X_i,X_j) = \esperance(X_i X_j) - \esperance(X_i) \esperance(X_j) = \frac{1}{90} - \frac{1}{100} = \frac{1}{900},
\]
puis
\[ 
\rho(X_i,X_j) = \frac{\Cov(X_i,X_j)}{\sqrt{\Var(X_i)} \sqrt{\Var(X_j)}} = \frac{1/900}{9/100} = \frac{1}{81}.
\]
\end{enumerate}
%%
\item
\begin{enumerate}
\item
La variable aléatoire $X$ représente le nombre d'enseignants qui repartent avec leur propre cadeau.
\item
On a
\[ 
\esperance(X_1 + \cdots + X_{10}) = \esperance(X_1) + \cdots + \esperance(X_{10}) = 1.
\]
\item
On a
\begin{align*}
\Var(X_1 + \cdots + X_{10})
&= \Var\big[ (X_1+\cdots + X_9) + X_{10}\big] \\
&= \Var(X_1 + \cdots + X_{9}) + \Var(X_{10}) + 2\Cov(X_1 + \cdots + X_9,X_{10}) \\
&= \Var(X_1 + \cdots + X_{9}) + \Var(X_{10}) + 2 \sum_{i=1}^9 \Cov(X_i,X_{10}) \\
&= \Var(X_1 + \cdots + X_{8}) + \Var(X_9)\\&\quad +  2 \sum_{i=1}^8 \Cov(X_i,X_9) + \Var(X_{10}) + 2 \sum_{i=1}^9 \Cov(X_i,X_{10})\\
&= \cdots \\
&= \Var(X_1) + \dots + \Var(X_{10}) + 2\sum_{j=2}^{10} \sum_{i=1}^j \Cov(X_i,X_j).
\end{align*}
Ainsi,
\[ 
\Var \left( \sum_{i=1}^{10} X_i \right) = \sum_{i=1}^{10} \Var(X_i) + 2 \sum_{i<j} \Cov(X_i,X_j).
\]
\item
Puisque $\Var(X_i)= \frac{9}{100}$, pour tout $i$ dans $\interent{1 10}$, on a
\[ 
\sum_{i=1}^{10} \Var(X_i) = 10 \cdot \frac{9}{100} = \frac{9}{10}.
\]
Par ailleurs, on a $\Cov(X_i,X_j) =  \frac{1}{900}$, pour tous $i$ et $j$ dans $\{1,\dots,10\}$ avec $i < j$. On a donc
\[ 
2 \sum_{(i,j) \, : \, i<j} \Cov(X_i,X_j) = 2\card\big( \{ (i,j) \in \interent{1 10}^2 ,\, \, i < j \} \big) \cdot \frac{1}{900} =2 \cdot
\frac{90}{2} \cdot \frac{1}{900} = \frac{1}{10}
\]
Finalement, on a
\[ 
\Var(X) = 1.
\]
\end{enumerate}
%%
\end{enumerate}
}

\begin{definition}
Deux variables aléatoires sont dites \textbf{non corrélées} si leur coefficient de corrélation linéaire est nul.
\end{definition}

\begin{proposition}
Deux variables aléatoires indépendantes sont non corrélées. La réciproque est fausse.
\end{proposition}

\begin{exemple}
Soient $X$ et $Y$ deux variables aléatoires de Bernoulli.
\begin{enumerate}
\item
Montrer que $XY$ suit une loi de Bernoulli.
\item
Montrer que $X$ et $Y$ sont indépendantes si, et seulement si, elles sont non corrélées.
\end{enumerate}
%%
\end{exemple}

\solution[20]{
\begin{enumerate}
\item
Les variables aléatoires $X$ et $Y$ étant à valeurs dans $\{0,1\}$, le produit $XY$ est aussi à valeurs dans $\{0,1\}$, c'est donc bien 
une loi de Bernoulli et son paramètre est donné par $\PP(X=1,Y=1)$.
\item
On sait déjà que si $X$ et $Y$ sont indépendantes alors $X$ et $Y$ sont non corrélées. Supposons à présent qu'elles sont non corrélées. 
On a alors
\[ 
0 = \Cov(X,Y) = \esperance(XY) - \esperance(X)\esperance(Y).
\]
Si $p$ est le paramètre de $X$ et $q$ celui de $Y$, on  a alors $\esperance(X)=p$ et $\esperance(Y)=q$. On en déduit donc que
\[ 
\esperance(XY)=pq.
\]
En particulier, le paramètre de $XY$ est $pq$, ce qui prouve que
\[ 
\PP(X=1,Y=1) = \PP(X=1) \PP(Y=1).
\]
Ceci prouve que $X$ et $Y$ sont indépendantes puisqu'elles ne prennent que deux valeurs chacune.
\end{enumerate}
}

\section{Suite de variables aléatoires discrètes}

\subsection{Généralités}

\begin{definition}
Soit $n$ un entier non nul et soient $X_1$, \dots, $X_n$ des variables aléatoires réelles et discrètes définies sur le même espace de 
probabilité $(\Omega,\AA,\PP)$. Le $n$-uplet $X=(X_1,\dots,X_n)$ est appelé \textbf{vecteur aléatoire} sur $(\Omega,\AA,\PP)$ et prend 
ses valeurs dans $\R^n$.
\end{definition}

\begin{theoreme}
Soit $X=(X_1,\dots,X_n)$ un vecteur aléatoire discret et $f$ une application de $X_1(\Omega) \times \dots \times X_n(\Omega)$ dans $\R$.
Alors $f(X)$ est une variable aléatoire discrète.
\end{theoreme}

\begin{corollaire}\vspace*{.1cm}
Si $X=(X_1,\dots,X_n)$ est un vecteur aléatoire discret, alors $\sum\limits_{i=1}X_i$, $\prod\limits_{i=1}^n X_i$, $\max(X)$ et 
$\min(X)$ sont des variables aléatoires discrètes.
\end{corollaire}

\begin{rappel}
Soit $n$ un entier non nul. Les variables aléatoires $X_1$, \dots, $X_n$  sont dites \textbf{mutuellement indépendantes} si pour tout 
$n$-uplet $(x_1,\dots,x_n)$ dans $X_{1}(\Omega) \times \dots \times X_{n}(\Omega)$ on a
\[ 
\PP(X_{1}=x_1,\dots,X_{n}=x_n) = \prod_{i=1}^n \PP(X_{i}=x_i).
\]
\end{rappel}

\begin{remarque}
Le terme \og mutuellement \fg \ est bien souvent omis sauf quand le contexte l'impose.
\end{remarque}

\begin{rappel}
On dit qu'une famille de variables aléatoires $(X_n)_{n\in \N^*}$ est une famille de variables aléatoires indépendante si, pour toute 
partie finie $I$ de $\N^*$, les variables aléatoires $(X_i)_{i \in I}$ sont mutuellement indépendantes.
\end{rappel}

\begin{theoreme}[Lemme des coalitions]
Soient $X_1$, \dots, $X_n$ des variables aléatoires indépendantes. Soient $I$ et $J$ deux parties de $\{1,\dots,n\}$ telles que $I\cap 
J=\vide$. Alors toute variable aléatoire fonction de $(X_i)_{i\in I}$ est indépendante de toute variable aléatoire fonction de $(X_j)_{j
\in J}$.
\end{theoreme}

\subsection{Stabilité des lois binomiales et de Poisson par l'addition}

\begin{theoreme}
Soient $m$ un entier non nul, $p$ un élément de $]0,1[ $, $(n_1, \dots,n_m)$ un $m$-uplet d'entiers non nuls et $X_1$,\dots,$X_m$ des 
variables aléatoires \textbf{indépendantes} telles que $X_i \suit \BB(n_i,p)$.

Alors $X=X_1 + \dots + X_m \suit \BB(n_1+\dots n_m,p)$.
\end{theoreme}

\begin{proposition}
Soient $X_1$, \dots, $X_n$ des variables aléatoires indépendantes et équidistribuées suivant la loi de Bernoulli de paramètre $p$ dans 
$]0,1[ $. Alors $X=X_1+\dots +X_n$ suit la loi binomiale $\BB(n,p)$ de paramètres $n$ et $p$.
\end{proposition}

\begin{attention}
Ceci est faux si la probabilité de succès $p$ n'est pas commune à toutes les variables aléatoires.
\end{attention}

\begin{theoreme}
Soient $n$ un entier non nul, $(\theta_1, \dots,\theta_n)$ un $n$-uplet de réels strictement positifs et $X_1$, \dots, $X_n$ des 
variables aléatoires indépendantes telles que $X_i$ suive la loi de Poisson de paramètre $\theta_i$. Alors $X=X_1+\dots +X_n$ suit la 
loi de Poisson de paramètre $\theta=\theta_1 + \dots + \theta_n$.
\end{theoreme}

\begin{demonstration}
Ces résultats se démontrent par récurrence à partir du résultat valable pour la somme de deux variables aléatoires.
\end{demonstration}

Enfin, voici un résultat fort utile dans les calculs de variance.
\begin{theoreme}
Soient $X_1$, \dots, $X_n$ des variables aléatoires \textbf{indépendantes}. Alors
\[ 
\Var(X_1 + \dots + X_n ) = \Var(X_1) + \dots + \Var(X_n).
\]
\end{theoreme}

\begin{remarque}
On retrouve ainsi sans effort la variance d'une loi binomiale $\BB(n,p)$. Soit $X=X_1 + \dots + X_n$ avec $X_1$,\dots, $X_n$ 
indépendantes et équidistribuées suivant une loi de Bernoulli de paramètre $p$. Alors $X \suit \BB(n,p)$. On a alors
\[ 
\Var(X)= \Var(X_1 + \dots + X_n ) = \Var(X_1) + \dots + \Var(X_n) = n p(1-p).
\]
\end{remarque}
%%% Fin du cours %%%
